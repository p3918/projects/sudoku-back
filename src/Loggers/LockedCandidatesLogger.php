<?php

namespace App\Loggers;

use App\Model\Block;
use App\Model\Cell;
use App\Model\CellCollection;
use App\Serialization\Formatter;

class LockedCandidatesLogger extends LoggerContract
{
    protected int    $level         = self::LEVEL_ADVANCED;
    protected string $phase         = 'Candidats verrouillés';
    protected string $canonicalName = 'locked_candidates';

    public function logPossibleRows(Block $block, int $rowId, int $value): void
    {
        $forbidden = new CellCollection();
        foreach ($block as $cell) {
            if ($cell->row->id === $rowId) {
                $cell->setEffect(Cell::EFFECT_ELEM_4);
                $cell->setAnnotation('X');
                $forbidden->append($cell);
            }
        }
        $this->proof[$block->id] = 'Dans le bloc ' . $block . ', la valeur ' . Formatter::v($value) . ' ne se trouve pas en ' . $forbidden;
        ksort($this->proof);
    }

    public function logPossibleColumns(Block $block, int $columnId, int $value): void
    {
        $forbidden = new CellCollection();
        foreach ($block as $cell) {
            if ($cell->column->id === $columnId) {
                $cell->setEffect(Cell::EFFECT_ELEM_4);
                $cell->setAnnotation('X');
                $forbidden->append($cell);
            }
        }
        $this->proof[$block->id] = 'Dans le bloc ' . $block . ', la valeur ' . Formatter::v($value) . ' ne se trouve pas en ' . $forbidden;
        ksort($this->proof);
    }

    public function logAbsentRow(int $value, int $blockRow, array $lockedColumns, int $rowInBlock, CellCollection $cells, Block $block, int $rowId): void
    {
        $found = new CellCollection();
        foreach ($block as $cell) {
            if ($cell->row->id === $rowId) {
                $cell->setEffect(Cell::EFFECT_FOUND);
                $found->append($cell);
                continue;
            }
            if ($cell->kumiKata[$value]) {
                $cell->setEffect(Cell::EFFECT_FORBIDDEN);
                continue;
            }
        }
        $this->proof[10] = 'Dans le ruban horizontal ' . Formatter::v($blockRow)
                           . ', les blocs ' . Formatter::v($lockedColumns)
                           . ' ont la valeur ' . Formatter::v($value) . ' absente sur la ligne ' . Formatter::v(3 * $blockRow + $rowInBlock);
        $this->proof[11] = 'Donc, dans le bloc ' . $block . ', la valeur ' . Formatter::v($value) . ' est forcément en ' . $found;
        ksort($this->proof);
        $this->message[] = 'On retire la valeur ' . Formatter::v($value) . ' dans ' . $cells;
    }

    public function logAbsentColumn(int $value, int $blockColumn, array $lockedRows, int $columnInBlock, CellCollection $cells, Block $block, int $columnId): void
    {
        $found = new CellCollection();
        foreach ($block as $cell) {
            if ($cell->column->id === $columnId) {
                $cell->setEffect(Cell::EFFECT_FOUND);
                $found->append($cell);
                continue;
            }
            if ($cell->kumiKata[$value]) {
                $cell->setEffect(Cell::EFFECT_FORBIDDEN);
                continue;
            }
        }
        $this->proof[10] = 'Dans le ruban vertical ' . Formatter::v($blockColumn)
                           . ', les blocs ' . Formatter::v($lockedRows)
                           . ' ont la valeur ' . Formatter::v($value) . ' absente sur la colonne ' . Formatter::v(3 * $blockColumn + $columnInBlock);
        $this->proof[11] = 'Donc, dans le bloc ' . $block . ', la valeur ' . Formatter::v($value) . ' est forcément en ' . $found;

        $this->message[] = 'On retire la valeur ' . Formatter::v($value) . ' dans ' . $cells;
    }
}