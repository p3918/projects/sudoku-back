<?php

namespace App\Tests\Techniques;

use App\Process\Set;
use App\Tests\TestCase;

class MediumSetsTest extends TestCase
{

    public function testMediumSets()
    {
        $grid = self::loadJson(__DIR__ . '/../data/success-75-basic.json');
        $this->debugGrid($grid) . PHP_EOL;
        $mediumSets = new Set($grid, 4);
        $grid->resetEffects();
        $step = $mediumSets->execute();
        self::assertTrue($step);
        $this->debugGrid($grid) . PHP_EOL;
        self::assertGrid(__DIR__ . '/../data/success-75-first-medium-set-completed.json', $grid);
    }
}