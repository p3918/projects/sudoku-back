<?php

namespace App\Loggers;

use App\Model\Cell;
use App\Model\CellCollection;
use App\Model\NewChain;
use App\Model\SelfEdge;
use App\Model\Zone;
use App\Serialization\Formatter;

class TigreRemotePairLogger extends ChainerLogger
{
    protected int $level = self::LEVEL_SMART;

    protected string $phase         = 'Tigre - paires distantes';
    protected string $canonicalName = 'tigre_remote_pairs';

    public function logRemotePairEven(NewChain $chain, CellCollection $viewedCells)
    {
        $collection = $chain->collect();
        $values     = $collection[0]->allowedValues();
        $collection->addEffect(Cell::EFFECT_ELEM_2);
        $collection->first()->setEffect(Cell::EFFECT_ELEM_1);
        $collection->last()->setEffect(Cell::EFFECT_ELEM_1);
        $k = 0;
        foreach ($chain as $edge) {
            if (!$edge instanceof SelfEdge) {
                $edge->cell->setAnnotation(Formatter::alpha($k));
                $k++;
            }
        }
        $viewedCells->addEffect(Cell::EFFECT_FORBIDDEN);

        $p = 'Si ' . $collection[0] . ' vaut ' . Formatter::v($values[0]) . ', alors ';
        for ($i = 1; $i < count($collection) - 1; $i++) {
            $p .= $collection[$i] . ' vaut ' . Formatter::v($values[$i % 2]) . ', ';
        }
        $p             .= 'et donc ' . $collection[count($collection) - 1] . ' vaut ' . Formatter::v($values[1]);
        $this->proof[] = $p;
        $this->proof[] = 'De la même manière : Si ' . $collection[0] . ' vaut ' . Formatter::v($values[1])
                         . ', alors ' . $collection[count($collection) - 1] . ' vaut ' . Formatter::v($values[0]);
        $this->proof[] = 'Donc ' . $viewedCells
                         .
                         ((count($viewedCells) > 1) ? ' ne pourront pas contenir les candidats ' : ' ne pourra pas contenir les candidats ')
                         . Formatter::v($values);

        $this->message[] = 'Chaine de paires distantes de taille paire : ' . $collection . '.'
                           . '0n supprime les candidats ' . Formatter::v($values) . ' dans les cases vues par '
                           . 'les 2 extrémités de la chaine ' . $viewedCells;
    }

    public function logRemotePairOdd(NewChain $chain, Zone $zone, int $position1, int $position2, int $position)
    {
        $collection = $chain->collect();
        $values     = $collection[0]->allowedValues();
        $collection->addEffect(Cell::EFFECT_ELEM_2);
        $collection->first()->setEffect(Cell::EFFECT_ELEM_1);
        $collection->last()->setEffect(Cell::EFFECT_ELEM_1);
        $zone[$position1]->setEffect(Cell::EFFECT_ELEM_3);
        $zone[$position2]->setEffect(Cell::EFFECT_ELEM_3);
        $zone[$position]->setEffect(Cell::EFFECT_ELEM_4);
        $k = 0;
        foreach ($chain as $edge) {
            if (!$edge instanceof SelfEdge) {
                $edge->cell->setAnnotation(Formatter::alpha($k));
                $k++;
            }
        }

        $p = 'Si ' . $collection[0] . ' vaut ' . Formatter::v($values[0]) . ', alors ';
        for ($i = 1; $i < count($collection) - 1; $i++) {
            $p .= $collection[$i] . ' vaut ' . Formatter::v($values[$i % 2]) . ', ';
        }
        $p             .= 'et donc ' . $collection[count($collection) - 1] . ' vaut ' . Formatter::v($values[0]);
        $this->proof[] = $p;
        $this->proof[] = 'De la même manière : Si ' . $collection[0] . ' vaut ' . Formatter::v($values[1])
                         . ', alors ' . $collection[count($collection) - 1] . ' vaut ' . Formatter::v($values[1]);

        $this->proof[] = 'On regarde ensuite ' . $zone . '. '
                         . 'Si ' . $collection[0] . ' et ' . $collection[count($collection) - 1]
                         . ' vallent toutes les deux ' . Formatter::v($values[0])
                         . ', alors dans ' . $zone . ' on ne pourra pas avoir ' . Formatter::v($values[0])
                         . ' aux positions ' . Formatter::v([$position1, $position2])
                         . ', donc la seule position possible pour ' . Formatter::v($values[0])
                         . ' sera ' . Formatter::v($position);
        $this->proof[] = 'De même, si ' . $collection[0] . ' et ' . $collection[count($collection) - 1]
                         . ' vallent toutes les deux ' . Formatter::v($values[1])
                         . ', alors la seule position possible pour ' . Formatter::v($values[1])
                         . ' sera ' . Formatter::v($position);
        $this->proof[] = 'On en déduit que ' . $zone[$position] . ' ne peut valoir que '
                         . Formatter::v($values[0]) . ' ou ' . Formatter::v($values[1])
                         . '. On supprime les autres candidats';

        $this->message[] = 'Chaine de paires distantes de taille impaire : ' . $collection
                           . '. En projetant les extrémités de la chaine sur ' . $zone
                           . ', on élimine les candidats autres que ' . Formatter::v($values[0]) . ' et ' . Formatter::v($values[1])
                           . ' dans ' . $zone[$position];
    }

}