<?php

namespace App\Tests\Unit;

use App\Model\DoubleEdge;
use App\Model\Edge;
use App\Model\NewChain;
use App\Model\SelfEdge;
use App\Tests\TestCase;

class TigreRemotePairUnitTest extends TestCase
{
    public function testRemotePair()
    {
        $grid = self::loadJson(__DIR__ . '/../data/gm-5-p37-before-remote-pair.json');
        $this->debugGrid($grid);
        $processor = new TigreRemotePairMock($grid);
        //Chaine espérée :
        //15 =F,1,4=> 15 =f,1,4=> 10 =F,1,4=> 10 =f,1,4=> 37 =F,1,4=> 37 =f,1,4=> 47 =F,1,4=> 47

        //15 =F,1,4=> 15
        $initialEdge = new Edge($grid[15]);
        $chain       = new NewChain(1, [$initialEdge]);
        $n           = $processor->getNeighbours($chain);
        self::assertCount(1, $n);
        self::assertEquals(15, $n[0]->cell->id);
        self::assertTrue($n[0]->strong);
        self::assertEquals(1, $n[0]->x);
        self::assertEquals(4, $n[0]->y);
        $chain->append($n[0]);

        // 15 =f,1,4=> 10
        $n = $processor->getNeighbours($chain);
        self::assertCount(1, $n);
        self::assertInstanceOf(DoubleEdge::class, $n[0]);
        self::assertEquals(10, $n[0]->cell->id);
        self::assertEquals(1, $n[0]->x);
        self::assertEquals(4, $n[0]->y);
        $chain->append($n[0]);

        // 10 =F,1,4=> 10
        $n = $processor->getNeighbours($chain);
        self::assertCount(1, $n);
        self::assertInstanceOf(SelfEdge::class, $n[0]);
        self::assertEquals(10, $n[0]->cell->id);
        self::assertEquals(1, $n[0]->x);
        self::assertEquals(4, $n[0]->y);
        $chain->append($n[0]);

        // 10 =f,1,4=> 37
        $n = $processor->getNeighbours($chain);
        self::assertCount(1, $n);
        self::assertInstanceOf(DoubleEdge::class, $n[0]);
        self::assertEquals(37, $n[0]->cell->id);
        self::assertEquals(1, $n[0]->x);
        self::assertEquals(4, $n[0]->y);
        $chain->append($n[0]);

        // 37 =F,1,4=> 37
        $n = $processor->getNeighbours($chain);
        self::assertCount(1, $n);
        self::assertInstanceOf(SelfEdge::class, $n[0]);
        self::assertEquals(37, $n[0]->cell->id);
        self::assertEquals(1, $n[0]->x);
        self::assertEquals(4, $n[0]->y);
        $chain->append($n[0]);

        // 37 =f,1,4=> 47 =F,1,4=> 47
        $n = $processor->getNeighbours($chain);
        self::assertCount(1, $n);
        self::assertInstanceOf(DoubleEdge::class, $n[0]);
        self::assertEquals(47, $n[0]->cell->id);
        self::assertEquals(1, $n[0]->x);
        self::assertEquals(4, $n[0]->y);
        $chain->append($n[0]);

        //47 =F,1,4=> 47
        $n = $processor->getNeighbours($chain);
        self::assertCount(1, $n);
        self::assertInstanceOf(SelfEdge::class, $n[0]);
        self::assertEquals(47, $n[0]->cell->id);
        self::assertEquals(1, $n[0]->x);
        self::assertEquals(4, $n[0]->y);
        $chain->append($n[0]);

        //Conséquence espérée :
        // Case 51 : retrait du candidat 4
        self::assertTrue($processor->checkValid($chain));
        self::assertTrue($processor->checkDo($chain));


        //$step = (new TigreRemotePair($grid))->execute();
        //$this->debugGrid($grid);
        $this->debugGrid(self::loadJson(__DIR__ . '/../data/gm-5-p37-after-remote-pair.json'));
        self::assertGrid(__DIR__ . '/../data/gm-5-p37-after-remote-pair.json', $grid);

    }
}