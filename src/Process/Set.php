<?php

namespace App\Process;

use App\Loggers\LoggerContract;
use App\Loggers\SetLogger;
use App\Model\Grid;
use App\Model\Zone;
use JetBrains\PhpStorm\Pure;

class Set extends ProcessorContract
{

    /** @var SetLogger */
    protected LoggerContract $logger;

    #[Pure] public function __construct(Grid $grid, private int $size)
    {
        parent::__construct($grid, new SetLogger($this->size));
    }

    public function execute(): bool
    {
        $success = false;
        $this->reset();
        for ($i = 0; $i < 27; $i++) {
            $success |= $this->doZone($this->grid->zones[$i], $this->size);
        }
        return $success;
    }

    public function doZone(Zone $zone, int $maxSize): bool
    {
        if (count($zone->freeValues()) > $maxSize) {
            return false;
        }
        $usefull = false;

        // On va chercher les valeurs manquantes, si une d'entre elle n'est possible que dans une case de la zone alors on la mettra là
        foreach ($zone->freeValues() as $value) {
            $positions = $zone->possibleCells($value);
            if (count($positions) === 1) {
                $this->logger->logOnlyOnePosition($value, $zone, $positions->first());
                $positions->first()->setValue($value);
                $usefull = true;
            }
        }

        // Sinon, si on a un Naked Single et qu'on n'a pas encore fait officiellement le kumi-kata on le met
        // Pourquoi ? Si on a déjà fait le KumiKata pas besoin de se limiter à l'étude des NakedSingle dans les petits ensemble,
        //  → dans ce cas, c'est NakedSingle qui va s'en occuper, on ne fait rien ici
        /** @see NakedSingle */
        if (!$this->grid->needKata) {
            foreach ($zone->freeCells() as $cell) {
                $values = $cell->allowedValues();
                if (count($values) === 1) {
                    $this->logger->logNakedSingle(reset($values), $cell, $zone->freeValues(), $zone);
                    $cell->setValue(reset($values));
                    $usefull = true;
                }
            }
        }

        return $usefull;
    }
}
