
# Algorithme

## Input 
Une valeur de 0 à 8

## Préparation
```
On marque en ELEM_1 les cases qui ont cette valeur
On marque en ELEM_2 les cases qui ont cette valeur lockée
Pour chaque case marquée ELEM_1
  Pour chaque case de la même ligne
     Marquer FORBIDDEN
  Pour chaque cas de la même colonne
     Marquer FORBIDDEN
Pour chaque block de la grille
  Si les cases marquées ELEM_2 sont sur une même ligne
     Pour chaque case de la même ligne
       Marquer FORBIDDEN
  Sinon Si les cases marquées ELEM_2 sont sur une même colonne
     Pour chaque case de la même colonne
       Marquer FORBIDDEN
Pour chaque block de la grille
  Si le block ne contient aucune case marquée ELEM_1 ou ELEM_2
    Pour chaque case du block
        Marquer ELEM_3
```

## Exécution
```
Pour chaque block de la grille
  POSSIBLES = [cases marquées ELEM_3 du block]
  Si taille(POSSIBLES) === 1
    Marquer la case 'FOUND'
    Mettre la valeur dans la case
  Si POSSIBLES > 1 et POSSIBLES <= 3
    Si les cases POSSIBLES sont sur une même ligne ou sur une même colonne
        Marquer les cases POSSIBLES en 'FOUND'
        Locker la valeur dans la case 
```
