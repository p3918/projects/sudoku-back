<?php

namespace App\Tests\Techniques;

use App\Process\SpyBugPlusOne;
use App\Tests\TestCase;

class BugPlusOneTest extends TestCase
{

    public function testBugPlusOne()
    {
        $grid = self::loadJson(__DIR__ . '/../data/power-21-before-bug+1.json');
        $this->debugGrid($grid);
        $step = (new SpyBugPlusOne($grid))->execute();
        $this->debugGrid($grid);
        self::assertTrue($step);
        self::assertGrid(__DIR__ . '/../data/power-21-after-bug+1.json', $grid);
    }
}