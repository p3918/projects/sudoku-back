<?php

namespace App\Controllers;

use App\Model\Grid;
use App\Process\Initiator;
use JetBrains\PhpStorm\NoReturn;
use JsonSerializable;

abstract class Controller
{

    protected int    $requestID = 0;
    protected ?Grid  $grid      = null;
    protected string $name      = '';

    public function __construct(protected string $path, protected string $method, protected array|null $body, ?array $query = null)
    {
        $this->requestID = intval(microtime(true) * 1000);

        if ($this->method === 'POST') {
            file_put_contents($this->fileIdentifier() . '-initial.json', json_encode($body));
            $this->grid = Initiator::initGrid($body);
        }
        if ($this->method === 'GET' && isset($query) && isset($query['grid'])) {
            $this->grid = Initiator::initGrid(
                json_decode(file_get_contents(
                    __DIR__ . '/../../storage/grids/' . $query['grid'] . '.json'), true
                )
            );
        }

    }

    /**
     * @return string
     */
    protected function fileIdentifier(): string
    {
        return __DIR__ . '/../../storage/gridLogs/'
               . $this->requestID . '-' . $this->path;
    }

    #[NoReturn] public abstract function dispatch(): void;

    /**
     * @return string
     */
    protected function fileReportIdentifier(): string
    {
        return __DIR__ . '/../../storage/reports/'
               . $this->requestID;
    }

    #[NoReturn] protected function sendResponse(JsonSerializable|int|string|float|bool|null|array $response): void
    {
        if ($this->grid) {
            $jsonGrid = json_encode($this->grid);
            file_put_contents($this->fileIdentifier() . '-response.json',
                $jsonGrid);
        }
        echo json_encode($response);
        die();
    }
}