<?php

namespace App\Loggers;

use JetBrains\PhpStorm\Pure;

abstract class LoggerContract
{
    const LEVEL_BASIC    = 0;
    const LEVEL_ADVANCED = 1;
    const LEVEL_SMART    = 2;
    const LEVEL_HARD     = 3;

    protected string $phase         = '';
    protected string $canonicalName = '';
    protected int    $level         = self::LEVEL_BASIC;
    /** @var string[] */
    protected array $message = [];
    /** @var string[] */
    protected array $proof = [];
    /** @var string[] */
    protected array $algoSteps = [];

    #[Pure] public function message(): array
    {
        return $this->message;
    }

    #[Pure] public function proof(): array
    {
        return array_values($this->proof);
    }

    #[Pure] public function algoSteps(): array
    {
        return $this->algoSteps;
    }

    #[Pure] public function phase(): string
    {
        return $this->phase;
    }

    #[Pure] public function canonicalName(): string
    {
        return $this->canonicalName;
    }

    #[Pure] public function level(): int
    {
        return $this->level;
    }

    public function reset(): void
    {
        $this->message   = [];
        $this->proof     = [];
        $this->algoSteps = [];
    }

}
