```
-----------------------------------------
[   ,   ,   , |   ,   ,   , |   ,   ,   ]
[   , AB,   , |   ,   ,   , |   , XX,   ] 
[   ,   ,   , |   ,   ,   , |   ,   ,   ]
-----------------------------------------
[   ,   ,   , |   ,   ,   , |   ,   ,   ]
[   , BC,   , |   , CD,   , |   ,   ,   ] 
[   ,   ,   , |   ,   ,   , |   ,   ,   ]
-----------------------------------------
[   ,   ,   , |   ,   ,   , |   ,   ,   ]
[   , XX,   , |   , DE,   , |   , EA,   ] 
[   ,   ,   , |   ,   ,   , |   ,   ,   ]
-----------------------------------------
```

AVEC LE NOUVEAU SYSTEME :
AB =F,a,b=> AB =f,b=> BC =F,b,c=> BC =f,c=> CD =F,c,d=> CD => f,d DE =F,d,e=> DE =f,e=> EA =F,e,a=> EA
M0 =F,a,b=> M1 =f,b=> M2 =F,b,c=> M3 =f,c=> M4 =F,c,d=> M5 => f,d M6 =F,d,e=> M7 =f,e=> M8 =F,e,a=> M9

On forme une chaine telle que :

- chaque maillon est une paire
- entre chaque maillon il y a un candidat commun
- les maillons se voient deux à deux
- le début et la fin de la chaine ou un candidat commun (A)
- ce candidat commun ne doit pas être le même que le candidat commun du dernier maillon
- ce candidat commun ne doit pas être le même que le candidat commun des 2 premiers maillons

Alors les cases vues par les 2 bouts de la chaine ne peuvent pas contenir le candidat A.

Preuve :

- Si C1 = A, alors XX est différent de A
- Sinon, C1 = B
    - donc C2 = C
    - donc C3 = D
    - donc C4 = E
    - donc C5 = A
    - donc XX est différent de A

## Cas particulier ?

```
-----------------------------------------
[   ,   ,   , |   ,   ,   , |   ,   ,   ]
[   , AB,   , |   , YY,   , |   , XX,   ] 
[   ,   ,   , |   ,   ,   , |   ,   ,   ]
-----------------------------------------
[   ,   ,   , |   ,   ,   , |   ,   ,   ]
[   , BC,   , |   , CA,   , |   ,   ,   ] 
[   ,   ,   , |   ,   ,   , |   ,   ,   ]
-----------------------------------------
[   ,   ,   , |   ,   ,   , |   ,   ,   ]
[   , XX,   , |   , CD,   , |   , DA,   ] 
[   ,   ,   , |   ,   ,   , |   ,   ,   ]
-----------------------------------------
```

Double chaine, une fait 3 éléments, l'autre en fait 5 On pourrait enlever A aussi dans YY ==> il faut vraiment trouver
toutes les chaines