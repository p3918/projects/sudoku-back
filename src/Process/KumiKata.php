<?php

namespace App\Process;

use App\Loggers\KumiKataLogger;
use App\Loggers\LoggerContract;
use App\Model\Grid;
use JetBrains\PhpStorm\Pure;

class KumiKata extends ProcessorContract
{
    /** @var KumiKataLogger */
    protected LoggerContract $logger;

    #[Pure] public function __construct(Grid $grid)
    {
        parent::__construct($grid, new KumiKataLogger());
    }

    public function execute(): bool
    {
        $this->reset();
        if (!$this->grid->needKata) {
            $this->grid->needKata = true;
            $this->logger->logExecuteKata();
            return true;
        }
        return false;
    }
}