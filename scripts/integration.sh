#!/usr/bin/env bash

docker-compose run --rm sudoku-testunit-cov php vendor/bin/phpunit --configuration /app/phpunit-silent.xml --group="full-basic"
docker-compose run --rm sudoku-testunit-cov php vendor/bin/phpunit --configuration /app/phpunit-silent.xml --group="full-advanced"
docker-compose run --rm sudoku-testunit-cov php vendor/bin/phpunit --configuration /app/phpunit-silent.xml --group="full-smart"
docker-compose run --rm sudoku-testunit-cov php vendor/bin/phpunit --configuration /app/phpunit-silent.xml --group="full-hard"
docker-compose run --rm sudoku-testunit-cov php vendor/bin/phpunit --configuration /app/phpunit-silent.xml --group="full-final"