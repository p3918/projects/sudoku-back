<?php

namespace App\Tests\Techniques;

use App\Process\EagleW;
use App\Tests\TestCase;

class EagleWTest extends TestCase
{

    public function testEagleW()
    {
        $grid = self::loadJson(__DIR__ . '/../data/issue-57-before-eagle-w.json');
        $grid->resetEffects();
        $this->debugGrid($grid);
        $step = (new EagleW($grid))->execute();
        self::assertTrue($step);
        $this->debugGrid($grid);
        self::assertGrid(__DIR__ . '/../data/issue-57-after-eagle-w.json', $grid);
    }
}