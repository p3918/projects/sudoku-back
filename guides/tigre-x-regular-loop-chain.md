```
-----------------------------------------
[ X3, X3, __, |   , X1,   , |   ,   ,   ]
[ __, A0, __, | __, A1, __, | __, __, __] 
[ X3, X3, A5, |   , X1,   , |   ,   ,   ]
-----------------------------------------
[   ,   , __, | __, __, __, |   ,   ,   ]
[   ,   , __, | __, A2, __, |   ,   ,   ] 
[ X2, X2, A4, | __, __, A3, | X2, X2, X2]
-----------------------------------------
[   ,   , __, |   , X1,   , |   ,   ,   ]
[   ,   , __, |   , X1,   , |   ,   ,   ] 
[   ,   , __, |   , X1,   , |   ,   ,   ]
-----------------------------------------
```

A1A2->X1 ; A3A4->X2 ; A5A0->X3 ; A(2k-1)A(2k)->Xk ; k=1->size/2

AVEC LE NOUVEAU SYSTEME :
A0 =F,a=> A1 =f,a=> A2 =F,a=> A3 =f,a=> A4 =F,a => A5 =f,a=> A0  
On forme une chaine de taille paire telle que :

- chaque maillon contient le candidat a
- on a une alternance de liens forts et faibles sur le candidat a, en commençant par un lien fort (les cases notées __
  ne contiennent pas le candidat A)
- le premier et le dernier maillon se voient, constituant un dernier lien faible permettant de refermer la chaine en
  boucle

A0 =F=> A1 =f=> A2 =F=> A3 =f=> A4 =F=> A5 =f=> A0

Alors les cases vues par 2 cases consécutives de la boucle ne peuvent pas contenir le candidat A (on note Xn ces cases).

Preuve : (// count($chain) / 2 = 3)

- Si A0 = a, alors, les cases X3 sont != a, et d'autre part
    - et d'autre part A5 != a
    - donc A4 = a (donc les cases X2 sont != a)
    - donc A3 != a
    - donc A2 = a (donc les cases X1 sont != a)
    - donc A1 != a
- Sinon, A0 != a
    - donc A1 = a (donc les cases X1 sont != a)
    - donc A2 != a
    - donc A3 = a (donc les cases X2 sont != a)
    - donc A4 != a
    - donc A5 = a (donc les cases X3 sont != a)
