<?php

namespace App\Model;

use App\Serialization\Formatter;
use JetBrains\PhpStorm\Pure;

class DoubleEdge extends Edge
{

    #[Pure] public function __construct(Cell $cell, bool $strong, int $x, public int $y)
    {
        parent::__construct($cell, $strong, $x);
    }

    #[Pure] public function edge(): string
    {
        return parent::edge() . ',' . Formatter::v($this->y);
    }

    /**
     * @param DoubleEdge $neighbour
     *
     * @return bool
     */
    public function equals(Edge $neighbour): bool
    {
        return parent::equals($neighbour) && $neighbour->y === $this->y;
    }

    #[Pure] public function weaken(): DoubleEdge
    {
        return $this->strong ? (new static($this->cell, false, $this->x, $this->y)) : $this;
    }
}