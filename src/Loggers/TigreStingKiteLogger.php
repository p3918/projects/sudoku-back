<?php

namespace App\Loggers;

use App\Model\Cell;
use App\Model\CellCollection;
use App\Serialization\Formatter;

class TigreStingKiteLogger extends LoggerContract
{
    protected int $level = self::LEVEL_SMART;

    protected string $phase         = 'Tigre - Cerf-Volant';
    protected string $canonicalName = 'tigre_sting_kite';

    public function logStingKite(
        CellCollection $zoneC1, CellCollection $zoneC2,
        Cell           $cell1, Cell $cell2,
        CellCollection $viewedCells, int $value)
    {
        $cell1->setEffect(Cell::EFFECT_ELEM_1);
        $cell1->setAnnotation('X1');
        $cell2->setEffect(Cell::EFFECT_ELEM_1);
        $cell2->setAnnotation('X2');
        $zoneC1->addEffect(Cell::EFFECT_ELEM_2);
        $zoneC1->addAnnotation('C1');
        $zoneC2->addEffect(Cell::EFFECT_ELEM_2);
        $zoneC2->addAnnotation('C2');
        foreach ($viewedCells as $viewedCell) {
            $viewedCell->setEffect(Cell::EFFECT_FORBIDDEN);
        }

        $this->proof[]   = 'Si ' . Formatter::v($value) . ' est en ' . $cell1
                           . ', alors il ne peut pas être dans ' . $viewedCells;
        $this->proof[]   = 'Sinon, ' . Formatter::v($value) . ' n\'est pas en ' . $cell1
                           . ', donc il est dans ' . $zoneC1
                           . ', donc il n\'est pas dans ' . $zoneC2
                           . ', donc il est en ' . $cell2
                           . ', donc il ne peut pas être dans ' . $viewedCells;
        $this->message[] = 'Cerf-Volant en ' . $cell1 . ', ' . $cell2 . ' , ' . $zoneC1->first()->block . '. On retire le candidat ' . Formatter::v($value) .
                           ' dans ' . $viewedCells;

    }
}