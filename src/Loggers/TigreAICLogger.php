<?php

namespace App\Loggers;


use App\Model\Cell;
use App\Model\Edge;
use App\Model\NewChain;
use App\Model\SelfEdge;
use App\Serialization\Formatter;

class TigreAICLogger extends ChainerLogger
{
    protected int    $level         = self::LEVEL_HARD;
    protected string $phase         = 'Tigre - approche du tigre';
    protected string $canonicalName = 'tigre_regular_aic';

    public function colorize(NewChain $chain)
    {
        $k = 0;
        foreach ($chain as $neighbour) {
            if ($neighbour->cell->annotation() === '') {
                $neighbour->cell->setAnnotation(Formatter::alpha($k));
                $k++;
            }
            if ($neighbour instanceof SelfEdge) {
                $neighbour->cell->setEffect(Cell::EFFECT_ELEM_3);
                continue;
            }
            $neighbour->cell->setEffect($k % 2 === 0 ? Cell::EFFECT_ELEM_1 : Cell::EFFECT_ELEM_2);
        }
        $this->proof[] = 'Chaine : ' . $chain;
    }

    public function messageOpenedChain(NewChain $chain, Edge $firstNode, int $firstCandidate, Edge $lastNode, int $lastCandidate)
    {
        $this->message[] = 'Approche du tigre sur la boucle ouverte ' . $chain;
        if ($lastNode->cell->isAllowed($firstCandidate)) {
            $this->message[] = 'On retire le candidat initial ' . Formatter::v($firstCandidate) . ' dans le dernier maillon ' . $lastNode->cell;
        }
        if ($firstNode->cell->isAllowed($lastCandidate)) {
            $this->message[] = 'On retire le candidat final ' . Formatter::v($lastCandidate) . ' dans le premier maillon ' . $firstNode->cell;
        }

        $this->proof[] = 'On a vu que dans les 2 cas possibles (' . $firstNode->cell . ' vaut ou ne vaut pas ' . Formatter::v($firstCandidate) . ') on a toujours :';
        $this->proof[] = '- ' . $firstNode->cell . ' est différent de ' . Formatter::v($lastCandidate);
        $this->proof[] = '- ' . $lastNode->cell . ' est différent de ' . Formatter::v($firstCandidate);
    }

    public function messageLoop(NewChain $chain, Edge $firstNode, int $firstCandidate, Edge $lastNode, int $lastCandidate)
    {
        $this->message[] = 'Approche du trigre sur la boucle ' . $chain;
        $this->message[] = 'On retire les candidats différents de ' . Formatter::v([$firstCandidate,
                                                                                    $lastCandidate]) . ' dans le premier et le dernier maillon ' . $firstNode->cell . ',' . $lastNode->cell;

        $this->proof[] = 'On a vu que dans les 2 cas possibles (' . $firstNode->cell . ' vaut ou ne vaut pas ' . Formatter::v($firstCandidate) . ') on a toujours :';
        $this->proof[] = '- ' . $firstNode->cell . ' est différent de ' . Formatter::v($lastCandidate);
        $this->proof[] = '- ' . $lastNode->cell . ' est différent de ' . Formatter::v($firstCandidate);

        for ($k = 0; $k < $chain->count(); $k++) {
            $M1 = $chain[$k];
            $M2 = $chain[($k + 1) % count($chain)];
            if ($M2->strong) {
                continue;
            }

            if ($M2 instanceof SelfEdge) {
                //élimination des autres candidats
                //  - M vaut soit x1, soit x2 (élimination des autres candidats)
                $otherCandidates = array_diff($M1->cell->allowedValues(), [$M2->x, $M2->y]);
                if (!empty($otherCandidates)) {
                    $this->proof[]   = '- ' . $M1->cell /* = $M2*/ . ' vaut soit ' . Formatter::v($M2->x) . ', soit ' . Formatter::v($M2->y) . ' (donc aucun autre candidat n\'est possible dans cette case)';
                    $this->message[] = 'Lien faible ' . (new NewChain(2, [$M1, $M2]))
                                       . ': On retire les candidats ' . Formatter::v($otherCandidates) . ' dans ' . $M1->cell;
                }
                continue;
            }
            // élimination de x dans la zone M1-M2
            //  - Soit M1 vaut x, soit M2 vaut x (élimination de x dans la zone M1-M2)
            $zone = $M1->cell->viewedCells($M2->x)->intersect($M2->cell->viewedCells($M2->x));
            if ($zone->count() > 0) {
                $zone->addEffect(Cell::EFFECT_ELEM_4);
                $this->proof[]   = '- soit ' . $M1->cell . ' vaut ' . Formatter::v($M2->x) . ', soit ' . $M2->cell . ' vaut ' . Formatter::v($M2->x)
                                   . ' donc toute case qui voit ' . $M1->cell . ' et ' . $M2->cell . ' est différente de ' . Formatter::v($M2->x);
                $this->message[] = 'Lien faible ' . (new NewChain(2, [$M1, $M2]))
                                   . ': On retire ' . Formatter::v($M2->x) . ' dans ' . $zone;
            }
        }
    }

    public function logChainInferences(NewChain $chain, Edge $firstNode, int $firstCandidate, Edge $lastNode, int $lastCandidate): void
    {
        $this->proof[] = 'Si ' . $firstNode->cell . ' vaut ' . Formatter::v($firstCandidate) . ', alors  d\'une part : ';
        $this->proof[] = '- ' . $firstNode->cell . ' est différent de ' . Formatter::v($lastCandidate);
        $this->proof[] = '- ' . $lastNode->cell . ' est différent de ' . Formatter::v($firstCandidate);

        // Parcours à l'envers
        // Début : M0 =F=> M6 =f=> M5 (je peux identifier M0 à M7)
        // Milieu : M(2k + 1) =F,a=> M(2k) =f,a=> M(2k-1)
        // Fin 1 : M3 =F=> M2 =f=> M1
        if ($chain->first()->strong) {
            $this->proof[] = 'et d\'autre part : ';
            for ($k = count($chain); $k > 0; $k--) {
                $M2 = $chain[$k % count($chain)];
                $M1 = $chain[$k - 1];
                if ($M2->strong && !($M2 instanceof SelfEdge)) {
                    $this->proof[] = '- ' . $M2->cell . ' est différent de ' . Formatter::v($M2->x)
                                     . ', donc ' . $M1->cell . ' vaut ' . Formatter::v($M2->x);
                }
                if ($M2->strong && $M2 instanceof SelfEdge) {
                    $this->proof[] = '- ' . $M2->cell . ' est différent de ' . Formatter::v($M2->y)
                                     . ', donc ' . $M2->cell . ' vaut ' . Formatter::v($M2->x);
                }
                if (!$M2->strong && !($M2 instanceof SelfEdge)) {
                    $this->proof[] = '- ' . $M2->cell . ' vaut ' . Formatter::v($M2->x)
                                     . ', donc ' . $M1->cell . ' est différent de ' . Formatter::v($M2->x);
                }
                if (!$M2->strong && $M2 instanceof SelfEdge) {
                    $this->proof[] = '- ' . $M2->cell . ' vaut ' . Formatter::v($M2->y)
                                     . ', donc ' . $M2->cell . ' est différent de ' . Formatter::v($M2->x);
                }
            }
        }

        $this->proof[] = 'Sinon, ' . $firstNode->cell . ' est différent de ' . Formatter::v($firstCandidate);
        // Parcours à l'endroit
        // Début : M0 =F=> M1 =f=> M2
        // Milieu : M(2k) =F,a=> M(2k+1) =f,a=> M(2k+2)
        // Fin 1 : M4 =F=> M5 =f=> M6 (+ ajout d'une mini étape à la fin : M6 =F=> M0)
        // Fin 2 : M6 =F=> M7 =voit as f=> M0 (M0 peut être considéré comme M8)
        $start = $chain->first()->strong ? 1 : 0;
        for ($k = $start; $k < count($chain); $k++) {
            $M1 = $chain[$k];
            $M2 = $chain[($k + 1) % count($chain)];
            if ($M2->strong && !($M2 instanceof SelfEdge)) {
                $this->proof[] = '- ' . $M1->cell . ' est différent de ' . Formatter::v($M2->x)
                                 . ', donc ' . $M2->cell . ' vaut ' . Formatter::v($M2->x);
            }
            if ($M2->strong && $M2 instanceof SelfEdge) {
                $this->proof[] = '- ' . $M2->cell . ' est différent de ' . Formatter::v($M2->x)
                                 . ', donc ' . $M2->cell . ' vaut ' . Formatter::v($M2->y);
            }
            if (!$M2->strong && !($M2 instanceof SelfEdge)) {
                $this->proof[] = '- ' . $M1->cell . ' vaut ' . Formatter::v($M2->x)
                                 . ', donc ' . $M2->cell . ' est différent de ' . Formatter::v($M2->x);
            }
            if (!$M2->strong && $M2 instanceof SelfEdge) {
                $this->proof[] = '- ' . $M2->cell . ' vaut ' . Formatter::v($M2->x)
                                 . ', donc ' . $M2->cell . ' est différent de ' . Formatter::v($M2->y);
            }
        }

    }

}
