<?php

namespace App\Tests\Techniques;

use App\Process\Bow;
use App\Tests\TestCase;

class BowTest extends TestCase
{

    public function testRowBow()
    {
        $grid = self::loadJson(__DIR__ . '/../data/success-01-projections-all-phase-00.json');
        $step = (new Bow($grid))->execute();
        self::assertTrue($step);
        $this->debugGrid($grid);
        self::assertGrid(__DIR__ . '/../data/success-01-bow-00.json', $grid);
    }
}