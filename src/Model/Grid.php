<?php

namespace App\Model;

use App\Process\Initiator;
use ArrayAccess;
use JetBrains\PhpStorm\Pure;
use JsonSerializable;

class Grid extends CellCollection implements JsonSerializable, ArrayAccess
{
    public static array $rowIndexes    = [
        [0, 1, 2, 3, 4, 5, 6, 7, 8,],
        [9, 10, 11, 12, 13, 14, 15, 16, 17,],
        [18, 19, 20, 21, 22, 23, 24, 25, 26,],
        [27, 28, 29, 30, 31, 32, 33, 34, 35,],
        [36, 37, 38, 39, 40, 41, 42, 43, 44,],
        [45, 46, 47, 48, 49, 50, 51, 52, 53,],
        [54, 55, 56, 57, 58, 59, 60, 61, 62,],
        [63, 64, 65, 66, 67, 68, 69, 70, 71,],
        [72, 73, 74, 75, 76, 77, 78, 79, 80,],
    ];
    public static array $columnIndexes = [
        [0, 9, 18, 27, 36, 45, 54, 63, 72],
        [1, 10, 19, 28, 37, 46, 55, 64, 73],
        [2, 11, 20, 29, 38, 47, 56, 65, 74],
        [3, 12, 21, 30, 39, 48, 57, 66, 75],
        [4, 13, 22, 31, 40, 49, 58, 67, 76],
        [5, 14, 23, 32, 41, 50, 59, 68, 77],
        [6, 15, 24, 33, 42, 51, 60, 69, 78],
        [7, 16, 25, 34, 43, 52, 61, 70, 79],
        [8, 17, 26, 35, 44, 53, 62, 71, 80],
    ];
    public static array $blockIndexes  = [
        [0, 1, 2, 9, 10, 11, 18, 19, 20],
        [3, 4, 5, 12, 13, 14, 21, 22, 23],
        [6, 7, 8, 15, 16, 17, 24, 25, 26],
        [27, 28, 29, 36, 37, 38, 45, 46, 47],
        [30, 31, 32, 39, 40, 41, 48, 49, 50],
        [33, 34, 35, 42, 43, 44, 51, 52, 53],
        [54, 55, 56, 63, 64, 65, 72, 73, 74],
        [57, 58, 59, 66, 67, 68, 75, 76, 77],
        [60, 61, 62, 69, 70, 71, 78, 79, 80],
    ];
    /** @var Row[] */
    public array $rows = [];
    /** @var Column[] */
    public array $columns = [];
    /** @var Block[] */
    public array $blocks = [];
    /** @var Zone[] */
    public array $zones = [];
    /** @var Cell[][] */
    public array  $positions       = [
        [], // Positions trouvées pour les 0 dans la grille
        [], // Positions trouvées pour les 1 dans la grille
        [], // ... pour les 2
        [], // etc
        [],
        [],
        [],
        [],
        [],
    ];
    public ?array $freeCells       = [];
    public int    $countValues     = 0;
    public bool   $needKata        = false;
    public bool   $invalidDetected = false;
    private bool  $isInitialized   = false;
    /** @var Cell[] */
    private array $dirtyCells = [];

    #[Pure] public function __construct()
    {
        parent::__construct(81);
        for ($i = 0; $i < 81; $i++) {
            $this->cells[]     = new Cell($i, $this);
            $this->freeCells[] = $this->cells[$i];
        }
        for ($i = 0; $i < 9; $i++) {
            $this->rows[$i]       = new Row($this->wherePosIn(self::$rowIndexes[$i])->toArray(), $this, $i);
            $this->columns[$i]    = new Column($this->wherePosIn(self::$columnIndexes[$i])->toArray(), $this, $i);
            $this->blocks[$i]     = new Block($this->wherePosIn(self::$blockIndexes[$i])->toArray(), $this, $i);
            $this->zones[$i]      = $this->rows[$i];
            $this->zones[$i + 9]  = $this->columns[$i];
            $this->zones[$i + 18] = $this->blocks[$i];
        }
    }

    #[Pure] public function freeCells(): CellCollection
    {
        return new CellCollection(81 - $this->countValues, $this->freeCells);
    }

    public function isInitialized(): bool
    {
        return $this->isInitialized;
    }

    public function setInitialized(): void
    {
        $this->isInitialized = true;
    }

    public function blockAt(int $r, int $c): Block
    {
        return $this->blocks[3 * $r + $c];
    }

    public function cellAt(int $r, int $c): Cell
    {
        return $this->cells[9 * $r + $c];
    }

    public function resetEffects(): void
    {
        foreach ($this->dirtyCells as $dirtyCell) {
            $dirtyCell->resetEffect();
        }
        $this->dirtyCells = [];
    }

    public function commonViewedCells(Cell $cell1, Cell $cell2, ?int $value = null): CellCollection
    {
        return $cell1->viewedCells($value)->intersect($cell2->viewedCells($value));
    }

    public function commonFreeViewedCells(Cell $cell1, Cell $cell2): CellCollection
    {
        return $cell1->freeViewedCells()->intersect($cell2->freeViewedCells());
    }

    #[Pure] public function whereIdIn(array $cellIds): CellCollection
    {
        return $this->wherePosIn($cellIds);
    }

    public function setDirty(Cell $cell): void
    {
        $this->dirtyCells[] = $cell;
    }

    public function clone(): Grid
    {
        return Initiator::initGrid(json_decode(json_encode($this), true));
    }
}
