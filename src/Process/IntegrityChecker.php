<?php

namespace App\Process;

use App\Loggers\IntegrityCheckLogger;
use App\Loggers\LoggerContract;
use App\Model\Cell;
use App\Model\Grid;
use App\Resolvers\Resolver;
use JetBrains\PhpStorm\Pure;

class IntegrityChecker extends ProcessorContract
{
    /** @var IntegrityCheckLogger */
    protected LoggerContract $logger;

    #[Pure] public function __construct(Grid $grid)
    {
        parent::__construct($grid, new IntegrityCheckLogger());
    }

    public function execute(): bool
    {
        $this->reset();
        if (!$this->grid->invalidDetected && $this->isInvalid()) {
            $this->grid->invalidDetected = true;
            return true;
        }
        $cell = $this->min();
        if (count($cell->allowedValues()) > 4) {
            return false;
        }
        return $this->goTryFrom($cell);
    }

    public function isInvalid(): bool
    {
        foreach ($this->grid->freeCells() as $cell) {
            if (empty($cell->allowedValues())) {
                $this->logger->logInvalidKumiKata($cell);
                return true;
            }
        }
        return false;
    }

    public function min(): Cell
    {
        $min = $this->grid->freeCells()->first();
        foreach ($this->grid->freeCells() as $cell) {
            if (count($cell->allowedValues()) < count($min->allowedValues())) {
                $min = $cell;
            }
        }
        return $min;
    }

    public function goTryFrom(Cell $cell): bool
    {
        foreach ($cell->allowedValues() as $value) {
            $newGrid = $this->grid->clone();
            $this->logger->logTrying($cell, $value);
            $newGrid[$cell->id]->setValue($value);
            Resolver::doEverything($newGrid);
            if ($newGrid->countValues === 81) {
                $this->logger->logSuccess($cell, $value);
                $cell->setValue($value);
                return true;
            }
            $newIntegrityChecker = new self($newGrid);
            if ($newIntegrityChecker->isInvalid()) {
                $this->logger->logFailure($cell, $value);
                $cell->disableValue($value);
                return true;
            }
        }
        return false;
    }
}