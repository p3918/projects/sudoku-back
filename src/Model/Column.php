<?php

namespace App\Model;

use App\Serialization\Formatter;

class Column extends Zone
{

    public function __construct(array $cells, Grid $grid, int $id)
    {
        parent::__construct($cells, $grid, $id);
        foreach ($this->cells as $cell) {
            $cell->column = $this;
        }
    }

    public function __toString(): string
    {
        // @codeCoverageIgnoreStart
        if (Formatter::$human) {
            return 'C' . ($this->id + 1);
        }
        // @codeCoverageIgnoreEnd
        return 'C' . ($this->id);
    }
}