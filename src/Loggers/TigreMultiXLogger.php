<?php

namespace App\Loggers;


use App\Model\Cell;
use App\Model\NewChain;
use App\Model\SelfEdge;
use App\Serialization\Formatter;

class TigreMultiXLogger extends ChainerLogger
{
    protected int    $level         = self::LEVEL_HARD;
    protected string $phase         = 'Tigre - chaine de liens forts';
    protected string $canonicalName = 'tigre_multi_x_chain';

    public function logTigreChain(NewChain $chain, $a1, $aN): void
    {
        $chain->collect()->addEffect(Cell::EFFECT_ELEM_2);
        $chain->first()->cell->setEffect(Cell::EFFECT_ELEM_1);
        $chain->last()->cell->setEffect(Cell::EFFECT_ELEM_1);
        $k = 0;
        foreach ($chain as $edge) {
            if (!$edge instanceof SelfEdge) {
                $edge->cell->setAnnotation(Formatter::alpha($k));
                $k++;
            }
        }
        $this->message[] = 'Chaine de liens forts : ' . $chain . ', on supprime les candidats opposés ' . Formatter::v($aN) . ' et ' . Formatter::v($a1)
                           . ' dans les 2 extrémités de la chaine : ' . $chain->first()->cell . ' et ' . $chain->last()->cell;

        $this->proof[] = 'Si ' . $chain->first()->cell . ' vaut ' . Formatter::v($a1) . ', alors : ';
        $this->proof[] = '- ' . $chain->first()->cell . ' est différent de ' . Formatter::v($aN);
        $this->proof[] = '- ' . $chain->last()->cell . ' est différent de ' . Formatter::v($a1);

        $this->proof[] = 'Sinon, ' . $chain->first()->cell . ' est différent de ' . Formatter::v($a1) . ', donc : ';

        for ($k = 1; $k < count($chain) - 1; $k++) {
            if ($chain[$k] instanceof SelfEdge) {
                $this->proof[] = '- ' . $chain[$k]->cell . ' est différent de ' . $chain[$k]->y;
                continue;
            }
            $this->proof[] = '- ' . $chain[$k]->cell . ' vaut ' . $chain[$k]->x;
        }
        $this->proof[] = '- et donc, ' . $chain->last()->cell . ' = ' . Formatter::v($aN) . ' (et donc différent de ' . Formatter::v($a1) . ')';
        $this->proof[] = '- et ' . $chain->first()->cell . ' est différent de ' . Formatter::v($aN);

        $this->proof[] = 'Finalement, dans les deux cas on constate que : ';
        $this->proof[] = '- ' . $chain->first()->cell . ' est différent de ' . Formatter::v($aN);
        $this->proof[] = '- ' . $chain->last()->cell . ' est différent de ' . Formatter::v($a1);
    }

}
