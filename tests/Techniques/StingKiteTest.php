<?php

namespace App\Tests\Techniques;

use App\Process\TigreStingKite;
use App\Tests\TestCase;

class StingKiteTest extends TestCase
{

    public function testStingKite()
    {
        $grid = self::loadJson(__DIR__ . '/../data/power-60-before-sting-kite.json');
        $this->debugGrid($grid);
        $step = (new TigreStingKite($grid))->execute();
        $this->debugGrid($grid);
        self::assertTrue($step);
        self::assertGrid(__DIR__ . '/../data/power-60-after-sting-kite.json', $grid);
    }
}