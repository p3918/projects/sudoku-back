#!/usr/bin/env bash

docker compose up -d sudoku-apache-blackfire-server blackfire

docker compose exec blackfire blackfire -v=2 --samples=3 curl http://sudoku-apache-blackfire-server/api/resolve/all-grids