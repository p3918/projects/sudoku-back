<?php

namespace App\Process;

use App\Loggers\LoggerContract;
use App\Loggers\TigreStingKiteLogger;
use App\Model\Block;
use App\Model\Cell;
use App\Model\CellCollection;
use App\Model\Column;
use App\Model\Grid;
use App\Model\Row;
use JetBrains\PhpStorm\Pure;

class TigreStingKite extends ProcessorContract
{
    /** @var TigreStingKiteLogger */
    protected LoggerContract $logger;

    #[Pure] public function __construct(Grid $grid)
    {
        parent::__construct($grid, new TigreStingKiteLogger());
    }

    public function execute(): bool
    {
        // Le cerf-volant
        $this->reset();
        foreach ($this->grid->rows as $row) {
            foreach ($this->grid->columns as $column) {
                $inter       = $row->intersect($column)->first();
                $commonBlock = $inter->block;
                $zone1       = $column->diff($commonBlock);
                $zone2       = $row->diff($commonBlock);

                // on boucle sur les valeurs pour trouver un candidat qui répond à cette condition
                for ($value = 0; $value < 9; $value++) {
                    if ($this->doStingKiteByValue($value, $inter, $commonBlock, $row, $column, $zone1, $zone2)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private function doStingKiteByValue(
        int            $value, Cell $inter, Block $commonBlock,
        Row            $row, Column $column,
        CellCollection $zone1, CellCollection $zone2
    ): bool {
        // Le candidat qu'on cherche doit apparaitre
        //  - une seule fois dans Z1,
        //  - une seule fois dans Z2
        // Et ne doit pas apparaitre dans la case I
        if ($inter->isAllowed($value)) {
            return false;
        }
        if (count($zone1->possiblePositions($value)) !== 1) {
            return false;
        }
        if (count($zone2->possiblePositions($value)) !== 1) {
            return false;
        }
        // On peut alors retirer le candidat des cases vues par X1 et X2
        $x1                = $zone1->possibleCells($value)->first();
        $x2                = $zone2->possibleCells($value)->first();
        $commonViewedCells = $this->grid->commonViewedCells($x1, $x2)
                                        ->diff($commonBlock)
                                        ->filter(fn(Cell $cell) => $cell->kumiKata[$value]);
        if (count($commonViewedCells) === 0) {
            return false;
        }
        $zoneC1 = $column->intersect($commonBlock)->possibleCells($value);
        $zoneC2 = $row->intersect($commonBlock)->possibleCells($value);
        $this->logger->logStingKite($zoneC1, $zoneC2, $x1, $x2, $commonViewedCells, $value);
        $commonViewedCells->walk(fn(Cell $cell) => $cell->disableValue($value));
        return true;
    }
}
