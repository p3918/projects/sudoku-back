<?php

namespace App\Helpers;

use JetBrains\PhpStorm\Pure;

class CellShare
{

    #[Pure] public static function viewEachOther($cellId1, $cellId2): bool
    {
        return self::inSameBlock($cellId1, $cellId2) || self::inSameRow($cellId1, $cellId2) || self::inSameColumn($cellId1, $cellId2);
    }

    #[Pure] public static function inSameBlock($cellId1, $cellId2): bool
    {
        return CellIndex::blockId($cellId1) === CellIndex::blockId($cellId2);

    }

    #[Pure] public static function inSameRow($cellId1, $cellId2): bool
    {
        return CellIndex::row($cellId1) === CellIndex::row($cellId2);
    }

    #[Pure] public static function inSameColumn($cellId1, $cellId2): bool
    {
        return CellIndex::column($cellId1) === CellIndex::column($cellId2);
    }
}