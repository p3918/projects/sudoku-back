<?php

namespace App\Loggers;

use App\Model\Block;
use App\Model\Cell;
use App\Model\CellCollection;
use App\Serialization\Formatter;

class CobraSueDeCoqLogger extends LoggerContract
{
    protected int    $level         = self::LEVEL_SMART;
    protected string $phase         = 'Cobra, Sue de Coq';
    protected string $canonicalName = 'cobra_sue_de_coq';

    public function logBlocksStudy(Block          $block, Block $otherBlock, Cell $cellC1,
                                   CellCollection $zoneC2, CellCollection $zoneT, CellCollection $zoneX, CellCollection $zoneV)
    {
        $cellC1->setEffect(Cell::EFFECT_ELEM_1);
        $cellC1->setAnnotation('C1');
        $zoneT->addEffect(Cell::EFFECT_ELEM_3);
        $zoneT->addAnnotation('T');
        $zoneX->addEffect(Cell::EFFECT_ELEM_2);
        $zoneV->addEffect(Cell::EFFECT_ELEM_4);
        $zoneV->addAnnotation('V');
        $zoneC2->addEffect(Cell::EFFECT_FOUND);
        $zoneC2->addAnnotation('C2');

        $this->proof[] = 'Etude des blocs ' . $block . ' et ' . $otherBlock;
        $this->proof[] = 'CAS 1 : Si ' . $cellC1 . ' vaut ' . Formatter::v($cellC1->allowedValues()[0])
                         . ', alors';
        $this->proof[] = '- dans ' . $otherBlock
                         . ', le ' . Formatter::v($cellC1->allowedValues()[0]) . ' ne peut pas être dans ' . $zoneT;
        $this->proof[] = ', donc la seule position possible pour ' . Formatter::v($cellC1->allowedValues()[0])
                         . ' sera ' . $zoneC2->first();
        $this->proof[] = ', et le ' . Formatter::v($cellC1->allowedValues()[1])
                         . ' sera dans ' . $zoneT;
        $this->proof[] = ', donc il ne pourra pas y avoir la valeur ' . Formatter::v($cellC1->allowedValues()[1])
                         . ' dans ' . $zoneV;
        $this->proof[] = '- dans ' . $zoneV . ', il ne pourra pas y avoir la valeur ' . Formatter::v($cellC1->allowedValues()[0]);
        $this->proof[] = 'CAS 2 : Sinon, ' . $cellC1 . ' vaut ' . Formatter::v($cellC1->allowedValues()[1]) . ', donc on peut faire le même raisonnement :';
        $this->proof[] = '- dans ' . $otherBlock
                         . ', le ' . Formatter::v($cellC1->allowedValues()[1]) . ' ne peut pas être dans ' . $zoneT;
        $this->proof[] = ', donc la seule position possible pour ' . Formatter::v($cellC1->allowedValues()[1])
                         . ' sera ' . $zoneC2->first();
        $this->proof[] = ', et le ' . Formatter::v($cellC1->allowedValues()[0])
                         . ' sera dans ' . $zoneT;
        $this->proof[] = ', donc il ne pourra pas y avoir la valeur ' . Formatter::v($cellC1->allowedValues()[0])
                         . ' dans ' . $zoneV;
        $this->proof[] = 'On en déduit que dans les 2 cas : ';
        $this->proof[] = '- ' . $zoneC2->first() . ' ne peut avoir pour valeur que ' . Formatter::v($cellC1->allowedValues()[0])
                         . ' ou ' . Formatter::v($cellC1->allowedValues()[1]) . ', donc on peut enlever les autres candidats.';
        $this->proof[] = '- ' . $zoneV . ' ne peut pas avoir pour valeur ' . Formatter::v($cellC1->allowedValues()[0])
                         . ' et ' . Formatter::v($cellC1->allowedValues()[1]) . ', on retire ces candidats.';
    }

    public function logRemoveCandidates(Cell $currentCell, Cell $otherCell)
    {
        $this->message[] = 'Sue De Coq => on retire les valeurs autres que la paire ' .
                           Formatter::v($currentCell->allowedValues()) . ' dans ' . $otherCell;
    }

    public function logRemovePair(Cell $currentCell, CellCollection $otherCells)
    {
        $this->message[] = 'Sue De Coq => on retire les valeurs de la paire ' .
                           Formatter::v($currentCell->allowedValues()) . ' dans ' . $otherCells;
    }
}