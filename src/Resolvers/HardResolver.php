<?php

namespace App\Resolvers;

use App\Model\Grid;
use App\Process\IntegrityChecker;
use App\Process\SpySixAligned;
use App\Process\TigreAIC;
use App\Process\TigreMultiXChain;
use App\Process\TigreRegularAlternateLoop;
use App\Serialization\StepsCollection;
use JetBrains\PhpStorm\Pure;

class HardResolver extends AbstractResolver
{
    #[Pure] public function __construct(Grid $grid, ?StepsCollection $allSteps, BasicResolver $previous, bool $acceptFinalStep = true)
    {
        parent::__construct($grid, $allSteps, $acceptFinalStep);

        $this->techniques = [
            new SpySixAligned($this->grid),
            new TigreMultiXChain($this->grid),
            new TigreRegularAlternateLoop($this->grid),
            new TigreAIC($this->grid)
        ];
        if ($this->acceptFinalStep) {
            $this->techniques[] = new IntegrityChecker($this->grid);
        }
        $this->nextResolverOnSuccess = $previous;
        $this->nextResolverOnFailure = null;
    }

}