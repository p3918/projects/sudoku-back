```
-----------------------------------------
[   ,   ,   , |   ,   ,   , |   ,   ,   ]
[   , AB,   , |   ,   , XX, |   ,   ,   ] 
[   ,   ,   , |   ,   ,   , |   ,   ,   ]
-----------------------------------------
[   ,   ,   , |   ,   ,   , |   ,   ,   ]
[   , BC,   , |   , CD,   , |   ,   ,   ] 
[   , XX,   , |   ,   ,   , | DA,   ,   ]
-----------------------------------------
[   ,   ,   , |   ,   ,   , |   ,   ,   ]
[   ,   ,   , |   ,   ,   , |   ,   ,   ] 
[   ,   ,   , |   , R1,   , | R2,   ,   ]
-----------------------------------------
```

On forme une chaine telle que :
- chaque maillon est une paire
- entre chaque maillon il y a un candidat commun
- les maillons se voient deux à deux OU BIEN possèdent un renforcement
- le début et la fin de la chaine ou un candidat commun (A)

Que signifie 'posséder un renforcement' ;
- on projette les 2 maillons sur une zone (ici R1,R2 projeté sur la ligne 9)
- on regarde si par chance R1 et R2 sont les 2 seules positions possibles du candidat commun aux deux maillons dans la zone.
=> Dans ce cas,
  - si CD = D, alors R1 != D donc R2 = D donc DA = A
  - sinon osef

Alors les cases vues par les 2 bouts de la chaine ne peuvent pas contenir le candidat A.

Preuve :
- Si C1 = A, alors XX est différent de A (d'où le 'osef')
- Sinon, C1 = B
    - donc C2 = C
    - donc C3 = D, donc R1 != D donc R2 = D donc C4 = A
    - donc XX est différent de A
