<?php

namespace App\Resolvers;

use App\Model\Grid;
use App\Process\HiddenSet;
use App\Process\KumiKata;
use App\Process\LockedCandidates;
use App\Process\NakedSet;
use App\Process\Set;
use App\Serialization\StepsCollection;
use JetBrains\PhpStorm\Pure;

class AdvancedResolver extends AbstractResolver
{
    #[Pure] public function __construct(Grid $grid, ?StepsCollection $allSteps, BasicResolver $previous, bool $acceptFinalStep = true)
    {
        parent::__construct($grid, $allSteps, $acceptFinalStep);

        $this->techniques            = [
            new KumiKata($this->grid),
            new Set($this->grid, 4),
            new Set($this->grid, 5),
            new NakedSet($this->grid, 2),
            new NakedSet($this->grid, 3),
            new HiddenSet($this->grid, 2),
            new HiddenSet($this->grid, 3),
            new LockedCandidates($this->grid)
        ];
        $this->nextResolverOnSuccess = $previous;
        $this->nextResolverOnFailure = new SmartResolver($this->grid, $this->logSteps, $previous, $acceptFinalStep);
    }
}