<?php

namespace App\Process;

use App\Loggers\LoggerContract;
use App\Loggers\SpyBugPlusOneLogger;
use App\Model\Cell;
use App\Model\Grid;
use JetBrains\PhpStorm\Pure;

class SpyBugPlusOne extends ProcessorContract
{
    /** @var SpyBugPlusOneLogger */
    protected LoggerContract $logger;

    #[Pure] public function __construct(Grid $grid)
    {
        parent::__construct($grid, new SpyBugPlusOneLogger());
    }

    public function execute(): bool
    {
        $this->reset();
        $pairs = $this->grid->freeCells()->filter(fn(Cell $cell) => count($cell->allowedValues()) === 2);
        if (count($pairs) !== count($this->grid->freeCells()) - 1) {
            return false;
        }
        $triple = $this->grid->freeCells()->findOne(fn(Cell $cell) => count($cell->allowedValues()) === 3);
        if ($triple === false) {
            return false;
        }

        $triple->setEffect(Cell::EFFECT_FOUND);
        $occurencesInRow    = [];
        $occurencesInColumn = [];
        $occurencesInBlock  = [];
        foreach ($triple->allowedValues() as $value) {
            $occurencesInRow[$value]    = count($triple->row->freeCells()->filter(fn(Cell $cell) => $cell->kumiKata[$value]));
            $occurencesInColumn[$value] = count($triple->column->freeCells()->filter(fn(Cell $cell) => $cell->kumiKata[$value]));
            $occurencesInBlock[$value]  = count($triple->block->freeCells()->filter(fn(Cell $cell) => $cell->kumiKata[$value]));
        }
        foreach ($occurencesInRow as $value => $count) {
            if ($count === 3 && $occurencesInColumn[$value] === 3 && $occurencesInBlock[$value] === 3) {
                $this->logger->logBug($triple, $value);
                $triple->setValue($value);
                return true;
            }
        }
        return false;
    }
}
