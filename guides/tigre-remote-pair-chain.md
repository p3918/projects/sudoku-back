```
-----------------------------------------
[   ,   ,   , |   ,   ,   , |   ,   ,   ]
[   , X1,   , |   , X2,   , |   ,   ,   ] 
[   ,   ,   , |   ,   ,   , |   ,   ,   ]
-----------------------------------------
[   ,   ,   , |   ,   ,   , |   ,   ,   ]
[   ,   ,   , |   , X3,   , |   ,   ,   ] 
[   , XX,   , |   ,   , X4, |   ,   ,   ]
-----------------------------------------
[   ,   ,   , |   ,   ,   , |   ,   ,   ]
[   ,   ,   , |   ,   ,   , |   ,   ,   ] 
[   ,   ,   , |   ,   ,   , |   ,   ,   ]
-----------------------------------------
```

On forme une chaine de taille paire (notons les candidats a,b) telle que :
- tous les maillons sont des paires identiques
- les maillons se voient deux à deux

AVEC LE NOUVEAU SYSTEME :
X1 =F,a,b=> X1 =f,a,b X2 =>F,a,b=> X2 =f,a,b=> X3 =F,a,b=> X3 =f,a,b=> X4 =F,a,b=> X4   etc.

Alors les cases vues par les 2 bouts de la chaine ne peuvent pas contenir les valeurs de la paire considérée.

Preuve :
- Si X1 vaut a, alors XX est différent de a
- Sinon, X1 vaut b 
  - donc X2 vaut a
  - donc X3 vaut b
  - donc X4 vaut a
  - donc XX est différent de a
Et tout pareil avec b
- Si X1 vaut b, alors XX est différent de b
- Sinon, X1 vaut a
  - donc X2 vaut b
  - donc X3 vaut a
  - donc X4 vaut b
  - donc XX est différent de b
