<?php

namespace App\Helpers;

use JetBrains\PhpStorm\Pure;

class ZoneIndex
{

    #[Pure] public static function viewed(int $cellId): array
    {
        return array_unique(array_merge(
            self::blockOf($cellId), self::rowOf($cellId), self::columnOf($cellId)
        ));
    }

    #[Pure] public static function blockOf(int $cellId): array
    {
        $blockRow    = CellIndex::blockRow($cellId);
        $blockColumn = CellIndex::blockColumn($cellId);
        $output      = [];
        for ($inBlockId = 0; $inBlockId < 9; $inBlockId++) {
            $otherCellId = CellIndex::fromBlockRowColumnInBlockId($blockRow, $blockColumn, $inBlockId);
            if ($otherCellId !== $cellId) {
                $output[] = $otherCellId;
            }
        }
        return $output;
    }

    #[Pure] public static function rowOf(int $cellId): array
    {
        $row    = CellIndex::row($cellId);
        $output = [];
        for ($column = 0; $column < 9; $column++) {
            $otherCellId = CellIndex::fromRowColumn($row, $column);
            if ($otherCellId !== $cellId) {
                $output[] = $otherCellId;
            }
        }
        return $output;
    }

    #[Pure] public static function columnOf(int $cellId): array
    {
        $column = CellIndex::column($cellId);
        $output = [];
        for ($row = 0; $row < 9; $row++) {
            $otherCellId = CellIndex::fromRowColumn($row, $column);
            if ($otherCellId !== $cellId) {
                $output[] = $otherCellId;
            }
        }
        return $output;
    }

    #[Pure] public static function all(): array
    {
        $output = [];
        for ($i = 0; $i < 9; $i++) {
            $output[] = self::row($i);
            $output[] = self::column($i);
            $output[] = self::block($i);
        }
        return $output;
    }

    #[Pure] public static function row(int $rowId): array
    {
        $start  = $rowId * 9;
        $output = [];
        for ($column = 0; $column < 9; $column++) {
            $output[] = $start + $column;
        }
        return $output;
    }

    #[Pure] public static function column(int $columnId): array
    {
        $output = [];
        for ($row = 0; $row < 9; $row++) {
            $output[] = $row * 9 + $columnId;
        }
        return $output;
    }

    #[Pure] public static function block(int $blockId): array
    {
        $blockRow    = floor($blockId / 3);
        $blockColumn = $blockId % 3;
        return self::blockFromCoordinate($blockRow, $blockColumn);
    }

    #[Pure] public static function blockFromCoordinate(int $blockRow, int $blockColumn): array
    {
        $output = [];
        for ($inBlockId = 0; $inBlockId < 9; $inBlockId++) {
            $output[] = CellIndex::fromBlockRowColumnInBlockId($blockRow, $blockColumn, $inBlockId);
        }
        return $output;
    }
}