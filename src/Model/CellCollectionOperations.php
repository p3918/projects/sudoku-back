<?php

namespace App\Model;

use JetBrains\PhpStorm\Pure;

trait CellCollectionOperations
{

    /** @noinspection PhpPureFunctionMayProduceSideEffectsInspection */
    #[Pure] public function whereIdNotIn(array $cellIds): CellCollection
    {
        return $this->filter(fn(Cell $cell) => !in_array($cell->id(), $cellIds));
    }

    public function filter(callable $closure): CellCollection
    {
        return new CellCollection(0, array_values(array_filter($this->cells, $closure, ARRAY_FILTER_USE_BOTH)));
    }

    /** @noinspection PhpPureFunctionMayProduceSideEffectsInspection */

    #[Pure] public function wherePosIn(array $positions): CellCollection
    {
        $output = [];
        foreach ($positions as $key) {
            $output[$key] = $this[$key];
        }
        return new CellCollection(count($positions), $output);
    }

    /** @noinspection PhpPureFunctionMayProduceSideEffectsInspection */

    #[Pure] public function implode(string $separator): string
    {
        return implode($separator, $this->cells);
    }

    /**
     * @return Cell[]
     */
    #[Pure] public function toArray(): array
    {
        return $this->cells;
    }

    public function map(callable $closure): array
    {
        return array_map($closure, $this->cells);
    }

    public function walk(callable $closure): CellCollection
    {
        array_walk($this->cells, $closure);
        return $this;
    }

    #[Pure] public function intersect(CellCollection $otherCollection): CellCollection
    {
        $cells = [];
        foreach ($this as $cell) {
            $cells[$cell->id] = $cell;
        }
        $inter = [];
        foreach ($otherCollection as $cell) {
            if (isset($cells[$cell->id])) {
                $inter[] = $cell;
            }
        }
        return new CellCollection(count($inter), $inter);
    }

    /** @noinspection PhpPureFunctionMayProduceSideEffectsInspection */
    #[Pure] public function diff(CellCollection $otherCollection): CellCollection
    {
        return $this->whereIdIn(array_diff($this->cellIds(), $otherCollection->cellIds()));
    }

    /** @noinspection PhpPureFunctionMayProduceSideEffectsInspection */
    #[Pure] public function whereIdIn(array $cellIds): CellCollection
    {
        return $this->filter(fn(Cell $cell) => in_array($cell->id(), $cellIds));
    }
}