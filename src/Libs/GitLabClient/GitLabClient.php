<?php

namespace App\Libs\GitLabClient;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

/**
 * @codeCoverageIgnore
 */
class GitLabClient
{
    private Client $http;
    private string $token;
    private string $url;
    private int    $userId;

    public function __construct()
    {
        $this->http   = new Client();
        $this->token  = getenv('GITLAB_KEY');
        $this->url    = getenv('GITLAB_URL');
        $this->userId = !empty($this->token) ? $this->userRequest()['id'] : 0;
    }

    public function userRequest(): array
    {
        try {
            $response = $this->http->request('GET', "https://gitlab.com/api/v4/user", [
                'query'   => [],
                'json'    => [],
                'headers' => ['Content-Type' => 'application/json', 'Authorization' => 'Bearer ' . $this->token],
            ]);
        } catch (ClientException $exception) {
            $response = $exception->getResponse();
        }

        return json_decode($response->getBody()->getContents(), true);
    }

    public function request(string $method, string $url, array $queryParams = [], array $bodyParams = []): array
    {
        try {
            $response = $this->http->request($method, $this->url . $url, [
                'query'   => $queryParams,
                'json'    => $bodyParams,
                'headers' => ['Content-Type' => 'application/json', 'Authorization' => 'Bearer ' . $this->token],
            ]);
        } catch (ClientException $exception) {
            $response = $exception->getResponse();
        }

        return json_decode($response->getBody()->getContents(), true);
    }

    public function getUserId(): mixed
    {
        return $this->userId;
    }
}
