<?php

namespace App\Tests\Unit;

use App\Model\CellCollection;
use App\Model\NewChain;
use App\Model\SelfEdge;
use App\Process\TigreRemotePair;
use JetBrains\PhpStorm\Pure;

class TigreRemotePairMock extends TigreRemotePair
{

    #[Pure] protected function cellsToExplore(): CellCollection
    {
        return new CellCollection(1, [$this->grid[15]]);
    }

    public function getNeighbours(NewChain $chain): array
    {
        $neighbours = $this->neighbours($chain);
        foreach ($neighbours as $key => $neighbour) {
            if($neighbour instanceof SelfEdge) {
                continue;
            }
            if($chain->exists($neighbour->cell->id)) {
                unset($neighbours[$key]);
            }
        }
        return array_values($neighbours);
    }

    #[Pure] public function checkValid(NewChain $chain): bool
    {
        return $this->isValid($chain);
    }

    public function checkDo(NewChain $chain): bool
    {
        return $this->tryToDo($chain);
    }

}