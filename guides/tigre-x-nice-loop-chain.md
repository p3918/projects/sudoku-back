# CAS 1 : boucle finie par un lien fort, avec un lien faible dans la case AB

```
-----------------------------------------
[ A0, __, __, | !a, !a, !a, | !a, !a, A1]
[ !b, !b, !b, |   ,   ,   , |   ,   ,   ] 
[ !b, B2, !b, |   ,   ,   , |   ,   ,   ]
-----------------------------------------
[   ,   ,   , |   ,   ,   , |   ,   ,   ]
[   ,   ,   , |   ,   ,   , |   ,   ,   ] 
[   ,   ,   , |   ,   ,   , |   ,   ,   ]
-----------------------------------------
[ !b, B1, !b, | !b, !b, !b, | __, AB, __]
[   ,   ,   , |   ,   ,   , | !a, !a, !a] 
[   ,   ,   , |   ,   ,   , | !a, !a, A2]
-----------------------------------------
```

A0 =F,a=> A1 =f,a=> A2 =F,a=> AB =f,a,b=> AB =F,b=> B1 =f,b=> B2 =F,b=> A0 (le dernier lien est optionnel, tant que B2
et A0 se voient)
M0 =F,a=> M1 =f,a=> M2 =F,a=> M3 =f,a,b=> M4 =F,b=> M5 =f,b=> M6 =F,b=> A0 (le dernier lien est optionnel, tant que B2
et A0 se voient)

On forme une chaine sur deux candidats a,b telle que :

- chaque maillon contient le candidat a dans la première partie, le candidat b dans la seconde
- on a une alternance de liens forts et faibles sur les candidats a et b, en commençant par un lien fort (les cases
  notées __
  ne contiennent pas le candidat a ou le candidat b)
- le premier et le dernier maillon se voient (et éventuellement forment un dernier lien fort sur le candidat b)

Preuve :

- Si A0 = a, alors A0 != b et
    - B2 = b
    - B1 != b
    - AB = b, et AB != a
    - A2 = a
    - A1 != a
- Sinon, A0 != a
    - A1 = a
    - A2 != a
    - AB = a, et AB != b
    - B1 = b
    - B2 != b
    - A0 = b

DANS LES 2 CAS :

- A0 vaut soit a, soit b (élimination des autres candidats)
- A1 ou A2 = a (élimination de a dans la zone A1-A2)
- AB vaut soit a, soit b (élimination des autres candidats)
- B1 ou B2 = b (élimination de b dans la zone B1-B2)

# CAS 2 : boucle non finie, avec un lien faible dans la case AB, terminant par un lien fort

```
-----------------------------------------
[ A0, __, __, | !a, !a, !a, | !a, !a, A1]
[   ,   ,   , |   ,   ,   , |   ,   ,   ] 
[   ,   ,   , |   ,   ,   , |   ,   ,   ]
-----------------------------------------
[ !b, !b, !b, |   ,   ,   , |   ,   ,   ]
[ B3, B2, !b, |   ,   ,   , |   ,   ,   ] 
[ !b, !b, !b, |   ,   ,   , |   ,   ,   ]
-----------------------------------------
[ !b, B1, !b, | !b, !b, !b, | __, AB, __]
[   ,   ,   , |   ,   ,   , | !a, !a, !a] 
[   ,   ,   , |   ,   ,   , | !a, !a, A2]
-----------------------------------------
```

A0 =F,a=> A1 =f,a=> A2 =F,a=> AB =f,a,b=> AB =F,b=> B1 =f,b=> B2 =F,b=> B3 voit A0 (le dernier lien est optionnel, tant
que B2 et A0 se voient)
M0 =F,a=> M1 =f,a=> M2 =F,a=> M3 =f,a,b=> M4 =F,b=> M5 =f,b=> M6 =F,b=> M7 voit A0 (le dernier lien est optionnel, tant
que B2 et A0 se voient)
// 7 éléments, de 0 à 6

On forme une chaine sur deux candidats a,b telle que :

- chaque maillon contient le candidat a dans la première partie, le candidat b dans la seconde
- on a une alternance de liens forts et faibles sur les candidats a et b, en commençant par un lien fort (les cases
  notées __
  ne contiennent pas le candidat a ou le candidat b)
- le premier et le dernier maillon se voient (et éventuellement forment un dernier lien fort sur le candidat b)

Preuve :

- Si A0 = a, alors A0 != b et B3 != a
- Sinon, A0 != a
    - A1 = a
    - A2 != a
    - AB = a, et AB != b
    - B1 = b
    - B2 != b
    - B3 = b, et B3 != a
    - A0 != b

DANS LES 2 CAS :

- A0 != b
- B3 != a

# CAS 3 : boucle finie par un lien fort, avec un lien fort dans la case AB

```
-----------------------------------------
[ A0, __, __, | !a, !a, !a, | !a, !a, A1]
[ !b, !b, !b, |   ,   ,   , |   ,   ,   ] 
[ !b, B1, !b, |   ,   ,   , |   ,   ,   ]
-----------------------------------------
[   ,   ,   , |   ,   ,   , |   ,   ,   ]
[   ,   ,   , |   ,   ,   , |   ,   ,   ] 
[   ,   ,   , |   ,   ,   , |   ,   ,   ]
-----------------------------------------
[   , AB,   , |   ,   ,   , | !a, A3, !a]
[   ,   ,   , |   ,   ,   , | !a, !a, !a] 
[   ,   ,   , |   ,   ,   , | !a, !a, A2]
-----------------------------------------
```

A0 =F,a=> A1 =f,a=> A2 =F,a=> A3 =f,a=> AB =F,a,b=> AB =f,b=> B1 =F,b=> A0 (le dernier lien est optionnel, tant que B2
et A0 se voient)
M0 =F,a=> M1 =f,a=> M2 =F,a=> M3 =f,a=> M4 =F,a,b=> M5 =f,b=> M6 =F,b=> M0 (le dernier lien est optionnel, tant que B2
et A0 se voient)
// 7 éléments, de 0 à 6 : $k = {0...3}

On forme une chaine sur deux candidats a,b telle que :

- chaque maillon contient le candidat a dans la première partie, le candidat b dans la seconde
- on a une alternance de liens forts et faibles sur les candidats a et b, en commençant par un lien fort (les cases
  notées __
  ne contiennent pas le candidat a ou le candidat b)
- le premier et le dernier maillon se voient (et éventuellement forment un dernier lien fort sur le candidat b)

Preuve :

- Si A0 = a, alors A0 != b et
    - B1 = b
    - AB != b
    - AB = a
    - A3 != a
    - A2 = a
    - A1 != a
- Sinon, A0 != a
    - A1 = a
    - A2 != a
    - A3 = a
    - AB != a, et AB = b
    - B1 != b
    - A0 = b

DANS LES 2 CAS :

- A0 vaut soit a, soit b (élimination des autres candidats)
- A1 ou A2 = a (élimination de a dans la zone A1-A2)
- AB vaut a ou A3 vaut A (élimination de a dans la zone A3-AB)
- AB ou B1 = b (élimination de b dans la zone AB-B1)

# CAS 4 : boucle non finie, avec un lien fort dans la case AB, terminant par un lien fort

```
-----------------------------------------
[ A0, __, __, | !a, !a, !a, | !a, !a, A1]
[   ,   ,   , |   ,   ,   , |   ,   ,   ] 
[   ,   ,   , |   ,   ,   , |   ,   ,   ]
-----------------------------------------
[ !b, !b, !b, |   ,   ,   , |   ,   ,   ]
[ B2, B1, !b, |   ,   ,   , |   ,   ,   ] 
[ !b, !b, !b, |   ,   ,   , |   ,   ,   ]
-----------------------------------------
[   , AB,   , |   ,   ,   , | !a, A3, !a]
[   ,   ,   , |   ,   ,   , | !a, !a, !a] 
[   ,   ,   , |   ,   ,   , | !a, !a, A2]
-----------------------------------------
```

A0 =F,a=> A1 =f,a=> A2 =F,a=> A3 =f,a=> AB =F,a,b=> AB =f,b=> B1 =F,b=> B2 voit A0 (le dernier lien est optionnel, tant
que B2 et A0 se voient)
// On peut considérer un faux lien faible entre M7 et A0, au sens ou si M7 != b alors A0 != b M0 =F,a=> M1 =f,a=> M2
=F,a=> M3 =f,a=> M4 =F,a,b=> M5 =f,b=> M6 =F,b=> M7 voit A0 (le dernier lien est optionnel, tant que B2 et A0 se voient)
// 8 éléments, de 0 à 7 : $k = {0..3}

On forme une chaine sur deux candidats a,b telle que :

- chaque maillon contient le candidat a dans la première partie, le candidat b dans la seconde
- on a une alternance de liens forts et faibles sur les candidats a et b, en commençant par un lien fort (les cases
  notées __
  ne contiennent pas le candidat a ou le candidat b)
- le premier et le dernier maillon se voient (et éventuellement forment un dernier lien fort sur le candidat b)

Preuve :

- Si A0 = a, alors A0 != b et B2 != a
- Sinon, A0 != a
    - A1 = a
    - A2 != a
    - A3 = a
    - AB != a
    - donc AB = b
    - B1 != b
    - B2 = b (et B2 != a)
    - A0 != b

DANS LES 2 CAS :

- A0 != b
- B2 != a

# CAS FOU, à 3 candidats : boucle finie par un lien fort, avec un lien faible dans la case AB

```
-----------------------------------------
[ A0, __, __, | !a, __, !a, | !a, __, A1]
[ !b, !b, !b, |   , !c,   , |   , !c,   ] 
[ !b, B2, !b, |   , !c,   , |   , !c,   ]
-----------------------------------------
[   ,   ,   , |   , !c,   , |   , !c,   ]
[   ,   ,   , |   , C2,   , |   , C1,   ] 
[   ,   ,   , |   , !c,   , |   , !c,   ]
-----------------------------------------
[   ,   ,   , |   , !c,   , | !a, AC, !a]
[ !b, B1, !b, | !b, CB, !b, | __, __, __] 
[   ,   ,   , |   , !c,   , | !a, __, A2]
-----------------------------------------
```

Preuve :

- Si A0 = a, alors A0 != b et B2 != a (si chaine ouverte on doit s'arrêter à ce constat)
    - B2 = b
    - B1 != b
    - BC = b, et BC != c
    - C2 = c
    - C1 != c
    - AC = c (AC != a)
    - A2 = a
    - A1 != a
- Sinon, A0 != a
    - A1 = a
    - A2 != a
    - AC = a, et AC != c
    - C1 = c
    - C2 != C
    - CB = c, CB != b
    - B1 = b
    - B2 != b (si chaine ouverte on arrive à la fin des B sur un lien fort avec BN = b, donc BN != a et A0 != b)
    - A0 = b

DANS LES 2 CAS :

- A0 = a ou b (élimination des autres candidats)
- AC = a ou c (élimination des autres candidats)
- BC = b ou c (élimination des autres candidats)
- A1 ou A2 vaut a (élimination des a dans la zone A1-A2)
- C1 ou C2 vaut c (élimination des c dans la zone C1-C2)
- B1 ou B2 vaut b (élimination des b dans la zone B1-B2)

# Récapitulatif

## Cas d'une boucle (lien fort sur le dernier candidat entre le dernier élément de la chaine et le premier)

ALORS Pour tous les liens faibles qu'on a :

- SI le lien est entre 2 candidats dans une case ALORS élimination des autres candidats
- SI le lien est entre 2 cases sur un candidat donné ALORS élimination du candidat dans la zone formée par les 2 cases

ATTENTION A FAIRE LES BONS LIENS QUAND 2 CANDIDATS SONT CONCERNES :
B2 =F,b=> BC =f,b,c=> BC =F,c=> C1 OU B2 =>f,b => BC =F,b,c => BC =f,c=>C1

PREUVE :

- SI 'premier maillon = premier candidat' ALORS dernier maillon != premier candidat et premier maillon != dernier
  candidat
    - Partant de 'premier maillon != dernier candidat'
    - On parcourt les liens forts de la chaine à l'envers ainsi
        - GENERALITE : DIF =(F)=> EG =(f)=> DIF
        - Si c'est 2 maillons (M1,M2) et 1 candidat x on dit
            - M1 != x donc M2 = x
        - Si c'est 1 maillon M et 2 candidats x1,x2 on dit
            - M != x1 donc M = x2

- SINON, 'premier maillon != premier candidat' (on démarre donc avec une inégalité)
    - On parcourt les liens forts de la chaine ainsi :
        - GENERALITE : DIF =(F)=> EG =(f)=> DIF
        - Si c'est 2 maillons (M1,M2) et 1 candidat x on dit
            - M1 != x donc M2 = x
        - Si c'est 1 maillon M et 2 candidats x1,x2 on dit
            - M != x1 donc M = x2

DANS LES 2 CAS :

- premier maillon = premier ou dernier candidat (élimination des autres candidats)
- dernier maillon = premier ou dernier candidat (élimination des autres candidats)
- Pour chaque lien faible de type 1 maillon, 2 candidats
    - M vaut soit x1, soit x2 (élimination des autres candidats)
- Pour chaque lien faible de type 2 maillons, 1 candidat
    - Soit M1 vaut x, soit M2 vaut x (élimination de X dans la zone M1-M2)

## Cas d'une chaine ouverte (le premier et le dernier se voient, sans lien fort)

ALORS :

- Le premier maillon est différent du dernier candidat
- Le dernier maillon est différent du premier candidat

PREUVE :

- SI 'premier maillon = premier candidat' ALORS
    - premier maillon != dernier candidat
    - dernier maillon != premier candidat
- SINON, 'premier maillon != premier candidat' (on démarre donc avec une inégalité)
    - On parcourt les liens forts de la chaine ainsi :
        - GENERALITE : DIF =(F)=> EG =(f)=> DIF
        - Si c'est 2 maillons (M1,M2) et 1 candidat x on dit
            - M1 != x donc M2 = x
        - Si c'est 1 maillon M et 2 candidats x1,x2 on dit
            - M != x1 donc M = x2

DANS LES 2 CAS :

- premier maillon != dernier candidat
- dernier maillon != premier candidat

# REPRENONS AVEC MES DERNIERS AJOUTS :

## Exemple de boucle fermée

| =f,7,2=>C0:[L1;C1]=F,2=>C1:[L1;C9]=f,2=>C2:[L2;C8]=F,2=>C3:[L7;C8]=f,2,7=>C3:[L7;C8]=F,7=>C4:[L7;C2]=f,7=>C5:[L8;C1]
=F,7=>C0:[L1;C1] |

## Exemple de boucle ouverte