<?php

namespace App\Process;

use App\Helpers\SwitchVar;
use App\Loggers\LoggerContract;
use App\Loggers\SwordfishLogger;
use App\Model\Cell;
use App\Model\CellCollection;
use App\Model\Column;
use App\Model\Grid;
use App\Model\Row;
use JetBrains\PhpStorm\Pure;

class Swordfish extends ProcessorContract
{
    /** @var SwordfishLogger */
    protected LoggerContract $logger;

    #[Pure] public function __construct(Grid $grid)
    {
        parent::__construct($grid, new SwordfishLogger());
    }

    public function execute(): bool
    {
        // On cherche des cases disposées en rectangle, selon 2 lignes, 2 colonnes, 4 blocks
        for ($blockIndex1 = 0; $blockIndex1 < 3; $blockIndex1++) {
            for ($blockIndex2 = $blockIndex1 + 1; $blockIndex2 < 3; $blockIndex2++) {
                for ($value = 0; $value < 9; $value++) {
                    $this->reset();
                    if ($this->rowSwordfish($value, $blockIndex1, $blockIndex2)) {
                        return true;
                    }
                    $this->reset();
                    if ($this->columnSwordfish($value, $blockIndex1, $blockIndex2)) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    private function rowSwordfish(int $value, int $blockRow1, int $blockRow2): bool
    {
        for ($r1 = 0; $r1 < 3; $r1++) {
            for ($r2 = 0; $r2 < 3; $r2++) {
                $row1 = $this->grid->rows[$blockRow1 * 3 + $r1];
                $row2 = $this->grid->rows[$blockRow2 * 3 + $r2];
                if ($this->analyze($value, $row1, $row2)) {
                    return true;
                }
            }
        }
        return false;
    }

    private function analyze(int $value, Row|Column $zone1, Row|Column $zone2): bool
    {
        $positions1 = $zone1->possiblePositions($value);
        $positions2 = $zone2->possiblePositions($value);

        //Il faut qu'au moins une des 2 zones puisse héberger N1,N2 (c'est-à-dire avoir exactement 2 positions possibles)
        if ((count($positions1) !== 2 && count($positions2) !== 2) || count($positions1) < 2 || count($positions2) < 2) {
            return false;
        }
        //J'aimerais que N1,N2 soient sur la première zone. Si besoin j'interchange les zones
        if (count($positions1) > 2) {
            SwitchVar::switchVars($zone1, $zone2);
            SwitchVar::switchVars($positions1, $positions2);
        }
        // J'ai donc bien fixé n1 et n2
        list($c1, $c2) = $positions1;
        list($n1, $n2, $n3, $n4) = [$zone1[$c1], $zone1[$c2], $zone2[$c1], $zone2[$c2]];

        // N1 et N2 ne doivent pas être dans le même block
        if ($n1->block->id === $n2->block->id) {
            return false;
        }

        // Si jamais la valeur est possible dans le block qui ne contient ni N3, N4, c'est mort pour l'espadon, mais on a potentiellement un gratte-ciel
        if (count($zone2->diff($n3->block)->diff($n4->block)->filter(fn(Cell $cell) => $cell->kumiKata[$value])) > 0) {
            return $this->skyscraper($value, $n1, $n2, $n3, $n4);
        }

        $this->logger->logFoundPotentialSwordfish($value, $n1, $n2, $n3, $n4);
        return $this->swordfish($value, $n1, $n2, $n3, $n4);
    }

    private function skyscraper(int $value, Cell $n1, Cell $n2, Cell $n3, Cell $n4): bool
    {
        $zoneN3N4 = ($n3->row->id === $n4->row->id) ? $n3->row : $n3->column;
        $zoneN1N3 = ($n3->row->id === $n4->row->id) ? $n3->column : $n3->row;
        $zoneN2N4 = ($n3->row->id === $n4->row->id) ? $n4->column : $n4->row;

        // On cherche cette configuration autour de N3 :
        // [N7, __, __]
        // [N3, N5, N6]
        // [N8, __, __]
        $notEmptyCellsAroundN3 = $n3->block->diff($zoneN3N4)->diff($zoneN1N3)->filter(fn(Cell $cell) => $cell->isAllowed($value));

        // Si on ne la trouve pas, on regarde si on la trouve autour de N4.
        // Si oui on interchange N3,N4 ; Si non on abandonne
        if (count($notEmptyCellsAroundN3) > 0) {
            $notEmptyCellsAroundN4 = $n4->block->diff($zoneN3N4)->diff($zoneN2N4)->filter(fn(Cell $cell) => $cell->isAllowed($value));
            if (count($notEmptyCellsAroundN4) > 0) {
                return false;
            }
            SwitchVar::switchVars($n3, $n4);
            SwitchVar::switchVars($n1, $n2);
            $zoneN3N4 = ($n3->row->id === $n4->row->id) ? $n3->row : $n3->column;
            $zoneN1N3 = ($n3->row->id === $n4->row->id) ? $n3->column : $n3->row;
        }

        // N4 doit contenir le candidat sinon ça ne servira à rien
        if (!$n4->isAllowed($value)) {
            return false;
        }

        // On positionne les valeurs
        $zone5 = $n3->block->intersect($zoneN3N4)->except($n3);
        $zone7 = $n3->block->intersect($zoneN1N3);
        list($n5, $n6) = $zone5;
        list($n7, $n8) = $zone7->except($n3);
        $this->logger->logFoundSkyscraper($value, $n1, $n2, $n3, $n4, $n5, $n6, $n7, $n8);

        // On retire le candidat dans N4
        $this->logger->logSuccessSkyscraper($value, $n1, $n2, $zone5, $zone7, $n4);
        $n4->disableValue($value);
        return true;
    }

    private function swordfish(int $value, Cell $n1, Cell $n2, Cell $n3, Cell $n4): bool
    {
        $zone = ($n3->row->id === $n4->row->id) ? $n3->row : $n3->column;

        // Maintenant je dois m'assurer que les éventuelles positions possibles pour la valeur soient du côté de $n3 et pas du côté de $n4
        // Si aucune des deux n'a de variations, alors c'est l'espadon X-WING
        // S'il y a des variations autour des deux, c'est fichu
        // S'il y a des variations autour de $n4 uniquement alors j'interchange les colonnes
        $variationsAroundN4 = $zone->intersect($n4->block)->whereIdNotIn([$n4->id])->filter(fn(Cell $cell) => $cell->id !== $n4->id && $cell->kumiKata[$value]);
        $variationsAroundN3 = $zone->intersect($n3->block)->whereIdNotIn([$n3->id])->filter(fn(Cell $cell) => $cell->id !== $n3->id && $cell->kumiKata[$value]);

        // S'il y a des variations autour des deux, c'est fichu pour un espadon standard, mais ça pourrait être bon pour un gratte-ciel éventuellement
        if (count($variationsAroundN3) && count($variationsAroundN4)) {
            $this->logger->logToMuchVariations($variationsAroundN3, $variationsAroundN4);
            return $this->skyscraper($value, $n1, $n2, $n3, $n4);
        }

        // S'il y a des variations autour de $n4 uniquement alors j'interchange les colonnes
        if (count($variationsAroundN4)) {
            SwitchVar::switchVars($n3, $n4);
            SwitchVar::switchVars($n1, $n2);
            $variationsAroundN3 = $zone->intersect($n3->block)->filter(fn(Cell $cell) => $cell->id !== $n3->id && $cell->kumiKata[$value]);
            $this->logger->logSwitchN3N4();
        }


        // Si aucune des deux n'a de variations, alors c'est l'espadon X-WING
        if (count($variationsAroundN3) === 0 && count($variationsAroundN4) === 0) {
            $this->logger->logFoundSwordfishXWing($value, $n1, $n2, $n3, $n4);
            return $this->xWing($value, $n1, $n2, $n3, $n4);
        }

        // Sinon c'est un peu plus complexe, on doit trouver $n5 et $n6
        list($n5, $n6) = $zone->intersect($n3->block)->whereIdNotIn([$n3->id]);
        if (!$n5->kumiKata[$value]) {
            SwitchVar::switchVars($n5, $n6);
        }

        // On cherche le type d'espadon en fonction des variations
        // Je vais maintenant distinguer les figures en fonction de N3,N5,N6
        if ($n3->kumiKata[$value]) {
            // ça sera du finned
            $this->logger->logFoundFinnedSwordfish($value, $n1, $n2, $n3, $n4, $n5, $n6);
            return $this->finned($value, $n1, $n2, $n3, $n4, $n5, $n6);
        }

        if ($n5->kumiKata[$value] && $n6->kumiKata[$value]) {
            // ça sera du sashimi double
            $this->logger->logFoundDoubleSashimiSwordfish($value, $n1, $n2, $n3, $n4, $n5, $n6);
            return $this->doubleSashimi($value, $n1, $n2, $n3, $n4, $n5, $n6);
        }

        $this->logger->logFoundSimpleSashimiSwordfish($value, $n1, $n2, $n3, $n4, $n5, $n6);
        return $this->simpleSashimi($value, $n1, $n2, $n3, $n5);
    }

    private function xWing(int $value, Cell $n1, Cell $n2, Cell $n3, Cell $n4): bool
    {
        $zone1    = ($n3->row->id === $n1->row->id) ? $n3->row : $n3->column;
        $n1n3Zone = $zone1->diff(new CellCollection(2, [$n1, $n3]));
        $zone2    = ($n4->row->id === $n2->row->id) ? $n4->row : $n4->column;
        $n2n4Zone = $zone2->diff(new CellCollection(2, [$n2, $n4]));

        $zone = $n1n3Zone->merge($n2n4Zone)->filter(fn(Cell $cell) => $cell->kumiKata[$value]);
        if (count($zone) === 0) {
            $this->logger->logNoCandidate();
            return false;
        }
        $this->logger->logSuccessSwordfishXWing($value, $n1, $n2, $n3, $n4, $zone1, $zone2, $zone);
        foreach ($zone as $cell) {
            $cell->disableValue($value);
        }
        return true;
    }

    private function finned(int $value, Cell $n1, Cell $n2, Cell $n3, Cell $n4, Cell $n5, Cell $n6): bool
    {
        $zone1         = ($n3->row->id === $n1->row->id) ? $n3->row : $n3->column;
        $zone2         = $n3->block;
        $n1n3BlockOfn3 = (($n3->row->id === $n1->row->id) ? $n3->row : $n3->column)
            ->whereIdNotIn([$n3->id])
            ->intersect($n3->block)
            ->filter(fn(Cell $cell) => $cell->kumiKata[$value]);
        if (count($n1n3BlockOfn3) === 0) {
            $this->logger->logNoCandidate();
            return false;
        }
        $this->logger->logSuccessFinnedSwordfish($value, $n1, $n2, $n3, $n4, $n5, $n6, $zone1, $zone2, $n1n3BlockOfn3);
        foreach ($n1n3BlockOfn3 as $cell) {
            $cell->disableValue($value);
        }
        return true;
    }

    private function doubleSashimi(int $value, Cell $n1, Cell $n2, Cell $n3, Cell $n4, Cell $n5, Cell $n6): bool
    {
        $zone1         = ($n3->row->id === $n1->row->id) ? $n3->row : $n3->column;
        $zone2         = $n3->block;
        $n1n3BlockOfn3 = (($n3->row->id === $n1->row->id) ? $n3->row : $n3->column)
            ->whereIdNotIn([$n3->id])
            ->intersect($n3->block)
            ->filter(fn(Cell $cell) => $cell->kumiKata[$value]);
        if (count($n1n3BlockOfn3) === 0) {
            $this->logger->logNoCandidate();
            return false;
        }
        $this->logger->logSuccessDoubleSashimiSwordfish($value, $n1, $n2, $n4, $n5, $n6, $zone1, $zone2, $n1n3BlockOfn3);
        foreach ($n1n3BlockOfn3 as $cell) {
            $cell->disableValue($value);
        }
        return true;
    }

    private function simpleSashimi(int $value, Cell $n1, Cell $n2, Cell $n3, Cell $n5): bool
    {
        $zone1         = ($n3->row->id === $n1->row->id) ? $n3->row : $n3->column;
        $n1n3BlockOfn3 = $zone1
            ->whereIdNotIn([$n3->id])
            ->intersect($n3->block);
        $zone5         = ($n3->row->id === $n1->row->id) ? $n5->row : $n5->column;
        $n5n1BlockOfn1 = $zone5
            ->whereIdNotIn([$n5->id])
            ->intersect($n1->block);
        $zone          = $n1n3BlockOfn3->merge($n5n1BlockOfn1);
        $zoneRM        = $zone->filter(fn(Cell $cell) => $cell->kumiKata[$value]);
        if (count($zoneRM) === 0) {
            $this->logger->logNoCandidate();
            return false;
        }
        foreach ($zoneRM as $cell) {
            $cell->disableValue($value);
        }
        $this->logger->logSuccessSimpleSashimiSwordfish($value, $n1, $n2, $n3, $n5, $zone1, $zone5, $n1->block, $n3->block, $zone);
        return true;
    }

    private function columnSwordfish(int $value, int $blockColumn1, int $blockColumn2): bool
    {
        for ($c1 = 0; $c1 < 3; $c1++) {
            for ($c2 = 0; $c2 < 3; $c2++) {
                $col1 = $this->grid->columns[$blockColumn1 * 3 + $c1];
                $col2 = $this->grid->columns[$blockColumn2 * 3 + $c2];
                if ($this->analyze($value, $col1, $col2)) {
                    return true;
                }
            }
        }
        return false;
    }


}