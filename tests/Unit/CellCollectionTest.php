<?php

namespace App\Tests\Unit;

use App\InvalidOperation;
use App\Model\Cell;
use App\Tests\TestCase;

class CellCollectionTest extends TestCase
{

    public function testArrayAccess()
    {
        $grid           = self::loadJson(__DIR__ . '/../data/empty-grid.json');
        $cellCollection = $grid->whereIdIn([1, 5, 12, 7]);
        self::assertCount(4, $cellCollection);
        self::assertEquals(4, $cellCollection->count());
        self::assertEquals(12, $cellCollection[2]->id);
        try {
            $cellCollection[2] = new Cell(13, $grid);
            self::fail('offset set should be forbidden');
        } catch (InvalidOperation $exception) {
            self::assertEquals('Invalid operation', $exception->getMessage());
        }
        try {
            unset($cellCollection[2]);
            self::fail('offset unset should be forbidden');
        } catch (InvalidOperation $exception) {
            self::assertEquals('Invalid operation', $exception->getMessage());
        }

        self::assertTrue(isset($cellCollection[2]));
        self::assertFalse(isset($cellCollection[5]));
    }
}