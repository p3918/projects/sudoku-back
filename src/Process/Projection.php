<?php

namespace App\Process;

use App\Loggers\LoggerContract;
use App\Loggers\ProjectionLogger;
use App\Model\Cell;
use App\Model\CellCollection;
use App\Model\Grid;
use JetBrains\PhpStorm\Pure;

class Projection extends ProcessorContract
{
    /** @var ProjectionLogger */
    protected LoggerContract $logger;

    #[Pure] public function __construct(Grid $grid, private int $currentValue = 0)
    {
        parent::__construct($grid, new ProjectionLogger());
    }

    public function execute(): bool
    {
        $start = $this->currentValue;
        for ($this->currentValue = $start; $this->currentValue < $start + 9; $this->currentValue++) {
            $this->reset();
            if ($this->doValue($this->currentValue % 9)) {
                return true;
            }
            $this->logger->logFailure($this->currentValue);
        }
        return false;
    }

    public function doValue(int $value): bool
    {
        if (count($this->grid->positions[$value]) === 9) {
            $this->logger->logYetNine($value);
            return false;
        }
        $this->phase1Colorize($value);
        return $this->phase2Deduce($value);
    }

    protected function phase1Colorize(int $value): void
    {
        foreach ($this->grid->blocks as $block) {
            if (count($block->possiblePositions($value)) === 1
                && $block->possibleCells($value)->first()->value() === $value
            ) {
                $this->logger->logFoundValue($value, $block, $block->possibleCells($value)->first());
                continue;
            }
            if (count($block->rowOf[$value]) === 1) {
                $rowInBlock = reset($block->rowOf[$value]);
                $rowId      = $block->rowInGrid($rowInBlock);
                $row        = $this->grid->rows[$rowId];
                $lockedYet  = $block->intersect($this->grid->rows[$rowId])
                                    ->exists(fn(Cell $cell) => $cell->lockedKata[$value]);
                if ($lockedYet) {
                    $this->logger->logFoundRowLock($value, $block, $rowInBlock, $row);
                }
                continue;
            }
            if (count($block->columnOf[$value]) === 1) {
                $columnInBlock = reset($block->columnOf[$value]);
                $columnId      = $block->columnInGrid($columnInBlock);
                $column        = $this->grid->columns[$columnId];
                $lockedYet     = $block->intersect($this->grid->columns[$columnId])
                                       ->exists(fn(Cell $cell) => $cell->lockedKata[$value]);
                if ($lockedYet) {
                    $this->logger->logFoundColumnLock($value, $block, $columnInBlock, $column);
                }
                continue;
            }
        }
    }

    protected function phase2Deduce(int $value): bool
    {
        $usefull = false;
        foreach ($this->grid->blocks as $block) {
            if (count($block->possiblePositions($value)) === 1
                && $block->possibleCells($value)->first()->value() === $value
            ) {
                continue;
            }
            $allowed = $block->possibleCells($value)->filter(fn(Cell $cell) => $cell->effect() !== Cell::EFFECT_FORBIDDEN);
            if (count($allowed) === 1) {
                $this->logger->logOnlyOne($value, $block, $allowed->first());
                $allowed->first()->setValue($value);
                $usefull = true;
                continue;
            }
            if (count($allowed) === 2 || count($allowed) === 3) {
                $rows    = array_keys(array_unique($allowed->map(fn(Cell $cell) => $cell->row->id)));
                $columns = array_keys(array_unique($allowed->map(fn(Cell $cell) => $cell->column->id)));
                if (count($rows) === 1) {
                    $row          = $allowed->first()->row;
                    $vCellsToLock = $row->intersect($block)
                                        ->freeCells()
                                        ->filter(fn(Cell $cell) => $cell->effect() !== Cell::EFFECT_FORBIDDEN);
                    $cellsToLock  = $vCellsToLock->filter(fn(Cell $cell) => !$cell->lockedKata[$value]);
                    if (count($cellsToLock) === count($vCellsToLock)) {
                        $this->logger->logOnlyOneRow($value, $block, $row, $cellsToLock);
                        $this->setLocks($value, $row, $cellsToLock);
                        $usefull = true;
                    }
                    continue;
                }
                if (count($columns) === 1) {
                    $column       = $allowed->first()->column;
                    $vCellsToLock = $column->intersect($block)
                                           ->freeCells()
                                           ->filter(fn(Cell $cell) => $cell->effect() !== Cell::EFFECT_FORBIDDEN);
                    $cellsToLock  = $vCellsToLock->filter(fn(Cell $cell) => !$cell->lockedKata[$value]);
                    if (count($cellsToLock) === count($vCellsToLock)) {
                        $this->logger->logOnlyOneColumn($value, $block, $column, $cellsToLock);
                        $this->setLocks($value, $column, $cellsToLock);
                        $usefull = true;
                    }
                    continue;
                }
            }
        }
        return $usefull;
    }

    protected function setLocks(int $value, CellCollection $zone, CellCollection $cellsToLock): void
    {
        foreach ($cellsToLock as $cell) {
            $cell->lockedKata[$value] = true;
        }

        $forbidden = $zone->diff($cellsToLock);
        foreach ($forbidden as $cell) {
            if (!$cell->kumiKata[$value]) {
                continue;
            }
            if ($this->grid->needKata) {
                $this->logger->logForbidden($value, $cell);
            }
            if ($cell->lockedKata[$value]) {
                $this->logger->logRemoveLock($value, $cell);
            }
            $cell->disableValue($value);
        }
    }

}
