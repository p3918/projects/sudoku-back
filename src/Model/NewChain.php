<?php

namespace App\Model;

use ArrayAccess;
use Countable;
use Iterator;
use JetBrains\PhpStorm\Pure;
use JsonSerializable;

class NewChain implements ArrayAccess, JsonSerializable, Iterator, Countable
{
    use CellIterator;

    /** @var Edge[] */
    protected array          $neighbours = [];
    protected CellCollection $collection;

    public function __construct(int $size = 0, array $cells = [])
    {
        $this->size       = $size;
        $this->neighbours = array_values($cells);
        if (count($this->neighbours) > $this->size) {
            $this->size = count($this->neighbours);
        }
        $this->collection = new CellCollection($this->size, array_map(fn(Edge $edge) => $edge->cell, $this->neighbours));
    }

    #[Pure] public function __toString(): string
    {
        //$view = [$this->neighbours[0]->cell];
        $view = [];
        for ($k = 0; $k < $this->size; $k++) {
            $view[] = '=' . $this->neighbours[$k]->edge() . '=>' . $this->neighbours[$k]->cell;
        }
        return '| ' . implode('', $view) . ' |';
    }

    #[Pure] public function offsetGet($offset): Edge
    {
        return $this->neighbours[$offset];
    }

    public function offsetSet($offset, $value): void
    {
        $this->neighbours[$offset] = $value;
    }

    public function current(): Edge
    {
        return $this[$this->current];
    }

    public function prepend(Edge $neighbour): NewChain
    {
        $this->size       += 1;
        $this->neighbours = array_values(array_merge([$neighbour], $this->neighbours));
        $this->collection->prepend($neighbour->cell);
        return $this;
    }

    public function append(Edge $neighbour): NewChain
    {
        $this->size         += 1;
        $this->neighbours[] = $neighbour;
        $this->collection->append($neighbour->cell);
        return $this;
    }

    public function jsonSerialize(): array
    {
        return $this->neighbours;
    }

    public function first(): Edge
    {
        return $this->neighbours[0];
    }

    public function second(): Edge
    {
        return $this->neighbours[1];
    }

    public function last(): Edge
    {
        return $this->neighbours[$this->size - 1];
    }

    public function beforeLast(): Edge
    {
        return $this->neighbours[$this->size - 2];
    }

    public function diff(NewChain $otherCollection): NewChain
    {
        return $this->whereIdIn(array_diff($this->collect()->cellIds(), $otherCollection->collect()->cellIds()));
    }

    public function whereIdIn(array $cellIds): NewChain
    {
        return $this->filter(fn(Edge $cell) => in_array($cell->cell->id(), $cellIds));
    }

    public function filter(callable $closure): NewChain
    {
        return new static(0, array_values(array_filter($this->neighbours, $closure, ARRAY_FILTER_USE_BOTH)));
    }

    public function collect(): CellCollection
    {
        return $this->collection;
    }

    public function clone(): NewChain
    {
        return new static($this->size, $this->neighbours);
    }

    public function slice(int $start, int $end): NewChain
    {
        $size = $end - $start + 1;
        return new static($size, array_values(array_slice($this->neighbours, $start, $size)));
    }

    public function exists(int $id): bool
    {
        foreach ($this->neighbours as $neighbour) {
            if ($neighbour->cell->id === $id) {
                return true;
            }
        }
        return false;
    }
}