<?php

namespace App\Model;

use App\InvalidOperation;
use JetBrains\PhpStorm\Pure;

class Zone extends CellCollection
{
    public array     $cellId            = [];
    public array     $indexOf           = [];
    protected ?array $freeCells         = [0, 1, 2, 3, 4, 5, 6, 7, 8];
    protected ?array $takenCells        = [];
    protected ?array $freeValues        = null;
    protected ?array $takenValues       = null; //[id dans la zone => id de la case]
    protected ?array $possiblePositions = [
        [0, 1, 2, 3, 4, 5, 6, 7, 8], // Le 0 peut être aux positions 0,1,2,3,4,5,6,7,8
        [0, 1, 2, 3, 4, 5, 6, 7, 8], // Le 1 peut être aux positions 0,1,2,3,4,5,6,7,8
        [0, 1, 2, 3, 4, 5, 6, 7, 8], // Le 2 ...
        [0, 1, 2, 3, 4, 5, 6, 7, 8], // Le 3 ...
        [0, 1, 2, 3, 4, 5, 6, 7, 8], // etc...
        [0, 1, 2, 3, 4, 5, 6, 7, 8],
        [0, 1, 2, 3, 4, 5, 6, 7, 8],
        [0, 1, 2, 3, 4, 5, 6, 7, 8],
        [0, 1, 2, 3, 4, 5, 6, 7, 8],
    ]; //[id de la case => id dans la zone]

    /**
     * @param Cell[] $cells
     * @param Grid   $grid
     * @param int    $id
     *
     * @noinspection PhpPureFunctionMayProduceSideEffectsInspection
     */
    #[Pure] public function __construct(array $cells, public Grid $grid, public int $id)
    {
        parent::__construct(9, $cells);
        $this->cellId      = array_map(fn(Cell $cell) => $cell->id, $this->cells);
        $this->indexOf     = array_flip($this->cellId);
        $this->freeValues  = [0, 1, 2, 3, 4, 5, 6, 7, 8];
        $this->takenValues = [];
    }

    #[Pure] public function whereIdIn(array $cellIds): CellCollection
    {
        $output = [];
        foreach ($cellIds as $cellId) {
            $output[] = $this->cells[$this->indexOf[$cellId]];
        }
        return new CellCollection(count($output), $output);
    }

    #[Pure] public function freeCells(): CellCollection
    {
        return $this->wherePosIn($this->freeCells);
    }

    #[Pure] public function takenCells(): CellCollection
    {
        return $this->wherePosIn($this->takenCells);
    }

    public function takenValues(): array
    {
        return $this->takenValues;
    }

    public function freeValues(): array
    {
        return $this->freeValues;
    }

    public function unsetPossiblePositions(int $value, array $positions): void
    {
        foreach ($positions as $p) {
            $this[$p]->kumiKata[$value]   = false;
            $this[$p]->lockedKata[$value] = false;
            $this[$p]->resetNeighbours();
            $this[$p]->viewedCells()->walk(fn(Cell $cell) => $cell->resetNeighbours());
        }
        $this->possiblePositions[$value] = array_values(array_diff($this->possiblePositions[$value], $positions));
    }

    public function setPosition(int $value, int $position): void
    {
        $this->possiblePositions[$value] = [$position];
        $this->freeCells                 = array_values(array_diff($this->freeCells, [$position]));
        $this->takenCells[]              = $position;
        $this->freeValues                = array_values(array_diff($this->freeValues, [$value]));
        $this->takenValues[]             = $value;
    }

    public function disableValue(int $value, int $cellId): void
    {
        $this->possiblePositions[$value] = array_values(
            array_diff($this->possiblePositions[$value], [$this->indexOf[$cellId]])
        );
    }

    public function offsetSet($offset, $value): void
    {
        throw new InvalidOperation();
    }

    public function offsetUnset($offset): void
    {
        throw new InvalidOperation();
    }

    public function hasValue(int $value): bool
    {
        if (count($this->possiblePositions($value)) !== 1) {
            return false;
        }
        return $this->possibleCells($value)->first()->value() === $value;
    }

    #[Pure] public function possiblePositions(int $value): array
    {
        return $this->possiblePositions[$value];
    }

    #[Pure] public function possibleCells(int $value): CellCollection
    {
        return $this->wherePosIn($this->possiblePositions[$value]);
    }
}