<?php

namespace App\Process;

use App\Loggers\HiddenSetLogger;
use App\Loggers\LoggerContract;
use App\Model\Cell;
use App\Model\Column;
use App\Model\Grid;
use App\Model\Row;
use App\Model\Zone;
use JetBrains\PhpStorm\Pure;

class HiddenSet extends ProcessorContract
{
    /** @var HiddenSetLogger */
    protected LoggerContract $logger;

    #[Pure] public function __construct(Grid $grid, private int $size)
    {
        parent::__construct($grid, new HiddenSetLogger());
    }

    public function execute(): bool
    {
        for ($i = 0; $i < 27; $i++) {
            $this->reset();
            if ($this->analyzeZone($this->grid->zones[$i], $this->size)) {
                return true;
            }
        }
        return false;
    }


    protected function analyzeZone(Zone $zone, int $size): bool
    {
        // Si l'ensemble de valeurs à trouver est trop grand, ou si on a déjà tout trouvé dans cette zone, on passe directement à la zone suivante
        if (count($zone->freeValues()) < 2) {
            return false;
        }
        // On colorie la zone de la grille qu'on est en train d'analyser
        $zone->freeCells()->addEffect(Cell::EFFECT_FORBIDDEN);

        // On range les valeurs dans un tableau, en fonction de l'ensemble de leurs positions possibles.
        // (uniquement si le nombre de positions possibles est cohérent par rapport à ce qu'on cherche)
        // Exemple : Si je cherche une paire cachée
        //  - la valeur 1 peut être aux positions 3 et 4,
        //  - la valeur 2 peut être aux positions 3 et 4,
        //  - tandis que la valeur 3 peut être aux positions 3 et 6
        //  - et la valeur 4 peut être aux positions 1,3 et 7 (3 positions, si je cherche une hidden pair ça ne m'intéressera pas)
        // Alors on aura :
        // "3.4" => [1,2], "3.6" => [3]
        // ==> on aura bien une paire cachée (valeurs 1 et 2 aux positions 3 et 4) car il y a une clé qui a 2 éléments
        $valuesByPosition = [];
        foreach ($zone->freeValues() as $value) {
            $positions = $zone->possiblePositions($value);
            if (count($positions) === $size) {
                $key = implode('.', $positions);
                if (!isset($valuesByPosition[$key])) {
                    $valuesByPosition[$key] = [];
                }
                $valuesByPosition[$key][] = $value;
            }
            // Le problème avec les triplets, c'est qu'ils peuvent être répartis en paires
            // Exemple : [0,1];[0,5];[1,5] forment un triplet 0.1.5
            // @TODO -- ma solution n'est vraiment pas merveilleuse, trouver mieux
            if (count($positions) === 2 && $size === 3) {
                foreach (array_diff([0, 1, 2, 3, 4, 5, 6, 7, 8], $positions) as $thirdPosition) {
                    $triple = array_merge($positions, [$thirdPosition]);
                    sort($triple);
                    $key = implode('.', $triple);
                    if (!isset($valuesByPosition[$key])) {
                        $valuesByPosition[$key] = [];
                    }
                    $valuesByPosition[$key][] = $value;
                }
            }
        }
        /** @noinspection DuplicatedCode */
        $possibleHiddenSets = array_filter($valuesByPosition, fn(array $values) => count($values) === $size);
        if (empty($possibleHiddenSets)) {
            return false;
        }

        // Si on a trouvé au moins une paire cachée / un triplet caché, on va les étudier
        foreach ($possibleHiddenSets as $positions => $values) {
            $positions = explode('.', $positions);
            if ($this->analyzeCandidate($zone, $values, $positions, $size)) {
                return true;
            }
        }
        return false;
    }

    private function analyzeCandidate(Zone $zone, array $values, array $positions, int $size): bool
    {
        // Une fois la paire cachée (resp. le triplet) trouvée,
        // on sait qu'on ne peut pas avoir d'autres valeurs que les 2 (resp. 3) valeurs de la paire dans les 2 (resp. 3) cases concernées
        // Exemple :
        //  - les valeurs 1 et 2 sont aux positions 3 et 4,
        //  => donc il ne peut pas y avoir un 6 en position 3 (sinon on finirait par avoir 1 ET 2 en position 4)
        $forbiddenValues    = array_diff([0, 1, 2, 3, 4, 5, 6, 7, 8], $values);
        $atLeastOneDisabled = false;
        foreach ($positions as $position) {
            $cell = $zone[$position];
            $cell->setEffect(Cell::EFFECT_FOUND);
            // Attention, il faut que la paire soit vraiement cachée !
            // (cad qu'il faut qu'il y ait d'autres valeurs possibles à enlever dans le kumiKata des cases concernées)
            if (count($cell->allowedValues()) > $size) {
                $atLeastOneDisabled = true;
                foreach ($forbiddenValues as $forbiddenValue) {
                    $cell->disableValue($forbiddenValue);
                }
            }
        }

        $this->logger->logSet($values, $positions, $zone);

        // Si la paire ou le triplet se trouve aussi à l'intersection avec une autre zone, alors il faut l'analyser aussi
        // Je ne fais que le cas Row/Column car on fait ces zones avant les blocks normalement
        /** @noinspection DuplicatedCode */
        if ($zone instanceof Row || $zone instanceof Column) {
            $intersectWithBlocks = array_unique($zone->wherePosIn($positions)->map(fn(Cell $cell) => $cell->block->id));
            if (count($intersectWithBlocks) === 1) {
                $intersectResult    = $this->analyzeZone($this->grid->blocks[reset($intersectWithBlocks)], $size);
                $atLeastOneDisabled = $atLeastOneDisabled || $intersectResult;
            }
        }

        return $atLeastOneDisabled;
    }

}
