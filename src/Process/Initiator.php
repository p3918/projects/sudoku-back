<?php

namespace App\Process;

use App\Model\Grid;

class Initiator
{

    public static function initGrid(array $inputGrid): Grid
    {
        $grid = new Grid();
        foreach ($grid as $cellIndex => $cell) {
            $cell->fromArray($inputGrid[$cellIndex]);
        }
        foreach ($grid as $cellIndex => $cell) {
            if (!is_null($inputGrid[$cellIndex]['value'])) {
                $cell->setValue($inputGrid[$cellIndex]['value']);
            }
        }
        $grid->setInitialized();
        foreach ($grid as $cellIndex => $cell) {
            for ($value = 0; $value < 9; $value++) {
                if ($cell->kumiKata[$value] && !$inputGrid[$cellIndex]['kumiKata'][$value]) {
                    // Si on m'envoie un KumiKata différent de celui que j'aurais trouvé, c'est qu'il faut en tenir compte,
                    // Ou alors j'ai passé l'info d'une autre manière à cause du fait que les projections écrivent dans le kumiKata ?
                    $cell->disableValue($value);
                }
            }
        }
        return $grid;
    }
}