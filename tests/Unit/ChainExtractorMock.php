<?php

namespace App\Tests\Unit;

use App\Model\Cell;
use App\Model\Grid;
use App\Model\NewChain;
use App\Model\SelfEdge;
use App\Process\NewChainerContract;

class ChainExtractorMock extends NewChainerContract
{

    public static $count           = 0;
    public static $countNeighbours = 0;
    public static $countChains     = 0;

    public function __construct(Grid $grid, protected int $chainMaxSize = 2, protected ?int $value = null, protected bool $takeSelfEdges = false)
    {
        parent::__construct($grid, new MockLogger());
    }

    protected function cellIsValidStart(Cell $cell): bool
    {
        return true;
    }

    protected function neighbours(NewChain $currentChain): array
    {
        if ($currentChain->count() < $this->chainMaxSize) {
            self::$count++;
            $neighbours = $currentChain->last()->cell->neighbours($this->value);
            if ($this->takeSelfEdges && !$currentChain->last() instanceof SelfEdge) {
                $neighbours = array_merge($neighbours, $currentChain->last()->cell->selfNeighbours($this->value));
            }
            self::$countNeighbours += count($neighbours);
            return $neighbours;
        }
        return [];
    }

    protected function isValid(NewChain $chain): bool
    {
        return true;
    }

    protected function tryToDo(NewChain $chain): bool
    {
        self::$countChains++;
        return false;
    }
}