<?php

namespace App\Loggers;


use App\Model\Cell;
use App\Model\CellCollection;
use App\Model\NewChain;
use App\Serialization\Formatter;

class TigreRegularAlternateLoopLogger extends ChainerLogger
{
    protected int    $level         = self::LEVEL_HARD;
    protected string $phase         = 'Tigre - boucle alternée régulière';
    protected string $canonicalName = 'tigre_regular_alternate_loop';

    /**
     * @param NewChain         $chain
     * @param CellCollection[] $viewedCells
     * @param CellCollection   $allViewedCells
     * @param int              $value
     */
    public function logTigreChain(NewChain $chain, array $viewedCells, CellCollection $allViewedCells, int $value): void
    {
        foreach ($chain as $k => $neighbour) {
            $neighbour->cell->setEffect($k % 2 === 0 ? Cell::EFFECT_ELEM_1 : Cell::EFFECT_ELEM_2);
            $neighbour->cell->setAnnotation(Formatter::alpha($k));
        }
        $allViewedCells->addEffect(Cell::EFFECT_FORBIDDEN);
        foreach ($chain as $k => $cell) {
            $cell->cell->setAnnotation(Formatter::alpha($k));
        }
        $this->message[] = 'Boucle de liens régulièrement alternés forts/faibles sur la valeur ' . Formatter::v($value) . ' : ' . $chain . '';
        $this->message[] = ', on supprime le candidat ' . Formatter::v($value)
                           . ' dans les cases vues par au moins 2 cases consécutives de la chaine : ' . $allViewedCells;


        //A0 =f=> A5 =F=> A4 =f=> A3 =F=> A2 =f=> A1 =F=> A0
        // a =f=> !a =F=>  a =f=> !a =F=>  a =f=> !a =F=> a
        // k in {1...size/2}
        //A1A2->X1 ; A3A4->X2 ; A5A0->X3 ;
        //A(2k-1)A(2k)->Xk ; k in {1...size/2}
        $this->proof[] = 'CAS 1: ' . $chain[0]->cell . ' vaut ' . Formatter::v($value);
        for ($k = count($chain); $k > 0; $k--) {
            $edge         = $chain[$k % count($chain)];
            $previousEdge = $chain[$k - 1];
            if ($edge->strong) {
                $this->proof[] = '- ' . $edge->cell . ' est différent de ' . Formatter::v($edge->x)
                                 . ' donc ' . $previousEdge->cell . ' vaut ' . Formatter::v($edge->x);
                continue;
            }
            $this->proof[] = '- ' . $edge->cell . ' vaut ' . Formatter::v($edge->x)
                             . ' donc ' . $previousEdge->cell . ' est différent de ' . Formatter::v($edge->x);
        }

        $this->proof[] = 'CAS 2: ' . $chain[0]->cell . ' est différent de ' . Formatter::v($value);
        for ($k = 0; $k < count($chain); $k++) {
            $edge     = $chain[$k];
            $nextEdge = $chain[($k + 1) % count($chain)];
            if ($nextEdge->strong) {
                $this->proof[] = '- ' . $edge->cell . ' est différent de ' . Formatter::v($edge->x)
                                 . ' donc ' . $nextEdge->cell . ' vaut ' . Formatter::v($edge->x);
                continue;
            }
            $this->proof[] = '- ' . $edge->cell . ' vaut ' . Formatter::v($edge->x)
                             . ' donc ' . $nextEdge->cell . ' est différent de ' . Formatter::v($edge->x);
        }

        $this->proof[] = 'DANS LES DEUX CAS: ';
        for ($k = 0; $k < count($chain); $k++) {
            $edge     = $chain[$k];
            $nextEdge = $chain[($k + 1) % count($chain)];
            if (!$nextEdge->strong && isset($viewedCells[$k])) {
                $this->proof[] = '- soit ' . $edge->cell . ' vaut ' . Formatter::v($value)
                                 . ', soit ' . $nextEdge . ' vaut ' . Formatter::v($value);
                $this->proof[] = 'donc, ' . $viewedCells[$k]
                                 . ((count($viewedCells[$k]) > 1) ? ' sont différents de ' : ' est différent de ')
                                 . Formatter::v($value);
            }
        }

        $this->proof[] = count($allViewedCells) > 1 ?
            'Finalement, dans les 2 cas on a conclu que les cases ' . $allViewedCells . ' sont différents de ' . Formatter::v($value)
            : 'Finalement, dans les 2 cas on a conclu que ' . $allViewedCells . ' est différent de ' . Formatter::v($value);
    }

}
