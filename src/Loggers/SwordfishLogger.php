<?php

namespace App\Loggers;

use App\Model\Block;
use App\Model\Cell;
use App\Model\CellCollection;
use App\Model\Column;
use App\Model\Row;
use App\Serialization\Formatter;

class SwordfishLogger extends LoggerContract
{
    protected int $level = self::LEVEL_SMART;

    protected string $phase         = 'Espadon';
    protected string $canonicalName = 'swordfish';

    public function logFoundPotentialSwordfish(int $value, Cell $n1, Cell $n2, Cell $n3, Cell $n4): void
    {
        $this->algoSteps[] = 'Configuration de type espadon trouvée pour la valeur ' . $value
                             . ' aux positions ' . implode(', ', [$n1, $n2, $n3, $n4]);
    }

    public function logToMuchVariations(CellCollection $variationsAroundN3, CellCollection $variationsAroundN4): void
    {
        $this->algoSteps[] = 'Il y a trop de cases autour de N3 et N4 qui contiennent le candidat';
        $this->algoSteps[] = 'Dans le bloc de N3 : ' . $variationsAroundN3;
        $this->algoSteps[] = 'Dans le bloc de N4 : ' . $variationsAroundN4;
    }

    public function logSwitchN3N4(): void
    {
        $this->algoSteps[] = 'Il y a des cases autour de N4 qui contiennent le candidat, on interchange N3 et N4';
    }

    public function logFoundSkyscraper(int   $value, Cell $n1, Cell $n2, Cell $n3, Cell $n4,
                                       ?Cell $n5 = null, ?Cell $n6 = null, ?Cell $n7 = null, ?Cell $n8 = null): void
    {
        $direction = ($n1->row->id === $n2->row->id) ? 'en ligne' : 'en colonne';
        $this->logFoundSwordfish($n1, $n2, $n3, $n4, $n5, $n6, $n7, $n8);
        $this->proof[] = 'Gratte-Ciel ' . $direction . ' pour la valeur ' . Formatter::v($value)
                         . ' en ' . implode(',', [$n1, $n2, $n3, $n4]);
    }

    private function logFoundSwordfish(
        Cell  $n1, Cell $n2, Cell $n3, Cell $n4,
        ?Cell $n5 = null, ?Cell $n6 = null, ?Cell $n7 = null, ?Cell $n8 = null
    ): void {
        $n1->setEffect(Cell::EFFECT_FOUND, 'N1');
        $n2->setEffect(Cell::EFFECT_ELEM_1, 'N2');
        $n3->setEffect(Cell::EFFECT_ELEM_2, 'N3');
        $n4->setEffect(Cell::EFFECT_ELEM_3, 'N4');
        if ($n5 && $n6) {
            $n5->setEffect(Cell::EFFECT_ELEM_4, 'N5');
            $n6->setEffect(Cell::EFFECT_ELEM_4, 'N6');
        }
        if ($n7 && $n8) {
            $n7->setEffect(Cell::EFFECT_ELEM_4, 'N7');
            $n8->setEffect(Cell::EFFECT_ELEM_4, 'N8');
        }
    }

    public function logSuccessSkyscraper(int $value, Cell $n1, Cell $n2, CellCollection $zone5, CellCollection $zone7, Cell $n4): void
    {
        $direction       = ($n1->row->id === $n2->row->id) ? 'en ligne' : 'en colonne';
        $this->message[] = 'Gratte-Ciel ' . $direction . '. On retire le candidat ' . Formatter::v($value) . ' de ' . $n4;
        $this->proof[]   = 'Si le candidat ' . Formatter::v($value) . ' est en ' . $n1 . ', alors';
        $this->proof[]   = '- Il n\'est pas dans ' . $zone7;
        $this->proof[]   = '- Donc il est forcément dans ' . $zone5;
        $this->proof[]   = '- Donc il n\'est pas en ' . $n4;
        $this->proof[]   = 'Sinon, le candidat ' . Formatter::v($value) . ' est en ' . $n2;
        $this->proof[]   = '- Donc il n\'est pas en ' . $n4;
        $this->proof[]   = 'Dans les 2 cas, on peut retirer le candidat de ' . $n4;
    }

    public function logFoundSwordfishXWing(int $value, Cell $n1, Cell $n2, Cell $n3, Cell $n4): void
    {
        $direction = ($n1->row->id === $n2->row->id) ? 'en ligne' : 'en colonne';
        $this->logFoundSwordfish($n1, $n2, $n3, $n4);
        $this->algoSteps[] = 'Espadon en croix, ' . $direction . ' pour la valeur ' . Formatter::v($value)
                             . ' ' . implode(',', [$n1, $n2, $n3, $n4]);
    }

    public function logSuccessSwordfishXWing(int        $value, Cell $n1, Cell $n2, Cell $n3, Cell $n4,
                                             Row|Column $zone1, Row|Column $zone2, CellCollection $zone): void
    {
        $zone->walk(fn(Cell $cell) => $cell->setEffect(Cell::EFFECT_FORBIDDEN));
        $direction       = ($n1->row->id === $n2->row->id) ? 'en ligne' : 'en colonne';
        $this->proof[]   = 'Si le candidat ' . Formatter::v($value) . ' est en ' . $n1
                           . ', alors d\'une part on peut l\'éliminer de ' . $zone1
                           . ', et d\'autre part il ne peut pas être en position ' . $n3
                           . ', donc il est forcément en position ' . $n4 .
                           ', donc on peut l\'éliminer dans toutes les autres cases de ' . $zone2;
        $this->proof[]   = 'Sinon, le candidat ' . Formatter::v($value) . ' sera forcément en ' . $n2
                           . ', donc d\'une part on peut l\'éliminer de ' . $zone2
                           . ', et d\'autre part il ne peut pas être en position ' . $n4
                           . ', donc il est forcément en position ' . $n3
                           . ', donc on peut l\'éliminer dans toutes les autres cases de ' . $zone1;
        $this->proof[]   = 'En conclusion, dans les deux cas on peut éliminer le candidat de ' . $zone1 . ' et ' . $zone2;
        $this->message[] = 'Sashimi en X, ' . $direction . '. On retire le candidat ' . Formatter::v($value) . ' de ' . $zone;
    }

    public function logFoundFinnedSwordfish(int $value, Cell $n1, Cell $n2, Cell $n3, Cell $n4, Cell $n5, Cell $n6): void
    {
        $direction = ($n1->row->id === $n2->row->id) ? 'en ligne' : 'en colonne';
        $this->logFoundSwordfish($n1, $n2, $n3, $n4, $n5, $n6);
        $this->algoSteps[] = 'Espadon avec nageoire, ' . $direction . ' pour la valeur ' . Formatter::v($value)
                             . ' ' . implode(',', [$n1, $n2, $n3, $n4, $n5, $n6]);
    }

    public function logSuccessFinnedSwordfish(int        $value, Cell $n1, Cell $n2, Cell $n3, Cell $n4, Cell $n5, Cell $n6,
                                              Row|Column $zone1, Block $zone2, CellCollection $zone): void
    {

        $zone->walk(fn(Cell $cell) => $cell->setEffect(Cell::EFFECT_FORBIDDEN));
        $direction       = ($n1->row->id === $n2->row->id) ? 'en ligne' : 'en colonne';
        $this->proof[]   = 'Si le candidat ' . Formatter::v($value) . ' est en ' . $n1
                           . ', alors on peut l\'éliminer de ' . $zone1;
        $this->proof[]   = 'Sinon, le candidat ' . Formatter::v($value) . ' sera forcément en ' . $n2
                           . ', donc il ne peut pas être en position ' . $n4
                           . ', donc il est forcément en position ' . $n3 . ' ou ' . $n5 . ' ou ' . $n6
                           . ', donc on peut l\'éliminer dans toutes les autres cases de ' . $zone2;
        $this->proof[]   = 'En conclusion, dans les deux cas on peut éliminer le candidat dans l\'intersection de '
                           . $zone1 . ' et ' . $zone2;
        $this->message[] = 'Espadon avec nageoire, ' . $direction . '. On retire le candidat ' . Formatter::v($value) . ' de ' . $zone;
    }

    public function logFoundDoubleSashimiSwordfish(int $value, Cell $n1, Cell $n2, Cell $n3, Cell $n4, Cell $n5, Cell $n6): void
    {
        $direction = ($n1->row->id === $n2->row->id) ? 'en ligne' : 'en colonne';
        $this->logFoundSwordfish($n1, $n2, $n3, $n4, $n5, $n6);
        $this->algoSteps[] = 'Espadon avec sashimi double, ' . $direction . ' pour la valeur ' . Formatter::v($value)
                             . ' ' . implode(',', [$n1, $n2, $n3, $n4, $n5, $n6]);
    }

    public function logSuccessDoubleSashimiSwordfish(int        $value, Cell $n1, Cell $n2, Cell $n4, Cell $n5, Cell $n6,
                                                     Row|Column $zone1, Block $zone2, CellCollection $zone): void
    {
        $zone->walk(fn(Cell $cell) => $cell->setEffect(Cell::EFFECT_FORBIDDEN));
        $direction       = ($n1->row->id === $n2->row->id) ? 'en ligne' : 'en colonne';
        $this->proof[]   = 'Si le candidat ' . Formatter::v($value) . ' est en ' . $n1
                           . ', alors on peut l\'éliminer de ' . $zone1;
        $this->proof[]   = 'Sinon, le candidat ' . Formatter::v($value) . ' sera forcément en ' . $n2
                           . ', donc il ne peut pas être en position ' . $n4
                           . ', donc il est forcément en position ' . $n5 . ' ou ' . $n6
                           . ', donc on peut l\'éliminer dans toutes les autres cases de ' . $zone2;
        $this->proof[]   = 'En conclusion, dans les deux cas on peut éliminer le candidat dans l\'intersection de '
                           . $zone1 . ' et ' . $zone2;
        $this->message[] = 'Espadon avec sashimi double, ' . $direction . '. On retire le candidat ' . Formatter::v($value) . ' de ' . $zone;
    }

    public function logFoundSimpleSashimiSwordfish(int $value, Cell $n1, Cell $n2, Cell $n3, Cell $n4, Cell $n5, Cell $n6): void
    {
        $direction = ($n1->row->id === $n2->row->id) ? 'en ligne' : 'en colonne';
        $this->logFoundSwordfish($n1, $n2, $n3, $n4, $n5, $n6);
        $this->algoSteps[] = 'Espadon avec sashimi simple, ' . $direction . ' pour la valeur ' . Formatter::v($value)
                             . ' ' . implode(',', [$n1, $n2, $n3, $n4, $n5, $n6]);
    }

    public function logSuccessSimpleSashimiSwordfish(int            $value, Cell $n1, Cell $n2, Cell $n4, Cell $n5,
                                                     Row|Column     $zone1, Row|Column $zone5, Block $block1, Block $block5,
                                                     CellCollection $zone): void
    {
        $zone->walk(fn(Cell $cell) => $cell->setEffect(Cell::EFFECT_FORBIDDEN));
        $direction       = ($n1->row->id === $n2->row->id) ? 'en ligne' : 'en colonne';
        $this->proof[]   = 'Si le candidat ' . Formatter::v($value) . ' est en ' . $n1
                           . ', alors on peut l\'éliminer de ' . $zone1 . ' et également de ' . $block1;
        $this->proof[]   = 'Sinon, le candidat ' . Formatter::v($value) . ' sera forcément en ' . $n2
                           . ', donc il ne peut pas être en position ' . $n4
                           . ', donc il est forcément en position ' . $n5
                           . ', donc on peut l\'éliminer dans toutes les autres cases de ' . $zone5 . ' et également de ' . $block5;
        $this->proof[]   = 'En conclusion, dans les deux cas on peut éliminer le candidat dans l\'intersection de '
                           . $zone1 . ' et ' . $block5 . ', et également dans l\'intersection de ' . $zone5 . ' et ' . $block1;
        $this->message[] = 'Espadon avec sashimi simple, ' . $direction . '. On retire le candidat ' . Formatter::v($value) . ' de ' . $zone;
    }

    public function logNoCandidate(): void
    {
        $this->algoSteps[] = 'La configuration était correcte mais aucune case vue ne contient le candidat, on abandonne';
    }
}
