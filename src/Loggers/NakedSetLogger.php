<?php

namespace App\Loggers;

use App\Model\CellCollection;
use App\Model\Zone;
use App\Serialization\Formatter;

class NakedSetLogger extends LoggerContract
{

    protected int    $level         = self::LEVEL_ADVANCED;
    protected string $phase         = '';
    protected string $canonicalName = 'naked_sets';

    public function logSet(array $values, CellCollection $foundPositions, CellCollection $forbiddenPositions, Zone $zone): void
    {
        $this->phase = (count($values) === 2 ? 'Paire isolée' : 'Triplet isolé');

        $this->proof[]   = 'Dans la zone ' . $zone . ', aux positions ' . $foundPositions
                           . ', il ne peut y avoir que les valeurs ' . Formatter::v($values);
        $this->proof[]   = 'Par conséquent, ces valeurs ne peuvent pas se trouver dans les autres cases de la zone.'
                           . ' On retire les candidats de toutes les autres cases';
        $this->message[] = 'On retire tous les candidats ' . Formatter::v($values) .
                           ' aux positions ' . $forbiddenPositions
                           . ' dans la zone ' . $zone;
    }
}
