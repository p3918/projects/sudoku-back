<?php

namespace App\Model;

use ArrayAccess;
use Countable;
use Iterator;
use JetBrains\PhpStorm\Pure;
use JsonSerializable;

class Chain implements ArrayAccess, JsonSerializable, Iterator, Countable
{
    use CellIterator;

    /** @var Neighbour[] */
    protected array $neighbours = [];

    #[Pure] public function __construct(int $size = 0, array $cells = [])
    {
        $this->size       = $size;
        $this->neighbours = array_values($cells);
        if (count($this->neighbours) > $this->size) {
            $this->size = count($this->neighbours);
        }
    }

    public function __toString(): string
    {
        $view = [$this->neighbours[0]->cell];
        for ($k = 1; $k < $this->size; $k++) {
            $view[] = '=' . $this->neighbours[$k]->formatEdge() . '=>' . $this->neighbours[$k]->cell;
        }
        return '| ' . implode('', $view) . ' |';
    }

    #[Pure] public function offsetGet($offset): Neighbour
    {
        return $this->neighbours[$offset];
    }

    public function current(): Neighbour
    {
        return $this[$this->current];
    }

    public function append(Neighbour $neighbour): Chain
    {
        $this->size         += 1;
        $this->neighbours[] = $neighbour;
        return $this;
    }

    public function jsonSerialize(): array
    {
        return $this->neighbours;
    }

    public function first(): Neighbour
    {
        return $this->neighbours[0];
    }

    public function second(): Neighbour
    {
        return $this->neighbours[1];
    }

    public function last(): Neighbour
    {
        return $this->neighbours[$this->size - 1];
    }

    public function beforeLast(): Neighbour
    {
        return $this->neighbours[$this->size - 2];
    }

    public function diff(Chain $otherCollection): Chain
    {
        return $this->whereIdIn(array_diff($this->collect()->cellIds(), $otherCollection->collect()->cellIds()));
    }

    public function whereIdIn(array $cellIds): Chain
    {
        return $this->filter(fn(Neighbour $cell) => in_array($cell->cell->id(), $cellIds));
    }

    public function filter(callable $closure): Chain
    {
        return new static(0, array_values(array_filter($this->neighbours, $closure, ARRAY_FILTER_USE_BOTH)));
    }

    public function collect(): CellCollection
    {
        return new CellCollection(
            $this->size,
            array_map(fn(Neighbour $neighbour) => $neighbour->cell, $this->neighbours)
        );
    }

    #[Pure] public function clone(): Chain
    {
        return new static($this->size, $this->neighbours);
    }

    #[Pure] public function slice(int $start, int $end): Chain
    {
        $size = $end - $start + 1;
        return new static($size, array_values(array_slice($this->neighbours, $start, $size)));
    }
}