<?php

namespace App\Process;

use App\Loggers\LoggerContract;
use App\Loggers\NakedSingleLogger;
use App\Model\Grid;
use JetBrains\PhpStorm\Pure;

class NakedSingle extends ProcessorContract
{
    /** @var NakedSingleLogger */
    protected LoggerContract $logger;

    #[Pure] public function __construct(Grid $grid)
    {
        parent::__construct($grid, new NakedSingleLogger());
    }

    public function execute(): bool
    {
        if (!$this->grid->needKata) {
            return false;
        }
        $this->logger->reset();
        $continue   = true;
        $atLeastOne = false;
        while ($continue) {
            $continue = false;
            foreach ($this->grid->freeCells() as $cell) {
                if (count($cell->allowedValues()) === 1) {
                    $value = $cell->allowedValues()[0];

                    $this->logger->logNakedSingle($value, $cell);
                    $cell->setValue($value);
                    $atLeastOne = true;
                    $continue   = true;
                }
            }
        }
        return $atLeastOne;
    }
}
