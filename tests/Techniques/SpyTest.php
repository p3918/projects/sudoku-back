<?php

namespace App\Tests\Techniques;

use App\Process\SpyRectangle;
use App\Tests\TestCase;

class SpyTest extends TestCase
{

    public function testRectangle1()
    {
        $grid = self::loadJson(__DIR__ . '/../data/expert-60-advanced.json');
        $step = (new SpyRectangle($grid))->execute();
        $this->debugGrid($grid);
        self::assertTrue($step);
        self::assertGrid(__DIR__ . '/../data/expert-60-rectangle-1.json', $grid);

        $grid = self::loadJson(__DIR__ . '/../data/expert-80-before-spy.json');
        $step = (new SpyRectangle($grid))->execute();
        $this->debugGrid($grid);
        self::assertTrue($step);
        self::assertGrid(__DIR__ . '/../data/expert-80-after-spy.json', $grid);

        $grid = self::loadJson(__DIR__ . '/../data/expert-82-before-spy.json');
        $step = (new SpyRectangle($grid))->execute();
        $this->debugGrid($grid);
        self::assertTrue($step);
        self::assertGrid(__DIR__ . '/../data/expert-82-after-spy.json', $grid);
    }

    public function testRectangle2()
    {
        $grid = self::loadJson(__DIR__ . '/../data/expert-84-before-spy-v2.json');
        $this->debugGrid($grid);
        $step = (new SpyRectangle($grid))->execute();
        $this->debugGrid($grid);
        self::assertTrue($step);
        self::assertGrid(__DIR__ . '/../data/expert-84-after-spy-v2.json', $grid);
    }

}