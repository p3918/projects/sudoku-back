<?php

namespace App\Tests\Unit;

use App\Model\Cell;
use App\Tests\TestCase;

class NeighbourHoodTest extends TestCase
{

    protected array $grids = [
        'success-01',
        'success-05',
        'expert-11',
        'expert-55',
        'expert-80',
        'power-15',
        'power-83',
        'maestro-1',
        'maestro-2',
        'maestro-3',
        'suprem-84',
        'suprem-83',
        'success-81',
        'invalid-grid',
    ];

    public function testInitializeNeighbours()
    {
        $initial      = memory_get_usage();
        foreach ($this->grids as $gridId) {
            $grid = self::loadJson(__DIR__ . '/../data/' . $gridId . '-initial.json');
            $ns   = $grid->walk(fn(Cell $cell) => $cell->setAnnotation(implode(',', $cell->neighbours())));
            $mem  = memory_get_usage() - $initial;
            self::assertLessThanOrEqual(5000000, $mem);
            unset($grid);
            unset($ns);
        }
    }

    public function testExtractChains()
    {
        $initial      = memory_get_usage();
        $chainMaxSize = 3;
        foreach ($this->grids as $gridId) {
            $grid = self::loadJson(__DIR__ . '/../data/' . $gridId . '-initial.json');
            foreach ([0, 1, 2, 3, 4, 5, 6, 7, 8, null] as $value) {
                foreach ([true, false] as $takeSelfEdges) {
                    $extractor = (new ChainExtractorMock($grid, $chainMaxSize, $value, $takeSelfEdges));
                    self::assertFalse($extractor->explore());

                    $mem  = memory_get_usage() - $initial;
                    $real = memory_get_usage(true);
                    self::assertLessThanOrEqual(5000000, $mem);
                    if (!$_ENV['silent']) {
                        echo 'GRID ' . $gridId . ' (params: ' . $chainMaxSize . ',' . $value . ',' . $takeSelfEdges . ')' . PHP_EOL
                             . '- chains : ' . $extractor::$countChains . PHP_EOL
                             . '- memory : ' . $mem . ' / ' . $real . ' allocated' . PHP_EOL . PHP_EOL;
                    }
                }
            }
        }

    }
}