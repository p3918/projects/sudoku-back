<?php

namespace App\Tests\Techniques;

use App\Process\TigreXYChain;
use App\Tests\TestCase;

class TigreXYTest extends TestCase
{

    public function testTigreXY()
    {
        $grid = self::loadJson(__DIR__ . '/../data/gm-6-p7-before-xy-chain.json');
        $this->debugGrid($grid);
        $step = (new TigreXYChain($grid))->execute();
        self::assertTrue($step);
        $this->debugGrid($grid);
        self::assertGrid(__DIR__ . '/../data/gm-6-p7-after-xy-chain.json', $grid);
    }

    public function testChainValid()
    {
        $grid = self::loadJson(__DIR__ . '/../data/xy-chain-before.json');
        $grid->resetEffects();
        $this->debugGrid($grid);
        $step = (new TigreXYChain($grid))->execute();
        self::assertTrue($step);
        $this->debugGrid($grid);
        self::assertGrid(__DIR__ . '/../data/xy-chain-after.json', $grid);
    }
}