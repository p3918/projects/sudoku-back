<?php

namespace App\Tests\Techniques;

use App\Process\TigreRemotePairMinusOne;
use App\Tests\TestCase;

class RemotePairMinusOneTest extends TestCase
{

    public function testRemotePairMinusOne()
    {
        $grid = self::loadJson(__DIR__ . '/../data/technique-tigre-before-remote-pair-minus-one.json');
        $this->debugGrid($grid);
        $step = (new TigreRemotePairMinusOne($grid))->execute();
        $this->debugGrid($grid);
        self::assertTrue($step);
        self::assertGrid(__DIR__ . '/../data/technique-tigre-after-remote-pair-minus-one.json', $grid);
    }
}