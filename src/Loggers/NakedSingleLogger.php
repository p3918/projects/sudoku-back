<?php

namespace App\Loggers;

use App\Model\Cell;
use App\Serialization\Formatter;

class NakedSingleLogger extends LoggerContract
{
    protected int    $level         = self::LEVEL_BASIC;
    protected string $phase         = 'Candidat isolé';
    protected string $canonicalName = 'naked_single';
    private int      $count         = 0;

    public function reset(): void
    {
        parent::reset();
        $this->count = 0;
    }

    public function logNakedSingle(int $value, Cell $cell)
    {
        $cell->setEffect(Cell::EFFECT_FOUND);
        $cell->setAnnotation('T' . (++$this->count));
        $this->proof[]   = 'Le kumiKata de ' . $cell . ' contient une seule valeur : ' . Formatter::v($value);
        $this->message[] = 'On trouve la valeur ' . Formatter::v($value) . ' en ' . $cell;
    }
}
