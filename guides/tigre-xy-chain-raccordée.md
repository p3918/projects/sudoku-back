```
-----------------------------------------
[   ,   ,   , |   ,   ,   , |   ,   ,   ]
[   , AB,   , |   ,   , XX, |   ,   ,   ] 
[   ,   ,   , |   ,   ,   , |   ,   ,   ]
-----------------------------------------
[   ,   ,   , |   ,   ,   , |   ,   ,   ]
[   , BC,   , |   , CD,   , |   ,   ,   ] 
[   , XX,   , |   ,   ,   , | DA,   ,   ]
-----------------------------------------
[   ,   ,   , |   ,   ,   , |   ,   ,   ]
[   ,   ,   , |   ,   ,   , |   ,   ,   ] 
[   ,   ,   , |   , R1,   , | R2,   ,   ]
-----------------------------------------
```

On se donne un candidat initial A
On forme une chaine telle que :
- le premier maillon contient A
- chaque maillon est une paire OU alors un triplet qui possède un raccord avec un des maillons précédents (ou les suivants aïe)
- les maillons se voient deux à deux
- les maillons ont un candidat commun
- le début et la fin de la chaine ou un candidat commun (A)

Que signifie 'posséder un raccord' avec un autre maillon;
- Les 2 maillons doivent se voir 
- L'autre maillon doit posséder la troisième valeur
- Exemple : 1,2 -> 2,6 -> 2,6,9
  - 2,6,9 n'est pas une paire, je peux choisir ma paire parmis : (2,6 ou 2,9 ou 6,9)
    - si je prends 2,6, le 3eme candidat est 9, pas dans les maillons précédents ==> impossible
    - si je prends 2,9, le 3eme candidat est 6, 
    - si je prends 6,9, le 3eme candidat est 2
    - => du coup je ne sais pas s'il faut prendre 2,9 ou 6,9 ; peut être qu'il faut étudier les 2 choix pour voir où ils mènent.


Il y a aussi les cas de raccord en fin de chaine, et j'ai pas bien compris si ça pouvait aussi arriver au milieu de la chaine ou pas

Pour l'instant ça me suffit pour comprendre si je peux ou pas faire un code générique de fabrication de chaines commun à tous les tigres

En règle général il faut que `l'hypothèse que nous formulons élimine les candidats supplémentaires de ces cases`,
(à chaque fois notre raisonnement commence par 'si c'est A, alors on conclut directement XX != A, sinon si c'est B (`si c'est B`=`l'hypothèse que nous formulons`)
C'est à dire qu'il faut choisir d'avance le candidat commun en début de chaine (donc faire 2 choix distincts, et une étude complète du chainage en fonction du choix initial)

En regardant les techniques suivantes piste et AIS, en fait on relache de plus en plus les liens entre 2 maillons,
et on commence ça se demander si on devrait pas regarder autrement, faire des chaines très peu contraintes et voir après coup si c'est bien des chaines.
