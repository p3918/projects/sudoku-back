<?php

namespace App\Serialization;

use JetBrains\PhpStorm\Pure;

class Formatter
{

    public static bool  $human = false;
    public static array $alpha = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q',
                                  'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];

    /** @noinspection PhpPureFunctionMayProduceSideEffectsInspection */
    #[Pure] public static function v(int|array $value): string
    {
        $transform = fn(int $v) => strval(self::$human ? ($v + 1) : $v);
        if (!is_array($value)) {
            return $transform($value);
        }
        return implode(',', array_map($transform, $value));
    }

    public static function alpha(int|array $value): string
    {
        $transform = fn(int $v) => self::$alpha[$v % 26] . ($v > 26 ? floor($v / 26) : '');
        if (!is_array($value)) {
            return $transform($value);
        }
        return implode(',', array_map($transform, $value));
    }

    #[Pure] public static function e(int $effect, string $content): string
    {
        return '#' . $effect . $content . '#';
    }
}