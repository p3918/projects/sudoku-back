<?php

namespace App\Tests\Techniques;

use App\Process\Projection;
use App\Tests\TestCase;

class ProjectionTest extends TestCase
{

    public function testProjections1()
    {
        $grid = self::loadJson(__DIR__ . '/../data/success-01-initial.json');
        self::assertGrid(__DIR__ . '/../data/success-01-kata.json', $grid);
        $step = (new Projection($grid))->execute();
        self::assertTrue($step);
        $this->debugGrid($grid);
        self::assertGrid(__DIR__ . '/../data/success-01-projections-00.json', $grid);

        $step = (new Projection($grid))->execute();
        $this->debugGrid($grid);
        self::assertTrue($step);
    }
}