<?php

namespace App\Tests\Unit;

use App\Model\Cell;
use App\Model\Edge;
use App\Tests\TestCase;

class TigreRegularAlternateLoopUnitTest extends TestCase
{
    public function testRegularAlternateLoop()
    {
        $grid       = self::loadJson(__DIR__ . '/../data/issue-65-before-regular-alternate-loop.json');
        $worker     = new TigreRegularAlternateLoopMock($grid);
        $neighbours = $worker->neighboursOf0();
        self::assertCount(1, self::filter($neighbours, 3));
        $neighbours = $worker->neighboursOf3();
        self::assertCount(1, self::filter($neighbours, 30));
        $neighbours = $worker->neighboursOf30();
        self::assertCount(1, self::filter($neighbours, 35));
        $neighbours = $worker->neighboursOf35();
        self::assertCount(1, self::filter($neighbours, 71));
        $neighbours = $worker->neighboursOf71();
        self::assertCount(1, self::filter($neighbours, 63));

        self::assertTrue($worker->isValidChainValid());
        self::assertTrue($worker->workOnValidChain());
        $this->debugGrid($grid);
    }

    private static function filter(array $neighbours, int $id): array
    {
        return array_filter($neighbours, fn(Edge $e) => $e->cell->id === $id);
    }

    public function testUselessLongChains()
    {
        $grid = self::loadJson(__DIR__ . '/../data/issue-65-before-regular-alternate-loop.json');
        $grid->resetEffects();

        $worker        = new TigreRegularAlternateLoopMock($grid);
        $currentChain  = $worker->prettyLongChain(); // 30,35,43,61
        $allNeighbours = $worker->allNeighboursOf61();
        $neighbours    = $worker->neighboursOf61();

        $currentChain->collect()->addEffect(Cell::EFFECT_ELEM_1);
        foreach ($allNeighbours as $neighbour) {
            $neighbour->cell->setEffect(Cell::EFFECT_ELEM_2);
        }
        $this->debugGrid($grid);

        self::assertCount(5, $allNeighbours); // [62,71,54,55,43]
        // 43 doit être éliminé, car déjà dans la chaine
        // 62 doit être éliminé, car ça fait un chemin détourné alors qu'il existe un chemin direct de 35 à 62
        // 71 doit être éliminé, car ça fait un chemin détourné alors qu'il existe un chemin direct de 35 à 71
        self::assertCount(2, $neighbours); // 54, 55
    }
}