<?php

namespace App\Helpers;

class SwitchVar
{

    public static function switchVars(mixed &$var1, mixed &$var2): void
    {
        $saveVar1 = $var1;
        $var1     = $var2;
        $var2     = $saveVar1;
    }
}