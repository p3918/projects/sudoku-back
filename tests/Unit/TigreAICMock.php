<?php

namespace App\Tests\Unit;

use App\Model\Edge;
use App\Model\NewChain;
use App\Model\SelfEdge;
use App\Process\TigreAIC;

class TigreAICMock extends TigreAIC
{

    public function finalChain(): NewChain
    {
        return new NewChain(7,
            [
                new Edge($this->grid[0], false, -1),
                new Edge($this->grid[8], true, 1),
                new Edge($this->grid[80], false, 1),
                new Edge($this->grid[61], true, 1),
                new SelfEdge($this->grid[61], false, 1, 6),
                new Edge($this->grid[55], true, 6),
                new Edge($this->grid[19], false, 6),
            ]
        );
    }

    public function chainToLoop($chain): NewChain
    {
        $this->checkChainData($chain);
        return $chain;
    }

    public function finalLoop(): NewChain
    {
        return new NewChain(7,
            [
                new Edge($this->grid[0], true, 6),
                new SelfEdge($this->grid[0], false, 6, 1),
                new Edge($this->grid[8], true, 1),
                new Edge($this->grid[80], false, 1),
                new Edge($this->grid[61], true, 1),
                new SelfEdge($this->grid[61], false, 1, 6),
                new Edge($this->grid[55], true, 6),
                new Edge($this->grid[19], false, 6),
            ]
        );
    }

    public function neighboursOf0(): array
    {
        $chain = new NewChain(1,
            [
                new Edge($this->grid[0], false, -1),
            ]
        );
        return array_values($this->neighbours($chain));
    }

    public function neighboursOf8(): array
    {
        $chain = new NewChain(1,
            [
                new Edge($this->grid[0], false, -1),
                new Edge($this->grid[8], true, 1),
            ]
        );
        return array_values($this->neighbours($chain));
    }

    public function neighboursOf80(): array
    {
        $chain = new NewChain(1,
            [
                new Edge($this->grid[0], false, -1),
                new Edge($this->grid[8], true, 1),
                new Edge($this->grid[80], false, 1),
            ]
        );
        return array_values($this->neighbours($chain));
    }

    public function neighboursOf61(): array
    {
        $chain = new NewChain(1,
            [
                new Edge($this->grid[0], false, -1),
                new Edge($this->grid[8], true, 1),
                new Edge($this->grid[80], false, 1),
                new Edge($this->grid[61], true, 1),
            ]
        );
        return array_values($this->neighbours($chain));
    }

    public function neighboursOf61b(): array
    {
        $chain = new NewChain(1,
            [
                new Edge($this->grid[0], false, -1),
                new Edge($this->grid[8], true, 1),
                new Edge($this->grid[80], false, 1),
                new Edge($this->grid[61], true, 1),
                new SelfEdge($this->grid[61], false, 1, 6),
            ]
        );
        return array_values($this->neighbours($chain));
    }

    public function neighboursOf55(): array
    {
        $chain = new NewChain(1,
            [
                new Edge($this->grid[0], false, -1),
                new Edge($this->grid[8], true, 1),
                new Edge($this->grid[80], false, 1),
                new Edge($this->grid[61], true, 1),
                new SelfEdge($this->grid[61], false, 1, 6),
                new Edge($this->grid[55], true, 6),
            ]
        );
        return array_values($this->neighbours($chain));
    }

    public function checkValid(NewChain $chain): bool
    {
        return $this->isValid($chain);
    }

    public function exploreToFindChain(): bool|NewChain
    {
        foreach ($this->cellsToExplore() as $cell) {
            if (!$this->cellIsValidStart($cell)) {
                continue;
            }
            $firstNode = new Edge($cell);
            $response  = $this->exploreCellToFindChain(new NewChain(1, [$firstNode]));
            if ($response) {
                return $response;
            }
        }
        return false;
    }


    private function exploreCellToFindChain(NewChain $currentChain): bool|NewChain
    {
        if (count($currentChain) >= 3 && $this->isValid($currentChain)) {
            $this->reset();
            $response = $this->tryToFindChain($currentChain);
            if ($response) {
                return $response;
            }
        }
        $neighbours = $this->neighbours($currentChain);
        foreach ($neighbours as $key => $neighbour) {
            if ($neighbour instanceof SelfEdge) {
                continue;
            }
            if ($currentChain->exists($neighbour->cell->id)) {
                unset($neighbours[$key]);
            }
            // for ($k = 0; $k < count($currentChain) - 1; $k++) {
            //     if ($currentChain[$k]->cell->linked($neighbour)) {
            //         unset($neighbours[$key]);
            //     }
            // }
        }
        if (empty($neighbours)) {
            return false;
        }
        foreach ($neighbours as $neighbour) {
            $nextChain = $currentChain->clone()->append($neighbour);
            $response  = $this->exploreCellToFindChain($nextChain);
            if ($response) {
                return $response;
            }
        }
        return false;
    }

    protected function tryToFindChain(NewChain $chain): bool|NewChain
    {
        list($firstNode, $firstCandidate, $lastNode, $lastCandidate, $loop) = $this->checkChainData($chain);
        $this->logger->colorize($chain);
        $this->logger->logChainInferences($chain, $firstNode, $firstCandidate, $lastNode, $lastCandidate);

        if ($loop && $this->doNiceLoop($chain)) {
            $this->logger->messageLoop($chain, $firstNode, $firstCandidate, $lastNode, $lastCandidate);
            return $chain;
        }

        if ($firstNode->cell->isAllowed($lastCandidate) || $lastNode->cell->isAllowed($firstCandidate)) {
            $this->logger->messageOpenedChain($chain, $firstNode, $firstCandidate, $lastNode, $lastCandidate);
            $firstNode->cell->disableValue($lastCandidate);
            $lastNode->cell->disableValue($firstCandidate);
        }

        $this->reset();
        return false;
    }

    public function exploreChainStartingWithDowngradedStrongLink(): bool
    {
        $cell = $this->grid[32];
        if (!$this->cellIsValidStart($cell)) {
            return false;
        }
        $firstNode = new Edge($cell);
        if ($this->exploreCell(new NewChain(1, [$firstNode]))) {
            return true;
        }
        return false;
    }

    public function chainEndingWithStrongLinkIsConsideredAsALoop(): bool
    {
        $chain = new NewChain(12, [
            new Edge($this->grid[0], false, -1),
            new Edge($this->grid[72], true, 3),
            new Edge($this->grid[78], false, 3),
            new SelfEdge($this->grid[78], true, 3, 8),
            new Edge($this->grid[24], false, 8),
            new SelfEdge($this->grid[24], true, 8, 6),
            new Edge($this->grid[21], false, 6),
            new Edge($this->grid[12], true, 6),
            new Edge($this->grid[11], false, 6),
            new SelfEdge($this->grid[11], true, 6, 8),
            new Edge($this->grid[10], false, 8),
            new SelfEdge($this->grid[10], true, 8, 7),
            new Edge($this->grid[9], false, 7),
            new SelfEdge($this->grid[9], true, 7, 2),
            // Et ensuite il y un lien fort final sur le candidat 2 vers la case initiale
            // Ce dernier lien fort NE DOIT PAS être considéré comme un moyen de refermer une boucle d'alternance régulière.
            // En effet, le dernier lien actuel défini ci-dessus `new SelfEdge($this->grid[9], true, 7, 2)` est fort, donc ça ferait 2 liens forts d'affilée
        ]);
        return $this->checkChainData($chain)[4];
    }
}