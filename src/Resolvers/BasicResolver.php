<?php

namespace App\Resolvers;

use App\Model\Grid;
use App\Process\Bow;
use App\Process\NakedLock;
use App\Process\NakedSingle;
use App\Process\Projection;
use App\Process\Set;
use App\Serialization\StepsCollection;
use JetBrains\PhpStorm\Pure;

class BasicResolver extends AbstractResolver
{
    #[Pure] public function __construct(Grid $grid, ?StepsCollection $allSteps = null, bool $acceptFinalStep = true)
    {
        parent::__construct($grid, $allSteps, $acceptFinalStep);
        $this->techniques            = [
            new Set($this->grid, 1),
            new NakedLock($this->grid),
            new NakedSingle($this->grid),
            new Projection($this->grid),
            new Bow($this->grid),
            new Set($this->grid, 2),
            new Set($this->grid, 3),
        ];
        $this->nextResolverOnSuccess = $this;
        $this->nextResolverOnFailure = new AdvancedResolver($this->grid, $this->logSteps, $this, $acceptFinalStep);
    }
}