<?php

namespace App\Loggers;

use App\Model\Cell;
use App\Model\Zone;
use App\Serialization\Formatter;

class SetLogger extends LoggerContract
{
    protected string $canonicalName = 'little_sets';
    private int      $count         = 0;

    public function __construct(private int $size)
    {
        $this->phase = (match ($size) {
            1       => 'Cases isolées',
            2, 3    => 'Étude des ensembles de 2 ou 3 élements',
            default => 'Étude des ensembles de 4 ou 5 éléments'
        });

        $this->level = (match ($this->size) {
            1, 2, 3 => self::LEVEL_BASIC,
            default => self::LEVEL_ADVANCED
        });
    }

    public function reset(): void
    {
        parent::reset();
        $this->count = 0;
    }

    public function logNakedSingle(int $value, Cell $cell, array $freeValues, Zone $zone)
    {
        $cell->setEffect(Cell::EFFECT_FOUND);
        $cell->setAnnotation('T' . ++$this->count);
        $this->proof[] =
            $this->size === 1 ?
                'Dans ' . $zone . ', la seule valeur restante à trouver est ' . Formatter::v($freeValues) :
                'Dans ' . $zone . ', les valeurs restantes à trouver sont ' . Formatter::v($freeValues);
        foreach ($freeValues as $v) {
            if ($v !== $value) {
                $this->proof[] = 'Le ' . Formatter::v($v) . ' ne peut pas être en case ' . $cell;
                $cell->viewedCells()->filter(fn(Cell $c) => $c->value() === $v)->addEffect(Cell::EFFECT_ELEM_1);
            }
        }
        $this->message[] = 'Candidat isolé, on place la valeur ' . Formatter::v($value) . ' en ' . $cell;
        $zone->filter(fn(Cell $cell) => $cell->effect() === null)->addEffect(Cell::EFFECT_ELEM_2);
    }

    public function logOnlyOnePosition(int $value, Zone $zone, Cell $position)
    {
        $position->setEffect(Cell::EFFECT_FOUND);
        $position->setAnnotation('T' . ++$this->count);
        $this->proof[]   =
            count($zone->freeValues()) > 1 ?
                'Dans ' . $zone . ', les valeurs restantes à trouver sont ' . Formatter::v($zone->freeValues())
                : 'Dans ' . $zone . ', la seule valeur restante à trouver est ' . Formatter::v($zone->freeValues());
        $this->proof[]   = 'La seule position possible pour ' . Formatter::v($value) . ' dans ' . $zone . ' est ' . $position;
        $this->message[] = 'On place la valeur ' . Formatter::v($value) . ' dans ' . $zone . ' en ' . $position;
        $zone->filter(fn(Cell $cell) => $cell->effect() === null)->addEffect(Cell::EFFECT_ELEM_2);
    }
}
