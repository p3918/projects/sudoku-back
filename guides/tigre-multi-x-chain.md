```
-----------------------------------------
[   ,   ,   , |   ,   ,   , |   ,   ,   ]
[   , A1,   , |   , A2,   , |   ,   ,   ] 
[   ,   ,   , |   ,   ,   , |   ,   ,   ]
-----------------------------------------
[   ,   ,   , |   ,   ,   , |   ,   ,   ]
[   , A4,   , |   , A3,   , |   ,   ,   ] 
[   ,   ,   , |   ,   ,   , |   ,   ,   ]
-----------------------------------------
[   ,   ,   , |   ,   ,   , |   ,   ,   ]
[   ,   ,   , |   ,   ,   , |   ,   ,   ] 
[   ,   ,   , |   ,   ,   , |   ,   ,   ]
-----------------------------------------
```

On forme une chaine de taille paire telle que :

- les maillons se voient deux à deux avec un lien fort
  (ils constituent les 2 seules positions possibles du candidat A dans leur zone commune)
- le premier et le dernier maillon se voient
- a1 != aN

AVEC LE NOUVEAU SYSTEME :
A0 =F,b=> A1 =f,b,c=> A1 =F,c=> A2 =f,c,d=> A2 =F,d=> A3 =f,d,e A3 =F,e=> A4 voit A0 (avec b != e)

- Si A0 vaut b alors 
  - A4 != a
  - A0 != g
- Sinon A0 != b
  - A1 = b
  - A1 != c
  - A2 = c
  - A2 != d
  - A3 = d
  - A3 != e
  - A4 = g (!= a)
  - A0 != g 

Je note

- a1 le candidat commun à A1,A2
- a2 le candidat commun à A2,A3
- a3 le candidat commun à A3,A4

Alors les extrémités de la chaine ne peuvent contenir le candidat formant le lien fort de l'extrémité opposée.

Preuve :

- Si A1 vaut a1, alors
    - A4 ne vaut pas a1
    - A1 ne vaut pas a3
- Sinon, A1 ne vaut pas a1,
    - donc A2 vaut forcément a1, donc il ne vaut pas a2
    - donc A3 vaut forcément a2, donc pas a3
    - donc A4 vaut forcément a3, et donc il ne vaut pas a1
    - donc A1 ne vaut pas a3

==> Dans les 2 cas on voit que A4 != a1 et A1 != a3
