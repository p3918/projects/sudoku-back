<?php

namespace App\Tests;

use App\Model\Grid;
use App\Process\Initiator;
use App\Serialization\ConsolePrinter;

/**
 *
 */
abstract class TestCase extends \PHPUnit\Framework\TestCase
{
    public static function loadJson(string $fileName): Grid
    {
        return Initiator::initGrid(json_decode(file_get_contents($fileName), true));
    }

    public static function assertGridOnlyValues(string $fileName, Grid $grid)
    {
        $actual       = json_decode(json_encode($grid), true);
        $actualValues = array_map(fn($c) => $c['value'], $actual);

        $expected       = json_decode(file_get_contents($fileName), true);
        $expectedValues = array_map(fn($c) => $c['value'], $expected);
        self::assertEquals($expectedValues, $actualValues);
    }

    public function overwriteExpectedGrid(string $fileName, Grid $grid): void
    {
        file_put_contents($fileName, json_encode($grid));
        $this->addWarning('Overwriting test data ... , you are doing the inverse of an assertion, are you sure ?');
        $this->markAsRisky();
        self::assertGrid($fileName, $grid);
    }

    public static function assertGrid(string $fileName, Grid $grid)
    {
        $actual   = json_decode(json_encode($grid), true);
        $expected = json_decode(file_get_contents($fileName), true);
        self::assertEquals($expected, $actual, $fileName);
    }

    protected function debugGrid(Grid|array $grid)
    {
        if (!$_ENV['silent']) {
            echo ConsolePrinter::gridToConsole($grid);
        }
    }
}