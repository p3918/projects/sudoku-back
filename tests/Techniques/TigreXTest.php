<?php

namespace App\Tests\Techniques;

use App\Process\TigreXChain;
use App\Tests\TestCase;

class TigreXTest extends TestCase
{

    public function testTigreX17()
    {
        $grid = self::loadJson(__DIR__ . '/../data/gm-5-p17-initial.json');
        $this->debugGrid($grid);
        $step = (new TigreXChain($grid))->execute();
        self::assertTrue($step);
        $this->debugGrid($grid);
        $step = (new TigreXChain($grid))->execute();
        self::assertTrue($step);
        $this->debugGrid($grid);
        self::assertGrid(__DIR__ . '/../data/gm-5-p17-tigre.json', $grid);
    }

    public function testTigreX20()
    {
        $grid = self::loadJson(__DIR__ . '/../data/gm-5-p20-initial.json');
        $this->debugGrid($grid);
        $tigreXChainTechnique = new TigreXChain($grid);
        $step                 = $tigreXChainTechnique->execute();
        self::assertTrue($step);
        $this->debugGrid($grid);
        self::assertGrid(__DIR__ . '/../data/gm-5-p20-tigre.json', $grid);
    }
}