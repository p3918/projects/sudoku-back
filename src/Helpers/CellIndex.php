<?php

namespace App\Helpers;

use JetBrains\PhpStorm\Pure;

class CellIndex
{

    #[Pure] public static function blockRow(int $cellId): int
    {
        return floor(self::blockId($cellId) / 3);
    }

    #[Pure] public static function blockId(int $cellId): int
    {
        return (floor(self::row($cellId) / 3) * 3) + floor(self::column($cellId) / 3);
    }

    #[Pure] public static function row(int $cellId): int
    {
        return floor($cellId / 9);
    }

    #[Pure] public static function column(int $cellId): int
    {
        return $cellId % 9;
    }

    #[Pure] public static function blockColumn(int $cellId): int
    {
        return self::blockId($cellId) % 3;
    }

    #[Pure] public static function inBlockId(int $cellId): int
    {
        return 3 * (self::row($cellId) % 3) + (self::column($cellId) % 3);
    }

    #[Pure] public static function inBlockRow(int $cellId): int
    {
        return self::row($cellId) % 3;
    }

    #[Pure] public static function inBlockColumn(int $cellId): int
    {
        return self::column($cellId) % 3;
    }

    #[Pure] public static function fromBlockIdInBlockId(int $blockId, int $inBlockId): int
    {
        $blockRow      = floor($blockId / 3);
        $blockColumn   = $blockId % 3;
        $inBlockRow    = floor($inBlockId / 3);
        $inBlockColumn = $inBlockId % 3;
        return self::fromBlockRowColumnInBlockRowColumn($blockRow, $blockColumn, $inBlockRow, $inBlockColumn);
    }

    #[Pure] public static function fromBlockRowColumnInBlockRowColumn(int $blockRow, int $blockColumn, int $inBlockRow, int $inBlockColumn): int
    {
        return self::fromRowColumn($blockRow * 3 + $inBlockRow, $blockColumn * 3 + $inBlockColumn);
    }

    #[Pure] public static function fromRowColumn(int $row, int $column): int
    {
        return 9 * $row + $column;
    }

    #[Pure] public static function fromBlockIdInBlockRowColumn(int $blockId, int $inBlockRow, int $inBlockColumn): int
    {
        $blockRow    = floor($blockId / 3);
        $blockColumn = $blockId % 3;
        return self::fromBlockRowColumnInBlockRowColumn($blockRow, $blockColumn, $inBlockRow, $inBlockColumn);
    }

    #[Pure] public static function fromBlockRowColumnInBlockId(int $blockRow, int $blockColumn, int $inBlockId): int
    {
        $inBlockRow    = floor($inBlockId / 3);
        $inBlockColumn = $inBlockId % 3;
        return self::fromBlockRowColumnInBlockRowColumn($blockRow, $blockColumn, $inBlockRow, $inBlockColumn);
    }
}