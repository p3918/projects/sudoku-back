<?php

namespace App\Process;

use App\Loggers\LoggerContract;
use App\Loggers\NakedSetLogger;
use App\Model\Cell;
use App\Model\CellCollection;
use App\Model\Column;
use App\Model\Grid;
use App\Model\Row;
use App\Model\Zone;
use JetBrains\PhpStorm\Pure;

class NakedSet extends ProcessorContract
{

    /** @var NakedSetLogger */
    protected LoggerContract $logger;

    #[Pure] public function __construct(Grid $grid, private int $size)
    {
        parent::__construct($grid, new NakedSetLogger());
    }

    public function execute(): bool
    {
        for ($i = 0; $i < 27; $i++) {
            $this->reset();
            if ($this->analyzeZone($this->grid->zones[$i], $this->size)) {
                return true;
            }
        }
        return false;
    }


    protected function analyzeZone(Zone $zone, int $size): bool
    {
        // Si l'ensemble de valeurs à trouver est trop grand, ou si on a déjà tout trouvé dans cette zone, on passe directement à la zone suivante
        if (count($zone->freeValues()) < 2) {
            return false;
        }

        // On range les cases dans un tableau, en fonction de l'ensemble de leurs valeurs possibles.
        // (uniquement si le nombre de valeurs possibles est cohérent par rapport à ce qu'on cherche)
        // Exemple : Si je cherche une paire isolée
        //  - en position 3 j'ai les valeurs 1 et 2
        //  - en position 4 j'ai les valeurs 1 et 2
        //  - en position 5 j'ai les valeurs 1,2 et 3 (3 positions, si je cherche une isolated pair ça ne m'intéressera pas)
        //  - en position 6 j'ai les valeurs 5 et 6
        // Alors on aura :
        // "1.2" => [3,4], "5.6" => [6]
        // ==> on aura bien une paire isolée (valeurs 1 et 2 aux positions 3 et 4) car il y a une clé qui a 2 éléments
        $positionsByValues = [];
        foreach ($zone->freeCells() as $cell) {
            $values = $cell->allowedValues();
            if (count($values) === $size) {
                $key = implode('.', $values);
                if (!isset($positionsByValues[$key])) {
                    $positionsByValues[$key] = [];
                }
                $positionsByValues[$key][] = $zone->indexOf[$cell->id];
            }
            // Le problème avec les triplets, c'est qu'ils peuvent être répartis en paires
            // Exemple : [0,1];[0,5];[1,5] forment un triplet 0.1.5
            // @TODO -- ma solution n'est vraiment pas merveilleuse, trouver mieux
            if (count($values) === 2 && $size === 3) {
                foreach (array_diff([0, 1, 2, 3, 4, 5, 6, 7, 8], $values) as $thirdValue) {
                    $triple = array_merge($values, [$thirdValue]);
                    sort($triple);
                    $key = implode('.', $triple);
                    if (!isset($positionsByValues[$key])) {
                        $positionsByValues[$key] = [];
                    }
                    $positionsByValues[$key][] = $zone->indexOf[$cell->id];
                }
            }
        }
        /** @noinspection DuplicatedCode (c'est pas trop un doublon, car on fait l'inverse de la hidden pair) */
        $possibleNakedSet = array_filter($positionsByValues, fn(array $values) => count($values) === $size);
        if (empty($possibleNakedSet)) {
            return false;
        }

        // Si on a trouvé au moins une paire (resp. un triplet) isolée, on va s'arrêter à la première
        foreach ($possibleNakedSet as $values => $positions) {
            $values = explode('.', $values);
            if ($this->analyzeCandidate($zone, $values, $positions, $size)) {
                return true;
            }
        }
        return false;
    }

    public function analyzeCandidate(Zone $zone, array $values, array $positions, int $size): bool
    {
        // Une fois la paire isolée (resp. le triplet) trouvée,
        // on sait qu'on ne peut pas avoir ces 2/3 valeurs dans les autres cases
        // Exemple :
        //  - les valeurs 1 et 2 sont aux positions 3 et 4,
        //  => donc les valeurs 1 et 2 ne peuvent pas être ailleurs
        // (exemple : si j'avais un 1 en position 5, alors 2 devrait être à la fois en position 3 et en position 4)
        $forbiddenPositions = array_diff([0, 1, 2, 3, 4, 5, 6, 7, 8], $positions);
        $atLeastOneDisabled = false;
        $foundCells         = new CellCollection();
        $forbiddenCells     = new CellCollection();
        $countFound         = 0;
        $countForbidden     = 0;
        foreach ($positions as $position) {
            $cell = $zone[$position];
            $cell->setEffect(Cell::EFFECT_FOUND);
            $cell->setAnnotation('T' . (++$countFound));
            $foundCells->append($zone[$position]);
        }
        foreach ($forbiddenPositions as $position) {
            $cell        = $zone[$position];
            $isForbidden = array_reduce($values, fn($carry, $v) => $carry || $cell->kumiKata[$v], false);
            if ($isForbidden) {
                $cell->setEffect(Cell::EFFECT_FORBIDDEN);
                $cell->setAnnotation('X' . (++$countForbidden));
                $forbiddenCells->append($cell);
            }
        }

        foreach ($forbiddenPositions as $position) {
            $cell = $zone[$position];
            foreach ($values as $forbiddenValue) {
                if ($cell->kumiKata[$forbiddenValue]) {
                    $atLeastOneDisabled = true;
                    $cell->disableValue($forbiddenValue);
                }
            }
        }

        $this->logger->logSet($values, $foundCells, $forbiddenCells, $zone);

        // Si la paire ou le triplet se trouve aussi à l'intersection avec une autre zone, alors il faut l'analyser aussi
        // Je ne fais que le cas Row/Column car on fait ces zones avant les blocks normalement
        /** @noinspection DuplicatedCode */
        if ($zone instanceof Row || $zone instanceof Column) {
            $intersectWithBlocks = array_unique($zone->wherePosIn($positions)->map(fn(Cell $cell) => $cell->block->id));
            if (count($intersectWithBlocks) === 1) {
                $intersectResult    = $this->analyzeZone($this->grid->blocks[reset($intersectWithBlocks)], $size);
                $atLeastOneDisabled = $atLeastOneDisabled || $intersectResult;
            }
        }

        return $atLeastOneDisabled;
    }
}
