<?php

namespace App\Resolvers;

use App\Model\Grid;
use App\Process\ProcessorContract;
use App\Serialization\StepsCollection;
use JetBrains\PhpStorm\Pure;

class BasicTechniqueResolver extends AbstractResolver
{
    #[Pure] public function __construct(Grid $grid, ProcessorContract $processor, ?StepsCollection $allSteps = null)
    {
        parent::__construct($grid, $allSteps);
        $this->techniques            = [
            $processor
        ];
        $this->nextResolverOnSuccess = $this;
        $this->nextResolverOnFailure = null;
    }
}