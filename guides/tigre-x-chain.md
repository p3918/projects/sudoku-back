```
-----------------------------------------
[   ,   ,   , |   ,   ,   , |   ,   ,   ]
[   , A1,   , |   , A2,   , |   ,   ,   ] 
[   ,   ,   , |   ,   ,   , |   ,   ,   ]
-----------------------------------------
[   ,   ,   , |   ,   ,   , |   ,   ,   ]
[   ,   ,   , |   , A3,   , |   ,   ,   ] 
[   , XX,   , |   ,   , A4, |   ,   ,   ]
-----------------------------------------
[   ,   ,   , |   ,   ,   , |   ,   ,   ]
[   ,   ,   , |   ,   ,   , |   ,   ,   ] 
[   ,   ,   , |   ,   ,   , |   ,   ,   ]
-----------------------------------------
```

On forme une chaine de taille paire telle que :
- chaque maillon contient le candidat A
- les maillons se voient deux à deux avec un lien fort 
    (ils constituent les 2 seules positions possibles du candidat A dans leur zone commune)

Alors les cases vues par les 2 bouts de la chaine ne peuvent pas contenir le candidat A.

Preuve :
- Si A1 est vrai, alors XX est différent de A
- Sinon, A2 est vrai
  - donc A3 est faux
  - donc A4 est vrai
  - donc XX est différent de A
