<?php

namespace App\Model;

use ArrayAccess;
use Closure;
use Countable;
use Iterator;
use JetBrains\PhpStorm\Pure;
use JsonSerializable;

class CellCollection implements ArrayAccess, JsonSerializable, Iterator, Countable
{
    use CellIterator;
    use CellCollectionOperations;

    #[Pure] public function __construct(int $size = 0, array $cells = [])
    {
        $this->size  = $size;
        $this->cells = array_values($cells);
        if (count($this->cells) > $this->size) {
            $this->size = count($this->cells);
        }
    }

    #[Pure] public function merge(CellCollection $otherCollection): CellCollection
    {
        $merged = array_merge($this->toArray(), $otherCollection->toArray());
        $cells  = [];
        foreach ($merged as $cell) {
            $cells[$cell->id] = $cell;
        }
        return new CellCollection(count($cells), array_values($cells));
    }

    /** @return Cell[] */
    #[Pure] public function toArray(): array
    {
        return $this->cells;
    }

    public function freeCells(): CellCollection
    {
        return $this->filter(fn(Cell $cell) => $cell->value() === null);
    }

    public function freeValues(): array
    {
        return array_values(array_diff([0, 1, 2, 3, 4, 5, 6, 7, 8], $this->takenValues()));
    }

    public function takenValues(): array
    {
        return $this->takenCells()->map(fn(Cell $cell) => $cell->value());
    }

    public function takenCells(): CellCollection
    {
        return $this->filter(fn(Cell $cell) => $cell->value() !== null);
    }

    /** @return int[]
     * @noinspection PhpPureFunctionMayProduceSideEffectsInspection
     */
    #[Pure] public function possiblePositions(int $value): array
    {
        return array_keys(array_filter($this->cells, fn(Cell $cell) => $cell->isAllowed($value)));
    }

    /** @noinspection PhpPureFunctionMayProduceSideEffectsInspection */
    #[Pure] public function possibleCells(int $value): CellCollection
    {
        return $this->filter(fn(Cell $cell) => $cell->isAllowed($value));
    }

    public function __toString(): string
    {
        return '{' . implode(',', $this->cells) . '}';
    }

    /**
     * @return int[]
     */
    public function cellIds(): array
    {
        return $this->map(fn(Cell $cell) => $cell->id());
    }

    /**
     * @return Cell[][]
     */
    public function allPairs(): array
    {
        $pairs = [];
        for ($i = 0; $i < $this->size; $i++) {
            for ($j = $i + 1; $j < $this->size; $j++) {
                $pairs[] = [$this[$i], $this[$j]];
            }
        }
        return $pairs;
    }

    public function findOne(Closure $closure): Cell|false
    {
        foreach ($this as $cell) {
            if ($closure($cell)) {
                return $cell;
            }
        }
        return false;
    }

    public function exists(Closure $closure): bool
    {
        foreach ($this as $cell) {
            if ($closure($cell)) {
                return true;
            }
        }
        return false;
    }

    public function addEffect(int $effect)
    {
        /** @var Cell $cell */
        foreach ($this->cells as $cell) {
            $cell->setEffect($effect);
        }
    }

    public function addAnnotation(string $annotation)
    {
        /** @var Cell $cell */
        foreach ($this->cells as $cell) {
            $cell->setAnnotation($annotation);
        }
    }

    public function first(): Cell
    {
        return $this->cells[0];
    }

    public function second(): Cell
    {
        return $this->cells[1];
    }

    public function last(): Cell
    {
        return $this->cells[$this->size - 1];
    }

    public function beforeLast(): Cell
    {
        return $this->cells[$this->size - 2];
    }

    public function append(Cell $cell): CellCollection
    {
        $this->size    += 1;
        $this->cells[] = $cell;
        return $this;
    }

    public function prepend(Cell $cell): CellCollection
    {
        $this->size  += 1;
        $this->cells = array_values(array_merge([$cell], $this->cells));
        return $this;
    }

    #[Pure] public function except(Cell $cell): CellCollection
    {
        return $this->whereIdNotIn([$cell->id()]);
    }

    #[Pure] public function clone(): CellCollection
    {
        return new self($this->size, $this->cells);
    }

    #[Pure] public function slice(int $start, int $end): CellCollection
    {
        $size = $end - $start + 1;
        return new self($size, array_values(array_slice($this->cells, $start, $size)));
    }

    public function chain(): Chain
    {
        return new Chain($this->size, $this->map(fn(Cell $cell) => new Neighbour($cell, 0)));
    }
}