<?php

namespace App;

use Exception;
use JetBrains\PhpStorm\Pure;

class InvalidOperation extends Exception
{

    #[Pure] public function __construct() { parent::__construct('Invalid operation'); }
}