<?php

namespace App\Loggers;

use App\Model\Cell;
use App\Model\CellCollection;
use App\Serialization\Formatter;

class SpyRectangleLogger extends LoggerContract
{
    protected int    $level         = self::LEVEL_SMART;
    protected string $phase         = 'Danse des espions';
    protected string $canonicalName = 'spy_rectangle_1';

    public function logRectangleV1(array $pairsIndexes, int $biggerSetIndex, CellCollection $rect, array $pair)
    {
        foreach ($pairsIndexes as $pKey) {
            $rect[$pKey]->setEffect(Cell::EFFECT_ELEM_1);
        }
        $cell = $rect[$biggerSetIndex];
        $cell->setEffect(Cell::EFFECT_FOUND);

        $this->proof[]   = 'Si une des valeurs ' . Formatter::v($pair) . ' était possible en ' . $cell . ', alors il y aurait 2 solutions possibles à la grille.';
        $this->message[] = 'Rectangle unique de type 1, on enlève les candidats ' . Formatter::v($pair) . ' en ' . $cell;
    }

    public function logRectangleV2(Cell $triple1, Cell $triple2, CellCollection $rect, int $additionnalValue, array $pair, CellCollection $cellsToRemoveV1)
    {
        $this->proof[]   = 'On observe le rectangle ' . $rect . ' qui contient 4 paires ' . Formatter::v($pair)
                           . ' et un candidat supplémentaire ' . Formatter::v($additionnalValue) . ' en ' . $triple1 . ' et ' . $triple2;
        $this->proof[]   = 'Si ni ' . $triple1 . ', ni ' . $triple2 . ' n\'est égale à ' . Formatter::v($additionnalValue)
                           . ', alors il y aurait 2 solutions possibles à la grille.'
                           . ' Donc le candidat ' . Formatter::v($additionnalValue) . ' se trouve soit en ' . $triple1
                           . ', soit en ' . $triple2;
        $this->proof[]   = 'Donc les cases qui voient ' . $triple1 . ' et ' . $triple2 . ' ne peuvent pas contenir ' . Formatter::v($additionnalValue);
        $this->message[] = 'Rectangle unique de type 2, on enlève le candidat ' . Formatter::v($additionnalValue) . ' dans les cases ' . $cellsToRemoveV1;
    }
}
