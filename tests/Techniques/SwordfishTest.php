<?php

namespace App\Tests\Techniques;

use App\Process\Swordfish;
use App\Tests\TestCase;

class SwordfishTest extends TestCase
{

    public function testSashimiSimple()
    {
        $grid = self::loadJson(__DIR__ . '/../data/power-37-before-swordfish.json');
        $this->debugGrid($grid);
        $step = (new Swordfish($grid))->execute();
        $this->debugGrid($grid);
        self::assertTrue($step);
        self::assertGrid(__DIR__ . '/../data/power-37-after-espadon.json', $grid);
    }

    public function testSkyScraperAndFinned()
    {
        $grid = self::loadJson(__DIR__ . '/../data/power-38-before-espadon.json');
        $this->debugGrid($grid);
        $step = (new Swordfish($grid))->execute();
        $this->debugGrid($grid);
        self::assertTrue($step);

        $step = (new Swordfish($grid))->execute();
        $this->debugGrid($grid);
        self::assertTrue($step);
        self::assertGrid(__DIR__ . '/../data/power-38-after-espadon.json', $grid);
    }

    public function testSkyscraper()
    {
        $grid = self::loadJson(__DIR__ . '/../data/issue-53-before-skyscraper.json');
        $grid->resetEffects();
        $this->debugGrid($grid);
        $step = (new Swordfish($grid))->execute();
        $this->debugGrid($grid);
        self::assertTrue($step);
        self::assertGrid(__DIR__ . '/../data/issue-53-after-skyscraper.json', $grid);
    }
}