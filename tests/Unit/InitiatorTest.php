<?php

namespace App\Tests\Unit;

use App\Tests\TestCase;

class InitiatorTest extends TestCase
{

    public function testEmptyGrid()
    {
        $grid = self::loadJson(__DIR__ . '/../data/empty-grid.json');
        self::assertGrid(__DIR__ . '/../data/empty-grid.json', $grid);
        $this->debugGrid($grid);
    }

    public function testSuccess01()
    {
        $grid = self::loadJson(__DIR__ . '/../data/success-01-initial.json');
        self::assertGrid(__DIR__ . '/../data/success-01-kata.json', $grid);
        $this->debugGrid($grid);
        self::assertEquals([0, 3, 4], $grid->blocks[0]->possiblePositions(4));
        self::assertCount(6, $grid->rows[0]->freeCells());
        self::assertEquals([0, 2, 3, 5, 7, 8], $grid->rows[0]->freeCells()->cellIds());
        self::assertCount(3, $grid->rows[0]->takenCells());
        self::assertCount(3, $grid->rows[0]->takenValues());
        self::assertEquals(24, $grid->countValues);
        self::assertCount(4, $grid->positions[7]);
        self::assertCount(6, $grid->rows[0]->freeValues());
        self::assertEquals([0, 1, 2], $grid->blocks[0]->rowOf[0]);
        self::assertEquals([0, 2], $grid->blocks[0]->columnOf[0]);
        self::assertEquals([0, 1], $grid->blocks[0]->columnOf[4]);
        self::assertEquals([0, 1], $grid->blocks[0]->rowOf[4]);
        self::assertEquals([0, 1], $grid->blocks[1]->columnOf[0]);
        self::assertEquals([2], $grid->blocks[4]->columnOf[0]);
        self::assertEquals([1], $grid->blocks[7]->columnOf[0]);
        self::assertEquals([1, 2], $grid->blocks[2]->columnOf[8]);
        self::assertEquals([2], $grid->blocks[2]->rowOf[8]);
    }

    public function testSuccess01Bis()
    {
        $grid = self::loadJson(__DIR__ . '/../data/success-01-projections-00.json');
        $this->debugGrid($grid);
        self::assertEquals([0], $grid->blocks[1]->rowOf[0]);
        self::assertEquals([0], $grid->blocks[1]->columnOf[0]);
    }

    public function testExpert47()
    {
        $grid = self::loadJson(__DIR__ . '/../data/expert-47-before-locked-candidates.json');
        $this->debugGrid($grid);
        self::assertEquals([3, 4], $grid->blocks[0]->possiblePositions(0));
        self::assertEquals([0, 1], $grid->blocks[0]->columnOf[0]);

    }
}