<?php

namespace App\Serialization;

use App\Helpers\ZoneIndex;
use App\Model\Grid;
use Closure;
use JetBrains\PhpStorm\Pure;

/**
 * @codeCoverageIgnore
 */
class ConsolePrinter
{

    /** @noinspection PhpPureFunctionMayProduceSideEffectsInspection */
    #[Pure] public static function gridToConsole(Grid|array $grid): string
    {
        /** @var array $grid */
        if (!is_array($grid)) {
            $grid = json_decode(json_encode($grid), true);
        }
        $output = ConsoleColors::WHITE->display(self::printLine());
        for ($rowIndex = 0; $rowIndex < 9; $rowIndex++) {
            $output .= self::printKata($grid, $rowIndex);
            $output .= self::printValue($grid, $rowIndex);
            $output .= self::printLockedKata($grid, $rowIndex);
            $color  = ($rowIndex % 3 == 2) ? ConsoleColors::WHITE : ConsoleColors::SMOKE;
            $output .= $color->display(self::printLine());
        }

        return $output;
    }

    /**
     * @return string
     */
    private static function printLine(): string
    {
        return ' ' . str_repeat('-', 109) . ' ' . PHP_EOL;
    }

    #[Pure] private static function printKata(array $grid, int $rowIndex): string
    {
        return self::printGeneralKata($rowIndex, fn(int $cellId) => $grid[$cellId]['kumiKata']);
    }

    /** @noinspection PhpPureFunctionMayProduceSideEffectsInspection */
    #[Pure] private static function printGeneralKata(int $rowIndex, Closure $callback): string
    {
        $output = '';
        foreach (ZoneIndex::row($rowIndex) as $column => $cellId) {
            $color  = ($column % 3 === 0) ? ConsoleColors::WHITE : ConsoleColors::SMOKE;
            $output .= $color->display(' | ');
            $output .= self::kataToConsole($callback($cellId));
        }
        $output .= ConsoleColors::WHITE->display(' | ') . PHP_EOL;
        return $output;
    }

    #[Pure] private static function kataToConsole(array $kata): string
    {
        $output = [];
        foreach ($kata as $value => $allowed) {
            $output[] = $allowed ? $value : ' ';
        }
        return ConsoleColors::SMOKE->display(implode('', $output));
    }

    #[Pure] private static function printValue(array $grid, int $rowIndex): string
    {
        $output = '';
        foreach (ZoneIndex::row($rowIndex) as $column => $cellId) {
            $color  = ($column % 3 === 0) ? ConsoleColors::WHITE : ConsoleColors::SMOKE;
            $output .= $color->display(' | ');
            $output .= self::valueToConsole($grid[$cellId]);
        }
        $output .= ConsoleColors::WHITE->display(' | ') . PHP_EOL;
        return $output;
    }

    #[Pure] private static function valueToConsole(array $cell): string
    {
        $color = ConsoleColors::fromEffect($cell['effect']);
        if ($cell['value'] !== null) {
            return $color->display(self::resizeContent(strval($cell['value'])));
        }
        if (strlen($cell['annotation']) > 0) {
            return $color->display(self::resizeContent($cell['annotation']));
        }
        return $color->display(self::resizeContent('_'));
    }

    #[Pure] private static function resizeContent(string $content): string
    {
        $size = strlen($content);
        if (9 < $size) {
            return substr($content, 0, 9);
        }
        $total = 9 - $size;
        $left  = floor($total / 2);
        $right = ceil($total / 2);
        return str_repeat(' ', $left) . $content . str_repeat(' ', $right);
    }

    #[Pure] private static function printLockedKata(array $grid, int $rowIndex): string
    {
        return self::printGeneralKata($rowIndex, fn(int $cellId) => $grid[$cellId]['lockedKata']);
    }


}