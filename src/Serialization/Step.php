<?php

namespace App\Serialization;

use JetBrains\PhpStorm\ArrayShape;
use JsonSerializable;

class Step implements JsonSerializable
{
    public function __construct(
        protected array  $grid,
        protected string $phase = '',
        protected string $canonicalPhaseName = '',
        /** @var string[] */
        protected array  $message = [],
        /** @var string[] */
        protected array  $proof = [],
        protected bool   $kumiKata = false,
        protected int    $level = 0
    ) {
    }

    #[ArrayShape(['title'         => "string",
                  'canonicalName' => "string",
                  'level'         => "int",
                  'kumiKata'      => "bool",
                  'messages'      => "array",
                  'proof'         => "array",
                  'grid'          => "array"])]
    public function jsonSerialize(): array
    {
        return [
            'title'         => $this->phase,
            'canonicalName' => $this->canonicalPhaseName,
            'level'         => $this->level,
            'kumiKata'      => $this->kumiKata,
            'messages'      => $this->message,
            'proof'         => $this->proof,
            'grid'          => $this->grid
        ];
    }
}