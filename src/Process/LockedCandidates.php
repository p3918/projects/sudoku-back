<?php

namespace App\Process;

use App\Loggers\LockedCandidatesLogger;
use App\Loggers\LoggerContract;
use App\Model\Cell;
use App\Model\Grid;
use JetBrains\PhpStorm\Pure;

class LockedCandidates extends ProcessorContract
{
    /** @var LockedCandidatesLogger */
    protected LoggerContract $logger;

    #[Pure] public function __construct(Grid $grid)
    {
        parent::__construct($grid, new LockedCandidatesLogger());
    }

    public function execute(): bool
    {
        for ($v = 0; $v < 9; $v++) {
            for ($blockRow = 0; $blockRow < 3; $blockRow++) {
                $this->reset();
                if ($this->doValueHorizontaly($v, $blockRow)) {
                    return true;
                }
            }
            for ($blockColumn = 0; $blockColumn < 3; $blockColumn++) {
                $this->reset();
                if ($this->doValueVerticaly($v, $blockColumn)) {
                    return $v;
                }
            }
        }
        return false;
    }

    private function doValueHorizontaly(int $value, int $blockRow): bool
    {
        // Pour chaque bloc du ruban, je note où la valeur est absente
        // Je fais un parcours horizontal, donc c'est la colonne qui varie
        $forbiddenRows = [];
        for ($blockColumn = 0; $blockColumn < 3; $blockColumn++) {
            $forbiddenRows[$blockColumn] = array_diff(
                [0, 1, 2],
                $this->grid->blocks[$blockRow * 3 + $blockColumn]->rowOf[$value]
            );
        }

        // Si 2 blocs du ruban parmis les 3 ont la même ligne absente,
        for ($rowInBlock = 0; $rowInBlock < 3; $rowInBlock++) {
            $lockedColumns = array_filter($forbiddenRows, fn(array $forbidden) => array_values($forbidden) == [$rowInBlock]);
            $rowId         = $blockRow * 3 + $rowInBlock;
            if (count($lockedColumns) !== 2) {
                continue;
            }

            $lockedColumns = array_keys($lockedColumns);

            // Alors le 3ᵉ bloc aura forcément la valeur présente dans cette ligne (donc pas dans les autres lignes)
            // Donc on va vérifier si c'est utile (cad si on savait déjà que la valeur était forcément sur cette ligne ou pas)
            $usefull = false;
            $this->grid->resetEffects();
            for ($blockColumn = 0; $blockColumn < 3; $blockColumn++) {
                $block = $this->grid->blocks[3 * $blockRow + $blockColumn];

                // Dans ces 2 blocks je note les 2 lignes juste dans un but d'affichage
                if (in_array($blockColumn, $lockedColumns)) {
                    $this->logger->logPossibleRows($block, $rowId, $value);
                    continue;
                }

                // J'enlève la valeur dans toutes les cases du block qui ne sont pas sur la ligne sélectionnée
                if ($block->rowOf[$value] != [$rowInBlock]) {
                    // Là on est sûrs que ça va être intéressant
                    $usefull = true;
                    $cells   = $block->filter(fn(Cell $cell) => $cell->row->id !== $rowId && $cell->kumiKata[$value]);
                    $this->logger->logAbsentRow($value, $blockRow, $lockedColumns, $rowInBlock, $cells, $block, $rowId);
                    foreach ($block as $cell) {
                        if ($cell->row->id !== $rowId && $cell->kumiKata[$value]) {
                            $cell->disableValue($value);
                        }
                    }
                }
            }

            // C'est gagné, pas besoin d'aller regarder les autres lignes
            if ($usefull) {
                return true;
            }
        }
        return false;
    }

    private function doValueVerticaly(int $value, int $blockColumn): bool
    {
        // Pour chaque bloc du ruban, je note où la valeur est absente
        $forbiddenColumns = [];
        for ($blockRow = 0; $blockRow < 3; $blockRow++) {
            $forbiddenColumns[$blockRow] = array_diff(
                [0, 1, 2],
                $this->grid->blocks[$blockRow * 3 + $blockColumn]->columnOf[$value]
            );
        }

        // Si 2 blocs du ruban parmis les 3 ont la même colonne absente,
        for ($columnInBlock = 0; $columnInBlock < 3; $columnInBlock++) {
            $lockedRows = array_filter($forbiddenColumns, fn(array $forbidden) => array_values($forbidden) == [$columnInBlock]);
            $columnId   = $blockColumn * 3 + $columnInBlock;
            if (count($lockedRows) !== 2) {
                continue;
            }

            $lockedRows = array_keys($lockedRows);

            // Alors le 3ᵉ bloc aura forcément la valeur présente dans cette colonne (donc pas dans les autres colonnes)
            // Donc on va vérifier si c'est utile (cad si on savait déjà que la valeur était forcément sur cette colonne ou pas)
            $usefull = false;
            $this->grid->resetEffects();
            for ($blockRow = 0; $blockRow < 3; $blockRow++) {
                $block = $this->grid->blocks[3 * $blockRow + $blockColumn];

                // Dans ces 2 blocks je note les 2 colonnes juste dans un but d'affichage
                if (in_array($blockRow, $lockedRows)) {
                    $this->logger->logPossibleColumns($block, $columnId, $value);
                    continue;
                }

                // J'enlève la valeur dans toutes les cases du block qui ne sont pas sur la colonne sélectionnée
                if ($block->columnOf[$value] != [$columnInBlock]) {
                    // Là on est sûrs que ça va être intéressant
                    $usefull = true;
                    $cells   = $block->filter(fn(Cell $cell) => $cell->column->id !== $columnId && $cell->kumiKata[$value]);
                    $this->logger->logAbsentColumn($value, $blockColumn, $lockedRows, $columnInBlock, $cells, $block, $columnId);
                    foreach ($block as $cell) {
                        $cell->setEffect(Cell::EFFECT_FORBIDDEN);
                        if ($cell->column->id !== $columnId && $cell->kumiKata[$value]) {
                            $cell->disableValue($value);
                            continue;
                        }
                    }
                }
            }

            // C'est gagné, pas besoin d'aller regarder les autres lignes
            if ($usefull) {
                return true;
            }
        }
        return false;
    }
}
