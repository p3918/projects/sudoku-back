<?php

namespace App\Controllers;

use App\Process\Initiator;
use App\Resolvers\Resolver;
use JetBrains\PhpStorm\NoReturn;

class ResolveController extends Controller
{

    #[NoReturn] public function dispatch(): void
    {
        if ($this->path === 'all-grids') {
            $this->doAllGrids();
        }
        if ($this->path === 'full') {
            $this->sendResponse(Resolver::doEverything($this->grid));
        }
        if ($this->path === 'list') {
            $this->sendResponse(Resolver::getList());
        }
        $this->sendResponse(Resolver::doSome($this->grid, $this->path));
    }

    #[NoReturn] private function doAllGrids()
    {
        $listFile  = __DIR__ . '/../../storage/grids/list.json';
        $gridsList = json_decode(file_get_contents($listFile));
        foreach ($gridsList as $gridName) {
            $grid = Initiator::initGrid(
                json_decode(file_get_contents(
                    __DIR__ . '/../../storage/grids/' . $gridName . '.json'), true
                )
            );
            Resolver::doEverything($grid);
            unset($grid);
        }
        echo json_encode([]);
        die();
    }
}