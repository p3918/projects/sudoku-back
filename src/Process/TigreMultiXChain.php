<?php

namespace App\Process;

use App\Loggers\LoggerContract;
use App\Loggers\TigreMultiXLogger;
use App\Model\Cell;
use App\Model\Edge;
use App\Model\Grid;
use App\Model\NewChain;
use App\Model\SelfEdge;
use JetBrains\PhpStorm\Pure;

class TigreMultiXChain extends NewChainerContract
{

    /** @var TigreMultiXLogger */
    protected LoggerContract $logger;

    #[Pure] public function __construct(Grid $grid)
    {
        parent::__construct($grid, new TigreMultiXLogger());
    }

    protected function neighbours(NewChain $currentChain): array
    {
        $lastNode = $currentChain->last();
        if (count($currentChain) === 1) {
            return array_filter(
                $lastNode->cell->neighbours(),
                fn(Edge $edge) => $edge->strong
            );
        }
        if ($lastNode instanceof SelfEdge) {
            return array_filter(
                $lastNode->cell->neighbours($lastNode->y),
                fn(Edge $edge) => $edge->strong
            );
        }

        return $lastNode->cell->selfNeighbours($lastNode->x);
    }

    protected function isValid(NewChain $chain): bool
    {
        if (!($chain->first()->cell->view($chain->last()->cell))) {
            return false;
        }
        $lastNode = $chain->last();
        if ($lastNode instanceof SelfEdge) {
            return false;
        }
        if ($chain->second()->x === $lastNode->x) {
            return false;
        }
        //@TODO voir le cas où je peux cloturer par un lien fort
        return true;
    }

    #[Pure] protected function cellIsValidStart(Cell $cell): bool
    {
        return true;
    }

    protected function tryToDo(NewChain $chain): bool
    {
        $a1 = $chain->second()->x;
        $aN = $chain->last()->x;

        if (!$chain->first()->cell->isAllowed($aN) && !$chain->last()->cell->isAllowed($a1)) {
            return false;
        }
        $this->logger->logTigreChain($chain, $a1, $aN);
        $chain->first()->cell->disableValue($aN);
        $chain->last()->cell->disableValue($a1);
        return true;
    }
}