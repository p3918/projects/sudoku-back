<?php

namespace App\Tests\Techniques;

use App\Process\Set;
use App\Tests\TestCase;

class LonelyTest extends TestCase
{

    public function testLonely()
    {
        $grid = self::loadJson(__DIR__ . '/../data/success-40-lonely-initial.json');
        $this->debugGrid($grid);
        $lonely = new Set($grid, 1);
        $step   = $lonely->execute();
        self::assertTrue($step);
        $step = $lonely->execute();
        self::assertTrue($step);
        $step = $lonely->execute();
        self::assertTrue($step);
        $this->debugGrid($grid);
        self::assertGrid(__DIR__ . '/../data/success-40-lonely-completed.json', $grid);
    }
}