<?php

namespace App\Tests\Techniques;

use App\Process\TigreAIC;
use App\Process\TigreMultiXChain;
use App\Tests\TestCase;

class TigreAICTest extends TestCase
{

    public function testTigreAIC()
    {
        $grid = self::loadJson(__DIR__ . '/../data/example-before-aic.json');
        $grid->resetEffects();
        $this->debugGrid($grid);
        $step = (new TigreAIC($grid))->execute();
        $this->debugGrid($grid);
        self::assertTrue($step);
        self::assertGrid(__DIR__ . '/../data/example-after-aic.json', $grid);
    }

    public function testTigreAlternatedRegularX()
    {
        $grid = self::loadJson(__DIR__ . '/../data/issue-55-before-aic.json');
        $grid->resetEffects();
        $this->debugGrid($grid);
        $step = (new TigreMultiXChain($grid))->execute();
        $this->debugGrid($grid);
        self::assertTrue($step);
        self::assertGrid(__DIR__ . '/../data/issue-55-after-aic.json', $grid);
    }
}