<?php

namespace App\Resolvers;

use App\Model\Grid;
use App\Process\KumiKata;
use App\Process\ProcessorContract;
use App\Serialization\StepsCollection;
use JetBrains\PhpStorm\Pure;

class TechniqueResolver extends AbstractResolver
{
    #[Pure] public function __construct(Grid $grid, ProcessorContract $processor, ?StepsCollection $allSteps = null)
    {
        parent::__construct($grid, $allSteps);

        $this->techniques            = [
            new KumiKata($this->grid),
            $processor
        ];
        $this->nextResolverOnSuccess = $this;
        $this->nextResolverOnFailure = null;
    }
}