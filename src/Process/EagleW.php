<?php

namespace App\Process;

use App\Helpers\SwitchVar;
use App\Loggers\EagleWLogger;
use App\Loggers\LoggerContract;
use App\Model\Cell;
use App\Model\Grid;
use JetBrains\PhpStorm\Pure;

class EagleW extends ProcessorContract
{
    /** @var EagleWLogger */
    protected LoggerContract $logger;

    #[Pure] public function __construct(Grid $grid)
    {
        parent::__construct($grid, new EagleWLogger());
    }

    public function execute(): bool
    {
        // On cherche des paires identiques, mais qui ne se voient pas
        $pairs = $this->grid->freeCells()->filter(fn(Cell $cell) => count($cell->allowedValues()) === 2);
        /**
         * @var Cell $wing1
         * @var Cell $wing2
         */
        foreach ($pairs->allPairs() as list($wing1, $wing2)) {
            if ($wing1->view($wing2)) {
                continue;
            }
            if (!empty(array_diff($wing1->allowedValues(), $wing2->allowedValues()))) {
                continue;
            }

            // Je choisis arbitrairement w et x dans la paire, en cas d'échec, je tenterai l'inverse
            list($w, $x) = $wing1->allowedValues();
            if ($this->doEagle($w, $x, $wing1, $wing2)) {
                return true;
            }
            SwitchVar::switchVars($w, $x);
            if ($this->doEagle($w, $x, $wing1, $wing2)) {
                return true;
            }
        }
        return false;
    }

    private function doEagle(int $w, int $x, Cell $wing1, Cell $wing2): bool
    {
        $potentialX1s = $wing1->viewedCells($x)
                              ->filter(fn(Cell $cell) => !$cell->view($wing2));
        $potentialX2s = $wing2->viewedCells($x)
                              ->filter(fn(Cell $cell) => !$cell->view($wing1));

        foreach ($potentialX1s as $x1) {
            foreach ($potentialX2s as $x2) {
                if (in_array($x, $x1->commonStrongLinks($x2))) {
                    $viewedCells = $wing1->viewedCells($w)->intersect($wing2->viewedCells($w));
                    if (count($viewedCells) > 0) {
                        $this->logger->eagleMessage($wing1, $wing2, $x1, $x2, $viewedCells, $w, $x);
                        $viewedCells->walk(fn(Cell $cell) => $cell->disableValue($w));
                        return true;
                    }
                }
            }
        }
        return false;
    }
}