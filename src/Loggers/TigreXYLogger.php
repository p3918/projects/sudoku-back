<?php

namespace App\Loggers;

use App\Model\Cell;
use App\Model\CellCollection;
use App\Model\NewChain;
use App\Model\SelfEdge;
use App\Serialization\Formatter;

class TigreXYLogger extends ChainerLogger
{
    protected int    $level         = self::LEVEL_SMART;
    protected string $phase         = 'Tigre - Chaine de paires';
    protected string $canonicalName = 'tigre_xy_chain';

    public function logTigreChain(NewChain $chain, CellCollection $viewedCells, int $value): void
    {
        $chain->collect()->addEffect(Cell::EFFECT_ELEM_2);
        $chain->first()->cell->setEffect(Cell::EFFECT_ELEM_1);
        $chain->last()->cell->setEffect(Cell::EFFECT_ELEM_1);
        $k = 0;
        foreach ($chain as $node) {
            if ($node instanceof SelfEdge) {
                continue;
            }
            $node->cell->setAnnotation(Formatter::alpha($k));
            $k++;
        }
        $viewedCells->addEffect(Cell::EFFECT_FORBIDDEN);
        $this->message[] = 'Chaine de paires en XY : ' . $chain . ', on supprime les candidats ' . Formatter::v($value)
                           . ' dans les cases vues par les 2 extrémités de la chaine : ' . $viewedCells;

        $this->proof[] = 'Si ' . $chain->first()->cell . ' = ' . Formatter::v($value) . ', alors ' . $viewedCells
                         . (count($viewedCells) > 1 ? ' sont différents de ' : ' est différent de ') . Formatter::v($value);

        $this->proof[] = 'Sinon, ';
        $isFirst       = true;
        foreach ($chain as $node) {
            if (!$node instanceof SelfEdge) {
                continue;
            }
            $this->proof[] = ($isFirst ? '- ' : '- donc ') . $node->cell . ' = ' . Formatter::v($node->y);
            $isFirst       = false;
        }
        $this->proof[] = '- donc ' . $viewedCells . (count($viewedCells) > 1 ? ' sont différents de ' : ' est différent de ') . Formatter::v($value);
    }
}
