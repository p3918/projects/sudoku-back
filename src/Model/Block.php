<?php

namespace App\Model;

use App\Helpers\CellIndex;
use App\Serialization\Formatter;
use JetBrains\PhpStorm\Pure;

class Block extends Zone
{
    public array $rowOf = [
        [0, 1, 2], // Le 0 peut être en ligne 0, en ligne 1 ou en ligne 2
        [0, 1, 2], // Le 1 peut être en ligne 0, en ligne 1 ou en ligne 2
        [0, 1, 2], // Le 2 ...
        [0, 1, 2], // etc ...
        [0, 1, 2],
        [0, 1, 2],
        [0, 1, 2],
        [0, 1, 2],
        [0, 1, 2],
    ];

    public array $columnOf = [
        [0, 1, 2], // Le 0 peut être en colonne 0, ou 1, ou 2
        [0, 1, 2], // Le 1 peut être en colonne 0, ou 1, ou 2
        [0, 1, 2], // Le 2 ...
        [0, 1, 2], // etc
        [0, 1, 2],
        [0, 1, 2],
        [0, 1, 2],
        [0, 1, 2],
        [0, 1, 2],
    ];

    /** @noinspection PhpPureFunctionMayProduceSideEffectsInspection */
    #[Pure] public function __construct(array $cells, Grid $grid, int $id)
    {
        parent::__construct($cells, $grid, $id);
        foreach ($this->cells as $cell) {
            $cell->block = $this;
        }
    }

    #[Pure] public function cellAt($r, $c): Cell
    {
        return $this->cells[3 * $r + $c];
    }

    public function setPosition(int $value, int $position): void
    {
        parent::setPosition($value, $position);
        $this->rowOf[$value]    = [CellIndex::inBlockRow($this[$position]->id)];
        $this->columnOf[$value] = [CellIndex::inBlockColumn($this[$position]->id)];
    }

    public function disableValue(int $value, int $cellId): void
    {
        parent::disableValue($value, $cellId);
        $this->recalculateRowColumn($value);
    }

    public function recalculateRowColumn(int $value): void
    {
        $this->rowOf[$value]    = [];
        $this->columnOf[$value] = [];
        foreach ($this->possiblePositions[$value] as $p) {
            $this->rowOf[$value][]    = CellIndex::inBlockRow($this->cells[$p]->id);
            $this->columnOf[$value][] = CellIndex::inBlockColumn($this->cells[$p]->id);
        }
        $this->rowOf[$value]    = array_values(array_unique($this->rowOf[$value]));
        $this->columnOf[$value] = array_values(array_unique($this->columnOf[$value]));
    }

    public function unsetPossiblePositions(int $value, array $positions): void
    {
        parent::unsetPossiblePositions($value, $positions);
        $this->recalculateRowColumn($value);
    }

    #[Pure] public function rowInGrid(int $row): int
    {
        return $this->blockRow() * 3 + $row;
    }

    #[Pure] public function blockRow(): int
    {
        return floor($this->id / 3);
    }

    #[Pure] public function columnInGrid(int $column): int
    {
        return $this->blockColumn() * 3 + $column;
    }

    #[Pure] public function blockColumn(): int
    {
        return $this->id % 3;
    }

    #[Pure] public function __toString(): string
    {
        if (Formatter::$human) {
            return 'B' . ($this->id + 1);
        }
        return 'B' . ($this->id);
    }
}