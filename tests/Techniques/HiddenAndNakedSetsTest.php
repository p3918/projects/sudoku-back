<?php

namespace App\Tests\Techniques;

use App\Process\HiddenSet;
use App\Process\NakedSet;
use App\Tests\TestCase;

class HiddenAndNakedSetsTest extends TestCase
{
    public function testHiddenPairs()
    {
        $grid = self::loadJson(__DIR__ . '/../data/success-77-before-hidden-pair.json');
        $this->debugGrid($grid) . PHP_EOL;
        $step = (new HiddenSet($grid, 2))->execute();
        self::assertEquals(22, $step);
        $this->debugGrid($grid) . PHP_EOL;
        self::assertGrid(__DIR__ . '/../data/success-77-after-hidden-pair.json', $grid);
    }

    public function testNakedTriple()
    {
        $grid = self::loadJson(__DIR__ . '/../data/example-triplet-isole-initial.json');
        $this->debugGrid($grid) . PHP_EOL;
        $step = (new NakedSet($grid, 3))->execute();
        $this->debugGrid($grid) . PHP_EOL;
        self::assertTrue($step);
        $step = (new NakedSet($grid, 3))->execute();
        $this->debugGrid($grid) . PHP_EOL;
        self::assertTrue($step);
        self::assertGrid(__DIR__ . '/../data/example-triplet-isole-response.json', $grid);
    }
}