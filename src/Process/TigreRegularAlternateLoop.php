<?php

namespace App\Process;

use App\Loggers\LoggerContract;
use App\Loggers\TigreRegularAlternateLoopLogger;
use App\Model\Cell;
use App\Model\CellCollection;
use App\Model\Edge;
use App\Model\Grid;
use App\Model\NewChain;
use JetBrains\PhpStorm\Pure;

class TigreRegularAlternateLoop extends NewChainerContract
{

    /** @var TigreRegularAlternateLoopLogger */
    protected LoggerContract $logger;

    #[Pure] public function __construct(Grid $grid)
    {
        parent::__construct($grid, new TigreRegularAlternateLoopLogger());
    }

    protected function neighbours(NewChain $currentChain): array
    {
        $neighbours = $this->findNeighbours($currentChain);
        foreach ($neighbours as $key => $neighbour) {
            // On évite les chaines inutilement longues (ie: les chemins indirects entre 2 cases de la chaine qui possèdent un lien direct)
            // MAIS attention on enlève le 1er élément, car c'est justement le but final d'arriver avec le début et la fin de la chaine qui se voient
            // et on enlève le dernier élément, car évidemment il est voisin du suivant et le chemin n'est pas indirect dans ce cas précis
            for ($k = 1; $k < count($currentChain) - 1; $k++) {
                if ($currentChain[$k]->cell->linked($neighbour)) {
                    unset($neighbours[$key]);
                }
            }
            // @TODO c'est peut être dommage que ce concept ne puisse pas s'appliquer aux autres scripts tel quel, à voir.
        }
        return array_values($neighbours);
    }

    protected function findNeighbours(NewChain $currentChain): array
    {
        $lastNode = $currentChain->last();
        if (count($currentChain) === 1) {
            return array_filter($lastNode->cell->neighbours(), fn(Edge $edge) => $edge->strong);
        }
        if ($lastNode->strong) {
            return array_map(
                fn(Edge $edge) => $edge->weaken(),
                $lastNode->cell->neighbours($lastNode->x)
            );
        }
        return array_filter($lastNode->cell->neighbours($lastNode->x), fn(Edge $edge) => $edge->strong);

    }

    protected function isValid(NewChain $chain): bool
    {
        if (!$chain->last()->strong) {
            return false;
        }
        if (!($chain->first()->cell->view($chain->last()->cell))) {
            return false;
        }
        return true;
    }

    #[Pure] protected function cellIsValidStart(Cell $cell): bool
    {
        return true;
    }

    #[Pure] protected function cellsToExplore(): CellCollection
    {
        return $this->grid->freeCells();
    }

    protected function tryToDo(NewChain $chain): bool
    {
        $viewedCells      = [];
        $allViewedCells   = new CellCollection();
        $value            = $chain[1]->x;
        $chain[0]->x      = $value;
        $chain[0]->strong = false;

        //A1A2->X1 ; A3A4->X2 ; A5A0->X3 ;
        //A(2k-1)A(2k)->Xk ; k in {1...size/2}
        for ($k = 0; $k < count($chain); $k++) {
            $edge     = $chain[$k];
            $nextEdge = $chain[($k + 1) % count($chain)];
            if ($nextEdge->strong) {
                continue;
            }
            $viewedByThePair = $edge->cell->viewedCells($nextEdge->x)
                                          ->intersect($nextEdge->cell->viewedCells($edge->x))
                                          ->diff($allViewedCells);
            if (count($viewedByThePair) > 0) {
                $viewedCells[$k] = $viewedByThePair;
                $allViewedCells  = $allViewedCells->merge($viewedCells[$k]);
            }
        }

        if (count($allViewedCells) === 0) {
            return false;
        }
        $this->logger->logTigreChain($chain, $viewedCells, $allViewedCells, $value);
        $allViewedCells->walk(fn(Cell $viewedCell) => $viewedCell->disableValue($value));
        return true;
    }
}