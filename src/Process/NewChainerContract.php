<?php

namespace App\Process;

use App\Model\Cell;
use App\Model\CellCollection;
use App\Model\Edge;
use App\Model\NewChain;
use App\Model\SelfEdge;
use JetBrains\PhpStorm\Pure;

/**
 * On s'inspire d'un parcours de graph en profondeur, en ajoutant quelques surcouches :
 * - une méthode abstraite 'neighbours' car la notion de 'voisinage' va changer en fonction des différents types de
 * chainage
 * - une méthode abstraite 'isValid' qu'on utilisera principalement sur les chainages complexes ou la notion de
 * 'voisinage' est trop complexe.
 *   ⇒ dans ces cas on calculera des chaines en étant pas sûrs qu'elles sont justes
 *      (on essaiera d'être le plus sûrs possibles, tant qu'au niveau perf ça reste plus intéressant que de vérifier a
 *      posteriori), et on les vérifiera à la fin via 'isValid'.
 *   ⇒ on utilisera également 'isValid' aussi sur les cas simples, car on a toujours des conditions globales
 *      (cases vues par les extrémités de la chaine, parité de la taille de la chaine)
 * - une notion de 'sous-chaines' car il nous les faut toutes :indeed:
 *
 */
abstract class NewChainerContract extends ProcessorContract
{
    public function execute(): bool
    {
        return $this->explore();
    }

    public function explore(): bool
    {
        foreach ($this->cellsToExplore() as $cell) {
            if (!$this->cellIsValidStart($cell)) {
                continue;
            }
            $firstNode = new Edge($cell);
            if ($this->exploreCell(new NewChain(1, [$firstNode]))) {
                return true;
            }
        }
        return false;
    }

    #[Pure] protected function cellsToExplore(): CellCollection
    {
        return $this->grid->freeCells();
    }

    protected abstract function cellIsValidStart(Cell $cell): bool;

    protected function exploreCell(NewChain $currentChain): bool
    {
        if (count($currentChain) >= 3 && $this->isValid($currentChain)) {
            $this->reset();
            if ($this->tryToDo($currentChain)) {
                return true;
            }
        }
        $neighbours = $this->neighbours($currentChain);
        foreach ($neighbours as $key => $neighbour) {
            if ($neighbour instanceof SelfEdge) {
                continue;
            }
            if ($currentChain->exists($neighbour->cell->id)) {
                unset($neighbours[$key]);
            }
        }
        if (empty($neighbours)) {
            return false;
        }
        foreach ($neighbours as $neighbour) {
            if ($this->exploreCell($currentChain->clone()->append($neighbour))) {
                return true;
            }
        }
        return false;
    }

    protected abstract function isValid(NewChain $chain): bool;

    protected abstract function tryToDo(NewChain $chain): bool;

    /**
     * @param NewChain $currentChain
     *
     * @return Edge[]
     */
    protected abstract function neighbours(NewChain $currentChain): array;
}