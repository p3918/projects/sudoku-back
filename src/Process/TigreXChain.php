<?php

namespace App\Process;

use App\Loggers\LoggerContract;
use App\Loggers\TigreXLogger;
use App\Model\Cell;
use App\Model\CellCollection;
use App\Model\Edge;
use App\Model\Grid;
use App\Model\NewChain;
use JetBrains\PhpStorm\Pure;

class TigreXChain extends NewChainerContract
{

    /** @var TigreXLogger */
    protected LoggerContract $logger;
    private int              $value = 0;

    #[Pure] public function __construct(Grid $grid)
    {
        parent::__construct($grid, new TigreXLogger());
    }

    public function execute(): bool
    {
        for ($this->value = 0; $this->value < 9; $this->value++) {
            if ($this->explore()) {
                return true;
            }
        }
        return false;
    }

    #[Pure] protected function cellsToExplore(): CellCollection
    {
        return $this->grid->possibleCells($this->value);
    }

    protected function neighbours(NewChain $currentChain): array
    {
        $lastNode = $currentChain->last();
        return array_filter($lastNode->cell->neighbours($this->value), fn(Edge $edge) => $edge->strong);
    }

    #[Pure] protected function isValid(NewChain $chain): bool
    {
        return ($chain->count() % 2 === 0);
    }

    #[Pure] protected function cellIsValidStart(Cell $cell): bool
    {
        return $cell->isAllowed($this->value);
    }

    protected function tryToDo(NewChain $chain): bool
    {
        $viewedCells = $this->grid->commonViewedCells($chain->first()->cell, $chain->last()->cell, $this->value)
                                  ->diff($chain->collect());
        if (count($viewedCells) === 0) {
            return false;
        }
        $this->logger->logTigreChain($chain->collect(), $viewedCells, $this->value);
        $viewedCells->walk(fn(Cell $cell) => $cell->disableValue($this->value));
        return true;
    }
}