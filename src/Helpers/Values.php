<?php

namespace App\Helpers;

class Values
{
    /**
     * @param int[] $values
     *
     * @return int[][]
     */
    public static function pairs(array $values): array
    {
        $pairs = [];
        for ($i = 0; $i < count($values); $i++) {
            for ($j = $i + 1; $j < count($values); $j++) {
                $pairs[] = [$values[$i], $values[$j]];
            }
        }
        return $pairs;
    }
}