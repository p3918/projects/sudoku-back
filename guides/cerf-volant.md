## Concept

```
-----------------------------------------
[   ,   ,   , |   ,   ,   , | __,   ,   ]
[   , XX,   , |   ,   ,   , | X1,   ,   ] 
[   ,   ,   , |   ,   ,   , | __,   ,   ]
-----------------------------------------
[   ,   ,   , |   ,   ,   , | __,   ,   ]
[   ,   ,   , |   ,   ,   , | __,   ,   ] 
[   ,   ,   , |   ,   ,   , | __,   ,   ]
-----------------------------------------
[   ,   ,   , |   ,   ,   , | C1,   ,   ]
[ __, X2, __, | __, __, __, | __, C2, C2]
[   ,   ,   , |   ,   ,   , | C1,   ,   ]
-----------------------------------------
```

Si le candidat est en X1,
  alors il n'est pas en XX.
Sinon, 
  le candidat est en C1 ou C1', 
  donc il n'est pas en C2 ni en C2'
  donc il est en X2
  donc il n'est pas en XX
Conclusion : on retire le candidat dans la case XX

## Algo

On boucle sur les combinaisons ligne/colonne

```
                              col
-----------------------------------------
[   ,   ,   , |   ,   ,   , | __,   ,   ]
[   , XX,   , |   ,   ,   , | X1,   ,   ] 
[   ,   ,   , |   ,   ,   , | __,   ,   ]
-----------------------------------------
[   ,   ,   , |   ,   ,   , | __,   ,   ]
[   ,   ,   , |   ,   ,   , | __,   ,   ] 
[   ,   ,   , |   ,   ,   , | __,   ,   ]
-----------------------------------------
[   ,   ,   , |   ,   ,   , | C1,   ,   ]
[ __, X2, __, | __, __, __, |  I, C2, C2] row
[   ,   ,   , |   ,   ,   , | C1,   ,   ]
-----------------------------------------
```

On cherche le block commun et les zones disjointes

```
                              col
-----------------------------------------
[   ,   ,   , |   ,   ,   , | Z1,   ,   ]
[   ,   ,   , |   ,   ,   , | Z1,   ,   ] 
[   ,   ,   , |   ,   ,   , | Z1,   ,   ]
-----------------------------------------
[   ,   ,   , |   ,   ,   , | Z1,   ,   ]
[   ,   ,   , |   ,   ,   , | Z1,   ,   ] 
[   ,   ,   , |   ,   ,   , | Z1,   ,   ]
-----------------------------------------
[   ,   ,   , |   ,   ,   , |           ]
[ Z2, Z2, Z2, | Z2, Z2, Z2, | block com.] row
[   ,   ,   , |   ,   ,   , |           ]
-----------------------------------------
```

Le candidat qu'on cherche doit apparaitre 
 - une seule fois dans Z1, 
 - une seule fois dans Z2
Et ne doit pas apparaitre dans la case I

=> on boucle sur les valeurs pour trouver un candidat qui répond à cette condition
Une fois trouvé on vérifie si la case XX contient bien le candidat (sinon ça ne sert à rien de l'enlever), et on l'enlève.
