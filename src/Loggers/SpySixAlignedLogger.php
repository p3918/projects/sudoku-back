<?php

namespace App\Loggers;

use App\Model\Cell;
use App\Model\CellCollection;
use App\Serialization\Formatter;

class SpySixAlignedLogger extends LoggerContract
{
    protected int    $level         = self::LEVEL_SMART;
    protected string $phase         = 'Danse des 6 espions alignés';
    protected string $canonicalName = '6_spy_aligned';

    public function logInvalidNotUnique(array $rectangle): void
    {
        $this->message[] = Formatter::e(Cell::EFFECT_FORBIDDEN,
            'Erreur, la zone ' . implode($rectangle) . ' forme un ensemble figé non unique.'
        );
        array_map(fn(Cell $cell) => $cell->setEffect(Cell::EFFECT_FORBIDDEN), $rectangle);
        $this->message[] = 'SVP, vérifiez que votre grille initiale est juste et possède une solution unique, et si oui veuillez signaler le bug via le formulaire ci-dessous';
    }

    /**
     * @param Cell[] $rectangle
     * @param int[]  $extras
     * @param int    $a
     * @param int    $b
     * @param int    $c
     * @param int    $k
     */
    public function logOnePosition(array $rectangle, array $extras, int $a, int $b, int $c, int $k): void
    {
        $this->colorizeRectangle($rectangle, $a, $b, $c);
        $this->proof[]   = 'Cela contredit l\'unicité de la solution, ' .
                           'donc on va faire en sorte d\'éviter ça en choisissant une des autres valeurs possibles (' . Formatter::v($extras) . ') en ' . $rectangle[$k] . '.';
        $this->message[] = 'Rectangle unique à 6 espions pour les valeurs ' . Formatter::v([$a, $b, $c])
                           . ' on choisit les valeurs ' . Formatter::v($extras) . ' en ' . $rectangle[$k] . '.';
    }

    /**
     * @param Cell[] $rectangle
     */
    private function colorizeRectangle(array $rectangle, int $a, int $b, int $c): void
    {
        $rectangle[0]->setEffect(Cell::EFFECT_ELEM_1);
        $rectangle[0]->setAnnotation('AB1');
        $rectangle[1]->setEffect(Cell::EFFECT_ELEM_1);
        $rectangle[1]->setAnnotation('AB2');
        $rectangle[2]->setEffect(Cell::EFFECT_ELEM_2);
        $rectangle[2]->setAnnotation('AC1');
        $rectangle[3]->setEffect(Cell::EFFECT_ELEM_2);
        $rectangle[3]->setAnnotation('AC2');
        $rectangle[4]->setEffect(Cell::EFFECT_ELEM_3);
        $rectangle[4]->setAnnotation('BC1');
        $rectangle[5]->setEffect(Cell::EFFECT_ELEM_3);
        $rectangle[5]->setAnnotation('BC2');

        $this->proof[] = 'Si on avait : ';
        $this->proof[] = '- ' . $rectangle[0] . ' = ' . Formatter::v($a) . ' ou ' . Formatter::v($b);
        $this->proof[] = '- ' . $rectangle[1] . ' = ' . Formatter::v($a) . ' ou ' . Formatter::v($b);
        $this->proof[] = '- ' . $rectangle[2] . ' = ' . Formatter::v($a) . ' ou ' . Formatter::v($c);
        $this->proof[] = '- ' . $rectangle[3] . ' = ' . Formatter::v($a) . ' ou ' . Formatter::v($c);
        $this->proof[] = '- ' . $rectangle[4] . ' = ' . Formatter::v($b) . ' ou ' . Formatter::v($c);
        $this->proof[] = '- ' . $rectangle[5] . ' = ' . Formatter::v($b) . ' ou ' . Formatter::v($c);
        $this->proof[] = 'Alors on aurait un modèle à solution double :';
        $this->proof[] = 'Solution 1 :';
        $this->proof[] = '- ' . $rectangle[0] . ' = ' . Formatter::v($a);
        $this->proof[] = '- ' . $rectangle[1] . ' = ' . Formatter::v($b);
        $this->proof[] = '- ' . $rectangle[2] . ' = ' . Formatter::v($c);
        $this->proof[] = '- ' . $rectangle[3] . ' = ' . Formatter::v($a);
        $this->proof[] = '- ' . $rectangle[4] . ' = ' . Formatter::v($b);
        $this->proof[] = '- ' . $rectangle[5] . ' = ' . Formatter::v($c);
        $this->proof[] = 'Solution 2 :';
        $this->proof[] = '- ' . $rectangle[0] . ' = ' . Formatter::v($b);
        $this->proof[] = '- ' . $rectangle[1] . ' = ' . Formatter::v($a);
        $this->proof[] = '- ' . $rectangle[2] . ' = ' . Formatter::v($a);
        $this->proof[] = '- ' . $rectangle[3] . ' = ' . Formatter::v($c);
        $this->proof[] = '- ' . $rectangle[4] . ' = ' . Formatter::v($c);
        $this->proof[] = '- ' . $rectangle[5] . ' = ' . Formatter::v($b);
        $this->proof[] = 'Ces 2 solutions conviennent et impliquent exactement les même conséquences dans le reste de la grille.';
    }

    /**
     * @param Cell[]         $rectangle
     * @param int            $extraValue
     * @param int            $a
     * @param int            $b
     * @param int            $c
     * @param CellCollection $viewedCells
     * @param int[]          $keys
     */
    public function logPlusOne(array $rectangle, int $extraValue, int $a, int $b, int $c, CellCollection $viewedCells, array $keys): void
    {
        $this->colorizeRectangle($rectangle, $a, $b, $c);
        $viewedCells->addEffect(Cell::EFFECT_FORBIDDEN);
        $extraValuePositions = implode(',', array_filter($rectangle, fn(Cell $cell, $key) => in_array($key, $keys)));
        $this->proof[]       = 'Cela contredit l\'unicité de la solution, ' .
                               'donc on va faire en sorte d\'éviter ça en choisissant la valeur ' . Formatter::v($extraValue)
                               . ' dans une des cases ou cela est possibles (' . $extraValuePositions . ')';
        $this->proof[]       = 'Ainsi, toutes les cases en dehors du rectangle qui voient ces cases ne pourront contenir le candidat ' . Formatter::v($extraValue);
        $this->message[]     = 'Rectangle unique à 6 espions pour les valeurs ' . Formatter::v([$a, $b, $c])
                               . ' on choisit la valeur ' . Formatter::v($extraValue) . ' parmis les cases ' . $extraValuePositions . ', et on élimine donc le candidat ' . Formatter::v($extraValue) .
                               ' dans ' . $viewedCells . '.';
    }

    /**
     * @param Cell[]         $rectangle
     * @param int[]          $extraValues
     * @param int            $a
     * @param int            $b
     * @param int            $c
     * @param CellCollection $viewedCells
     * @param int[]          $keys
     * @param Cell           $pair1
     * @param Cell           $pair2
     */
    public function logPlusThree(array $rectangle, array $extraValues, int $a, int $b, int $c, CellCollection $viewedCells, array $keys, Cell $pair1, Cell $pair2): void
    {
        $this->colorizeRectangle($rectangle, $a, $b, $c);
        $viewedCells->addEffect(Cell::EFFECT_FORBIDDEN);
        $pair1->setEffect(Cell::EFFECT_FOUND);
        $pair2->setEffect(Cell::EFFECT_FOUND);
        $extraValuePositions = implode(',', array_filter($rectangle, fn(Cell $cell, $key) => in_array($key, $keys)));
        $this->proof[]       = 'Cela contredit l\'unicité de la solution, ' .
                               'donc on va faire en sorte d\'éviter ça en choisissant une des valeurs ' . Formatter::v($extraValues)
                               . ' dans une des cases ou cela est possibles (' . $extraValuePositions . ')';
        list($x, $y, $z) = $extraValues;
        $this->proof[]   = 'Si on choisit la valeur ' . Formatter::v($x) . ' alors les cases ' . $pair1 . ' et ' . $pair2
                           . ' prendront les valeurs ' . Formatter::v([$y, $z]);
        $this->proof[]   = '- donc les cases ' . $viewedCells . ' ne contiendront ni ' . Formatter::v($x) . ' ni ' . Formatter::v($y) . ' ni ' . Formatter::v($z);
        $this->proof[]   = 'Sinon, si on choisit la valeur ' . Formatter::v($y) . ' alors les cases ' . $pair1 . ' et ' . $pair2
                           . ' prendront les valeurs ' . Formatter::v([$x, $z]);
        $this->proof[]   = '- donc les cases ' . $viewedCells . ' ne contiendront ni ' . Formatter::v($y) . ' ni ' . Formatter::v($x) . ' ni ' . Formatter::v($z);
        $this->proof[]   = 'Sinon, on choisit la valeur ' . Formatter::v($z) . ' et dans ce cas les cases ' . $pair1 . ' et ' . $pair2
                           . ' prendront les valeurs ' . Formatter::v([$x, $y]);
        $this->proof[]   = '- donc les cases ' . $viewedCells . ' ne contiendront ni ' . Formatter::v($z) . ' ni ' . Formatter::v($x) . ' ni ' . Formatter::v($y);
        $this->proof[]   = 'Ainsi, toutes les cases en dehors du rectangle qui voient ces cases et les paires '
                           . $pair1 . ' et ' . $pair2 . ' ne pourront contenir les candidats ' . Formatter::v($extraValues);
        $this->message[] = 'Rectangle unique à 6 espions pour les valeurs ' . Formatter::v([$a, $b, $c])
                           . ' on choisit une des valeurs ' . Formatter::v($extraValues) . ' parmis les cases ' . $extraValuePositions
                           . ', et les 2 autres valeurs dans les cases ' . $pair1 . ' et ' . $pair2 . '.';
        $this->message[] = 'On peut alors éliminer les candidats ' . Formatter::v($extraValues) .
                           ' dans ' . $viewedCells . '.';
    }

    /**
     * @param Cell[]         $rectangle
     * @param int[]          $extraValues
     * @param int            $a
     * @param int            $b
     * @param int            $c
     * @param CellCollection $viewedCells
     * @param int[]          $keys
     * @param Cell           $pair
     */
    public function logPlusTwo(array $rectangle, array $extraValues, int $a, int $b, int $c, CellCollection $viewedCells, array $keys, Cell $pair): void
    {
        $this->colorizeRectangle($rectangle, $a, $b, $c);
        $viewedCells->addEffect(Cell::EFFECT_FORBIDDEN);
        $pair->setEffect(Cell::EFFECT_FOUND);
        $extraValuePositions = implode(',', array_filter($rectangle, fn(Cell $cell, $key) => in_array($key, $keys), ARRAY_FILTER_USE_BOTH));
        $this->proof[]       = 'Cela contredit l\'unicité de la solution, ' .
                               'donc on va faire en sorte d\'éviter ça en choisissant une des valeurs ' . Formatter::v($extraValues)
                               . ' dans une des cases ou cela est possibles (' . $extraValuePositions . ')';
        list($x, $y) = $extraValues;
        $this->proof[]   = 'Si on choisit la valeur ' . Formatter::v($x) . ' alors la case ' . $pair . ' prendra la valeur ' . Formatter::v($y);
        $this->proof[]   = '- donc les cases ' . $viewedCells . ' ne contiendront ni ' . Formatter::v($x) . ' ni ' . Formatter::v($y);
        $this->proof[]   = 'Sinon, on choisit la valeur ' . Formatter::v($y) . ' et dans ce cas la case ' . $pair . ' prendra la valeur ' . Formatter::v($x);
        $this->proof[]   = '- donc les cases ' . $viewedCells . ' ne contiendront ni ' . Formatter::v($y) . ' ni ' . Formatter::v($x);
        $this->proof[]   = 'Ainsi, toutes les cases en dehors du rectangle qui voient ces cases et la paire ' . $pair . ' ne pourront contenir les candidats ' . Formatter::v($extraValues);
        $this->message[] = 'Rectangle unique à 6 espions pour les valeurs ' . Formatter::v([$a, $b, $c])
                           . ' on choisit une des valeurs ' . Formatter::v($extraValues) . ' parmis les cases ' . $extraValuePositions . ', et l\'autre valeur dans la case ' . $pair . '.';
        $this->message[] = 'On peut alors éliminer les candidats ' . Formatter::v($extraValues) .
                           ' dans ' . $viewedCells . '.';
    }
}