<?php

namespace App\Model;

use App\Helpers\CellIndex;
use JetBrains\PhpStorm\ArrayShape;
use JetBrains\PhpStorm\Pure;
use JsonSerializable;

class Cell implements JsonSerializable
{
    const EFFECT_ELEM_1    = 1;
    const EFFECT_FOUND     = 2;
    const EFFECT_ELEM_2    = 3;
    const EFFECT_FORBIDDEN = 4;
    const EFFECT_ELEM_3    = 5;
    const EFFECT_ELEM_4    = 6;
    public array            $kumiKata           = [true, true, true, true, true, true, true, true, true,];
    public array            $lockedKata         = [false, false, false, false, false, false, false, false, false];
    public string           $annotation         = '';
    public Row              $row;
    public Column           $column;
    public Block            $block;
    protected ?int          $value              = null;
    private ?int            $effect             = null;
    private ?CellCollection $allViewedCells     = null;
    private ?CellCollection $allFreeViewedCells = null;
    /** @var CellCollection[]|null */
    private array $viewedCells = [];
    /** @var SelfEdge[][]|null */
    private ?array $selfNeighbours = null;
    /** @var SelfEdge[]|null */
    private ?array $allSelfNeighbours = null;
    /** @var Edge[][]|null */
    private ?array $neighbours = null;
    /** @var Edge[]|null */
    private ?array $allNeighbours = null;

    public function __construct(public int $id, public Grid $grid) { }

    public function linked(Edge $edge): bool
    {
        if ($edge instanceof SelfEdge) {
            $myEdge = $this->selfNeighbours($edge->x, $edge->y);
            return !empty($myEdge) && $edge->lessThanOrEquals($myEdge);
        }
        if (!$edge instanceof DoubleEdge) {
            $myEdges = $this->neighbours($edge->x);
            foreach ($myEdges as $myEdge) {
                if ($edge->lessThanOrEquals($myEdge)) {
                    return true;
                }
            }
            return false;
        }
        $myEdges = array_map(
            fn(Edge $e) => new DoubleEdge($e->cell, false, $edge->x, $edge->y),
            array_filter(
                $edge->cell->neighbours($edge->x),
                fn($neighbour) => $neighbour->cell->isAllowed($edge->y)
            )
        );
        foreach ($myEdges as $myEdge) {
            if ($edge->lessThanOrEquals($myEdge)) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param int|null $value
     * @param int|null $secondValue
     *
     * @return SelfEdge[]|SelfEdge
     */
    public function selfNeighbours(int $value = null, int $secondValue = null): array|SelfEdge
    {
        if (is_null($this->selfNeighbours)) {
            $this->initializeNeighbours();
        }
        if (!is_null($secondValue) && !is_null($value)) {
            if (isset($this->selfNeighbours[$value]) && isset($this->selfNeighbours[$value][$secondValue])) {
                return $this->selfNeighbours[$value][$secondValue];
            }
            return [];
        }
        if (!is_null($value)) {
            if (isset($this->selfNeighbours[$value])) {
                return $this->selfNeighbours[$value];
            }
            return [];
        }
        return $this->allSelfNeighbours;
    }

    private function initializeNeighbours(): void
    {
        $this->neighbours        = [];
        $this->allNeighbours     = [];
        $this->selfNeighbours    = [];
        $this->allSelfNeighbours = [];

        foreach ($this->allowedValues() as $value) {
            if (!isset($this->selfNeighbours[$value])) {
                $this->selfNeighbours[$value] = [];
                $this->neighbours[$value]     = [];
            }
            foreach ($this->allowedValues() as $otherValue) {
                if ($value !== $otherValue) {
                    $this->selfNeighbours[$value][$otherValue] = new SelfEdge($this, count($this->allowedValues()) === 2, $value, $otherValue);
                    $this->allSelfNeighbours[]                 = $this->selfNeighbours[$value][$otherValue];
                }
            }

            foreach ($this->viewedCells($value) as $viewedCell) {
                $edge                       = new Edge($viewedCell, count($this->commonZone($viewedCell)->possiblePositions($value)) === 2, $value);
                $this->neighbours[$value][] = $edge;
                $this->allNeighbours[]      = $edge;
            }
        }
    }

    /** @return int[] */
    public function allowedValues(): array
    {
        return array_keys(array_filter($this->kumiKata));
    }

    public function viewedCells(?int $value = null): CellCollection
    {
        if (!is_null($value)) {
            if (!isset($this->viewedCells[$value])) {
                $this->viewedCells[$value] = $this->freeViewedCells()->filter(fn(Cell $cell) => $cell->isAllowed($value));
            }
            return $this->viewedCells[$value];
        }
        if (is_null($this->allViewedCells)) {
            $this->allViewedCells = $this->block->merge($this->row)->merge($this->column)->whereIdNotIn([$this->id]);
        }
        return $this->allViewedCells;
    }

    public function freeViewedCells(): CellCollection
    {
        if (is_null($this->allFreeViewedCells)) {
            $this->allFreeViewedCells = $this->viewedCells()->filter(fn(Cell $cell) => $cell->value() === null);
        }
        return $this->allFreeViewedCells;
    }

    public function value(): int|null
    {
        return $this->value;
    }

    #[Pure] public function isAllowed(int $value): bool
    {
        return $this->value() === $value || $this->kumiKata[$value];
    }

    public function commonZone(Cell $otherCell): ?Zone
    {
        if ($this->row->id === $otherCell->row->id) {
            return $this->row;
        }
        if ($this->column->id === $otherCell->column->id) {
            return $this->column;
        }
        if ($this->block->id === $otherCell->block->id) {
            return $this->block;
        }
        return null;
    }

    /**
     * @param int|null $value
     *
     * @return Edge[]
     */
    public function neighbours(?int $value = null): array
    {
        if (is_null($this->neighbours)) {
            $this->initializeNeighbours();
        }
        if (is_null($value)) {
            return $this->allNeighbours;
        }
        if (!isset($this->neighbours[$value])) {
            return [];
        }
        return $this->neighbours[$value];
    }

    public function setValue(int $value): void
    {
        $this->value = $value;
        unset($this->neighbours);
        unset($this->allNeighbours);
        unset($this->selfNeighbours);
        unset($this->allSelfNeighbours);
        $this->grid->positions[$value][] = $this->id;
        $rowInBlock                      = CellIndex::inBlockRow($this->id);
        $columnInBlock                   = CellIndex::inBlockColumn($this->id);
        $idInBlock                       = CellIndex::inBlockId($this->id);
        $blockRow                        = $this->block->blockRow();
        $blockColumn                     = $this->block->blockColumn();
        // - Dans les blocks verticaux différents de mon block
        for ($br = 0; $br < 3; $br++) {
            if ($br !== $blockRow) {
                // La position de cette valeur ne peut pas être dans les 3 cases qui sont sur la même colonne
                $threeCells = array_map(fn(int $r) => $r * 3 + $columnInBlock, [0, 1, 2]);
                $otherBlock = $this->grid->blockAt($br, $blockColumn);
                $otherBlock->unsetPossiblePositions($value, $threeCells);
                // Je dois répercuter sur les lignes qui contiennent ces cases
                foreach ($otherBlock->wherePosIn($threeCells) as $otherCell) {
                    $otherCell->row->unsetPossiblePositions($value, [$this->column->id]);
                }
                $otherBlock->recalculateRowColumn($value);
            }
        }
        // - Dans les blocks horizontaux différents de mon block
        for ($bc = 0; $bc < 3; $bc++) {
            if ($bc !== $blockColumn) {
                // La position de cette valeur ne peut pas être dans les 3 cases qui sont sur la même ligne
                $threeCells = array_map(fn(int $c) => $rowInBlock * 3 + $c, [0, 1, 2]);
                $otherBlock = $this->grid->blockAt($blockRow, $bc);
                $otherBlock->unsetPossiblePositions($value, $threeCells);
                // Je dois répercuter sur les colonnes qui contiennent ces cases
                foreach ($otherBlock->wherePosIn($threeCells) as $otherCell) {
                    $otherCell->column->unsetPossiblePositions($value, [$this->row->id]);
                }
                $otherBlock->recalculateRowColumn($value);
            }
        }
        // - Dans mon block, sur toutes les lignes, la valeur ne peut pas être dans les 3 cases communes avec mon block
        for ($r = 0; $r < 3; $r++) {
            $t          = ($r !== $rowInBlock) ? [0, 1, 2] : array_diff([0, 1, 2], [$columnInBlock]);
            $threeCells = array_map(fn(int $c) => $blockColumn * 3 + $c, $t);
            $this->grid->rows[3 * $blockRow + $r]->unsetPossiblePositions($value, $threeCells);
        }
        // - Dans mon block, sur toutes les colonnes, la valeur ne peut pas être dans les 3 cases communes avec mon block
        for ($c = 0; $c < 3; $c++) {
            $t          = ($c !== $columnInBlock) ? [0, 1, 2] : array_diff([0, 1, 2], [$rowInBlock]);
            $threeCells = array_map(fn(int $r) => $blockRow * 3 + $r, $t);
            $this->grid->columns[3 * $blockColumn + $c]->unsetPossiblePositions($value, $threeCells);
        }

        // - Dans mon block / ma ligne / ma colonne
        // Cette valeur ne peut être qu'ici
        $this->block->setPosition($value, $idInBlock);
        $this->row->setPosition($value, $this->column->id);
        $this->column->setPosition($value, $this->row->id);
        // Les autres valeurs ne peuvent pas être ici
        for ($otherValue = 0; $otherValue < 9; $otherValue++) {
            if ($otherValue !== $value) {
                $this->block->unsetPossiblePositions($otherValue, [$idInBlock]);
                $this->row->unsetPossiblePositions($otherValue, [$this->column->id]);
                $this->column->unsetPossiblePositions($otherValue, [$this->row->id]);
            }
        }
        $this->kumiKata   = array_fill(0, 9, false);
        $this->lockedKata = array_fill(0, 9, false);

        $this->grid->countValues++;
        unset($this->grid->freeCells[$this->id]);
    }

    public function disableValue(int $value): void
    {
        if (!$this->kumiKata[$value] && $this->grid->isInitialized()) {
            return; // Elle est déjà disabled, pas la peine de travailler
        }
        $this->kumiKata[$value]   = false;
        $this->lockedKata[$value] = false;
        $this->resetNeighbours();
        $this->viewedCells()->walk(fn(Cell $cell) => $cell->resetNeighbours());
        $this->row->disableValue($value, $this->id);
        $this->column->disableValue($value, $this->id);
        $this->block->disableValue($value, $this->id);
    }

    public function resetNeighbours(): void
    {
        $this->neighbours         = null;
        $this->allNeighbours      = [];
        $this->selfNeighbours     = null;
        $this->allSelfNeighbours  = [];
        $this->viewedCells        = [];
        $this->allFreeViewedCells = null;
    }

    public function fromArray(array $input): static
    {
        $this->lockedKata = $input['lockedKata'];
        $this->setEffect($input['effect'], $input['annotation']);
        return $this;
    }

    public function setEffect(int|null $effect, ?string $annotation = ''): void
    {
        $this->effect     = $effect;
        $this->annotation = empty($annotation) ? $this->annotation : $annotation;
        $this->grid->setDirty($this);
    }

    #[ArrayShape([
        'value'      => "int|null",
        'kumiKata'   => "bool[]",
        'lockedKata' => "bool[]",
        'effect'     => "int|null",
        'annotation' => "string"
    ])]
    #[Pure] public function jsonSerialize(): array
    {
        return [
            'value'      => $this->value,
            'kumiKata'   => $this->kumiKata,
            'lockedKata' => $this->lockedKata,
            'effect'     => $this->effect(),
            'annotation' => $this->annotation(),
        ];
    }

    public function effect(): ?int
    {
        return $this->effect;
    }

    public function annotation(): string
    {
        return $this->annotation;
    }

    public function __toString(): string
    {
        return ($this->effect ? '#' . $this->effect : '') .
               $this->annotation . (empty($this->annotation) ? '' : ':')
               . '[' . $this->row . ';' . $this->column . ']'
               . ($this->effect ? '#' : '');
    }

    public function view(Cell $otherCell): bool
    {
        return $this->row->id === $otherCell->row->id
               || $this->column->id === $otherCell->column->id
               || $this->block->id === $otherCell->block->id;
    }

    public function commonStrongLinks(Cell $otherCell): array
    {
        $zone = $this->commonZone($otherCell);
        if (is_null($zone)) {
            return [];
        }
        $commonValues      = array_intersect($this->allowedValues(), $otherCell->allowedValues());
        $commonStrongLinks = [];
        foreach ($commonValues as $value) {
            if (count($zone->possiblePositions($value)) === 2) {
                $commonStrongLinks[] = $value;
            }
        }
        return $commonStrongLinks;
    }

    #[Pure] public function hasStrongLink(Cell $otherCell, int $value): bool
    {
        $zone = $this->commonZone($otherCell);
        if (is_null($zone)) {
            return false;
        }
        return (count($zone->possiblePositions($value)) === 2);
    }

    public function resetEffect(): void
    {
        $this->effect     = null;
        $this->annotation = '';
    }

    public function setAnnotation(string $annotation = ''): void
    {
        $this->annotation = $annotation;
        $this->grid->setDirty($this);
    }

    public function id(): int
    {
        return $this->id;
    }


}