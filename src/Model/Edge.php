<?php

namespace App\Model;

use App\Serialization\Formatter;
use JetBrains\PhpStorm\Pure;

class Edge
{

    public function __construct(public Cell $cell, public bool $strong = false, public int $x = -1) { }

    #[Pure] public function __toString(): string
    {
        return '|' . $this->edge() . ' ' . $this->cell->id . '|';
    }

    #[Pure] public function edge(): string
    {
        return ($this->strong ? 'F' : 'f') . ($this->x > -1 ? (',' . Formatter::v($this->x)) : '');
    }

    public function equals(Edge $neighbour): bool
    {
        if (get_class($neighbour) !== get_class($this)) {
            return false;
        }
        if ($neighbour->cell->id !== $this->cell->id) {
            return false;
        }
        if ($neighbour->x !== $this->x) {
            return false;
        }
        return $neighbour->strong === $this->strong;
    }

    public function lessThanOrEquals(Edge $neighbour): bool
    {
        if (get_class($neighbour) !== get_class($this)) {
            return false;
        }
        if ($neighbour->cell->id !== $this->cell->id) {
            return false;
        }
        if ($neighbour->x !== $this->x) {
            return false;
        }
        // this <= neighbour :
        // this est fort, neighbour est fort     ==> 'this === neighbour'      ===> (&lte est VRAI)
        // this est faible, neighbour est faible ==> 'this === neighbour'      ===> (&lte est VRAI)
        // this est faible, neighbour est fort   ==> 'this < neighbours'       ===> (&lte est VRAI)
        // this est fort, neighbour est faible   ==> 'this > neighbours'       ===> (&lte est FAUX) (c'est ce qu'on fait ci-dessous)
        if ($this->strong && !$neighbour->strong) {
            return false;
        }
        return true;
    }

    #[Pure] public function weaken(): Edge
    {
        return $this->strong ? new static($this->cell, false, $this->x) : $this;
    }
}