<?php

namespace App\Controllers;

use App\Libs\GitLabClient\GitLabIssues;
use App\Process\Initiator;
use Exception;
use JetBrains\PhpStorm\NoReturn;

class ReportController extends Controller
{

    #[NoReturn] public function dispatch(): void
    {
        if ($this->method === 'PUT') {
            if (!isset($this->body['message']) || !is_string($this->body['message'])
                || !isset($this->body['grid1']) || !isset($this->body['grid2'])
            ) {
                $this->sendResponse(false);
            }
            try {
                Initiator::initGrid($this->body['grid1']);
                Initiator::initGrid($this->body['grid2']);
                Initiator::initGrid($this->body['gridI']);
            } catch (Exception $exception) {
                $this->sendResponse($exception->getMessage());
            }

            (new GitLabIssues())->createIssue('[' . $this->requestID . '] ' . htmlspecialchars($this->body['message']), ['Report'], $this->body);
            $this->sendResponse(true);
        }
        $this->sendResponse(false);
    }
}