
# Principe
Je suppose que le candidat recherché n'est présent qu'aux positions notées 'N' dans les lignes 1 et 7 :
On va à chaque fois étudier 2 possibilités :
- Soit le candidat se trouve à la position N1,
- Soit il ne s'y trouve pas
  Légende : N[0-9] = le candidat peut être à cette position
  __ = le candidat ne peut pas être à cette position
  '  ' = osef, il peut être à cette position ou pas ça nous est égal
  XX : je peux éliminer le candidat de cette position dans les 2 cas, donc je peux l'éliminer définitement
  ?? : j'ai pu éliminer le candidat seulement dans un des cas, c'est insuffisant

# X-WING - en ligne
```
----------------------------------------
[XX,   ,   , |   ,   ,   , | XX,   ,   ]
[N1, __, __, | __, __, __, | N2, __, __] <- xIndex1 = 1 - positions1 = [0,6]
[XX,   ,   , |   ,   ,   , | XX,   ,   ]
-------------------------------- -------
[XX,   ,   , |   ,   ,   , | XX,   ,   ]
[XX,   ,   , |   ,   ,   , | XX,   ,   ]
[XX,   ,   , |   ,   ,   , | XX,   ,   ]
-------------------------------- -------
[XX,   ,   , |   ,   ,   , | XX,   ,   ]
[N3, __, __, | __, __, __, | N4, __, __] <- xIndex2 = 7 - positions1 = [0,6]
[XX,   ,   , |   ,   ,   , | XX,   ,   ]
----------------------------------------
```
- Si le candidat est en N1
    - Alors je peux l'éliminer dans la colonne 0
    - Par conséquent il ne se trouve pas en N3, donc il se trouve forcément en N4,
      Donc je peux l'éliminer dans la colonne 6
- Sinon, le candidat n'est pas en N1 donc il est en N2
    - Alors je peux l'éliminer dans la colonne 6,
    - Par conséquent il ne se trouve pas en N4, donc il se trouve forcément en N3
      Donc je peux l'éliminer dans la colonne 0
      ==> finalement je peux éliminer définitivement le candidat dans les colonnes 0 et 6

## X-WING - en colonnes

```
----------------------------------------
[__,   ,   , |   ,   ,   , | __,   ,   ]
[N1, XX, XX, | XX, XX, XX, | N3, XX, XX] <- yIndex1
[__,   ,   , |   ,   ,   , | __,   ,   ]
-------------------------------- -------
[__,   ,   , |   ,   ,   , | __,   ,   ]
[__,   ,   , |   ,   ,   , | __,   ,   ]
[__,   ,   , |   ,   ,   , | __,   ,   ]
-------------------------------- -------
[__,   ,   , |   ,   ,   , | __,   ,   ]
[N2, XX, XX, | XX, XX, XX, | N4, XX, XX] <- yIndex2
[__,   ,   , |   ,   ,   , | __,   ,   ]
----------------------------------------
 ^- xIndex1 = 0 - positions1 = [1,7]
                             ^- xIndex1 = 6 - positions1 = [1,7]
```
- Si le candidat est en N1
    - Alors je peux l'éliminer dans la ligne 1
    - Par conséquent il ne se trouve pas en N3, donc il se trouve forcément en N4,
      Donc je peux l'éliminer dans la ligne 7
- Sinon, le candidat n'est pas en N1 donc il est en N2
    - Alors je peux l'éliminer dans la ligne 7,
    - Par conséquent il ne se trouve pas en N4, donc il se trouve forcément en N3
      Donc je peux l'éliminer dans la lign 1
      ==> finalement je peux éliminer définitivement le candidat dans les lignes 1 et 7

# FINNED X-WING - en ligne
```
----------------------------------------
[??,   ,   , |   ,   ,   , | ??,   ,   ]
[N1, __, __, | __, __, __, | N2, __, __] <- xIndex1 = 1 - positions1 = [0,6]
[??,   ,   , |   ,   ,   , | ??,   ,   ]
-------------------------------- -------
[??,   ,   , |   ,   ,   , | ??,   ,   ]
[??,   ,   , |   ,   ,   , | ??,   ,   ]
[??,   ,   , |   ,   ,   , | ??,   ,   ]
-------------------------------- -------
[XX,   ,   , |   ,   ,   , | ??,   ,   ]
[N3, N5, N6, | __, __, __, | N4, __, __] (il faut avoir N5 ou N6 ou les deux - dans la même ligne et le même block que N3)
[XX,   ,   , |   ,   ,   , | ??,   ,   ] ^- xIndex1 = 7 - positions1 = [0,1,2,6]
----------------------------------------
```
- Si le candidat est en N1
    - Alors je peux l'éliminer dans la colonne 0
- Sinon le candidat n'est pas en N1 donc il est forcément en N2
    - Donc il n'est pas en N4, donc il est forcément en N3 ou N5 ou N6
    - Alors je peux l'éliminer dans le block où se trouvent N3,N5,N6
      ==> finalement je peux éliminer définitement le candidat dans l'intersection (colonne 0, block 6)

# FINNED X-WING - en colonne
```
----------------------------------------
[__,   ,   , |   ,   ,   , | N5,   ,   ]
[N1, ??, ??, | ??, ??, ??, | N3, XX, XX]
[__,   ,   , |   ,   ,   , | N6,   ,   ] (il faut avoir N5 ou N6 ou les deux - dans la même colonne et le même block que N3)
-------------------------------- -------
[__,   ,   , |   ,   ,   , | __,   ,   ]
[__,   ,   , |   ,   ,   , | __,   ,   ]
[__,   ,   , |   ,   ,   , | __,   ,   ]
-------------------------------- -------
[__,   ,   , |   ,   ,   , | __,   ,   ]
[N2, ??, ??, | ??, ??, ??, | N4, ??, ??]
[__,   ,   , |   ,   ,   , | __,   ,   ]
----------------------------------------
 ^- xIndex1 = 0 - positions1 = [1,7]
                             ^- xIndex1 = 6 - positions1 = [0,1,2,7]
```
- Si le candidat est en N1
    - Alors je peux l'éliminer dans la ligne 1
- Sinon le candidat n'est pas en N1 donc il est forcément en N2
    - Donc il n'est pas en N4, donc il est forcément en N3 ou N5 ou N6
    - Alors je peux l'éliminer dans le block où se trouvent N3,N5,N6
      ==> finalement je peux éliminer définitement le candidat dans l'intersection (ligne 1 block 2)

# SASHIMI SIMPLE en ligne

```
----------------------------------------   or   * ----------------------------------------
[??, XX,   , |   ,   ,   , | ??,   ,   ]   or   * [??,   , XX, |   ,   ,   , | ??,   ,   ]
[N1, __, __, | __, __, __, | N2, __, __]   or   * [N1, __, __, | __, __, __, | N2, __, __] <- xIndex1 = 1 - positions1 = [0,6]
[??, XX,   , |   ,   ,   , | ??,   ,   ]   or   * [??,   , XX, |   ,   ,   , | ??,   ,   ]
-------------------------------- -------   or   * -------------------------------- -------
[??, ??,   , |   ,   ,   , | ??,   ,   ]   or   * [??,   ,   , |   ,   ,   , | ??,   ,   ]
[??, ??,   , |   ,   ,   , | ??,   ,   ]   or   * [??,   ,   , |   ,   ,   , | ??,   ,   ]
[??, ??,   , |   ,   ,   , | ??,   ,   ]   or   * [??,   ,   , |   ,   ,   , | ??,   ,   ]
-------------------------------- -------   or   * -------------------------------- -------
[XX, ??,   , |   ,   ,   , | ??,   ,   ]   or   * [XX,   , ??, |   ,   ,   , | ??,   ,   ]
[__, N5, __, | __, __, __, | N4, __, __]   or   * [__, __, N5, | __, __, __, | N4, __, __] (il faut avoir N5 - dans la même ligne et le même block que le N3 qu'on aurait eu dans un X-Wing)
[XX, ??,   , |   ,   ,   , | ??,   ,   ]   or   * [XX,   , ??, |   ,   ,   , | ??,   ,   ] ^- xIndex1 = 7 - positions1 = [1,6]
----------------------------------------   or   * ----------------------------------------
```
- Si le candidat est en N1
    - Alors je peux l'éliminer dans la colonne 0
    - Je peux aussi l'éliminer dans le block ou se trouve N1
- Sinon le candidat n'est pas en N1 donc il est forcément en N2
    - Donc il n'est pas en N4, donc il est forcément en N5
    - Alors je peux l'éliminer dans le block où se trouve N5 et dans la colonne où se trouve N5
      ==> finalement je peux éliminer définitement le candidat dans l'intersection (colonne 0, block 6) et dans l'intersection (colonne 1, block 0)

# SASHIMI SIMPLE en colonne
```
----------------------------------------  or  ----------------------------------------
[__, XX, XX, |   ,   ,   , | N5,   ,   ]  or  [__,   ,   , |   ,   ,   , | __,   ,   ]
[N1, ??, ??, | ??, ??, ??, | __, XX, XX]  or  [N1, ??, ??, | ??, ??, ??, | __, XX, XX]
[__,   ,   , |   ,   ,   , | __,   ,   ]  or  [__, XX, XX, |   ,   ,   , | N5,   ,   ] (il faut avoir N5 - dans la même colonne et le même block que le N3 qu'on aurait eu dans un X-Wing)
-------------------------------- -------  or  -------------------------------- -------
[__,   ,   , |   ,   ,   , | __,   ,   ]  or  [__,   ,   , |   ,   ,   , | __,   ,   ]
[__,   ,   , |   ,   ,   , | __,   ,   ]  or  [__,   ,   , |   ,   ,   , | __,   ,   ]
[__,   ,   , |   ,   ,   , | __,   ,   ]  or  [__,   ,   , |   ,   ,   , | __,   ,   ]
-------------------------------- -------  or  -------------------------------- -------
[__,   ,   , |   ,   ,   , | __,   ,   ]  or  [__,   ,   , |   ,   ,   , | __,   ,   ]
[N2, ??, ??, | ??, ??, ??, | N4, ??, ??]  or  [N2, ??, ??, | ??, ??, ??, | N4, ??, ??]
[__,   ,   , |   ,   ,   , | __,   ,   ]  or  [__,   ,   , |   ,   ,   , | __,   ,   ]
----------------------------------------  or  ----------------------------------------
 ^- xIndex1 = 0 - positions1 = [1,7]
                             ^- xIndex1 = 6 - positions1 = [0,7]
```
- Si le candidat est en N1
    - Alors je peux l'éliminer dans la ligne 1
    - Je peux aussi l'éliminer dans le block ou se trouve N1
- Sinon le candidat n'est pas en N1 donc il est forcément en N2
    - Donc il n'est pas en N4, donc il est forcément en N5
    - Alors je peux l'éliminer dans le block où se trouve N5 et dans la ligne où se trouve N5
      ==> finalement je peux éliminer définitement le candidat dans l'intersection (ligne 1, block 2) et dans l'intersection (ligne 0, block 0)

# SASHIMI DOUBLE en ligne
```
----------------------------------------
[??,   ,   , |   ,   ,   , | ??,   ,   ]
[N1, __, __, | __, __, __, | N2, __, __] <- xIndex1 = 1 - positions1 = [0,6]
[??,   ,   , |   ,   ,   , | ??,   ,   ]
-------------------------------- -------
[??, ??,   , |   ,   ,   , | ??,   ,   ]
[??, ??,   , |   ,   ,   , | ??,   ,   ]
[??, ??,   , |   ,   ,   , | ??,   ,   ]
-------------------------------- -------
[XX, ??,   , |   ,   ,   , | ??,   ,   ]
[__, N5, N6, | __, __, __, | N4, __, __]  (il faut avoir N5 et N6 - dans la même ligne et le même block que le N3 qu'on aurait eu dans un X-Wing)
[XX, ??,   , |   ,   ,   , | ??,   ,   ] ^- xIndex1 = 7 - positions1 = [1,2,6]
----------------------------------------
```
- Si le candidat est en N1
    - Alors je peux l'éliminer dans la colonne 0
- Sinon le candidat n'est pas en N1 donc il est forcément en N2
    - Donc il n'est pas en N4, donc il est forcément en N5 ou N6
    - Alors je peux l'éliminer dans le block où se trouve N5,N6
      ==> finalement je peux éliminer définitement le candidat dans l'intersection (colonne 0, block 6)

# SASHIMI DOUBLE en colonne

```
----------------------------------------
[__,   ,   , |   ,   ,   , | N5,   ,   ]
[N1, ??, ??, | ??, ??, ??, | __, XX, XX]
[__,   ,   , |   ,   ,   , | N6,   ,   ] (il faut avoir N5 et N6 - dans la même colonne et le même block que le N3 qu'on aurait eu dans un X-Wing)
-------------------------------- -------
[__,   ,   , |   ,   ,   , | __,   ,   ]
[__,   ,   , |   ,   ,   , | __,   ,   ]
[__,   ,   , |   ,   ,   , | __,   ,   ]
-------------------------------- -------
[__,   ,   , |   ,   ,   , | __,   ,   ]
[N2, ??, ??, | ??, ??, ??, | N4, ??, ??]
[__,   ,   , |   ,   ,   , | __,   ,   ]
----------------------------------------
 ^- xIndex1 = 0 - positions1 = [1,7]
                             ^- xIndex1 = 6 - positions1 = [0,2,7]
```
- Si le candidat est en N1
    - Alors je peux l'éliminer dans la ligne 1
- Sinon le candidat n'est pas en N1 donc il est forcément en N2
    - Donc il n'est pas en N4, donc il est forcément en N5 ou N6
    - Alors je peux l'éliminer dans le block où se trouve N5,N6
      ==> finalement je peux éliminer définitement le candidat dans l'intersection (ligne 1, block 2)


# Gratte-ciel en ligne
```
----------------------------------------
[??,   ,   , |   ,   ,   , | ??,   ,   ]
[N1, __, __, | __, __, __, | N2, __, __] <- $row1
[??,   ,   , |   ,   ,   , | ??,   ,   ]
-------------------------------- -------
[??, ??,   , |   ,   ,   , | ??,   ,   ]
[??, ??,   , |   ,   ,   , | ??,   ,   ]
[??, ??,   , |   ,   ,   , | ??,   ,   ]
-------------------------------- -------
[N7, __, __, |   ,   ,   , | ??,   ,   ]
[N3, N5, N6, | ??, ??, ??, | N4, ??, ??] <- $row2 = $zone
[N8, __, __, |   ,   ,   , | ??,   ,   ] 
----------------------------------------
```
- Si le candidat est en N1
    - Alors il n'est pas en N7,N3 ou N8,
    - Donc il est forcément en N5 ou N6
    - Donc il n'est pas en N4
- Sinon le candidat n'est pas en N1 donc il est forcément en N2
    - Donc il n'est pas en N4

Dans les 2 cas, je peux éliminer le candidat dans N4

# Gratte-ciel en colonne

```
----------------------------------------
[__,   ,   , |   ,   ,   , | N5, __, __]
[N1, ??, ??, | ??, ??, ??, | N3, N7, N8]
[__,   ,   , |   ,   ,   , | N6, __, __] 
-------------------------------- -------
[__,   ,   , |   ,   ,   , | ??,   ,   ]
[__,   ,   , |   ,   ,   , | ??,   ,   ]
[__,   ,   , |   ,   ,   , | ??,   ,   ]
-------------------------------- -------
[__,   ,   , |   ,   ,   , | ??,   ,   ]
[N2, ??, ??, | ??, ??, ??, | N4, ??, ??]
[__,   ,   , |   ,   ,   , | ??,   ,   ]
----------------------------------------
```

- Si le candidat est en N1
    - Alors il n'est pas en N7,N3 ou N8,
    - Donc il est forcément en N5 ou N6
    - Donc il n'est pas en N4
- Sinon le candidat n'est pas en N1 donc il est forcément en N2
    - Donc il n'est pas en N4

Dans les 2 cas, je peux éliminer le candidat dans N4