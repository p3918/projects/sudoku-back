FROM celinederoland/php8.1-composer:latest AS composer
COPY ./src /app/src
COPY composer.json /app/composer.json
COPY composer.lock /app/composer.lock
RUN mkdir /app/storage && mkdir /app/storage/gridLogs && mkdir /app/storage/grids && mkdir /app/storage/reports
COPY ./resources /app/storage/grids
WORKDIR /app
RUN composer install --no-dev

FROM composer AS composer-test
COPY ./tests /app/tests
RUN composer install --dev

FROM celinederoland/php8.1-apache AS php-apache
COPY --from=composer /app/src /var/www/html/src
COPY --from=composer /app/vendor /var/www/html/vendor
COPY --from=composer /app/storage /var/www/html/storage
RUN chown -R www-data:www-data /var/www/html/storage
COPY ./public /var/www/html/public
WORKDIR /var/www/html

FROM celinederoland/php8.1-pcov AS php-test
COPY --from=composer-test /app/src /app/src
COPY --from=composer-test /app/tests /app/tests
COPY --from=composer-test /app/vendor /app/vendor
COPY --from=composer-test /app/storage /app/storage
COPY ./phpunit-silent.xml /app/phpunit-silent.xml
WORKDIR /app
