<?php

namespace App\Loggers;

use App\Model\Zone;
use App\Serialization\Formatter;

class HiddenSetLogger extends LoggerContract
{
    protected int $level = self::LEVEL_ADVANCED;

    public function logSet(array $values, array $positions, Zone $zone): void
    {
        $this->phase = (count($values) === 2 ? 'Paire cachée' : 'Triplet caché');

        $this->proof[]   = 'Les valeurs ' . Formatter::v($values)
                           . ' ne peuvent être qu\'en positions ' . Formatter::v($positions)
                           . ' dans la zone ' . $zone;
        $this->proof[]   = 'Par conséquent, à ces positions là il ne peut y avoir que ces valeurs là.'
                           . ' On retire tous les autres candidats';
        $this->message[] = (count($values) === 2 ? 'Paire cachée' : 'Triplet caché')
                           . '. On retire tous les candidats autres que ' . Formatter::v($values) .
                           ' aux positions ' . Formatter::v($positions)
                           . ' dans la zone ' . $zone;
    }
}
