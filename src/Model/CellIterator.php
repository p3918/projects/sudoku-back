<?php

namespace App\Model;

use App\InvalidOperation;
use JetBrains\PhpStorm\Pure;

trait CellIterator
{
    protected int $size = 0;
    /** @var Cell[] */
    protected array $cells   = [];
    private int     $current = 0;

    #[Pure] public function count(): int
    {
        return $this->size;
    }

    #[Pure] public function offsetExists($offset): bool
    {
        return $offset >= 0 && $offset < $this->size;
    }

    #[Pure] public function offsetGet($offset): Cell
    {
        return $this->cells[$offset];
    }

    public function offsetSet($offset, $value): void
    {
        throw new InvalidOperation();
    }

    public function offsetUnset($offset): void
    {
        throw new InvalidOperation();
    }

    public function jsonSerialize(): array
    {
        return $this->cells;
    }

    public function current(): Cell
    {
        return $this[$this->current];
    }

    public function next(): void
    {
        $this->current += 1;
    }

    public function key(): int
    {
        return $this->current;
    }

    #[Pure] public function valid(): bool
    {
        return $this->current < $this->size;
    }

    public function rewind(): void
    {
        $this->current = 0;
    }
}