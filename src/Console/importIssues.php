<?php

use App\Libs\GitLabClient\GitLabIssues;

require __DIR__ . '/../../vendor/autoload.php';

$client = new GitLabIssues();

$issues = $client->listIssues();

$listFile  = __DIR__ . '/../../storage/grids/list.json';
$gridsList = json_decode(file_get_contents($listFile));

foreach ($issues as $issue) {
    if (!is_array($issue)
        || !isset($issue['description']['grid1'])
        || !isset($issue['description']['grid2'])
        || !isset($issue['description']['message'])
    ) {
        echo 'Issue ' . $issue['ref'] . ' doesn\'t match pattern ' . PHP_EOL;
        continue;
    }

    $gridName    = 'issue-' . substr($issue['ref'], 1);
    $grid1       = $issue['description']['grid1'];
    $grid2       = $issue['description']['grid2'];
    $gridsList[] = $gridName;

    $outputFile  = __DIR__ . '/../../storage/grids/' . $gridName . '.json';
    $outputFile2 = __DIR__ . '/../../tests/data/' . $gridName . '-initial.json';
    $outputFile3 = __DIR__ . '/../../tests/issues/' . $gridName . '.json';
    file_put_contents($outputFile, json_encode($grid1));
    file_put_contents($outputFile2, json_encode($grid2));
    file_put_contents($outputFile3, json_encode($issue['description']));

    $client->assignIssue($issue['iid']);
}
file_put_contents($listFile, json_encode($gridsList));


