<?php

namespace App\Controllers;

use App\Process\Initiator;
use App\Resolvers\Resolver;
use JetBrains\PhpStorm\NoReturn;

class GridController extends Controller
{

    #[NoReturn] public function dispatch(): void
    {
        $gridName = $this->path;
        if ($this->method === 'POST') {
            $this->store($gridName);
        }
        if ($gridName === 'list') {
            $this->list();
        }
        $this->retrieve($gridName);
    }

    /**
     * @param string $gridName
     */
    #[NoReturn] protected function store(string $gridName): void
    {
        $listFile    = __DIR__ . '/../../storage/grids/list.json';
        $gridsList   = json_decode(file_get_contents($listFile));
        $gridsList[] = $gridName;
        file_put_contents($listFile, json_encode($gridsList));
        $outputFile  = __DIR__ . '/../../storage/grids/' . $gridName . '.json';
        $outputFile2 = __DIR__ . '/../../tests/data/' . $gridName . '-initial.json';
        file_put_contents($outputFile, json_encode($this->grid));
        file_put_contents($outputFile2, json_encode($this->grid));
        $otherGrid = Initiator::initGrid(json_decode(file_get_contents($outputFile), true));
        Resolver::doEverything($otherGrid);
        $outputSolution = __DIR__ . '/../../tests/data/' . $gridName . '-solution.json';
        file_put_contents($outputSolution, json_encode($otherGrid));
        $this->sendResponse($this->grid);
    }

    #[NoReturn] protected function list()
    {
        $listFile = __DIR__ . '/../../storage/grids/list.json';
        $grids    = json_decode(file_get_contents($listFile));
        echo json_encode($grids);
        die();
    }

    #[NoReturn] protected function retrieve(string $gridName): void
    {
        $this->requestID = intval(microtime(true) * 1000);
        $this->body      = json_decode(file_get_contents(__DIR__ . '/../../storage/grids/' . $gridName . '.json'), true);
        $this->grid      = Initiator::initGrid($this->body);
        $this->sendResponse($this->grid);
    }
}