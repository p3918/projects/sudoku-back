<?php

namespace App\Process;

use App\Loggers\LoggerContract;
use App\Loggers\TigreRemotePairLogger;
use App\Model\Cell;
use App\Model\CellCollection;
use App\Model\DoubleEdge;
use App\Model\Edge;
use App\Model\Grid;
use App\Model\NewChain;
use App\Model\SelfEdge;
use JetBrains\PhpStorm\Pure;

class TigreRemotePair extends NewChainerContract
{
    /** @var TigreRemotePairLogger */
    protected LoggerContract $logger;

    #[Pure] public function __construct(public Grid $grid)
    {
        parent::__construct($grid, new TigreRemotePairLogger());
    }

    #[Pure] protected function cellsToExplore(): CellCollection
    {
        return $this->grid->freeCells();
    }

    protected function neighbours(NewChain $currentChain): array
    {
        $lastNode = $currentChain->last();

        if (count($currentChain) === 1) {
            return array_filter($lastNode->cell->selfNeighbours(), fn(SelfEdge $edge) => ($edge->strong && $edge->x < $edge->y));
        }

        if ($lastNode instanceof SelfEdge) {
            return array_map(
                fn(Edge $edge) => new DoubleEdge($edge->cell, false, $lastNode->x, $lastNode->y),
                array_filter(
                    $lastNode->cell->neighbours($lastNode->x),
                    fn($neighbour) => $neighbour->cell->isAllowed($lastNode->y) && count($neighbour->cell->allowedValues()) === 2
                )
            );
        }

        if ($lastNode instanceof DoubleEdge) {
            return [$lastNode->cell->selfNeighbours($lastNode->x, $lastNode->y)];
        }

        return [];
    }

    #[Pure] protected function isValid(NewChain $chain): bool
    {
        return ($chain->count() % 4 === 0);
    }

    protected function cellIsValidStart(Cell $cell): bool
    {
        return count($cell->allowedValues()) === 2;
    }

    protected function tryToDo(NewChain $chain): bool
    {
        $collection = $chain->collect();
        list($value1, $value2) = $collection->first()->allowedValues();
        $viewedCells = $this->grid->commonViewedCells($collection->first(), $collection->last())
                                  ->diff($collection)
                                  ->filter(fn(Cell $cell) => $cell->isAllowed($value1) || $cell->isAllowed($value2));
        if (count($viewedCells) === 0) {
            return false;
        }
        $this->logger->logRemotePairEven($chain, $viewedCells);
        $viewedCells->walk(function (Cell $cell) use ($value2, $value1) {
            $cell->disableValue($value1);
            $cell->disableValue($value2);
        });
        return true;
    }
}