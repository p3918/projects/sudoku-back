<?php

namespace App\Resolvers;

use App\Model\Grid;
use App\Process\ProcessorContract;
use App\Serialization\Step;
use App\Serialization\StepsCollection;
use JetBrains\PhpStorm\Pure;

class AbstractResolver
{
    public StepsCollection $logSteps;
    /** @var ProcessorContract[] */
    protected array             $techniques            = [];
    protected AbstractResolver  $nextResolverOnSuccess;
    protected ?AbstractResolver $nextResolverOnFailure = null;

    #[Pure] public function __construct(protected Grid $grid, ?StepsCollection $logSteps = null, protected bool $acceptFinalStep = true)
    {
        $this->logSteps = is_null($logSteps) ? (new StepsCollection()) : $logSteps;
    }

    /**
     * @return Step[]
     */
    public function steps(): array
    {
        return $this->logSteps->steps;
    }

    public function initialize()
    {
        $this->logSteps->steps[] = new Step(
            grid: json_decode(json_encode($this->grid), true),
            phase: 'Initialize grid',
            canonicalPhaseName: 'init',
            message: [],
            proof: [],
            kumiKata: false,
            level: 0
        );
    }

    public function execute(): bool
    {
        if ($this->grid->countValues === 81) {
            return false;
        }
        foreach ($this->techniques as $technique) {
            if ($technique->execute()) {
                $this->logSteps->steps[] = new Step(
                    grid: json_decode(json_encode($this->grid), true),
                    phase: $technique->phase(),
                    canonicalPhaseName: $technique->canonicalName(),
                    message: $technique->message(),
                    proof: $technique->proof(),
                    kumiKata: $this->grid->needKata,
                    level: $technique->level()
                );
                return $this->nextResolverOnSuccess->execute();
            }
        }
        if ($this->nextResolverOnFailure) {
            return $this->nextResolverOnFailure->execute();
        }
        return false;
    }
}
