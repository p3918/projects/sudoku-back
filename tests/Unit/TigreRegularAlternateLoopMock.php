<?php

namespace App\Tests\Unit;

use App\Model\Edge;
use App\Model\Grid;
use App\Model\NewChain;
use App\Process\TigreRegularAlternateLoop;
use JetBrains\PhpStorm\Pure;

class TigreRegularAlternateLoopMock extends TigreRegularAlternateLoop
{
    #[Pure] public function __construct(Grid $grid)
    {
        parent::__construct($grid);
    }

    public function neighboursOf0(): array
    {
        $cell  = new Edge($this->grid[0], false, 4);
        $chain = new NewChain(1, [$cell]);
        return $this->neighbours($chain);
    }

    public function neighboursOf3(): array
    {
        $cell1 = new Edge($this->grid[0], false, 4);
        $cell2 = new Edge($this->grid[3], true, 4);
        $chain = new NewChain(2, [$cell1, $cell2]);
        return $this->neighbours($chain);
    }

    public function neighboursOf30(): array
    {
        $cell1 = new Edge($this->grid[0], false, 4);
        $cell2 = new Edge($this->grid[3], true, 4);
        $cell3 = new Edge($this->grid[30], false, 4);
        $chain = new NewChain(3, [$cell1, $cell2, $cell3]);
        return $this->neighbours($chain);
    }

    public function neighboursOf35(): array
    {
        $cell1 = new Edge($this->grid[0], false, 4);
        $cell2 = new Edge($this->grid[3], true, 4);
        $cell3 = new Edge($this->grid[30], false, 4);
        $cell4 = new Edge($this->grid[35], true, 4);
        $chain = new NewChain(4, [$cell1, $cell2, $cell3, $cell4]);
        return $this->neighbours($chain);
    }

    public function neighboursOf71(): array
    {
        $cell1 = new Edge($this->grid[0], false, 4);
        $cell2 = new Edge($this->grid[3], true, 4);
        $cell3 = new Edge($this->grid[30], false, 4);
        $cell4 = new Edge($this->grid[35], true, 4);
        $cell5 = new Edge($this->grid[71], false, 4);
        $chain = new NewChain(5, [$cell1, $cell2, $cell3, $cell4, $cell5]);
        return $this->neighbours($chain);
    }

    public function isValidChainValid(): bool
    {
        $chain = $this->validChain();
        return $this->isValid($chain);
    }

    public function validChain(): NewChain
    {
        $cell1 = new Edge($this->grid[0], false, 4);
        $cell2 = new Edge($this->grid[3], true, 4);
        $cell3 = new Edge($this->grid[30], false, 4);
        $cell4 = new Edge($this->grid[35], true, 4);
        $cell5 = new Edge($this->grid[71], false, 4);
        $cell6 = new Edge($this->grid[63], true, 4);
        return new NewChain(6, [$cell1, $cell2, $cell3, $cell4, $cell5, $cell6]);
    }

    public function allNeighboursOf61(): array
    {
        return $this->findNeighbours($this->prettyLongChain());
    }

    public function prettyLongChain(): NewChain
    {
        $cell3 = new Edge($this->grid[30], false, 4);
        $cell4 = new Edge($this->grid[35], true, 4);
        $cell5 = new Edge($this->grid[43], false, 4);
        $cell6 = new Edge($this->grid[61], true, 4);
        return new NewChain(4, [$cell3, $cell4, $cell5, $cell6]);
    }

    public function neighboursOf61(): array
    {
        return $this->neighbours($this->prettyLongChain());
    }

    public function workOnValidChain(): bool
    {
        return $this->tryToDo($this->validChain());
    }
}