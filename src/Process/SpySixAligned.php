<?php

namespace App\Process;

use App\Helpers\CellIndex;
use App\Helpers\SwitchVar;
use App\Helpers\Values;
use App\Loggers\LoggerContract;
use App\Loggers\SpySixAlignedLogger;
use App\Model\Cell;
use App\Model\Grid;
use JetBrains\PhpStorm\Pure;

class SpySixAligned extends ProcessorContract
{

    /** @var SpySixAlignedLogger */
    protected LoggerContract $logger;

    #[Pure] public function __construct(Grid $grid)
    {
        parent::__construct($grid, new SpySixAlignedLogger());
    }

    public function execute(): bool
    {
        for ($ribbonIndex = 0; $ribbonIndex < 3; $ribbonIndex++) {
            if ($this->doVertical($ribbonIndex) || $this->doHorizontal($ribbonIndex)) {
                return true;
            }
        }
        return false;
    }

    public function doVertical(int $ribbonIndex): bool
    {
        list($block1, $block2, $block3) = [$this->grid->blockAt(0, $ribbonIndex), $this->grid->blockAt(1, $ribbonIndex),
                                           $this->grid->blockAt(2, $ribbonIndex)];
        $values = $block1->freeValues();
        $pairs  = Values::pairs($values);
        foreach ($pairs as $pair) {
            list($a, $b) = $pair;
            $containsAB = $block1->freeCells()->filter(fn(Cell $cell) => $cell->isAllowed($a) && $cell->isAllowed($b));

            foreach ($containsAB->allPairs() as list($ab1, $ab2)) {
                if ($ab1->row->id !== $ab2->row->id) {
                    continue;
                }
                $column1 = CellIndex::inBlockColumn($ab1->id);
                $column2 = CellIndex::inBlockColumn($ab2->id);
                for ($row1 = 0; $row1 < 3; $row1++) {
                    $ac1 = $block2->cellAt($row1, $column1);
                    $ac2 = $block2->cellAt($row1, $column2);
                    for ($row2 = 0; $row2 < 3; $row2++) {
                        $bc1 = $block3->cellAt($row2, $column1);
                        $bc2 = $block3->cellAt($row2, $column2);
                        if ($this->doIt($a, $b, $ab1, $ab2, $ac1, $ac2, $bc1, $bc2)) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    protected function doIt(int $a, int $b, Cell $ab1, Cell $ab2, Cell $ac1, Cell $ac2, Cell $bc1, Cell $bc2): bool
    {
        if (!$ac1->isAllowed($a) && $bc1->isAllowed($a) && $ac1->isAllowed($b)) {
            SwitchVar::switchVars($ac1, $bc1);
            SwitchVar::switchVars($ac2, $bc2);
        }
        if (!$ac1->isAllowed($a) || !$ac2->isAllowed($a) || !$bc1->isAllowed($b) || !$bc2->isAllowed($b)) {
            return false;
        }

        $potentialCs = array_diff(array_intersect(
            $ac1->allowedValues(), $ac2->allowedValues(), $bc1->allowedValues(), $bc2->allowedValues()
        ), [$a, $b]);
        foreach ($potentialCs as $c) {
            $rectangle = [$ab1, $ab2, $ac1, $ac2, $bc1, $bc2];
            /** @var int[][] $extras */
            $extras    = [
                array_diff($ab1->allowedValues(), [$a, $b]),
                array_diff($ab2->allowedValues(), [$a, $b]),
                array_diff($ac1->allowedValues(), [$a, $c]),
                array_diff($ac2->allowedValues(), [$a, $c]),
                array_diff($bc1->allowedValues(), [$b, $c]),
                array_diff($bc1->allowedValues(), [$b, $c]),
            ];
            $hasExtras = array_filter($extras, fn(array $values) => !empty($values));
            if (count($hasExtras) === 0) {
                $this->logger->logInvalidNotUnique($rectangle);
            }

            // - si on a un seul candidat supplémentaire, on le choisira pour éviter d'arriver à la configuration non unique
            // - si on a plusieurs candidats supplémentaires, mais à un seul emplacement, c'est un peu pareil, on choisira un de ces candidats, donc pas la paire
            if (count($hasExtras) === 1 && $this->doOnePosition($extras, $hasExtras, $rectangle, $a, $b, $c)) {
                return true;
            }
            $extraValues = array_values(array_unique(array_merge(...$extras)));

            // - si on a plusieurs fois le même candidat supplémentaire, on ne sait pas lequel choisir, mais il faudra bien en prendre un,
            // - on peut donc éliminer le candidat de toutes les cases en dehors de l'ensemble figé qui voient toutes les cases contenant x :
            if (count($extraValues) === 1 && $this->doPlusOne($extraValues, $hasExtras, $rectangle, $a, $b, $c)) {
                return true;
            }

            // - si on a 2 candidats supplémentaires, on ne sait pas lequel choisir, mais avec un peu de chance on peut se coupler avec une paire correspondante
            if (count($extraValues) === 2 && $this->doPlusTwo($extraValues, $hasExtras, $rectangle, $a, $b, $c)) {
                return true;
            }

            if (count($extraValues) === 3 && $this->doPlusThree($extraValues, $hasExtras, $rectangle, $a, $b, $c)) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param int[][] $extras
     * @param int[][] $hasExtras
     * @param Cell[]  $rectangle
     * @param int     $a
     * @param int     $b
     * @param int     $c
     *
     * @return bool
     */
    private function doOnePosition(array $extras, array $hasExtras, array $rectangle, int $a, int $b, int $c): bool
    {
        reset($hasExtras);
        $k      = key($hasExtras);
        $extras = $extras[$k];
        $this->logger->logOnePosition($rectangle, $extras, $a, $b, $c, $k);
        foreach (array_diff($rectangle[$k]->allowedValues(), $extras) as $v) {
            $rectangle[$k]->disableValue($v);
        }
        return true;
    }

    /**
     * @param int[]   $extraValues
     * @param int[][] $hasExtras
     * @param Cell[]  $rectangle
     * @param int     $a
     * @param int     $b
     * @param int     $c
     *
     * @return bool
     */
    private function doPlusOne(array $extraValues, array $hasExtras, array $rectangle, int $a, int $b, int $c): bool
    {
        $extraValue  = reset($extraValues);
        $keys        = array_keys($hasExtras);
        $viewedCells = $rectangle[$keys[0]]->viewedCells($extraValue);
        for ($k = 1; $k < count($keys); $k++) {
            $viewedCells = $viewedCells->intersect(
                $rectangle[$k]->viewedCells($extraValue)
            );
        }
        if (count($viewedCells) === 0) {
            return false;
        }
        $this->logger->logPlusOne($rectangle, $extraValue, $a, $b, $c, $viewedCells, $keys);
        $viewedCells->walk(fn(Cell $cell) => $cell->disableValue($extraValue));
        return true;
    }

    /**
     * @param int[]   $extraValues
     * @param int[][] $hasExtras
     * @param Cell[]  $rectangle
     * @param int     $a
     * @param int     $b
     * @param int     $c
     *
     * @return bool
     */
    private function doPlusTwo(array $extraValues, array $hasExtras, array $rectangle, int $a, int $b, int $c): bool
    {
        list($x, $y) = $extraValues;
        $keys        = array_keys($hasExtras);
        $viewedCells = $rectangle[$keys[0]]->viewedCells()->filter(fn(Cell $cell) => $cell->isAllowed($x) || $cell->isAllowed($y));
        for ($k = 1; $k < count($keys); $k++) {
            $viewedCells = $viewedCells->intersect(
                $rectangle[$k]->viewedCells()->filter(fn(Cell $cell) => $cell->isAllowed($x) || $cell->isAllowed($y))
            );
        }
        $pairs = $viewedCells->filter(fn(Cell $cell) => $cell->isAllowed($x) && $cell->isAllowed($y) && count($cell->allowedValues()) === 2);
        if (count($viewedCells) === 0 || count($viewedCells) === 1 || count($pairs) === 0) {
            return false;
        }
        $pair        = $pairs->first();
        $viewedCells = $viewedCells->except($pair)->intersect($pair->viewedCells());
        if (count($viewedCells) === 0) {
            return false;
        }
        $this->logger->logPlusTwo($rectangle, $extraValues, $a, $b, $c, $viewedCells, $keys, $pair);
        $viewedCells->walk(function (Cell $cell) use ($x, $y) {
            $cell->disableValue($x);
            $cell->disableValue($y);
        });
        return true;
    }

    /**
     * @param int[]   $extraValues
     * @param int[][] $hasExtras
     * @param Cell[]  $rectangle
     * @param int     $a
     * @param int     $b
     * @param int     $c
     *
     * @return bool
     */
    private function doPlusThree(array $extraValues, array $hasExtras, array $rectangle, int $a, int $b, int $c): bool
    {
        list($x, $y, $z) = $extraValues;
        $keys        = array_keys($hasExtras);
        $viewedCells = $rectangle[$keys[0]]->viewedCells()->filter(fn(Cell $cell) => $cell->isAllowed($x) || $cell->isAllowed($y));
        for ($k = 1; $k < count($keys); $k++) {
            $viewedCells = $viewedCells->intersect(
                $rectangle[$k]->viewedCells()->filter(fn(Cell $cell) => $cell->isAllowed($x) || $cell->isAllowed($y) || $cell->isAllowed($z))
            );
        }
        // @TODO ou pas, à voir, il y a d'autres combinaisons possibles qui seront en mesure de figer le triplet, là je considère uniquement le cas basique : (x,y) + (y,z)
        $triples = $viewedCells->filter(
            fn(Cell $cell) => count(array_diff($cell->allowedValues(), [$x, $y, $z])) === 0
                              && count($cell->allowedValues()) === 2
        );
        if (count($viewedCells) === 0 || count($viewedCells) === 1 || count($viewedCells) === 2 || count($triples) < 2) {
            return false;
        }
        $chosenTriples = null;
        $t1            = null;
        $t2            = null;
        foreach ($triples->allPairs() as $triplePair) {
            list($t1, $t2) = $triplePair;
            $coveredValues = array_unique(array_intersect(
                [$x, $y, $z], array_merge($t1->allowedValues(), $t2->allowedValues())
            ));
            if (count($coveredValues) === 3) {
                $chosenTriples = $triplePair;
                break;
            }
        }
        if (is_null($chosenTriples)) {
            return false;
        }
        $viewedCells = $viewedCells->except($t1)->except($t2)->intersect($t1->viewedCells())->intersect($t2->viewedCells());
        if (count($viewedCells) === 0) {
            return false;
        }
        $this->logger->logPlusThree($rectangle, $extraValues, $a, $b, $c, $viewedCells, $keys, $t1, $t2);
        $viewedCells->walk(function (Cell $cell) use ($z, $x, $y) {
            $cell->disableValue($x);
            $cell->disableValue($y);
            $cell->disableValue($z);
        });
        return true;
    }

    private function doHorizontal(int $ribbonIndex): bool
    {
        list($block1, $block2, $block3) = [$this->grid->blockAt($ribbonIndex, 0), $this->grid->blockAt($ribbonIndex, 1),
                                           $this->grid->blockAt($ribbonIndex, 2)];
        $values = $block1->freeValues();
        $pairs  = Values::pairs($values);
        foreach ($pairs as $pair) {
            list($a, $b) = $pair;
            $containsAB = $block1->freeCells()->filter(fn(Cell $cell) => $cell->isAllowed($a) && $cell->isAllowed($b));

            foreach ($containsAB->allPairs() as list($ab1, $ab2)) {
                if ($ab1->column->id !== $ab2->column->id) {
                    continue;
                }
                $row1 = CellIndex::inBlockRow($ab1->id);
                $row2 = CellIndex::inBlockRow($ab2->id);
                for ($column1 = 0; $column1 < 3; $column1++) {
                    $ac1 = $block2->cellAt($row1, $column1);
                    $ac2 = $block2->cellAt($row2, $column1);
                    for ($column2 = 0; $column2 < 3; $column2++) {
                        $bc1 = $block3->cellAt($row1, $column2);
                        $bc2 = $block3->cellAt($row2, $column2);
                        if ($this->doIt($a, $b, $ab1, $ab2, $ac1, $ac2, $bc1, $bc2)) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }


}