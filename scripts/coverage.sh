#!/usr/bin/env bash

docker-compose run --rm sudoku-testunit-cov php vendor/bin/phpunit --configuration /app/phpunit-with-coverage.xml
docker-compose run --rm sudoku-testunit-cov php vendor/bin/phpunit --configuration /app/phpunit-unit-only-with-coverage.xml
docker-compose run --rm sudoku-testunit-cov php vendor/bin/phpunit --configuration /app/phpunit-silent.xml