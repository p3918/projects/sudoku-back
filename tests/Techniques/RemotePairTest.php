<?php

namespace App\Tests\Techniques;

use App\Process\TigreRemotePair;
use App\Tests\TestCase;

class RemotePairTest extends TestCase
{

    public function testRemotePair()
    {
        $grid = self::loadJson(__DIR__ . '/../data/gm-5-p37-before-remote-pair.json');
        $this->debugGrid($grid);
        $step = (new TigreRemotePair($grid))->execute();
        $this->debugGrid($grid);
        self::assertTrue($step);
        self::assertGrid(__DIR__ . '/../data/gm-5-p37-after-remote-pair.json', $grid);
    }
}