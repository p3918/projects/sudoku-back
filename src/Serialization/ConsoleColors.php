<?php /** @noinspection PhpLanguageLevelInspection */

namespace App\Serialization;

use App\Model\Cell;
use JetBrains\PhpStorm\Pure;

/**
 * @codeCoverageIgnore
 */
enum ConsoleColors: string
{
    case NONE = '';
    case BLUE = '1;34';
    case GREEN = '1;32';
    case CYAN = '1;36';
    case RED = '1;31';
    case PURPLE = '1;35';
    case YELLOW = '1;33';
    case SMOKE = '0;37';
    case WHITE = '1;39';

    /**
     * @codeCoverageIgnore
     */
    #[Pure] public static function fromEffect(?int $effect): self
    {
        return match ($effect) {
            null                   => self::NONE,
            Cell::EFFECT_ELEM_1    => self::BLUE,
            Cell::EFFECT_FOUND     => self::GREEN,
            Cell::EFFECT_ELEM_2    => self::CYAN,
            Cell::EFFECT_FORBIDDEN => self::RED,
            Cell::EFFECT_ELEM_3    => self::PURPLE,
            Cell::EFFECT_ELEM_4    => self::YELLOW,
            default                => self::WHITE,
        };
    }

    /**
     * @codeCoverageIgnore
     */
    #[Pure] public function display(string $string): string
    {
        if ($this === self::NONE) {
            return $string;
        }
        return chr(27) . '[' . $this->value . 'm'
               . $string . chr(27) . '[0m';
    }
}