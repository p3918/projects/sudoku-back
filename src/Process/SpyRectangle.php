<?php

namespace App\Process;

use App\Loggers\LoggerContract;
use App\Loggers\SpyRectangleLogger;
use App\Model\Cell;
use App\Model\CellCollection;
use App\Model\Grid;
use JetBrains\PhpStorm\Pure;

class SpyRectangle extends ProcessorContract
{
    /** @var SpyRectangleLogger */
    protected LoggerContract $logger;

    #[Pure] public function __construct(Grid $grid)
    {
        parent::__construct($grid, new SpyRectangleLogger());
    }

    public function execute(): bool
    {
        // On cherche des cases disposées en rectangle, selon 2 lignes, 2 colonnes, 2 blocks, avec 2 ou 3 valeurs possibles
        foreach ($this->grid->freeCells() as $freeCell) {
            $row1 = $freeCell->row->id;
            $col1 = $freeCell->column->id;
            // Je continue le rectangle avec une case disposée sur la même colonne, ayant également 2 valeurs possibles ou plus
            for ($row2 = $row1 + 1; $row2 < 9; $row2++) {
                $allowedValues = $this->grid->cellAt($row2, $col1)->allowedValues();
                if (count($allowedValues) < 2) {
                    continue;
                }
                // Je vais chercher 2 cases en face
                for ($col2 = $col1 + 1; $col2 < 9; $col2++) {
                    $this->reset();
                    if ($this->analyzeRectangle($row1, $row2, $col1, $col2)) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    private function analyzeRectangle(int $row1, int $row2, int $col1, int $col2): bool
    {
        /** @var Cell[] $rect */
        $rect           = [
            $this->grid->cellAt($row1, $col1),
            $this->grid->cellAt($row1, $col2),
            $this->grid->cellAt($row2, $col1),
            $this->grid->cellAt($row2, $col2),
        ];
        $rectCollection = new CellCollection(4, $rect);
        $blocks         = array_unique(array_map(fn(Cell $cell) => $cell->block->id, $rect));
        if (count($blocks) != 2) {
            return false;
        }

        $countRect = array_map(fn(Cell $cell) => count($cell->allowedValues()), $rect);
        $pairs     = array_filter($countRect, fn(int $count) => $count === 2);
        if (count($pairs) === 3) {
            return $this->doV1($countRect, $pairs, $rect, $rectCollection);
        }
        if (count($pairs) === 2) {
            return $this->doV2($countRect, $pairs, $rect, $rectCollection);
        }
        return false;
    }

    /**
     * @param int[]          $countRect
     * @param int[]          $pairs
     * @param Cell[]         $rect
     * @param CellCollection $rectCollection
     *
     * @return bool
     */
    private function doV1(array $countRect, array $pairs, array $rect, CellCollection $rectCollection): bool
    {
        $biggerSets = array_filter($countRect, fn(int $count) => $count > 2);
        if (count($biggerSets) !== 1) {
            return false;
        }

        reset($pairs);
        $firstPair = key($pairs);
        // Il faut vérifier que ce sont les même paires
        if (!$this->areSamePairs($firstPair, $pairs, $rect)) {
            return false;
        }

        // Ensuite, il faut vérifier que l'ensemble plus grand contient les 2 valeurs des paires
        reset($biggerSets);
        $biggerSet = key($biggerSets);

        if (!$this->setContainPair($firstPair, $rect, $biggerSet)) {
            return false;
        }

        $this->logger->logRectangleV1(array_keys($pairs), key($biggerSets), $rectCollection, $rect[$firstPair]->allowedValues());

        $cell = $rect[key($biggerSets)];
        $v    = array_diff($cell->allowedValues(), $rect[$firstPair]->allowedValues());
        if (count($v) === 1) {
            $cell->setValue(reset($v));
            return true;
        }
        foreach ($rect[$firstPair]->allowedValues() as $valueToRemove) {
            $cell->disableValue($valueToRemove);
        }
        return true;
    }

    /**
     * @param int    $firstPair
     * @param int[]  $pairs
     * @param Cell[] $rect
     *
     * @return bool
     */
    private function areSamePairs(int $firstPair, array $pairs, array $rect): bool
    {
        $firstKey = implode('.', $rect[$firstPair]->allowedValues());
        foreach (array_keys($pairs) as $pair) {
            $secondKey = implode('.', $rect[$pair]->allowedValues());
            if ($secondKey !== $firstKey) {
                return false;
            }
        }
        return true;
    }

    /**
     * @param int    $firstPair
     * @param Cell[] $rect
     * @param int    $biggerSet
     *
     * @return bool
     */
    private function setContainPair(int $firstPair, array $rect, int $biggerSet): bool
    {
        $intersectTripleAndFirstPair = array_intersect($rect[$biggerSet]->allowedValues(), $rect[$firstPair]->allowedValues());
        if (count($intersectTripleAndFirstPair) !== 2) {
            return false;
        }
        return true;
    }

    /**
     * @param int[]          $countRect
     * @param int[]          $pairs
     * @param Cell[]         $rect
     * @param CellCollection $rectCollection
     *
     * @return bool
     */
    private function doV2(array $countRect, array $pairs, array $rect, CellCollection $rectCollection): bool
    {
        $triples = array_filter($countRect, fn(int $count) => $count === 3);
        if (count($triples) !== 2) {
            return false;
        }
        // On vérifie que les triplets contiennent la paire
        reset($pairs);
        $firstPair = key($pairs);
        foreach (array_keys($triples) as $triple) {
            if (!$this->setContainPair($firstPair, $rect, $triple)) {
                return false;
            }
        }
        foreach (array_keys($pairs) as $pKey) {
            $rect[$pKey]->setEffect(Cell::EFFECT_ELEM_1);
        }

        // On vérifie que le 3ᵉ candidat est commun aux 2 triplets
        list($k1, $k2) = array_keys($triples);
        list($cell1, $cell2) = [$rect[$k1], $rect[$k2]];
        list($v1, $v2) = [
            array_diff($cell1->allowedValues(), $rect[$firstPair]->allowedValues()),
            array_diff($cell2->allowedValues(), $rect[$firstPair]->allowedValues())
        ];
        list($v1, $v2) = [reset($v1), reset($v2)];
        if ($v1 !== $v2) {
            $this->grid->resetEffects();
            return false;
        }
        $cell1->setEffect(Cell::EFFECT_ELEM_2);
        $cell2->setEffect(Cell::EFFECT_ELEM_2);

        // On va pouvoir enlever le candidat commun dans toutes les cases qui voient les 2 triplets.
        $cells = $this->grid->commonViewedCells($cell1, $cell2)
                            ->freeCells()
                            ->filter(fn(Cell $cell) => $cell->id !== $cell1->id && $cell->id !== $cell2->id)
                            ->filter(fn(Cell $cell) => $cell->kumiKata[$v1]);
        if (count($cells) === 0) {
            return false;
        }
        foreach ($cells as $cell) {
            $cell->disableValue($v1);
            $cell->setEffect(Cell::EFFECT_FOUND);
        }
        $this->logger->logRectangleV2($cell1, $cell2, $rectCollection, $v1, $rect[$firstPair]->allowedValues(), $cells);
        return true;
    }
}
