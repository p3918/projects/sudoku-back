<?php

namespace App\Process;

use App\Loggers\LoggerContract;
use App\Loggers\TigreRemotePairLogger;
use App\Model\NewChain;
use App\Model\Zone;
use JetBrains\PhpStorm\Pure;

class TigreRemotePairMinusOne extends TigreRemotePair
{
    /** @var TigreRemotePairLogger */
    protected LoggerContract $logger;

    #[Pure] protected function isValid(NewChain $chain): bool
    {
        return ($chain->count() % 4 === 2);
    }

    protected function tryToDo(NewChain $chain): bool
    {
        $collection = $chain->collect();
        list($value1, $value2) = $collection->first()->allowedValues();
        foreach ($this->grid->rows as $row) {
            if ($collection->first()->row->id === $row->id || $collection->last()->row->id === $row->id) {
                continue;
            }
            $position1 = $collection->first()->column->id;
            $position2 = $collection->last()->column->id;
            if ($this->tryToProject($row, $position1, $position2, $chain, $value1, $value2)) {
                return true;
            }
        }
        foreach ($this->grid->columns as $column) {
            if ($collection->first()->column->id === $column->id || $collection->last()->column->id === $column->id) {
                continue;
            }
            $position1 = $collection->first()->row->id;
            $position2 = $collection->last()->row->id;
            if ($this->tryToProject($column, $position1, $position2, $chain, $value1, $value2)) {
                return true;
            }
        }

        return false;
    }

    protected function tryToProject(Zone $zone, int $position1, int $position2, NewChain $chain, int $value1, int $value2): bool
    {
        if (count(array_intersect($zone[$position1]->allowedValues(), $zone[$position2]->allowedValues(), $chain->first()->cell->allowedValues())) === 0) {
            return false;
        }
        $otherPositions1 = array_diff($zone->possiblePositions($value1), [$position1, $position2]);
        $otherPositions2 = array_diff($zone->possiblePositions($value2), [$position1, $position2]);
        if (count($otherPositions1) !== 1 || count($otherPositions2) !== 1 || count(array_intersect($otherPositions1, $otherPositions2)) !== 1) {
            return false;
        }
        $position    = reset($otherPositions1);
        $otherValues = array_diff($zone[$position]->allowedValues(), $chain->first()->cell->allowedValues());
        if (count($otherValues) === 0) {
            return false;
        }
        $this->logger->logRemotePairOdd($chain, $zone, $position1, $position2, $position);
        foreach ($otherValues as $v) {
            $zone[$position]->disableValue($v);
        }
        return true;
    }
}