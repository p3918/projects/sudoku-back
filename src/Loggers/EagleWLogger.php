<?php

namespace App\Loggers;

use App\Model\Cell;
use App\Model\CellCollection;
use App\Serialization\Formatter;

class EagleWLogger extends LoggerContract
{
    protected int    $level         = self::LEVEL_SMART;
    protected string $phase         = 'Aile de l\'aigle';
    protected string $canonicalName = 'eagle_wing';

    public function eagleMessage(Cell $wing1, Cell $wing2, Cell $x1, Cell $x2, CellCollection $viewedCells, int $w, int $x): void
    {
        $wing1->setEffect(Cell::EFFECT_ELEM_1);
        $wing1->setAnnotation('W1');
        $wing2->setEffect(Cell::EFFECT_ELEM_1);
        $wing2->setAnnotation('W2');
        $x1->setEffect(Cell::EFFECT_ELEM_2);
        $x1->setAnnotation('X1');
        $x2->setEffect(Cell::EFFECT_ELEM_2);
        $x2->setAnnotation('X2');
        $viewedCells->addEffect(Cell::EFFECT_FORBIDDEN);
        $viewedCells->addAnnotation('V');
        $this->message[] = 'Les ailes sont ' . $wing1 . ' et ' . $wing2 . ' et les serres sont ' . $x1 . ' et ' . $x2;
        $this->message[] = 'On peut éliminer le candidat ' . Formatter::v($w) . ' des cases vues par les deux ailes : ' . $viewedCells;
        $this->proof[]   = 'Si ' . $wing1 . ' = ' . Formatter::v($w) . ', alors ' . $viewedCells . ' sont différents de ' . Formatter::v($w);
        $this->proof[]   = 'Sinon, ' . $wing1 . ' = ' . Formatter::v($x) . ',';
        $this->proof[]   = '- donc ' . $x1 . ' est différent de ' . Formatter::v($x);
        $this->proof[]   = '- donc ' . $x2 . ' = ' . Formatter::v($x);
        $this->proof[]   = '- donc ' . $wing2 . ' = ' . Formatter::v($w);
        $this->proof[]   = '- donc ' . $viewedCells . ' sont différents de ' . Formatter::v($w);
        $this->proof[]   = 'Dans les deux cas, on a : ' . $viewedCells . ' sont différents de ' . Formatter::v($w);
    }
}
