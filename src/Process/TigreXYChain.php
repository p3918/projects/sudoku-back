<?php

namespace App\Process;

use App\Loggers\LoggerContract;
use App\Loggers\TigreXYLogger;
use App\Model\Cell;
use App\Model\CellCollection;
use App\Model\Edge;
use App\Model\Grid;
use App\Model\NewChain;
use App\Model\SelfEdge;
use JetBrains\PhpStorm\Pure;

class TigreXYChain extends NewChainerContract
{

    /** @var TigreXYLogger */
    protected LoggerContract $logger;

    #[Pure] public function __construct(Grid $grid)
    {
        parent::__construct($grid, new TigreXYLogger());
    }

    protected function cellsToExplore(): CellCollection
    {
        return $this->grid->freeCells()->filter(fn(Cell $cell) => count($cell->allowedValues()) === 2);
    }

    protected function neighbours(NewChain $currentChain): array
    {
        $lastNode = $currentChain->last();

        if (count($currentChain) === 1) {
            return $lastNode->cell->selfNeighbours();
        }
        if ($lastNode instanceof SelfEdge) {
            return array_filter(
                $lastNode->cell->neighbours($lastNode->y),
                fn(Edge $edge) => count($edge->cell->allowedValues()) === 2
            );
        }
        return $lastNode->cell->selfNeighbours($lastNode->x);
        // @TODO Vérifier si on évite les chaines inutilement longues, si on voit qu'on emprunte un chemin indirect pour relier des points entre lesquels il existe un chemin direct, on arrête.
    }

    protected function isValid(NewChain $chain): bool
    {
        $lastNode = $chain->last();
        if (!$lastNode instanceof SelfEdge) {
            return false;
        }
        if ($lastNode->y !== $chain->second()->x) {
            return false;
        }

        return true;
    }

    #[Pure] protected function cellIsValidStart(Cell $cell): bool
    {
        return true;
    }

    protected function tryToDo(NewChain $chain): bool
    {
        $lastNode = $chain->last();
        if (!$lastNode instanceof SelfEdge) {
            return false;
        }
        $value       = $lastNode->y;
        $viewedCells = $this->grid->commonViewedCells($chain->first()->cell, $lastNode->cell, $value)
                                  ->diff($chain->collect());
        if (count($viewedCells) === 0) {
            return false;
        }
        $this->logger->logTigreChain($chain, $viewedCells, $value);
        $viewedCells->walk(fn(Cell $cell) => $cell->disableValue($value));
        return true;
    }
}