<?php

namespace App\Tests\Techniques;

use App\Process\LockedCandidates;
use App\Tests\TestCase;

class LockedCandidatesTest extends TestCase
{

    public function testLockedCandidates()
    {
        $grid = self::loadJson(__DIR__ . '/../data/expert-47-before-locked-candidates.json');
        $this->debugGrid($grid) . PHP_EOL;
        $step = (new LockedCandidates($grid))->execute();
        self::assertEquals(0, $step);
        $this->debugGrid($grid) . PHP_EOL;
        self::assertGrid(__DIR__ . '/../data/expert-47-after-locked-candidates.json', $grid);
    }

    public function testLockedCandidatesPointing()
    {
        $grid = self::loadJson(__DIR__ . '/../data/power-21-before-locked-candidate.json');
        $this->debugGrid($grid) . PHP_EOL;
        $step = (new LockedCandidates($grid))->execute();
        self::assertEquals(5, $step);
        $this->debugGrid($grid) . PHP_EOL;
        self::assertGrid(__DIR__ . '/../data/power-21-after-locked-candidates.json', $grid);

    }
}