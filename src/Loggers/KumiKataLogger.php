<?php

namespace App\Loggers;

class KumiKataLogger extends LoggerContract
{
    protected int    $level         = self::LEVEL_ADVANCED;
    protected string $phase         = 'Kumi-Kata';
    protected string $canonicalName = 'kumikata';

    public function logExecuteKata()
    {
        $this->proof[]   = 'Avec les techniques les plus basiques, nous ne trouvons plus de résultats,'
                           . ' le Kumi-Kata devient nécessaire';
        $this->message[] = 'On fait le Kumi-Kata';
    }
}