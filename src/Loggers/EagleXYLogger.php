<?php

namespace App\Loggers;

use App\Model\Cell;
use App\Model\CellCollection;
use App\Serialization\Formatter;

class EagleXYLogger extends LoggerContract
{
    protected int    $level         = self::LEVEL_SMART;
    protected string $phase         = 'Aigle';
    protected string $canonicalName = 'eagle';

    public function logPotentialEagle(Cell $eagle, int $a, int $b): void
    {
        $this->algoSteps[] = 'En ' . $eagle . ' on pourrait avoir un aigle avec la paire ' . Formatter::v([$a, $b]);
    }

    public function logPotentialEagleC(Cell $eagle, int $a, int $b, int $c): void
    {
        $this->algoSteps[] = 'En ' . $eagle . ' on pourrait avoir un aigle avec le triplet ' . Formatter::v([$a, $b,
                                                                                                             $c]);
    }

    public function logPotentialTalon(Cell $cell, int $c): void
    {
        $this->algoSteps[] = 'On a trouvé une serre potentielle : ' . $cell . ' avec la valeur ' . Formatter::v($c);
    }

    public function logStudyTalons(int $c, Cell $talonA, Cell $talonB): void
    {
        $this->algoSteps[] = 'Les serres pour ' . Formatter::v($c) . ' pourraient être ' . $talonA . ' et ' . $talonB;
    }

    public function logInvalidConfiguration1(): void
    {
        $this->algoSteps[] = 'Cette configuration ne convient pas : les serres se voient';
    }

    public function logInvalidConfiguration2(int $a, int $b): void
    {
        $this->algoSteps[] = 'Cette configuration ne convient pas : les serres ne contiennent pas resp. les valeurs '
                             . Formatter::v($a) . ' et ' . Formatter::v($b);
    }

    public function logInvalidConfiguration3(int $c): void
    {
        $this->algoSteps[] = 'Cette configuration ne convient pas : '
                             . 'il n\'y a pas de case qui voit les deux serres et contient la valeur '
                             . Formatter::v($c);
    }

    public function eagleMessage(Cell $eagle, Cell $talonA, Cell $talonB): void
    {
        $eagle->setEffect(Cell::EFFECT_ELEM_1);
        $eagle->setAnnotation('A');
        $talonA->setEffect(Cell::EFFECT_ELEM_2);
        $talonA->setAnnotation('S1');
        $talonB->setEffect(Cell::EFFECT_ELEM_2);
        $talonB->setAnnotation('S2');
        $this->message[] = 'L\'aigle est ' . $eagle . ' et les serres sont ' . $talonA . ' et ' . $talonB;
    }

    public function logActionOnCandidateAB(int $a, int $b, int $c, CellCollection $candidates, Cell $eagle, Cell $talonA, Cell $talonB)
    {
        $candidates->addEffect(Cell::EFFECT_FORBIDDEN);
        $this->logActionAB($c, $candidates, $eagle, $a, $talonA, $b, $talonB);
        $this->proof[] = 'Conclusion : dans tous les cas possibles, ' . $candidates
                         . (count($candidates) > 1 ? ' sont différents de ' : ' est différent de ') . Formatter::v($c)
                         . ' (Les deux seules possibilités pour ' . $eagle . ' sont '
                         . Formatter::v($a) . ' et ' . Formatter::v($b) . ')';
    }

    // ```
    // ----------------------------------------
    // [   ,   ,   , |   ,   ,   , |    ,   ,   ]
    // [   , AB,   , |   , AC,   , |    ,   ,   ]
    // [   ,   ,   , |   ,   ,   , |    ,   ,   ]
    // -------------------------------- -------
    // [   ,   ,   , |   ,   ,   , |    ,   ,   ]
    // [   , BC,   , |   , XX,   , |    ,   ,   ]
    // [   ,   ,   , |   ,   ,   , |    ,   ,   ]
    // -------------------------------- -------
    // [   ,   ,   , |   ,   ,   , |    ,   ,   ]
    // [   ,   ,   , |   ,   ,   , |    ,   ,   ]
    // [   ,   ,   , |   ,   ,   , |    ,   ,   ]
    // ----------------------------------------
    // ```

    private function logActionAB(int $c, CellCollection $candidates, Cell $eagle, int $a, Cell $talonA, int $b, Cell $talonB): void
    {
        $this->message[] = 'On retire le candidat ' . Formatter::v($c) . ' dans ' . $candidates;
        $this->proof[]   = 'Si ' . $eagle . ' = ' . Formatter::v($a) . ', alors ' . $talonA . ' = ' . Formatter::v($c)
                           . ', donc ' . $candidates
                           . (count($candidates) > 1 ? ' sont différents de ' : ' est différent de ') . Formatter::v($c);
        $this->proof[]   = 'Sinon, si ' . $eagle . ' = ' . Formatter::v($b) . ', alors ' . $talonB . ' = ' . Formatter::v($c)
                           . ', donc ' . $candidates
                           . (count($candidates) > 1 ? ' sont différents de ' : ' est différent de ') . Formatter::v($c);
    }

    // ```
    // ----------------------------------------
    // [   , XX,   , |   ,   ,   , |    ,   ,   ]
    // [   ,ABC,   , |   ,   ,   , |    ,   ,   ]
    // [   , XX, AC, |   ,   ,   , |    ,   ,   ]
    // -------------------------------- -------
    // [   ,   ,   , |   ,   ,   , |    ,   ,   ]
    // [   , BC,   , |   ,   ,   , |    ,   ,   ]
    // [   ,   ,   , |   ,   ,   , |    ,   ,   ]
    // -------------------------------- -------
    // [   ,   ,   , |   ,   ,   , |    ,   ,   ]
    // [   ,   ,   , |   ,   ,   , |    ,   ,   ]
    // [   ,   ,   , |   ,   ,   , |    ,   ,   ]
    // ----------------------------------------
    // ```

    public function logActionOnCandidateABC(int $a, int $b, int $c, CellCollection $candidates, Cell $eagle, Cell $talonA, Cell $talonB)
    {
        $candidates->addEffect(Cell::EFFECT_FORBIDDEN);
        $this->logActionAB($c, $candidates, $eagle, $a, $talonA, $b, $talonB);
        $this->proof[] = 'Sinon, si ' . $eagle . ' = ' . Formatter::v($c) . ', alors '
                         . $candidates
                         . (count($candidates) > 1 ? ' sont différents de ' : ' est différent de ') . Formatter::v($c);
        $this->proof[] = 'Conclusion : dans tous les cas possibles, ' . $candidates
                         . (count($candidates) > 1 ? ' sont différents de ' : ' est différent de ') . Formatter::v($c)
                         . ' (Les trois seules possibilités pour ' . $eagle . ' sont '
                         . Formatter::v($a) . ', ' . Formatter::v($b) . ' et ' . Formatter::v($c) . ')';
    }
}
