<?php

namespace App\Process;

use App\Loggers\BowLogger;
use App\Loggers\LoggerContract;
use App\Model\Cell;
use App\Model\Grid;
use App\Model\Zone;
use JetBrains\PhpStorm\Pure;

class Bow extends ProcessorContract
{

    /** @var BowLogger */
    protected LoggerContract $logger;
    private int              $currentValue = 0;

    #[Pure] public function __construct(Grid $grid)
    {
        parent::__construct($grid, new BowLogger());
    }

    public function execute(): bool
    {
        $start = $this->currentValue;
        for ($this->currentValue = $start; $this->currentValue < $start + 9; $this->currentValue++) {
            $this->reset();
            if ($this->doOnValueRow($this->currentValue % 9)) {
                return true;
            }
            $this->reset();
            if ($this->doOnValueColumn($this->currentValue % 9)) {
                return true;
            }
        }
        return false;
    }

    public function doOnValueRow(int $value): bool
    {
        if (count($this->grid->positions[$value]) === 9) {
            return false;
        }

        // Pour toutes les lignes
        // - Si je trouve la valeur exacte ou lockée dans la ligne, je vire la ligne
        $this->phase1ColorizeRow($value);

        // Pour toutes les colonnes, s'il reste une seule case libre je la trouve
        return $this->phase2AnalyzeZones($this->grid->columns, $value);
    }

    protected function phase1ColorizeRow(int $value): void
    {
        foreach ($this->grid->blocks as $block) {
            if (count($block->rowOf[$value]) === 1) {
                $rowId = $block->rowInGrid(reset($block->rowOf[$value]));
                $this->logger->logColorize($value, $block, $this->grid->rows[$rowId]);
                continue;
            }
        }
    }

    /** @param Zone[] $zones */
    protected function phase2AnalyzeZones(array $zones, int $value): bool
    {
        $usefull = false;
        foreach ($zones as $zone) {
            if ($zone->hasValue($value)) {
                continue;
            }
            $possiblePositions  = $zone->freeCells()->filter(
                fn(Cell $cell) => ($cell->effect() !== Cell::EFFECT_FORBIDDEN && $cell->value() === null)
            );
            $forbiddenPositions = $zone->freeCells()->filter(
                fn(Cell $cell) => ($cell->effect() === Cell::EFFECT_FORBIDDEN)
            );
            $this->logger->logResult($value, $zone, $forbiddenPositions, $possiblePositions);
            if (count($possiblePositions) === 1) {
                $possiblePositions[0]->setValue($value);
                $usefull = true;
            }
        }
        return $usefull;
    }

    public function doOnValueColumn(int $value): bool
    {
        $this->grid->resetEffects();
        if (count($this->grid->positions[$value]) === 9) {
            return false;
        }

        // Pour toutes les colonnes
        // - Si je trouve la valeur exacte ou lockée dans la colonne, je vire la colonne
        $this->phase1ColorizeColumn($value);

        // Pour toutes les lignes, s'il reste une seule case libre je la trouve
        return $this->phase2AnalyzeZones($this->grid->rows, $value);
    }

    protected function phase1ColorizeColumn(int $value): void
    {
        foreach ($this->grid->blocks as $block) {
            if (count($block->columnOf[$value]) === 1) {
                $columnId = $block->columnInGrid(reset($block->columnOf[$value]));
                $this->logger->logColorize($value, $block, $this->grid->columns[$columnId]);
                continue;
            }
        }
    }
}