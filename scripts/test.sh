#!/usr/bin/env bash

#docker compose run --rm sudoku-testunit php -dxdebug.mode=debug,develop -dxdebug.client_host=host.docker.internal -dxdebug.client_port=9000 -dxdebug.start_with_request=yes vendor/bin/phpunit ${@}
docker-compose run --rm sudoku-testunit php vendor/bin/phpunit --configuration /app/phpunit.xml ${@}