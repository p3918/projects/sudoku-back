```
-----------------------------------------
[   ,   ,   , |   ,   ,   , |   ,   ,   ]
[   , X1,   , |   , X2,   , |   ,   ,   ] 
[   ,   ,   , |   ,   ,   , |   ,   ,   ]
-----------------------------------------
[   ,   ,   , |   ,   ,   , |   ,   ,   ]
[   ,   ,   , |   , X3,   , |   ,   ,   ] 
[   ,   ,   , |   ,   , X4, |   , X5,   ]
-----------------------------------------
[   ,   ,   , |   ,   ,   , |   ,   ,   ]
[   , P1, AB, |   ,   ,   , |   , P2,   ] 
[   ,   ,   , |   ,   ,   , |   ,   ,   ]
-----------------------------------------
```

On forme une chaine de taille impaire (notons les candidats a,b) telle que :
- tous les maillons sont des paires identiques
- les maillons se voient deux à deux

Puis on projette les 2 extrémités de la chaine sur une zone (ligne, colonne ou block), je note P1,P2,
et telle que à part en P1,P2 les candidates a,b n'apparaissent qu'une seule fois, ensemble, je note ça AB.
Alors je peux retirer les candidats différents de a,b de la case AB

- Si X1 vaut a, alors P1 ne vaut pas a et
  - X2 vaut b 
  - donc X3 vaut a
  - donc X4 vaut b
  - donc X5 vaut a, donc P2 ne vaut pas a
  - donc dans L7, ni P1 ni P2 ne vallent a, donc AB vaut a 
- Sinon, X1 vaut b, donc P1 ne vaut pas b
  - donc X2 vaut a 
  - donc X3 vaut b
  - donc X4 vaut a
  - donc X5 vaut b, donc P2 ne vaut pas b
  - donc dans L7, ni P1 ni P2 ne vallent b, donc AB vaut b
==> Finalement, dans les deux cas, AB vaut soit a, soit b, pas autre chose, on retire les autres candidats
  
