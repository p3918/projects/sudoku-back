<?php

namespace App\Resolvers;

use App\Model\Grid;
use App\Process\Bow;
use App\Process\CobraSueDeCoq;
use App\Process\EagleW;
use App\Process\EagleXY;
use App\Process\HiddenSet;
use App\Process\IntegrityChecker;
use App\Process\LockedCandidates;
use App\Process\NakedLock;
use App\Process\NakedSet;
use App\Process\NakedSingle;
use App\Process\Projection;
use App\Process\Set;
use App\Process\SpyBugPlusOne;
use App\Process\SpyRectangle;
use App\Process\SpySixAligned;
use App\Process\Swordfish;
use App\Process\TigreAIC;
use App\Process\TigreMultiXChain;
use App\Process\TigreRegularAlternateLoop;
use App\Process\TigreRemotePair;
use App\Process\TigreRemotePairMinusOne;
use App\Process\TigreStingKite;
use App\Process\TigreXChain;
use App\Process\TigreXYChain;
use App\Serialization\Step;

class Resolver
{
    /**
     * @param Grid $grid
     * @param bool $acceptFinalStep
     *
     * @return Step[]
     */
    public static function doEverything(Grid $grid, bool $acceptFinalStep = true): array
    {
        return self::do($grid, new BasicResolver($grid, null, $acceptFinalStep));
    }

    private static function do(Grid $grid, AbstractResolver $resolver): array
    {
        $resolver->initialize();
        $resolver->execute();
        $grid->resetEffects();
        return $resolver->steps();
    }

    public static function doSome(Grid $grid, string $technique): array
    {
        return match ($technique) {
            'lonely_sets'                  => self::doLonelySets($grid),
            'naked_locks'                  => self::doNakedLocks($grid),
            'naked_single_basic'           => self::doNakedSingleBasic($grid),
            'naked_single'                 => self::doNakedSingleWithKata($grid),
            'projection'                   => self::doProjection($grid),
            'bow'                          => self::doBow($grid),
            'sets_2'                       => self::doSize2Sets($grid),
            'sets_3'                       => self::doSize3Sets($grid),
            'sets_4'                       => self::doSize4Sets($grid),
            'sets_5'                       => self::doSize5Sets($grid),
            'naked_sets_2'                 => self::doSize2NakedSets($grid),
            'naked_sets_3'                 => self::doSize3NakedSets($grid),
            'hidden_sets_2'                => self::doSize2HiddenSets($grid),
            'hidden_sets_3'                => self::doSize3HiddenSets($grid),
            'locked_candidates'            => self::doLockedCandidates($grid),
            'spy_bug_plus_one'             => self::doBugPlusOne($grid),
            'spy_rectangle'                => self::doSpyRectangle($grid),
            'spy_six_aligned'              => self::doSpySixAligned($grid),
            'eagle'                        => self::doEagle($grid),
            'swordfish'                    => self::doSwordfish($grid),
            'cobra_sue_de_coq'             => self::doCobraSueDeCoq($grid),
            'eagle_w'                      => self::doEagleW($grid),
            'tigre_remote_pair'            => self::doTigreRemotePairs($grid),
            'tigre_remote_pair_minus_one'  => self::doTigreRemotePairsMinusOne($grid),
            'tigre_kite'                   => self::doTigreStingKite($grid),
            'tigre_x'                      => self::doTigreXChain($grid),
            'tigre_xy'                     => self::doTigreXYChain($grid),
            'integrity_check'              => self::doIntegrityCheck($grid),
            'tigre_multi_x'                => self::doTigreMultiXChain($grid),
            'tigre_regular_alternate_loop' => self::doTigreRegularAlternateLoop($grid),
            'tigre_aic'                    => self::doTigreAIC($grid),
            default                        => self::getList()
        };
    }

    public static function doLonelySets(Grid $grid): array
    {
        return self::do($grid, new BasicTechniqueResolver($grid, new Set($grid, 1)));
    }

    public static function doNakedLocks(Grid $grid): array
    {
        return self::do($grid, new BasicTechniqueResolver($grid, new NakedLock($grid)));
    }

    public static function doNakedSingleBasic(Grid $grid): array
    {
        return self::do($grid, new BasicTechniqueResolver($grid, new NakedSingle($grid)));
    }

    public static function doNakedSingleWithKata(Grid $grid): array
    {
        return self::do($grid, new TechniqueResolver($grid, new NakedSingle($grid)));
    }

    public static function doProjection(Grid $grid): array
    {
        return self::do($grid, new BasicTechniqueResolver($grid, new Projection($grid)));
    }

    public static function doBow(Grid $grid): array
    {
        return self::do($grid, new BasicTechniqueResolver($grid, new Bow($grid)));
    }

    public static function doSize2Sets(Grid $grid): array
    {
        return self::do($grid, new BasicTechniqueResolver($grid, new Set($grid, 2)));
    }

    public static function doSize3Sets(Grid $grid): array
    {
        return self::do($grid, new BasicTechniqueResolver($grid, new Set($grid, 3)));
    }

    public static function doSize4Sets(Grid $grid): array
    {
        return self::do($grid, new TechniqueResolver($grid, new Set($grid, 4)));
    }

    public static function doSize5Sets(Grid $grid): array
    {
        return self::do($grid, new TechniqueResolver($grid, new Set($grid, 5)));
    }

    public static function doSize2NakedSets(Grid $grid): array
    {
        return self::do($grid, new TechniqueResolver($grid, new NakedSet($grid, 2)));
    }

    public static function doSize3NakedSets(Grid $grid): array
    {
        return self::do($grid, new TechniqueResolver($grid, new NakedSet($grid, 3)));
    }

    public static function doSize2HiddenSets(Grid $grid): array
    {
        return self::do($grid, new TechniqueResolver($grid, new HiddenSet($grid, 2)));
    }

    public static function doSize3HiddenSets(Grid $grid): array
    {
        return self::do($grid, new TechniqueResolver($grid, new HiddenSet($grid, 3)));
    }

    public static function doLockedCandidates(Grid $grid): array
    {
        return self::do($grid, new TechniqueResolver($grid, new LockedCandidates($grid)));
    }

    public static function doBugPlusOne(Grid $grid): array
    {
        return self::do($grid, new TechniqueResolver($grid, new SpyBugPlusOne($grid)));
    }

    public static function doSpyRectangle(Grid $grid): array
    {
        return self::do($grid, new TechniqueResolver($grid, new SpyRectangle($grid)));
    }

    public static function doSpySixAligned(Grid $grid): array
    {
        return self::do($grid, new TechniqueResolver($grid, new SpySixAligned($grid)));
    }

    public static function doEagle(Grid $grid): array
    {
        return self::do($grid, new TechniqueResolver($grid, new EagleXY($grid)));
    }

    public static function doSwordfish(Grid $grid): array
    {
        return self::do($grid, new TechniqueResolver($grid, new Swordfish($grid)));
    }

    public static function doCobraSueDeCoq(Grid $grid): array
    {
        return self::do($grid, new TechniqueResolver($grid, new CobraSueDeCoq($grid)));
    }

    public static function doEagleW(Grid $grid): array
    {
        return self::do($grid, new TechniqueResolver($grid, new EagleW($grid)));
    }

    public static function doTigreRemotePairs(Grid $grid): array
    {
        return self::do($grid, new TechniqueResolver($grid, new TigreRemotePair($grid)));
    }

    public static function doTigreRemotePairsMinusOne(Grid $grid): array
    {
        return self::do($grid, new TechniqueResolver($grid, new TigreRemotePairMinusOne($grid)));
    }

    public static function doTigreStingKite(Grid $grid): array
    {
        return self::do($grid, new TechniqueResolver($grid, new TigreStingKite($grid)));
    }

    public static function doTigreXChain(Grid $grid): array
    {
        return self::do($grid, new TechniqueResolver($grid, new TigreXChain($grid)));
    }

    public static function doTigreXYChain(Grid $grid): array
    {
        return self::do($grid, new TechniqueResolver($grid, new TigreXYChain($grid)));
    }

    public static function doIntegrityCheck(Grid $grid): array
    {
        return self::do($grid, new TechniqueResolver($grid, new IntegrityChecker($grid)));
    }

    public static function doTigreMultiXChain(Grid $grid): array
    {
        return self::do($grid, new TechniqueResolver($grid, new TigreMultiXChain($grid)));
    }

    public static function doTigreRegularAlternateLoop(Grid $grid): array
    {
        return self::do($grid, new TechniqueResolver($grid, new TigreRegularAlternateLoop($grid)));
    }

    public static function doTigreAIC(Grid $grid): array
    {
        return self::do($grid, new TechniqueResolver($grid, new TigreAIC($grid)));
    }

    public static function getList(): array
    {
        return [
            'lonely_sets'                  => 'Case libre unique',
            'naked_locks'                  => 'Verrous esseulés',
            'naked_single_basic'           => 'Candidat isolé (sans Kumi-Kata)',
            'naked_single'                 => 'Candidat isolé (avec Kumi-Kata)',
            'projection'                   => 'Projections',
            'bow'                          => 'L\'arc et la plume',
            'sets_2'                       => 'Ensembles à 2 éléments (sans Kumi-Kata)',
            'sets_3'                       => 'Ensembles à 3 éléments (sans Kumi-Kata)',
            'sets_4'                       => 'Ensembles à 4 éléments (avec Kumi-Kata)',
            'sets_5'                       => 'Ensembles à 5 éléments (avec Kumi-Kata)',
            'naked_sets_2'                 => 'Paires isolées',
            'naked_sets_3'                 => 'Triplets isolés',
            'hidden_sets_2'                => 'Paires cachées',
            'hidden_sets_3'                => 'Triplets cachés',
            'locked_candidates'            => 'Candidats verrouillés',
            'spy_bug_plus_one'             => 'Espion - Bug+1',
            'spy_rectangle'                => 'Espion - Rectangles 1 et 2',
            'spy_six_aligned'              => 'Espion - Danse à 6 espions alignés',
            'eagle'                        => 'Aigle',
            'swordfish'                    => 'Espadon',
            'tigre_kite'                   => 'Tigre - cerf-volant',
            'cobra_sue_de_coq'             => 'Cobra - sue de coq',
            'eagle_w'                      => 'Aile de l\'aigle',
            'tigre_remote_pair'            => 'Tigre - chaine de paires distantes de taille paire',
            'tigre_remote_pair_minus_one'  => 'Tigre - chaine de paires distantes de taille impaire',
            'tigre_x'                      => 'Tigre - chaine X',
            'tigre_xy'                     => 'Tigre - chaine XY',
            'tigre_multi_x'                => 'Tigre - chaine multi X',
            'tigre_regular_alternate_loop' => 'Tigre - boucle régulière de liens alternés',
            'tigre_aic'                    => 'Tigre - approche du tigre',
            'integrity_check'              => 'Final - vérification d\'intégrité / essais / erreurs'
        ];
    }
}