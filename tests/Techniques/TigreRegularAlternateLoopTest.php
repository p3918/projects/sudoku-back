<?php

namespace App\Tests\Techniques;

use App\Process\TigreRegularAlternateLoop;
use App\Tests\TestCase;

class TigreRegularAlternateLoopTest extends TestCase
{

    public function testRegularAlternateLoop()
    {
        $grid = self::loadJson(__DIR__ . '/../data/issue-65-before-regular-alternate-loop.json');
        $step = (new TigreRegularAlternateLoop($grid))->execute();
        $this->debugGrid($grid);
        self::assertTrue($step);
        $this->debugGrid(self::loadJson(__DIR__ . '/../data/issue-65-after-regular-alternate-loop.json'));
        self::assertGrid(__DIR__ . '/../data/issue-65-after-regular-alternate-loop.json', $grid);
    }
}