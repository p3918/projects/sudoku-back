<?php

namespace App\Tests\Techniques;

use App\Model\Edge;
use App\Tests\TestCase;
use App\Tests\Unit\TigreAICMock;

class TigreAICUnitTest extends TestCase
{

    public function testTigreAIC()
    {
        $grid = self::loadJson(__DIR__ . '/../data/example-before-aic.json');
        $grid->resetEffects();
        $this->debugGrid($grid);

        $processor = new TigreAICMock($grid);

        $n0 = $processor->neighboursOf0();
        $c0 = implode(',', $n0);
        self::assertEquals('|F,1 8|,|F,6 19|,|F,6 63|', $c0);
        self::assertCount(3, $n0); // 8,19,63

        $n8 = $processor->neighboursOf8();
        $c8 = implode(',', $n8);
        self::assertEquals('|f,1 16|,|f,1 17|,|f,1 0|,|f,1 80|,|f,1,2 8|,|f,1,3 8|', $c8);

        $n80 = $processor->neighboursOf80();
        $c80 = implode(',', $n80);
        self::assertEquals('|F,1 61|,|F,1,4 80|', $c80);

        $n61 = $processor->neighboursOf61();
        $c61 = implode(',', $n61);
        self::assertEquals('|f,1 80|,|f,1 57|,|f,1 59|,|f,1 16|,|f,1,6 61|,|f,1,8 61|', $c61);

        $n61b = $processor->neighboursOf61b();
        $c61b = implode(',', $n61b);
        self::assertEquals('|F,6 55|', $c61b);

        $n55 = $processor->neighboursOf55();
        $c55 = implode(',', $n55);
        self::assertEquals('|f,6 63|,|f,6 64|,|f,6 61|,|f,6 19|,|f,6,2 55|,|f,6,3 55|', $c55);
        self::assertTrue(true);

        $chain = $processor->finalChain();
        self::assertTrue($processor->checkValid($chain));
        $transChain = $processor->chainToLoop($chain);
        self::assertEquals((string)$processor->finalLoop(), (string)$transChain);

        self::assertTrue($processor->checkCanDo($processor->finalChain()));

        $this->debugGrid($grid);
        //self::assertTrue($step);
        //self::assertGrid(__DIR__ . '/../data/example-after-aic.json', $grid);
    }

    public function testFindTheChain()
    {
        $grid = self::loadJson(__DIR__ . '/../data/example-before-aic.json');
        $grid->resetEffects();
        $this->debugGrid($grid);

        $processor = new TigreAICMock($grid);
        $response  = $processor->exploreToFindChain();
        $this->debugGrid($grid);
        $grid->resetEffects();
        self::assertEquals('| =F,6=>[R0;C0]=f,6,1=>[R0;C0]=F,1=>[R0;C8]=f,1=>[R1;C7]=F,1=>[R6;C7]=f,1,6=>[R6;C7]=F,6=>[R6;C1]=f,6=>[R7;C0] |', $response);
    }

    public function testLoopWithStrongLink()
    {
        $grid = self::loadJson(__DIR__ . '/../data/suprem-83-before-aic-loop-bug.json');

        $firstCell     = $grid[1];
        $lastCell      = $grid[28];
        $lastCandidate = 2;
        $expectedLink  = new Edge($firstCell, true, $lastCandidate);
        $isLoop        = $lastCell->linked($expectedLink);
        self::assertFalse($isLoop);
    }

    public function testWeAlwaysStartWithAStrongLink()
    {
        $grid = self::loadJson(__DIR__ . '/../data/suprem-83-before-aic-loop-bug-start-with-weak-link.json');
        $grid->resetEffects();
        $this->debugGrid($grid);
        $processor = new TigreAICMock($grid);
        $response  = $processor->exploreChainStartingWithDowngradedStrongLink();
        $this->debugGrid($grid);
        self::assertFalse($response);
    }

    public function testInvalidLoopFinishingWithAStrongLink()
    {
        $grid = self::loadJson(__DIR__ . '/../data/suprem-84-before-bug-aic-loop-finishing-with-two-strong-links.json');
        $grid->resetEffects();
        $this->debugGrid($grid);
        $processor = new TigreAICMock($grid);
        $response  = $processor->chainEndingWithStrongLinkIsConsideredAsALoop();
        $this->debugGrid($grid);
        self::assertFalse($response);
    }
}