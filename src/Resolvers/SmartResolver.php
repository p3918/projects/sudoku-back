<?php

namespace App\Resolvers;

use App\Model\Grid;
use App\Process\CobraSueDeCoq;
use App\Process\EagleW;
use App\Process\EagleXY;
use App\Process\SpyBugPlusOne;
use App\Process\SpyRectangle;
use App\Process\Swordfish;
use App\Process\TigreRemotePair;
use App\Process\TigreRemotePairMinusOne;
use App\Process\TigreStingKite;
use App\Process\TigreXChain;
use App\Process\TigreXYChain;
use App\Serialization\StepsCollection;
use JetBrains\PhpStorm\Pure;

class SmartResolver extends AbstractResolver
{
    #[Pure] public function __construct(Grid $grid, ?StepsCollection $allSteps, BasicResolver $previous, bool $acceptFinalStep = true)
    {
        parent::__construct($grid, $allSteps, $acceptFinalStep);

        $this->techniques            = [
            new SpyBugPlusOne($this->grid),
            new SpyRectangle($this->grid),
            new EagleXY($this->grid),
            new Swordfish($this->grid),
            new TigreStingKite($this->grid),
            new CobraSueDeCoq($this->grid),
            new EagleW($this->grid),
            new TigreRemotePair($this->grid),
            new TigreRemotePairMinusOne($this->grid),
            new TigreXChain($this->grid),
            new TigreXYChain($this->grid),
        ];
        $this->nextResolverOnSuccess = $previous;
        $this->nextResolverOnFailure = new HardResolver($this->grid, $this->logSteps, $previous, $acceptFinalStep);
    }
}