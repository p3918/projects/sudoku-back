<?php

namespace App\Loggers;

use App\Model\Block;
use App\Model\Cell;
use App\Model\CellCollection;
use App\Model\Column;
use App\Model\Row;
use App\Serialization\Formatter;

class BowLogger extends LoggerContract
{
    protected int    $level         = self::LEVEL_BASIC;
    protected string $phase         = 'L\'arc et la plume';
    protected string $canonicalName = 'bow';

    public function logResult(int $value, CellCollection $zone, CellCollection $forbiddenPositions, CellCollection $possiblePositions)
    {
        if (count($possiblePositions) === 1) {
            $possiblePositions[0]->setEffect(Cell::EFFECT_FOUND);
            $this->message[] = 'Dans ' . $zone . ', il reste une seule position possible pour ' . Formatter::v($value) . ': ' . $possiblePositions;
        }
        $proof1        = 'Dans ' . $zone . ', les positions interdites pour ' . Formatter::v($value) . ' sont ' . $forbiddenPositions;
        $proof2        = count($possiblePositions) > 1 ?
            'Dans ' . $zone . ', les positions possibles restantes pour ' . Formatter::v($value) . ' sont ' . $possiblePositions . '. Impossible de conclure.'
            : 'Dans ' . $zone . ', la seule position possible restante pour ' . Formatter::v($value) . ' est ' . $possiblePositions;
        $this->proof[] = $proof1;
        $this->proof[] = $proof2;
    }

    public function logColorize(int $value, Block $block, Row|Column $zone)
    {
        $this->algoSteps[] = 'Dans ' . $block . ', la valeur ' . Formatter::v($value) . ' est en ' . $zone;
        $zone->addEffect(Cell::EFFECT_FORBIDDEN);
        $zone->possibleCells($value)->addEffect(Cell::EFFECT_ELEM_1);
    }
}