<?php

namespace App\Model;

use App\Serialization\Formatter;

class Row extends Zone
{

    public function __construct(array $cells, Grid $grid, int $id)
    {
        parent::__construct($cells, $grid, $id);
        foreach ($this->cells as $cell) {
            $cell->row = $this;
        }
    }

    public function __toString(): string
    {
        // @codeCoverageIgnoreStart
        if (Formatter::$human) {
            return 'L' . ($this->id + 1);
        }
        // @codeCoverageIgnoreEnd
        return 'R' . ($this->id);
    }
}