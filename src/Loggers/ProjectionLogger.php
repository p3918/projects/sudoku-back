<?php

namespace App\Loggers;

use App\Model\Block;
use App\Model\Cell;
use App\Model\CellCollection;
use App\Model\Column;
use App\Model\Row;
use App\Serialization\Formatter;

class ProjectionLogger extends LoggerContract
{
    protected int    $level           = self::LEVEL_BASIC;
    protected string $phase           = 'Projections';
    protected string $canonicalName   = 'projections';
    private int      $countLockAlone  = 0;
    private int      $countFound      = 0;
    private int      $countFoundLocks = 0;

    public function reset(): void
    {
        parent::reset();
        $this->countLockAlone  = 0;
        $this->countFound      = 0;
        $this->countFoundLocks = 0;
    }

    public function logFailure(int $value): void
    {
        $this->proof[] = 'Avec les ' . Formatter::v($value) . ' ça ne donne rien';
    }

    public function logYetNine(int $value): void
    {
        $message           = 'Il y a déjà 9 occurrences de ' . Formatter::v($value) . ' dans la grille';
        $this->algoSteps[] = $message;
        $this->proof[]     = $message;
    }

    public function logFoundValue(int $value, Block $block, Cell $position): void
    {
        $position->row->addEffect(Cell::EFFECT_FORBIDDEN);
        $position->column->addEffect(Cell::EFFECT_FORBIDDEN);
        $position->setEffect(Cell::EFFECT_ELEM_1);
        $this->proof[] = 'Dans le bloc ' . $block . ' le ' . Formatter::v($value) . ' est en ' . $position;
        $this->proof[] = 'Donc on peut interdire la ligne ' . $position->row . ' et la colonne ' . $position->column;
    }

    public function logFoundRowLock(int $value, Block $block, int $rowInBlock, Row $row): void
    {
        $this->addFoundLockEffects($value, $block, $row);
        $this->proof[] = 'Dans le bloc ' . $block . ' le ' . Formatter::v($value) . ' est verrouillé sur '
                         . Formatter::e(Cell::EFFECT_ELEM_1, 'la ligne ' . Formatter::v($rowInBlock));
        $this->proof[] = 'Donc on peut interdire la ligne ' . $row;
    }

    private function addFoundLockEffects(int $value, Block $block, Row|Column $zone): void
    {
        foreach ($zone as $cell) {
            if ($cell->block->id === $block->id && $cell->lockedKata[$value]) {
                $cell->setEffect(Cell::EFFECT_ELEM_1);
                continue;
            }
            $cell->setEffect(Cell::EFFECT_FORBIDDEN);
        }
    }

    public function logFoundColumnLock(int $value, Block $block, int $columnInBlock, Column $column): void
    {
        $this->addFoundLockEffects($value, $block, $column);
        $this->proof[] = 'Dans le bloc ' . $block . ' le ' . Formatter::v($value) . ' est verrouillé sur '
                         . Formatter::e(Cell::EFFECT_ELEM_1, 'la colonne ' . Formatter::v($columnInBlock));
        $this->proof[] = 'Donc on peut interdire la colonne ' . $column;
    }

    public function logOnlyOne(int $value, Block $block, Cell $position): void
    {
        $position->setEffect(Cell::EFFECT_FOUND);
        $position->setAnnotation('T' . (++$this->countFound));
        $this->message[] = 'On trouve ' . Formatter::v($value) . ' en case ' . $position;
        $this->proof[]   = 'Dans le bloc ' . $block . ', la seule case encore disponible est ' . $position;
    }

    public function logOnlyOneRow(int $value, Block $block, Row $row, CellCollection $cellsToLock): void
    {
        $cellsToLock->addEffect(Cell::EFFECT_FOUND);
        $cellsToLock->addAnnotation('V' . (++$this->countFoundLocks));
        $this->message[] = 'On verrouille ' . Formatter::v($value) . ' en ligne ' . $row
                           . ' dans le bloc ' . $block . ' => ' . $cellsToLock;
        $this->proof[]   = 'Dans le bloc ' . $block . ', les seules cases encore disponibles pour le ' . Formatter::v($value) . ' sont sur la même ligne : ' . $row;
        $this->proof[]   = 'Donc on peut interdire la ligne ' . $row;
    }

    public function logOnlyOneColumn(int $value, Block $block, Column $column, CellCollection $cellsToLock): void
    {
        $cellsToLock->addEffect(Cell::EFFECT_FOUND);
        $cellsToLock->addAnnotation('V' . (++$this->countFoundLocks));
        $this->message[] = 'On verrouille ' . Formatter::v($value) . ' en colonne ' . $column
                           . ' dans le bloc ' . $block . ' => ' . $cellsToLock;
        $this->proof[]   = 'Dans le bloc ' . $block . ', les seules cases encore disponibles pour le ' . Formatter::v($value) . ' sont sur la même colonne : ' . $column;
        $this->proof[]   = 'Donc on peut interdire la colonne ' . $column;
    }

    public function logForbidden(int $value, Cell $cell): void
    {
        $this->message[] = 'On enlève la valeur ' . Formatter::v($value) . ' du KumiKata de la case ' . $cell;
    }

    public function logRemoveLock(int $value, Cell $cell): void
    {
        $this->message[] = 'On enlève le verroux sur ' . Formatter::v($value) . ' dans la case ' . $cell;
    }

    public function logFoundLockAlone(int $value, Cell $cell, Block $block): void
    {
        $cell->setAnnotation('V' . (++$this->countLockAlone));
        $cell->setEffect(Cell::EFFECT_FOUND);
        $this->message[] = 'On trouve ' . Formatter::v($value) . ' en case ' . $cell . ' dans le bloc ' . $block;
        $this->proof[]   = 'On remarque que dans le block ' . $block . ', la valeur ' . Formatter::v($value) . ' n\'a plus qu\'un seul verroux';
    }
}
