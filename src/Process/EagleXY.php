<?php

namespace App\Process;

use App\Helpers\SwitchVar;
use App\Loggers\EagleXYLogger;
use App\Loggers\LoggerContract;
use App\Model\Cell;
use App\Model\CellCollection;
use App\Model\Grid;
use JetBrains\PhpStorm\Pure;

class EagleXY extends ProcessorContract
{
    /** @var EagleXYLogger */
    protected LoggerContract $logger;

    #[Pure] public function __construct(Grid $grid)
    {
        parent::__construct($grid, new EagleXYLogger());
    }

    public function execute(): bool
    {
        // On cherche des cases disposées en triangle
        foreach ($this->grid->freeCells() as $eagle) {
            if (count($eagle->allowedValues()) === 2 && $this->doEagleAB($eagle)) {
                return true;
            }
            if (count($eagle->allowedValues()) === 3 && $this->doEagleABC($eagle)) {
                return true;
            }
        }
        return false;
    }

    /**
     * AB = Aigle
     * AC = Serre 1 : doit voir l'aigle
     * BC = Serre 2 : doit voir l'aigle, ne doit pas voir la Serre 1
     * XX = Proie : doit voir les 2 serres
     * CONCLUSION : La proie ne peut pas avoir le candidat C
     *
     * ```
     * ----------------------------------------
     * [   ,   ,   , |   ,   ,   , |    ,   ,   ]
     * [   , AB,   , |   , AC,   , |    ,   ,   ]
     * [   ,   ,   , |   ,   ,   , |    ,   ,   ]
     * -------------------------------- -------
     * [   ,   ,   , |   ,   ,   , |    ,   ,   ]
     * [   , BC,   , |   , XX,   , |    ,   ,   ]
     * [   ,   ,   , |   ,   ,   , |    ,   ,   ]
     * -------------------------------- -------
     * [   ,   ,   , |   ,   ,   , |    ,   ,   ]
     * [   ,   ,   , |   ,   ,   , |    ,   ,   ]
     * [   ,   ,   , |   ,   ,   , |    ,   ,   ]
     * ----------------------------------------
     * ```
     */
    public function doEagleAB(Cell $eagle): bool
    {
        $this->reset();
        list($a, $b) = $eagle->allowedValues();
        $viewedCells    = $eagle->viewedCells();
        $possibleTalons = [];
        $this->logger->logPotentialEagle($eagle, $a, $b);
        // On s'est fixé un aigle potentiel, on va chercher si on peut trouver 2 serres qui voient l'aigle
        // Chaque serre doit avoir une seule valeur en commun avec l'aigle
        foreach ($viewedCells as $cell) {
            if (count($cell->allowedValues()) !== 2) {
                continue;
            }
            $diff = array_diff($cell->allowedValues(), [$a, $b]);
            if (count($diff) !== 1) {
                continue;
            }
            $c = reset($diff);
            $this->logger->logPotentialTalon($cell, $c);
            if (!isset($possibleTalons[$c])) {
                $possibleTalons[$c] = [];
            }
            $possibleTalons[$c][] = $cell;
        }

        foreach ($possibleTalons as $c => $cells) {

            /**
             * @var Cell $talonA
             * @var Cell $talonB
             */
            foreach ((new CellCollection(count($cells), $cells))->allPairs() as list($talonA, $talonB)) {
                if (!$this->checkTalonsConfiguration($a, $b, $c, $talonA, $talonB)) {
                    continue;
                }

                // Alors toute case qui voit les deux serres ne peut pas contenir $c
                $candidates = $this->grid->commonViewedCells($talonA, $talonB)
                                         ->freeCells()
                                         ->filter(fn(Cell $cell) => $cell->kumiKata[$c]);
                if (count($candidates) === 0) {
                    $this->logger->logInvalidConfiguration3($c);
                    continue;
                }
                $this->removeAB($a, $b, $c, $eagle, $talonA, $talonB, $candidates);
                return true;
            }
        }
        return false;
    }

    private function checkTalonsConfiguration(int $a, int $b, int $c, Cell &$talonA, Cell &$talonB): bool
    {
        $this->logger->logStudyTalons($c, $talonA, $talonB);

        // Les serres ne doivent pas se voir
        if ($talonA->view($talonB)) {
            $this->logger->logInvalidConfiguration1();
            return false;
        }
        // Une des serres doit contenir $a et l'autre $b
        if (!$this->oneTalonContainsAAndTheOtheB($a, $talonA, $b, $talonB)) {
            $this->logger->logInvalidConfiguration2($a, $b);
            return false;
        }
        if (!in_array($a, $talonA->allowedValues())) {
            SwitchVar::switchVars($talonA, $talonB);
        }
        return true;
    }

    /**
     * @param int  $a
     * @param Cell $talon1
     * @param int  $b
     * @param Cell $talon2
     *
     * @return bool
     */
    private function oneTalonContainsAAndTheOtheB(int $a, Cell $talon1, int $b, Cell $talon2): bool
    {
        return (in_array($a, $talon1->allowedValues()) && in_array($b, $talon2->allowedValues()))
               || (in_array($b, $talon1->allowedValues()) && in_array($a, $talon2->allowedValues()));
    }

    private function removeAB(int $a, int $b, int $c, Cell $eagle, Cell $talonA, Cell $talonB, CellCollection $candidates): void
    {
        $this->logger->eagleMessage($eagle, $talonA, $talonB);
        $this->logger->logActionOnCandidateAB($a, $b, $c, $candidates, $eagle, $talonA, $talonB);
        foreach ($candidates as $candidate) {
            $candidate->disableValue($c);
        }
    }

    /**
     * ABC = Aigle
     * AC = Serre 1 : doit voir l'aigle
     * BC = Serre 2 : doit voir l'aigle, ne doit pas voir la Serre 1
     * XX = Proie : doit voir les 2 serres et l'aigle
     * CONCLUSION : La proie ne peut pas avoir le candidat C
     *
     * ```
     * ----------------------------------------
     * [   , XX,   , |   ,   ,   , |    ,   ,   ]
     * [   ,ABC,   , |   ,   ,   , |    ,   ,   ]
     * [   , XX, AC, |   ,   ,   , |    ,   ,   ]
     * -------------------------------- -------
     * [   ,   ,   , |   ,   ,   , |    ,   ,   ]
     * [   , BC,   , |   ,   ,   , |    ,   ,   ]
     * [   ,   ,   , |   ,   ,   , |    ,   ,   ]
     * -------------------------------- -------
     * [   ,   ,   , |   ,   ,   , |    ,   ,   ]
     * [   ,   ,   , |   ,   ,   , |    ,   ,   ]
     * [   ,   ,   , |   ,   ,   , |    ,   ,   ]
     * ----------------------------------------
     * ```
     */
    private function doEagleABC(Cell $eagle): bool
    {
        $this->reset();
        list($a1, $b1, $c1) = $eagle->allowedValues();
        $viewedCells    = $eagle->viewedCells();
        $possibleTalons = [];
        $this->logger->logPotentialEagleC($eagle, $a1, $b1, $c1);
        // On s'est fixé un aigle potentiel, on va chercher si on peut trouver 2 serres qui voient l'aigle
        // Chaque serre doit avoir une seule valeur en commun avec l'aigle
        foreach ($viewedCells as $cell) {
            if (count($cell->allowedValues()) !== 2) {
                continue;
            }
            $diff = array_diff($eagle->allowedValues(), $cell->allowedValues());
            if (count($diff) !== 1) {
                continue;
            }

            list($a, $c) = $cell->allowedValues();
            $this->logger->logPotentialTalon($cell, $c);
            if (!isset($possibleTalons[$c])) {
                $possibleTalons[$c] = [];
            }
            $possibleTalons[$c][] = $cell;

            $this->logger->logPotentialTalon($cell, $a);
            if (!isset($possibleTalons[$a])) {
                $possibleTalons[$a] = [];
            }
            $possibleTalons[$a][] = $cell;
        }

        foreach ($possibleTalons as $c => $cells) {
            list($a, $b) = array_values(array_diff([$a1, $b1, $c1], [$c]));
            /**
             * @var Cell $talonA
             * @var Cell $talonB
             */
            foreach ((new CellCollection(count($cells), $cells))->allPairs() as list($talonA, $talonB)) {
                if (!$this->checkTalonsConfiguration($a, $b, $c, $talonA, $talonB)) {
                    continue;
                }

                // Alors toute case qui voit les deux serres et l'aigle ne peut pas contenir $c
                $candidates = $this->grid->commonViewedCells($talonA, $talonB, $c)
                                         ->intersect($eagle->viewedCells($c));
                if (count($candidates) === 0) {
                    $this->logger->logInvalidConfiguration3($c);
                    continue;
                }
                $this->removeABC($a, $b, $c, $eagle, $talonA, $talonB, $candidates);
                return true;
            }
        }
        return false;
    }

    private function removeABC(int $a, int $b, int $c, Cell $eagle, Cell $talonA, Cell $talonB, CellCollection $candidates): void
    {
        $this->logger->eagleMessage($eagle, $talonA, $talonB);
        $this->logger->logActionOnCandidateABC($a, $b, $c, $candidates, $eagle, $talonA, $talonB);
        foreach ($candidates as $candidate) {
            $candidate->disableValue($c);
        }
    }
}