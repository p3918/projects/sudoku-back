<?php

namespace App\Tests\Techniques;

use App\Process\CobraSueDeCoq;
use App\Tests\TestCase;

class CobraSueDeCoqTest extends TestCase
{

    public function testCobraSueDeCoq()
    {
        $grid = self::loadJson(__DIR__ . '/../data/power-55-before-cobra-sue-de-coq.json');
        $this->debugGrid($grid);
        $step = (new CobraSueDeCoq($grid))->execute();
        $this->debugGrid($grid);
        self::assertTrue($step);
        self::assertGrid(__DIR__ . '/../data/power-55-after-cobra-sue-de-coq.json', $grid);
    }
}