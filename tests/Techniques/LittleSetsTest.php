<?php

namespace App\Tests\Techniques;

use App\Process\Set;
use App\Tests\TestCase;

class LittleSetsTest extends TestCase
{

    public function testLittleSets()
    {
        $grid = self::loadJson(__DIR__ . '/../data/success-40-initial.json');
        $this->debugGrid($grid) . PHP_EOL;
        $littleSets = new Set($grid, 3);
        $step       = $littleSets->execute();
        self::assertTrue($step);
        $this->debugGrid($grid) . PHP_EOL;
        self::assertGrid(__DIR__ . '/../data/success-40-little-sets-completed.json', $grid);
    }
}