<?php

namespace App\Tests\Grids;

use App\Resolvers\Resolver;
use App\Tests\TestCase;

class ResolverFullTest extends TestCase
{

    protected array $basic = [
        'success-01',
        'success-02',
        'success-03',
        'success-04',
        'success-05',
        'success-10',
        'success-22',
        'success-40',
        'success-49',
        'success-69',
        'success-81',
    ];

    protected array $advanced = [
        'success-75',
        'success-77',
        'success-83',
        'expert-01',
        'expert-11',
        'expert-21',
        'expert-33',
        'expert-39',
        'expert-47',
        'expert-50',
        'expert-55',
    ];

    protected array $smart = [
        'expert-60',
        'expert-65',
        'expert-70',
        'expert-75',
        'expert-80',
        'expert-kumi-kata',
        'expert-81',
        'expert-82',
        'expert-83',
        'expert-84',
        'power-01',
        'power-05',
        'power-11',
        'power-15',
        'power-21',
        'power-25',
        'power-31',
        'power-35',
        'power-36',
        'power-37',
        'power-38',
        'power-39',
        'power-40',
        'power-41',
        'power-43',
        'power-44',
        'power-45',
        'power-47',
        'power-51',
        'power-55',
        'power-57',
        'power-60',
        'power-65',
        'power-67',
        'power-70',
        'power-71',
        'power-72',
        'power-73',
        'power-75',
        'power-77',
        'power-79',
        'power-81',
        'power-83',
        'maestro-1',
        'maestro-2',
    ];

    protected array $hard = [
        'maestro-3',
        'maestro-5',
        'maestro-7',
        'maestro-24',
        'maestro-27',
        'maestro-31',
    ];

    protected array $final = [
        'invalid-grid',
        'suprem-83',
        'suprem-84',
        'maestro-25',
    ];

    /**
     * @group full-basic
     */
    public function testBasic()
    {
        $this->testGroup($this->basic);
    }

    protected function testGroup($grids, bool $acceptFinalStep = false)
    {
        foreach ($grids as $id) {
            $grid = self::loadJson(__DIR__ . '/../data/' . $id . '-initial.json');
            Resolver::doEverything($grid, $acceptFinalStep);
            $this->debugGrid($grid);
            self::assertGrid(__DIR__ . '/../data/' . $id . '-solution.json', $grid);
        }
    }

    /**
     * @group full-advanced
     */
    public function testAdvanced()
    {
        $this->testGroup($this->advanced);
    }

    /**
     * @group full-smart
     */
    public function testSmart()
    {
        $this->testGroup($this->smart);
    }

    /**
     * @group full-hard
     */
    public function testHard()
    {
        $this->testGroup($this->hard);
    }

    /**
     * @group full-final
     */
    public function testFinal()
    {
        $this->testGroup($this->final, true);
    }
}