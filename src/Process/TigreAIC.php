<?php

namespace App\Process;

use App\Loggers\LoggerContract;
use App\Loggers\TigreAICLogger;
use App\Model\Cell;
use App\Model\CellCollection;
use App\Model\Edge;
use App\Model\Grid;
use App\Model\NewChain;
use App\Model\SelfEdge;
use JetBrains\PhpStorm\Pure;

class TigreAIC extends NewChainerContract
{

    /** @var TigreAICLogger */
    protected LoggerContract $logger;

    #[Pure] public function __construct(Grid $grid)
    {
        parent::__construct($grid, new TigreAICLogger());
    }

    public function checkCanDo(NewChain $chain): bool
    {
        return $this->tryToDo($chain);
    }

    protected function tryToDo(NewChain $chain): bool
    {
        $chain = $chain->clone();
        list($firstNode, $firstCandidate, $lastNode, $lastCandidate, $loop) = $this->checkChainData($chain);
        $this->logger->colorize($chain);
        $this->logger->logChainInferences($chain, $firstNode, $firstCandidate, $lastNode, $lastCandidate);

        if ($loop) {
            $this->logger->messageLoop($chain, $firstNode, $firstCandidate, $lastNode, $lastCandidate);
            return $this->doNiceLoop($chain);
        }

        if ($firstNode->cell->isAllowed($lastCandidate) || $lastNode->cell->isAllowed($firstCandidate)) {
            $this->logger->messageOpenedChain($chain, $firstNode, $firstCandidate, $lastNode, $lastCandidate);
            $firstNode->cell->disableValue($lastCandidate);
            $lastNode->cell->disableValue($firstCandidate);
            return true;
        }

        $this->reset();
        return false;
    }

    /**
     * @param NewChain $chain
     *
     * @return array
     */
    protected function checkChainData(NewChain $chain): array
    {
        $firstNode      = $chain->first();
        $firstCandidate = $chain->second()->x;
        $lastNode       = $chain->last();
        $lastCandidate  = $lastNode instanceof SelfEdge ? $lastNode->y : $lastNode->x;
        $expectedLink   = new Edge($chain->first()->cell, true, $lastCandidate);
        // C'est une boucle si :
        //  - Le dernier lien actuel est faible, (sinon on a aucune chance de mettre un lien fort après un lien fort)
        //  - Le futur lien suivant est fort (qui sera le tout dernier lien final refermant la boucle)
        $isLoop = !$lastNode->strong && $lastNode->cell->linked($expectedLink);
        if ($isLoop) {
            $firstNode = new SelfEdge($firstNode->cell, false, $lastCandidate, $firstCandidate);
            $chain[0]  = $firstNode;
            $firstNode = new Edge($firstNode->cell, true, $lastCandidate);
            $chain->prepend($firstNode);
        } else {
            $firstNode->x = $lastCandidate;
        }
        return [$firstNode, $firstCandidate, $lastNode, $lastCandidate, $isLoop];
    }

    public function doNiceLoop(NewChain $chain): bool
    {
        // Cas de boucle: 8 maillons {0..6,0} = (7 maillons + 1 retour fort vers M0)
        // 7 / 2 = 3 ||
        // k = 0; M0, M1, M2
        // k = 1; M2, M3, M4
        // k = 2; M4, M5, M6
        // Ajouter le retour fort en dernier

        // Cas de chaine ouverte : 9 maillons {0..7,0} = (8 maillons + 1 retour faible vers M0)
        // 8 / 2 = 4
        // k = 0; M0, M1, M2
        // k = 1; M2, M3, M4
        // k = 2; M4, M5, M6
        // k = 3; M6, M7, M0

        $usefull = false;

        for ($k = 0; $k < $chain->count(); $k++) {
            $M1 = $chain[$k];
            $M2 = $chain[($k + 1) % count($chain)];
            if ($M2->strong) {
                continue;
            }

            if ($M2 instanceof SelfEdge) {
                //élimination des autres candidats
                //  - M vaut soit x1, soit x2 (élimination des autres candidats)
                $otherCandidates = array_diff($M1->cell->allowedValues(), [$M2->x, $M2->y]);
                if (!empty($otherCandidates)) {
                    $usefull = true;
                    foreach ($otherCandidates as $v) {
                        $M1->cell->disableValue($v);
                    }
                }
                continue;
            }
            // élimination de x dans la zone M1-M2
            //  - Soit M1 vaut x, soit M2 vaut x (élimination de x dans la zone M1-M2)
            $zone = $M1->cell->viewedCells($M2->x)->intersect($M2->cell->viewedCells($M2->x));
            if ($zone->count() > 0) {
                $usefull = true;
                $zone->walk(fn(Cell $c) => $c->disableValue($M2->x));
            }
        }

        return $usefull;
    }

    protected function neighbours(NewChain $currentChain): array
    {
        $lastNode = $currentChain->last();

        if (count($currentChain) === 1) {
            return array_filter($lastNode->cell->neighbours(), fn(Edge $edge) => $edge->strong);
        }

        if ($lastNode instanceof SelfEdge) {
            if ($lastNode->strong) {
                return array_map(fn(Edge $e) => $e->weaken(), $lastNode->cell->neighbours($lastNode->y));
            }
            return array_filter($lastNode->cell->neighbours($lastNode->y), fn(Edge $edge) => $edge->strong);
        }

        $neighbours = array_merge($lastNode->cell->neighbours($lastNode->x), $lastNode->cell->selfNeighbours($lastNode->x));
        if ($lastNode->strong) {
            return array_map(fn(Edge $e) => $e->weaken(), $neighbours);
        }
        return array_filter($neighbours, fn(Edge $e) => $e->strong);
    }

    protected function isValid(NewChain $chain): bool
    {
        if (!($chain->first()->cell->view($chain->last()->cell))) {
            return false;
        }

        $firstCandidate = $chain->second()->x;
        $lastNode       = $chain->last();
        $lastCandidate  = $lastNode instanceof SelfEdge ? $lastNode->y : $lastNode->x;
        if ($firstCandidate === $lastCandidate) {
            return false;
        }

        if ($chain->last()->strong) {
            return true;
        }
        // Si la chaine ne termine pas par un lien fort,
        // alors on se doit de refermer la boucle par un dernier lien fort
        // entre le dernier et le premier élément
        $lastNode     = $chain->last();
        $value        = $lastNode instanceof SelfEdge ? $lastNode->y : $lastNode->x;
        $expectedLink = new Edge($chain->first()->cell, true, $value);
        if ($lastNode->cell->linked($expectedLink)) {
            return true;
        }
        return false;
    }

    #[Pure] protected function cellIsValidStart(Cell $cell): bool
    {
        return true;
    }

    #[Pure] protected function cellsToExplore(): CellCollection
    {
        return $this->grid->freeCells();
    }

}