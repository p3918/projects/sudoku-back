<?php

namespace App\Process;

use App\Loggers\LoggerContract;
use App\Loggers\ProjectionLogger;
use App\Model\Cell;
use App\Model\Grid;
use JetBrains\PhpStorm\Pure;

class NakedLock extends ProcessorContract
{
    /** @var ProjectionLogger */
    protected LoggerContract $logger;

    #[Pure] public function __construct(Grid $grid)
    {
        parent::__construct($grid, new ProjectionLogger());
    }

    public function execute(): bool
    {
        $this->logger->reset();
        $continue   = true;
        $atLeastOne = false;
        while ($continue) {
            $continue = false;
            foreach ($this->grid->blocks as $block) {
                for ($value = 0; $value < 9; $value++) {
                    if ($block->hasValue($value)) {
                        continue;
                    }
                    $locks = ($block->freeCells()->filter(fn(Cell $cell) => $cell->lockedKata[$value]));
                    if (count($locks) === 1) {
                        $continue   = true;
                        $atLeastOne = true;
                        $cell       = $locks->first();
                        $this->logger->logFoundLockAlone($value, $cell, $block);
                        $cell->setValue($value);
                    }
                }
            }
        }
        return $atLeastOne;
    }

}
