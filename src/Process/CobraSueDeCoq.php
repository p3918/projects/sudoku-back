<?php

namespace App\Process;

use App\Loggers\CobraSueDeCoqLogger;
use App\Loggers\LoggerContract;
use App\Model\Block;
use App\Model\Cell;
use App\Model\CellCollection;
use App\Model\Grid;
use JetBrains\PhpStorm\Pure;

class CobraSueDeCoq extends ProcessorContract
{

    /** @var CobraSueDeCoqLogger */
    protected LoggerContract $logger;

    #[Pure] public function __construct(Grid $grid)
    {
        parent::__construct($grid, new CobraSueDeCoqLogger());
    }

    public function execute(): bool
    {
        // Le cas dégénéré : Remote Pair - impair, avec 1 seule paire
        foreach ($this->grid->freeCells() as $cell) {
            if (count($cell->allowedValues()) !== 2) {
                continue;
            }
            $block       = $cell->block;
            $blockRow    = $block->blockRow();
            $blockColumn = $block->blockColumn();
            $atLeastOne  = false;
            $this->grid->resetEffects();

            // Je parcours le ruban vertical (je fais varier la ligne)
            foreach (array_diff([0, 1, 2], [$blockRow]) as $otherBlockRow) {
                // Dans l'autre block, sur les cases qui ne sont pas sur la même colonne,
                // je dois trouver la paire une seule fois, et avec d'autres candidats
                $otherBlock = $this->grid->blockAt($otherBlockRow, $blockColumn);
                $zoneX      = $otherBlock->freeCells()->diff($cell->column);
                $zoneT      = $otherBlock->freeCells()->intersect($cell->column);
                $zoneV      = $cell->column->freeCells()->diff($otherBlock)->except($cell);

                $atLeastOne |= $this->doSueDeCoq($zoneX, $cell, $block, $otherBlock, $zoneT, $zoneV);
            }
            // Je parcours le ruban horizontal (je fais varier la colonne)
            foreach (array_diff([0, 1, 2], [$blockColumn]) as $otherBlockColumn) {
                // Dans l'autre block, sur les cases qui ne sont pas sur la même ligne, je dois trouver la paire une seule fois, et avec d'autres candidats
                $otherBlock = $this->grid->blockAt($blockRow, $otherBlockColumn);
                $zoneX      = $otherBlock->freeCells()->diff($cell->row);
                $zoneT      = $otherBlock->freeCells()->intersect($cell->row);
                $zoneV      = $cell->row->freeCells()->diff($otherBlock)->except($cell);
                $atLeastOne |= $this->doSueDeCoq($zoneX, $cell, $block, $otherBlock, $zoneT, $zoneV);
            }
            if ($atLeastOne) {
                return true;
            }
        }
        return false;
    }

    private function doSueDeCoq(CellCollection $zoneX, Cell $cellC1, Block $block, Block $otherBlock, CellCollection $zoneT, CellCollection $zoneV): bool
    {
        // 1) Est-on dans une configuration valide ?

        // On doit trouver exactement une fois la paire en case C2 avec d'autres candidats (ou pas)
        $zoneC2 = $zoneX->filter(
            fn(Cell $cellInBlock) => self::containsThePairPlusOtherCandidates($cellC1, $cellInBlock) || self::containsExactlyThePair($cellC1, $cellInBlock)
        );
        if (count($zoneC2) !== 1) {
            return false;
        }
        // On doit s'assurer de ne pas trouver d'autres morceaux de la paire dans la zone X
        $cellsWithPartOfThePair = $zoneX->filter(
            fn(Cell $cellInBlock) => self::containsOneValueOfThePair($cellC1, $cellInBlock)
        );
        if (count($cellsWithPartOfThePair) !== 0) {
            return false;
        }

        // 2) Est-on dans une configuration utile ?

        // Là où on a trouvé la paire, s'il y a d'autres candidats en plus c'est mieux, car c'est utile : on va pouvoir les enlever
        $zoneC2Usefull = $zoneX->filter(
            fn(Cell $cellInBlock) => self::containsThePairPlusOtherCandidates($cellC1, $cellInBlock)
        );
        // Ça sera utile aussi si on a des morceaux de la paire dans la zone V : on va pouvoir les enlever
        $zoneVUsefull = $zoneV->filter(
            fn(Cell $cellInZone) => self::containsOneValueOfThePair($cellC1, $cellInZone) || self::containsThePairPlusOtherCandidates($cellC1, $cellInZone)
        );
        if (count($zoneC2Usefull) + count($zoneVUsefull) === 0) {
            return false;
        }

        // 3) actions
        $this->logger->logBlocksStudy($block, $otherBlock, $cellC1, $zoneC2, $zoneT, $zoneX, $zoneV);
        $this->removeOtherCandidates($zoneC2Usefull, $cellC1);
        $this->removeThePair($zoneVUsefull, $cellC1);
        return true;
    }

    public static function containsThePairPlusOtherCandidates(Cell $cell, Cell $otherCell): bool
    {
        return count(array_intersect($cell->allowedValues(), $otherCell->allowedValues())) === 2 && count($otherCell->allowedValues()) > 2;
    }

    public static function containsExactlyThePair(Cell $cell, Cell $cellInBlock): bool
    {
        return count(array_intersect($cell->allowedValues(), $cellInBlock->allowedValues())) === 2 && count($cellInBlock->allowedValues()) === 2;
    }

    public static function containsOneValueOfThePair(Cell $cell, Cell $cellInBlock): bool
    {
        return count(array_intersect($cell->allowedValues(), $cellInBlock->allowedValues())) === 1;
    }

    private function removeOtherCandidates(CellCollection $cells, Cell $currentCell): void
    {
        if (count($cells) === 0) {
            return;
        }
        $otherCell       = $cells->first();
        $otherCandidates = array_diff($otherCell->allowedValues(), $currentCell->allowedValues());
        $this->logger->logRemoveCandidates($currentCell, $otherCell);
        foreach ($otherCandidates as $candidate) {
            $otherCell->disableValue($candidate);
        }
    }

    private function removeThePair(CellCollection $zoneV, Cell $cell): void
    {
        $this->logger->logRemovePair($cell, $zoneV);
        foreach ($zoneV as $otherCell) {
            foreach ($cell->allowedValues() as $value) {
                $otherCell->disableValue($value);
            }
        }
    }

}
