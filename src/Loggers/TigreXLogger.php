<?php

namespace App\Loggers;


use App\Model\Cell;
use App\Model\CellCollection;
use App\Serialization\Formatter;

class TigreXLogger extends ChainerLogger
{
    protected int    $level         = self::LEVEL_SMART;
    protected string $phase         = 'Tigre - Chaine de liens forts';
    protected string $canonicalName = 'tigre_x_chain';

    public function logTigreChain(CellCollection $chain, CellCollection $viewedCells, int $value): void
    {
        $chain->addEffect(Cell::EFFECT_ELEM_2);
        $chain->first()->setEffect(Cell::EFFECT_ELEM_1);
        $chain->last()->setEffect(Cell::EFFECT_ELEM_1);
        foreach ($chain as $k => $cell) {
            $cell->setAnnotation(Formatter::alpha($k));
        }
        $viewedCells->addEffect(Cell::EFFECT_FORBIDDEN);
        $this->message[] = 'Chaine de liens fots en X : ' . $chain . ', on supprime les candidats ' . Formatter::v($value)
                           . ' dans les cases vues par les 2 extrémités de la chaine : ' . $viewedCells;

        $this->proof[] = 'Si ' . $chain->first() . ' = ' . Formatter::v($value) . ', alors ' . $viewedCells
                         . (count($viewedCells) > 1 ? ' sont différents de ' : ' est différent de ') . Formatter::v($value);
        $this->proof[] = 'Sinon, ';
        $current       = false;
        foreach ($chain as $k => $cell) {
            $this->proof[] = ($k > 0 ? '- donc ' : '- ') . $cell . ($current ? ' = ' : ' est différent de ') . Formatter::v($value);
            $current       = !$current;
        }
        $this->proof[] = '- donc ' . $viewedCells . (count($viewedCells) > 1 ? ' sont différents de ' : ' est différent de ') . Formatter::v($value);
    }
}
