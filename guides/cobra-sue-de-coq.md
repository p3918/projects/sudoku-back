```
-----------------------------------------
[   ,   ,   , |   ,   ,   , |   ,   ,   ]
[   ,   ,   , |   ,   ,   , |   ,   ,   ] 
[   ,   ,   , |   ,   ,   , |   ,   ,   ]
-----------------------------------------
[   ,   ,   , |   ,   ,   , |   ,   ,   ]
[   ,   ,   , |   ,   ,   , |   ,   ,   ] 
[   ,   ,   , |   ,   ,   , |   ,   ,   ]
-----------------------------------------
[ V5, C1, V4 ,| V1, V2, V3, | T1, T2, T3]
[   ,   ,   , |   ,   ,   , | __, C2, __] 
[   ,   ,   , |   ,   ,   , | __, __, __]
-----------------------------------------
   block                      otherblock
```

C1 = a,b
C2 = a,b + autres candidats
__ = n'importe quoi sauf a,b

Si C1 = a, alors 
 - pas de a dans T, donc forcément un a dans C2, et un b dans T, donc pas de b dans V
 - pas de a dans V
Sinon C1 = b, donc
 - pas de b dans T, donc forcément un b dans C2, et un a dans T, donc pas de a dans V
 - pas de b dans V

On en déduit que dans les 2 cas :
 - C2 ne peut valoir que a ou b => suppression des autres candidats que a et b
 - V ne peut contenir ni a ni b => suppression des candidats a et b dans V