<?php

namespace App\Loggers;

use App\Model\Cell;
use App\Serialization\Formatter;

class IntegrityCheckLogger extends LoggerContract
{
    protected int    $level = self::LEVEL_HARD;
    protected string $phase = 'Final Integrity Check';

    public function logInvalidKumiKata(Cell $cell): void
    {
        $this->message[] = Formatter::e(Cell::EFFECT_FORBIDDEN,
            'Erreur, le kumi-kata de la case ' . $cell . ' ne contient plus de valeur possible.'
        );
        $this->message[] = 'SVP, vérifiez que votre grille initiale est juste et si oui veuillez signaler le bug via le formulaire ci-dessous';
    }

    public function logTrying(Cell $cell, int $value): void
    {
        $this->message[] = Formatter::e(Cell::EFFECT_ELEM_1,
            'On ne sait plus quoi faire, on va faire des essais/erreurs.');
        $cell->setEffect(Cell::EFFECT_ELEM_1);
        $this->proof[] = 'En case ' . $cell . ', on essaye la valeur ' . Formatter::v($value);
    }

    public function logFailure(Cell $cell, int $value): void
    {
        $this->message[] = Formatter::e(Cell::EFFECT_ELEM_2,
            'La valeur ' . Formatter::v($value) . ' ' . ' produit une grille invalide');
        $cell->setEffect(Cell::EFFECT_FORBIDDEN);
        $this->message[] = 'En case ' . $cell . ', on retire la valeur ' . Formatter::v($value);
    }

    public function logSuccess(Cell $cell, int $value): void
    {
        $this->message[] = Formatter::e(Cell::EFFECT_FOUND,
            'La valeur ' . Formatter::v($value) . ' ' . ' produit une grille valide');
        $cell->setEffect(Cell::EFFECT_FOUND);
        $this->message[] = 'En case ' . $cell . ', on choisit la valeur ' . Formatter::v($value);
    }
}
