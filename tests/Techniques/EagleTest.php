<?php

namespace App\Tests\Techniques;

use App\Process\EagleXY;
use App\Tests\TestCase;

class EagleTest extends TestCase
{

    public function testEagleXY()
    {
        $grid = self::loadJson(__DIR__ . '/../data/power-35-before-eagle-xy.json');
        $this->debugGrid($grid);
        $step = (new EagleXY($grid))->execute();
        $this->debugGrid($grid);
        self::assertTrue($step);
        self::assertGrid(__DIR__ . '/../data/power-35-after-eagle-xy.json', $grid);
    }

    public function testEagleXYZ()
    {
        $grid = self::loadJson(__DIR__ . '/../data/power-43-before-eagle-xyz.json');
        $this->debugGrid($grid);
        $step = (new EagleXY($grid))->execute();
        $this->debugGrid($grid);
        self::assertTrue($step);
        self::assertGrid(__DIR__ . '/../data/power-43-after-eagle-xyz.json', $grid);
    }
}