<?php

namespace App\Libs\GitLabClient;

use JsonSerializable;

/**
 * Class GitLabIssues
 *
 * @codeCoverageIgnore
 */
class GitLabIssues
{
    private GitLabClient $client;

    public function __construct()
    {
        $this->client = new GitLabClient();
    }

    public function listIssues(): array
    {
        $issues = $this->client->request('GET', 'issues', ['state'  => 'opened', 'assignee_id' => 'None',
                                                           'labels' => 'Report']);
        return array_map(function ($issue) {
            return [
                'id'          => $issue['id'],
                'iid'         => $issue['iid'],
                'title'       => $issue['title'],
                'ref'         => $issue['references']['short'],
                'description' => json_decode(trim($issue['description']), true)
            ];
        }, $issues);
    }

    public function createIssue(string                                            $title,
                                array                                             $labels,
                                int|string|float|bool|null|array|JsonSerializable $description
    ): array {
        return $this->client->request('POST', 'issues', [],
            ['title'       => substr($title, 0, 200),
             'labels'      => implode(',', $labels),
             'description' => json_encode($description, JSON_PRETTY_PRINT)]);
    }

    public function assignIssue(int $issueIID): array
    {
        return $this->client->request('PUT', 'issues/' . $issueIID, ['assignee_id' => $this->client->getUserId()]);
    }
}
