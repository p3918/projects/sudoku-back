<?php

namespace App\Model;

use App\Serialization\Formatter;
use JetBrains\PhpStorm\ArrayShape;
use JetBrains\PhpStorm\Pure;
use JsonSerializable;

class Neighbour implements JsonSerializable
{
    public function __construct(public Cell $cell, public int|string|array $edge) { }

    public function formatEdge(): string
    {
        return self::format($this->edge);
    }

    private static function format(mixed $e): string
    {
        if (is_array($e)) {
            return implode(',', array_map(fn(mixed $e2) => self::format($e2), $e));
        }
        if (is_integer($e)) {
            return Formatter::v($e);
        }
        return (string)$e;
    }

    #[Pure] #[ArrayShape(['value'  => "\int|null", 'kumiKata' => "bool[]", 'lockedKata' => "bool[]",
                          'effect' => "\int|null", 'annotation' => "string"])]
    public function jsonSerialize(): array
    {
        return $this->cell->jsonSerialize();
    }
}