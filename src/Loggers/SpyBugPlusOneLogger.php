<?php

namespace App\Loggers;

use App\Model\Cell;
use App\Serialization\Formatter;

class SpyBugPlusOneLogger extends LoggerContract
{
    protected int    $level         = self::LEVEL_SMART;
    protected string $phase         = 'Danse des espions';
    protected string $canonicalName = 'spy_bug_plus_one';

    public function logBug(Cell $triple, int $value): void
    {
        $this->proof[]   = 'Il y a uniquement des paires dans la grille, à l\'exception de ' . $triple;
        $this->proof[]   = 'Il y a 3 occurrences de ' . Formatter::v($value) . ' dans ' . $triple->row . ', ' . $triple->column . ' et ' . $triple->block;
        $this->message[] = 'BUG+1, on choisit le candidat ' . Formatter::v($value) . ' en ' . $triple;
    }
}
