<?php

use App\Controllers\Controller;
use App\Controllers\GridController;
use App\Controllers\ReportController;
use App\Controllers\ResolveController;
use App\Serialization\Formatter;

require __DIR__ . '/../vendor/autoload.php';
ini_set('memory_limit', '2048M');
ini_set('max_execution_time', '600');
$method = strtoupper($_SERVER['REQUEST_METHOD']);
$uri    = $_SERVER['REQUEST_URI'];
$query  = $_GET;
$body   = json_decode(file_get_contents('php://input'), true);

if (str_starts_with($uri, '/api')) {
    header('Content-Type: application/json');
    header('Accept: application/json');
    header('Accept-Language: *');
    header('Content-Language: fr');
}

if ($method === 'OPTIONS') {
    die();
}

$uri  = substr($uri, 1);
$uri  = explode('?', $uri)[0];
$path = explode('/', $uri);
if ($path[0] !== 'api') {
    http_response_code(404);
    echo json_encode([
        'error'    => 'not_found',
        'resource' => $path[0]
    ]);
    die();
}

Formatter::$human = !isset($_GET['computer']) || !intval($_GET['computer']);
try {
    /** @var Controller $controller */
    $controller = match ($path[1]) {
        'grid'    => new GridController($path[2], $method, $body),
        'resolve' => new ResolveController($path[2], $method, $body, $query),
        'report'  => new ReportController("", $method, $body, $query),
        default   => new GridController('empty', 'GET', null)
    };

    $controller->dispatch();
} catch (Throwable $exception) {
    http_response_code(500);
    echo json_encode([
        'error' => $exception->getMessage(),
        'trace' => $exception->getTrace()
    ]);
}