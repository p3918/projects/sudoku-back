#!/usr/bin/env bash

docker-compose run --rm sudoku-testunit-cov php vendor/bin/phpunit --configuration /app/phpunit-with-coverage.xml