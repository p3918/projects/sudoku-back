<?php

namespace App\Tests\Unit;

use App\Helpers\CellIndex;
use App\Helpers\CellShare;
use App\Helpers\ZoneIndex;
use App\Tests\TestCase;

class IndexTest extends TestCase
{

    public function testIndex()
    {
        $ids            = [
            0, 1, 2, 3, 4, 5, 6, 7, 8,
            9, 10, 11, 12, 13, 14, 15, 16, 17,
            18, 19, 20, 21, 22, 23, 24, 25, 26,
            27, 28, 29, 30, 31, 32, 33, 34, 35,
            36, 37, 38, 39, 40, 41, 42, 43, 44,
            45, 46, 47, 48, 49, 50, 51, 52, 53,
            54, 55, 56, 57, 58, 59, 60, 61, 62,
            63, 64, 65, 66, 67, 68, 69, 70, 71,
            72, 73, 74, 75, 76, 77, 78, 79, 80,
        ];
        $rows           = [
            0, 0, 0, 0, 0, 0, 0, 0, 0,
            1, 1, 1, 1, 1, 1, 1, 1, 1,
            2, 2, 2, 2, 2, 2, 2, 2, 2,
            3, 3, 3, 3, 3, 3, 3, 3, 3,
            4, 4, 4, 4, 4, 4, 4, 4, 4,
            5, 5, 5, 5, 5, 5, 5, 5, 5,
            6, 6, 6, 6, 6, 6, 6, 6, 6,
            7, 7, 7, 7, 7, 7, 7, 7, 7,
            8, 8, 8, 8, 8, 8, 8, 8, 8,
        ];
        $columns        = [
            0, 1, 2, 3, 4, 5, 6, 7, 8,
            0, 1, 2, 3, 4, 5, 6, 7, 8,
            0, 1, 2, 3, 4, 5, 6, 7, 8,
            0, 1, 2, 3, 4, 5, 6, 7, 8,
            0, 1, 2, 3, 4, 5, 6, 7, 8,
            0, 1, 2, 3, 4, 5, 6, 7, 8,
            0, 1, 2, 3, 4, 5, 6, 7, 8,
            0, 1, 2, 3, 4, 5, 6, 7, 8,
            0, 1, 2, 3, 4, 5, 6, 7, 8,
        ];
        $blockIds       = [
            0, 0, 0, 1, 1, 1, 2, 2, 2,
            0, 0, 0, 1, 1, 1, 2, 2, 2,
            0, 0, 0, 1, 1, 1, 2, 2, 2,
            3, 3, 3, 4, 4, 4, 5, 5, 5,
            3, 3, 3, 4, 4, 4, 5, 5, 5,
            3, 3, 3, 4, 4, 4, 5, 5, 5,
            6, 6, 6, 7, 7, 7, 8, 8, 8,
            6, 6, 6, 7, 7, 7, 8, 8, 8,
            6, 6, 6, 7, 7, 7, 8, 8, 8,
        ];
        $blockRows      = [
            0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0,
            1, 1, 1, 1, 1, 1, 1, 1, 1,
            1, 1, 1, 1, 1, 1, 1, 1, 1,
            1, 1, 1, 1, 1, 1, 1, 1, 1,
            2, 2, 2, 2, 2, 2, 2, 2, 2,
            2, 2, 2, 2, 2, 2, 2, 2, 2,
            2, 2, 2, 2, 2, 2, 2, 2, 2,
        ];
        $blockColumns   = [
            0, 0, 0, 1, 1, 1, 2, 2, 2,
            0, 0, 0, 1, 1, 1, 2, 2, 2,
            0, 0, 0, 1, 1, 1, 2, 2, 2,
            0, 0, 0, 1, 1, 1, 2, 2, 2,
            0, 0, 0, 1, 1, 1, 2, 2, 2,
            0, 0, 0, 1, 1, 1, 2, 2, 2,
            0, 0, 0, 1, 1, 1, 2, 2, 2,
            0, 0, 0, 1, 1, 1, 2, 2, 2,
            0, 0, 0, 1, 1, 1, 2, 2, 2,
        ];
        $inBlockIds     = [
            0, 1, 2, 0, 1, 2, 0, 1, 2,
            3, 4, 5, 3, 4, 5, 3, 4, 5,
            6, 7, 8, 6, 7, 8, 6, 7, 8,
            0, 1, 2, 0, 1, 2, 0, 1, 2,
            3, 4, 5, 3, 4, 5, 3, 4, 5,
            6, 7, 8, 6, 7, 8, 6, 7, 8,
            0, 1, 2, 0, 1, 2, 0, 1, 2,
            3, 4, 5, 3, 4, 5, 3, 4, 5,
            6, 7, 8, 6, 7, 8, 6, 7, 8,
        ];
        $inBlockRows    = [
            0, 0, 0, 0, 0, 0, 0, 0, 0,
            1, 1, 1, 1, 1, 1, 1, 1, 1,
            2, 2, 2, 2, 2, 2, 2, 2, 2,
            0, 0, 0, 0, 0, 0, 0, 0, 0,
            1, 1, 1, 1, 1, 1, 1, 1, 1,
            2, 2, 2, 2, 2, 2, 2, 2, 2,
            0, 0, 0, 0, 0, 0, 0, 0, 0,
            1, 1, 1, 1, 1, 1, 1, 1, 1,
            2, 2, 2, 2, 2, 2, 2, 2, 2,
        ];
        $inBlockColumns = [
            0, 1, 2, 0, 1, 2, 0, 1, 2,
            0, 1, 2, 0, 1, 2, 0, 1, 2,
            0, 1, 2, 0, 1, 2, 0, 1, 2,
            0, 1, 2, 0, 1, 2, 0, 1, 2,
            0, 1, 2, 0, 1, 2, 0, 1, 2,
            0, 1, 2, 0, 1, 2, 0, 1, 2,
            0, 1, 2, 0, 1, 2, 0, 1, 2,
            0, 1, 2, 0, 1, 2, 0, 1, 2,
            0, 1, 2, 0, 1, 2, 0, 1, 2,
        ];

        self::assertEquals($rows, array_map(fn(int $id) => CellIndex::row($id), $ids));
        self::assertEquals($columns, array_map(fn(int $id) => CellIndex::column($id), $ids));
        self::assertEquals($blockIds, array_map(fn(int $id) => CellIndex::blockId($id), $ids));
        self::assertEquals($blockRows, array_map(fn(int $id) => CellIndex::blockRow($id), $ids));
        self::assertEquals($blockColumns, array_map(fn(int $id) => CellIndex::blockColumn($id), $ids));
        self::assertEquals($inBlockIds, array_map(fn(int $id) => CellIndex::inBlockId($id), $ids));
        self::assertEquals($inBlockRows, array_map(fn(int $id) => CellIndex::inBlockRow($id), $ids));
        self::assertEquals($inBlockColumns, array_map(fn(int $id) => CellIndex::inBlockColumn($id), $ids));

        self::assertEquals($ids, array_map(
            fn(int $row, int $column) => CellIndex::fromRowColumn($row, $column),
            $rows, $columns
        ));
        self::assertEquals($ids, array_map(
            fn(int $blockId, int $inBlockId) => CellIndex::fromBlockIdInBlockId($blockId, $inBlockId),
            $blockIds, $inBlockIds
        ));

        self::assertEquals($ids, array_map(
            fn(int $blockId, int $inBlockRow, int $inBlockColumn) => CellIndex::fromBlockIdInBlockRowColumn($blockId, $inBlockRow, $inBlockColumn),
            $blockIds, $inBlockRows, $inBlockColumns
        ));

        self::assertEquals($ids, array_map(
            fn(int $blockRow, int $blockColumn, int $inBlockId) => CellIndex::fromBlockRowColumnInBlockId($blockRow, $blockColumn, $inBlockId),
            $blockRows, $blockColumns, $inBlockIds
        ));

        self::assertEquals($ids, array_map(
            fn(int $blockRow, int $blockColumn, int $inBlockRow, int $inBlockColumn) => CellIndex::fromBlockRowColumnInBlockRowColumn($blockRow, $blockColumn, $inBlockRow, $inBlockColumn),
            $blockRows, $blockColumns, $inBlockRows, $inBlockColumns
        ));
    }

    public function testZoneIndex()
    {
        $rows = [
            [0, 1, 2, 3, 4, 5, 6, 7, 8,],
            [9, 10, 11, 12, 13, 14, 15, 16, 17,],
            [18, 19, 20, 21, 22, 23, 24, 25, 26,],
            [27, 28, 29, 30, 31, 32, 33, 34, 35,],
            [36, 37, 38, 39, 40, 41, 42, 43, 44,],
            [45, 46, 47, 48, 49, 50, 51, 52, 53,],
            [54, 55, 56, 57, 58, 59, 60, 61, 62,],
            [63, 64, 65, 66, 67, 68, 69, 70, 71,],
            [72, 73, 74, 75, 76, 77, 78, 79, 80,],
        ];

        self::assertEquals($rows, array_map(fn($i) => ZoneIndex::row($i), [0, 1, 2, 3, 4, 5, 6, 7, 8]));
        self::assertEquals([1, 2, 3, 4, 5, 6, 7, 8,], ZoneIndex::rowOf(0));
        self::assertEquals([0, 2, 3, 4, 5, 6, 7, 8,], ZoneIndex::rowOf(1));
        self::assertEquals([0, 1, 3, 4, 5, 6, 7, 8,], ZoneIndex::rowOf(2));
        self::assertEquals([0, 1, 2, 4, 5, 6, 7, 8,], ZoneIndex::rowOf(3));
        self::assertEquals([0, 1, 2, 3, 5, 6, 7, 8,], ZoneIndex::rowOf(4));
        self::assertEquals([0, 1, 2, 3, 4, 6, 7, 8,], ZoneIndex::rowOf(5));
        self::assertEquals([0, 1, 2, 3, 4, 5, 7, 8,], ZoneIndex::rowOf(6));
        self::assertEquals([0, 1, 2, 3, 4, 5, 6, 8,], ZoneIndex::rowOf(7));
        self::assertEquals([0, 1, 2, 3, 4, 5, 6, 7,], ZoneIndex::rowOf(8));
        self::assertEquals([10, 11, 12, 13, 14, 15, 16, 17,], ZoneIndex::rowOf(9));
        self::assertEquals([9, 10, 11, 12, 14, 15, 16, 17,], ZoneIndex::rowOf(13));
        self::assertEquals([9, 10, 11, 12, 13, 14, 15, 16,], ZoneIndex::rowOf(17));
        self::assertEquals([19, 20, 21, 22, 23, 24, 25, 26,], ZoneIndex::rowOf(18));
        self::assertEquals([27, 28, 30, 31, 32, 33, 34, 35,], ZoneIndex::rowOf(29));
        self::assertEquals([36, 37, 38, 39, 40, 41, 42, 43,], ZoneIndex::rowOf(44));
        self::assertEquals([73, 74, 75, 76, 77, 78, 79, 80,], ZoneIndex::rowOf(72));
        self::assertEquals([72, 73, 74, 75, 76, 77, 78, 79,], ZoneIndex::rowOf(80));

        $columns = [
            [0, 9, 18, 27, 36, 45, 54, 63, 72],
            [1, 10, 19, 28, 37, 46, 55, 64, 73],
            [2, 11, 20, 29, 38, 47, 56, 65, 74],
            [3, 12, 21, 30, 39, 48, 57, 66, 75],
            [4, 13, 22, 31, 40, 49, 58, 67, 76],
            [5, 14, 23, 32, 41, 50, 59, 68, 77],
            [6, 15, 24, 33, 42, 51, 60, 69, 78],
            [7, 16, 25, 34, 43, 52, 61, 70, 79],
            [8, 17, 26, 35, 44, 53, 62, 71, 80],
        ];

        self::assertEquals($columns, array_map(fn($i) => ZoneIndex::column($i), [0, 1, 2, 3, 4, 5, 6, 7, 8]));
        self::assertEquals([9, 18, 27, 36, 45, 54, 63, 72], ZoneIndex::columnOf(0));
        self::assertEquals([0, 9, 27, 36, 45, 54, 63, 72], ZoneIndex::columnOf(18));
        self::assertEquals([0, 9, 18, 27, 36, 45, 54, 63], ZoneIndex::columnOf(72));
        self::assertEquals([15, 24, 33, 42, 51, 60, 69, 78], ZoneIndex::columnOf(6));
        self::assertEquals([6, 15, 24, 33, 42, 51, 60, 78], ZoneIndex::columnOf(69));
        self::assertEquals([6, 15, 24, 33, 42, 51, 60, 69], ZoneIndex::columnOf(78));
        self::assertEquals([17, 26, 35, 44, 53, 62, 71, 80], ZoneIndex::columnOf(8));
        self::assertEquals([8, 26, 35, 44, 53, 62, 71, 80], ZoneIndex::columnOf(17));
        self::assertEquals([8, 17, 26, 35, 44, 53, 62, 71], ZoneIndex::columnOf(80));


        $blocks = [
            [0, 1, 2, 9, 10, 11, 18, 19, 20],
            [3, 4, 5, 12, 13, 14, 21, 22, 23],
            [6, 7, 8, 15, 16, 17, 24, 25, 26],
            [27, 28, 29, 36, 37, 38, 45, 46, 47],
            [30, 31, 32, 39, 40, 41, 48, 49, 50],
            [33, 34, 35, 42, 43, 44, 51, 52, 53],
            [54, 55, 56, 63, 64, 65, 72, 73, 74],
            [57, 58, 59, 66, 67, 68, 75, 76, 77],
            [60, 61, 62, 69, 70, 71, 78, 79, 80],
        ];
        self::assertEquals($blocks, array_map(fn($i) => ZoneIndex::block($i), [0, 1, 2, 3, 4, 5, 6, 7, 8]));
        self::assertEquals([1, 2, 9, 10, 11, 18, 19, 20], ZoneIndex::blockOf(0));
        self::assertEquals([0, 2, 9, 10, 11, 18, 19, 20], ZoneIndex::blockOf(1));
        self::assertEquals([0, 1, 2, 9, 10, 11, 19, 20], ZoneIndex::blockOf(18));
        self::assertEquals([0, 1, 2, 9, 10, 11, 18, 19], ZoneIndex::blockOf(20));
        self::assertEquals([4, 5, 12, 13, 14, 21, 22, 23], ZoneIndex::blockOf(3));
        self::assertEquals([3, 4, 5, 12, 13, 14, 22, 23], ZoneIndex::blockOf(21));
        self::assertEquals([3, 4, 5, 12, 13, 14, 21, 22], ZoneIndex::blockOf(23));
        self::assertEquals([30, 31, 32, 40, 41, 48, 49, 50], ZoneIndex::blockOf(39));
        self::assertEquals([60, 61, 62, 69, 70, 71, 78, 79], ZoneIndex::blockOf(80));


        $blocksByCoordinate = [
            [
                [0, 1, 2, 9, 10, 11, 18, 19, 20],
                [3, 4, 5, 12, 13, 14, 21, 22, 23],
                [6, 7, 8, 15, 16, 17, 24, 25, 26],
            ],
            [
                [27, 28, 29, 36, 37, 38, 45, 46, 47],
                [30, 31, 32, 39, 40, 41, 48, 49, 50],
                [33, 34, 35, 42, 43, 44, 51, 52, 53],
            ],
            [
                [54, 55, 56, 63, 64, 65, 72, 73, 74],
                [57, 58, 59, 66, 67, 68, 75, 76, 77],
                [60, 61, 62, 69, 70, 71, 78, 79, 80],
            ]
        ];
        for ($r = 0; $r < 3; $r++) {
            for ($c = 0; $c < 3; $c++) {
                self::assertEquals($blocksByCoordinate[$r][$c], ZoneIndex::blockFromCoordinate($r, $c));
            }
        }

        $expected0 = [
            1, 2, 3, 4, 5, 6, 7, 8,
            9, 18, 27, 36, 45, 54, 63, 72,
            10, 11, 19, 20,
        ];
        $actual0   = ZoneIndex::viewed(0);
        sort($expected0);
        sort($actual0);
        self::assertEquals($expected0, $actual0);
        $expected41 = [
            36, 37, 38, 39, 40, 42, 43, 44,
            5, 14, 23, 32, 50, 59, 68, 77,
            30, 31, 48, 49
        ];
        $actual41   = ZoneIndex::viewed(41);
        sort($expected41);
        sort($actual41);
        self::assertEquals($expected41, $actual41);
        $expected80 = [
            72, 73, 74, 75, 76, 77, 78, 79,
            8, 17, 26, 35, 44, 53, 62, 71,
            60, 61, 69, 70,
        ];
        $actual80   = ZoneIndex::viewed(80);
        sort($expected80);
        sort($actual80);
        self::assertEquals($expected80, $actual80);


        $all = array_merge($rows, $columns, $blocks);

        usort($all, fn($array1, $array2) => min($array1) * array_sum($array1) - min($array2) * array_sum($array2));
        $zones = ZoneIndex::all();
        usort($zones, fn($array1, $array2) => min($array1) * array_sum($array1) - min($array2) * array_sum($array2));
        self::assertEquals($all, $zones);
    }

    public function testShare()
    {

        self::assertTrue(CellShare::inSameRow(0, 5));
        self::assertTrue(CellShare::inSameRow(0, 8));
        self::assertTrue(CellShare::inSameRow(18, 25));
        self::assertTrue(CellShare::inSameRow(29, 30));
        self::assertTrue(CellShare::inSameRow(73, 80));
        self::assertFalse(CellShare::inSameRow(73, 0));
        self::assertFalse(CellShare::inSameRow(29, 18));
        self::assertFalse(CellShare::inSameRow(9, 8));
        self::assertFalse(CellShare::inSameRow(9, 0));

        self::assertTrue(CellShare::inSameColumn(0, 9));
        self::assertTrue(CellShare::inSameColumn(0, 18));
        self::assertTrue(CellShare::inSameColumn(0, 72));
        self::assertTrue(CellShare::inSameColumn(7, 70));
        self::assertFalse(CellShare::inSameColumn(0, 19));
        self::assertFalse(CellShare::inSameColumn(0, 80));
        self::assertFalse(CellShare::inSameColumn(65, 73));

        self::assertTrue(CellShare::inSameBlock(0, 9));
        self::assertTrue(CellShare::inSameBlock(0, 20));
        self::assertTrue(CellShare::inSameBlock(12, 13));
        self::assertTrue(CellShare::inSameBlock(42, 53));
        self::assertFalse(CellShare::inSameBlock(3, 53));
        self::assertFalse(CellShare::inSameBlock(54, 57));

        self::assertTrue(CellShare::viewEachOther(0, 20));
        self::assertTrue(CellShare::viewEachOther(0, 7));
        self::assertTrue(CellShare::viewEachOther(0, 45));
        self::assertTrue(CellShare::viewEachOther(39, 40));
        self::assertTrue(CellShare::viewEachOther(39, 44));
        self::assertTrue(CellShare::viewEachOther(39, 75));
        self::assertFalse(CellShare::viewEachOther(80, 63));
        self::assertFalse(CellShare::viewEachOther(1, 21));
    }
}