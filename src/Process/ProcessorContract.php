<?php

namespace App\Process;

use App\Loggers\LoggerContract;
use App\Model\Grid;
use JetBrains\PhpStorm\Pure;

abstract class ProcessorContract
{

    public function __construct(protected Grid $grid, protected LoggerContract $logger) { }

    public abstract function execute(): bool;

    #[Pure] public function message(): array
    {
        return $this->logger->message();
    }

    #[Pure] public function proof(): array
    {
        return $this->logger->proof();
    }

    #[Pure] public function algoSteps(): array
    {
        return $this->logger->algoSteps();
    }

    #[Pure] public function phase(): string
    {
        return $this->logger->phase();
    }

    public function reset(): void
    {
        $this->logger->reset();
        $this->grid->resetEffects();
    }

    #[Pure] public function level(): int
    {
        return $this->logger->level();
    }

    #[Pure] public function canonicalName(): string
    {
        return $this->logger->canonicalName();
    }

}
