<?php

namespace App\Serialization;

class StepsCollection
{
    /** @var Step[] */
    public array $steps = [];
}